<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */


    // general
    'name' => 'نام',
    'name_dr' => 'نام دری',
    'name_en' => 'نام انگلیسی',
    'name_pa' => 'نام پشتو',
    'father_name' => 'نام پدر',
    'employee_allotments' => 'جمع و قید کارمندان وکارکنان',
    'motamed_allotments' => 'جمع و قید  معتمدین',
    'add_motamed' => 'اضافه نمودن معتمد',
    'add_motamed_type' =>'اضافه نمودن نوعیت معتمد',
    'motamed_type' => 'نوعیت معتمد',
    'motamed_saved' => 'معتمد موفقانه اضافه گردید',
    'error_store_message' =>'مشکلی پیش آمد لطفا دوباره تلاش کنید',
    'external_employee_allotments' => ' جمع و قید کارمندان وکارکنان بیرونی',
    'designation' => 'عنوان وظیفه',
    'description' => 'توضیحات',
    'condition' => 'حالت جنس',
    'type' => 'نوعیت',
    'actions' => 'عملیات',
    'edit_external_department'=>'تصحیح اداره / وزارت',
    'edit_external_employee'=>'تصحیح کارمند بیرونی ',
    'file_upload'=>'اپلود فایل',
    'back' => 'برگشت',
    'edit_motamed_type' => 'تصحیح نوعیت معتمد',
    'edit' => 'تصحیح',
    'edit_motamed' => 'تصحیح معتمد',
    'motamed_allotments' => 'جمع و قید معتمد',

    'submit' => 'ثبت',
    'continue' => 'ثبت و ادامه',
    'status' => 'حالت',
    'reject' => 'رد نمودن',
    'reject_reason' => 'دلیل رد را بنویسید',
    'reject_reason_show' => 'دلیل رد',
    'cancel' => 'تنظیم مجدد',
    'sub_category' => 'کتگوری فرعی',

    'create_successful' => 'موفقانه ایجاد گردید',
    'update_successful' => 'موفقانه تصحیح گردید',
    'delete_successful' => 'موفقانه حذف گردید',
    'reject_successful' => 'موفقانه رد گردید',

    // units
    'units' => 'واحدات',
    'unit' => 'واحد',
    'edit_unit' => 'تصحیح واحد',
    'create_unit' => 'ایجاد واحد',
    'delete_unit' => 'حذف واحد',
    'price' => 'قیمت',
    //flash message localizetion
    'success' => 'معلومات موفقانه اضافه گردید.',
    'add_record' => 'اضافه نمودن',
    'edit_record' => 'تصحیح',
    'update' => 'معلومات موفقانه تصحیح گردید.',
    'delete' => 'معلومات موافقانه هذف گردید.',
    'approve' => 'معلومات موافقانه قبول گردید.',

    // department
    'departments' => 'ادارات /  وزارت ها',
    'department' => 'اداره',
    'department_type.0' => 'اصلی',
    'department_type.1' => 'فرعی',
    'edit_department' => 'تصحیح اداره / وزارت',
    'create_department' => 'ایجاد اداره / وزارت',
    'delete_department' => 'حذف اداره / وزارت',
    'external_departments' => ' ادارات / وزارت های بیرونی',
    'create_external_departments' => '  ایجاد اداراه / وزارت بیرونی',


    // vendors
    'vendors' => 'کمپنی ها',
    'vendor' => 'کمپنی',
    'edit_vendor' => 'تصحیح کمپنی',
    'create_vendor' => 'ایجاد کمپنی',
    'delete_vendor' => 'حذف کمپنی',

    // vendors
    'sub_categories_keys' => 'نوعیت مشخصات',
    'edit_sub_categories_key' => 'تصحیح نوعیت مشخصات',
    'create_sub_categories_key' => 'ایجاد نوعیت مشخصات',
    'delete_sub_categories_key' => 'حذف نوعیت مشخصات',

    //Sub category key table col
    'col1' => 'عنوان مشخصات اول',
    'col2' => 'عنوان مشخصات دوم',
    'col3' => 'عنوان مشخصات سوم',
    'col4' => 'عنوان مشخصات چهارم',
    'col5' => 'عنوان مشخصات پنچم',
    'col6' => 'عنوان مشخصات ششم',
    'col7' => 'عنوان مشخصات هفتم',
    'col8' => 'عنوان مشخصات هشتم',
    'col9' => 'عنوان مشخصات نهم',
    'col10' => 'عنوان مشخصات دهم',
    'col11' => 'عنوان مشخصات یازدهم',
    'col12' => 'عنوان مشخصات دوازدهم',
    

    // v9ndor8
    'mai9_cat9gories' => 'کتگوری های اصلی',
    'edit_main_category' => 'تصحیح کتگوری اصلی',
    'create_main_category' => 'ایجاد کتگوری اصلی',
    'delete_main_category' => 'حذف کتگوری اصلی',
    'main_category' => 'کتگوری اصلی',

    // Main categories
    'main_categories' => 'کتگوری اصلی',

    // Sub Categories
    'sub_categories' => 'کتگوری های فرعی',
    'edit_sub_category' => 'تصحیح کتگوری فرعی',
    'create_sub_category' => 'ایجاد کتگوری فرعی',
    'delete_sub_category' => 'حذف کتگوری فرعی',

    // End Categories
    'end_categories' => 'کتگوری های نهایی',
    'edit_end_category' => 'تصحیح کتگوری نهایی',
    'create_end_category' => 'ایجاد کتگوری نهایی',
    'delete_end_category' => 'حذف کتگوری نهایی',
    'end_category' => 'کتگوری نهایی',


    // Item Details
    'item_details' => 'اجناس',
    'item_details_requested' => 'لست اجناس درخواست شده',
    'item_details_accept' => 'لست اجناس اضافه شده',
    'item_details_reject' => 'لست اجناس رد شده',
    'user' => 'استفاده کننده',
    'edit_item_details' => 'تصحیح جنس',
    'create_item_details' => 'ایجاد جنس',
    'delete_item_details' => 'حذف جنس',
    'total_alloted_items' => 'تعداد توزیع شده',
    'total_remaining_items' => 'تعداد باقی مانده',
    'return_request' => 'درخواست اعاده اجناس',
    'item_details_index' => 'لست اجناس',
    'item_details_create' => 'اضافه نمودن جنس',

    // fece9
    'requesting_department' => 'اداره درخواست کننده',
    'fece9_number' => 'نمبر ف س ۹',
    'fece9_date' => 'تاریخ',
    'item' => 'جنس',
    'quantity' => 'مقدار',
    'serial_number' => 'سریال نمبر',
    // 'item_details' => 'ثبت اجناس',
    'edit_item_details' => 'تصحیح اجناس',
    'nature' => 'نوعیت جنس',
    'edit_fece9' => 'تصحیح ف س ۹',
    'create_fece9' => 'ایجاد ف س ۹',
    'fece9s' => 'ف س ۹ ها',
    'edit_fece9' => 'تصحیح ف س ۹',
    // fece2
    'create_fece2' => 'ایجاد ف س ۲',
    'fece2s' => 'ف س ۲ ها',
    'fece2_number' => 'نمبر   ارسالی',
    'fece2_date' => 'تاریخ',
    'item_details_specifications' => 'مشخصات جنس',
    'edit_fece2' => 'تصحیح ف س ۲',


    // meem7
    'meem7s' => 'م ۷',
    'meem7_number' => 'نمبر م ۷',
    'meem7_date' => 'تاریخ',
    'maktoob_number' => 'نمبر مکتوب',
    'maktoob_date' => 'تاریخ مکتوب',
    'meem7_submit' => 'ثبت م ۷',
    'create_meem7' => 'اضافه نمودن م ۷',

    //  Meem3
    'meem3_file' => "فایل م۳",

    // fece5
    'fece5_number' => 'نمبر ف س ۵',
    'fece5_date' => 'تاریخ',
    'create_fece5' => 'اضافه نمودن ف س ۵',
    'edit_fece5' => 'تصحیح ف س ۵',
    'fece5s' => 'ف س ۵ ها',
    'distributer_employee_id' => 'توزیع کننده',
    'receiver_employee_id' => 'دریافت کننده',
    'create_successful_requested_items' => 'درخواست برای اضافه نمودن اجناس موفقانه ارسال گردید.',
    'requested_items' => 'اجناس درخواست شده (اضافه سیستم ګردد)',
    'currency' => 'واحد پولی',
    'fece5fece5_number' => 'شماره م ۷',
    'akhz_as_wajh' => 'اخذ از وجه',
    'dar_wajh' => 'در وجه',
    'mahal_taslimi' => 'محل تسلیمی',
    'meem3_file'  =>  'فایل م ۳',
    'receiver_employee'  =>  'تسلیم گیرینده',
    'create_external_employee'  =>  ' ایجاد کارمند بیرونی',
    'current_position_dr	'  =>  'عنوان وظیفه',
    'external_employee'  =>  ' کارمندان بیرونی  ',


    //
    'allote_all' => 'اعاده اجناس',
    'item_report' => 'راپور اجناس',
    'search' => 'جستجو',
    'contractor_company' => 'شرکت های قراردادی',
    'contracts' => 'قرارداد ها',
    'company_name'=>'نام شرکت',
    'license_number'=>'نمبر جواز',
    'tin'=>'نمبر تشخیصه',
    'number'=>'شماره تماس',
    'email'=>'ایمیل',
    'contractor_company_add'=>'اضاقه نمودن شرکت قراردادی',
    'contractor_company_edit'=>'تصحیح نمودن شرکت قراردادی',
    'contract_type' => 'نوعیت قرارداد',
    'create_contract_type' => 'ثبت نوعیت قرارداد',
    'edit_contract_type' => 'تصحیح نوعیت قرارداد',
    'fece9_type' => 'نوعیت ف س ۹',
    'add_contract'=>'اضافه نمودن قرارداد',
    'contract_type' => 'نوعیت قرارداد',
    'select_contranct_type' => 'نوعیت قرارداد را انتخاب کنید',
    'identification_number' => 'نمبر تشخصیه',
    'year' => 'سال',
    'contract_amount' => 'مقدار پول',
    'start_date' => 'تاریخ آغاز',
    'end_date' => 'تاریخ ختم',
    'contract_file' => 'فایل قرارداد',
    'contract_discount'=> 'مبلغ تخفیف داده شده',
    'contract_discount_perce'=> 'فیصدی تخفیف',
    'fece9_request_number' => 'نمبر درخواست',
    'fece9_request_date' => 'تاریخ درخواست',
    'fece9_request_file' => 'فایل درخواست',
    'akhz_az_qarardad'  => 'اخذ از قرارداد',
    'bank_account'  =>'حساب بانکی',
    'company_file' =>'فایل',
    'details' =>'مشخصات',
  ];
