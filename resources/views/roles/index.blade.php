@extends('layouts.master')
@section('title',' نقش')
@section('content')
<table class="table table-bordered jquery-datatable">
  <thead>
    <tr>
       <th>شماره</th>
       <th>نقش</th>
       <th width="280px">عملیات</th>
    </tr>
  </thead>
    @foreach ($roles as $key => $role)
  <tbody>
    <tr>
      <td class="">{{ ++$i }}</td>
      <td>{{ $role->name }}</td>
      <td>
          <a class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" href="{{ route('roles.show',$role->id) }}"><i class="la la-eye"></i></a>
          {{-- @can('roles-edit') --}}
            <a class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" href="{{ route('roles.edit',$role->id) }}"><i class="la la-edit"></i></a>
          {{-- @endcan --}}
          {{-- @can('roles-delete') --}}
              {!! Form::open(['method' => 'DELETE','route' => ['roles.destroy', $role->id],'style'=>'display:inline']) !!}
              {!! Form::button('<i class="fa fa-trash"></i>', ['type'=>'submit','class' => 'm-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill ']) !!}
              {!! Form::close() !!}
          {{-- @endcan --}}
      </td>
    </tr>
  </tbody>
    @endforeach
</table>
{!! $roles->render() !!}

@endsection
@push('create-button')

    {{-- @can('roles-create') --}}
       {{-- @can('users-create') --}}
           <a href="{{ route('users.index') }}" class="btn btn-info m-btn m-btn--custom ">
               <span>
               <span>یوزر</span>
               <i class="la la-users"></i>
               </span>
           </a>
           <a href="{{ route('permissions.index') }}" class="btn btn-info m-btn m-btn--custom ">
               <span>
               <span>صلاحیت ها</span>
               <i class="la la-key"></i>
               </span>
           </a>
           {{-- @endcan --}}
           <a href="{{ route('roles.create') }}" class="btn btn-success m-btn m-btn--custom ">
               <span>
               <span>اضافه نمودن نقش</span>
               <i class="la la-plus"></i>
               </span>
           </a>
    {{-- @endcan --}}
@endpush
