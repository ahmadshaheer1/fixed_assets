@extends('layouts.master')
@section('title',' صلاحیت')
@section('content')



<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>نقش:</strong>
            {{ $role->name }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>صلاحیت ها:</strong>
            @if(!empty($rolePermissions))
                @foreach($rolePermissions as $v)
                    <label class="label label-success">{{ $v->name_dr }},</label>
                @endforeach
            @endif
        </div>
    </div>
</div>
@endsection
{{-- @push('create-button')
 <a href="{{ route('roles.index') }}" class="btn btn-success m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air">
     <span>
     <span>برگشت</span>
     <i class="la la-share"></i>
     </span>
 </a>
@endpush --}}
