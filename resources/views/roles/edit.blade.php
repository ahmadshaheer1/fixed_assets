@extends('layouts.master')
@section('title',' تصحیح صلاحیت')
@section('content')
  @if (count($errors) > 0)
      <div class="alert alert-danger">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
          </ul>
      </div>
  @endif


  {!! Form::model($role, ['method' => 'PATCH','route' => ['roles.update', $role->id]]) !!}
  <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="form-group">
              <strong>نام</strong>
              {!! Form::text('name', null, array('placeholder' => 'نام','class' => 'form-control')) !!}
          </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="form-group">
                <h4>صلاحیت ها</h4>
                <br/>
              @foreach($permission as $value)
                  <label>{{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'name')) }}
                  {{ $value->name_dr }}</label>
              <br/>
              @endforeach
          </div>
      </div>
      <div class="row">
          <div class="col-2">
          </div>
          <div class="col-12">
              <button  type="submit" class="btn btn-success">تصحیح</button>
              <button  type="reset" class="btn btn-warning">حذف اطلاعات</button>
              <a  href="{{route('roles.index')}}" class="btn btn-info">برگشت</a>
          </div>
      </div>
  </div>
  {!! Form::close() !!}

@endsection
