@extends('layouts.master')
@section('title',' اضافه نمودن نقش جدید')
@section('content')
  @if (count($errors) > 0)
      <div class="alert alert-danger">
          <strong>Whoops!</strong> There were some problems with your input.<br><br>
          <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
          </ul>
      </div>
  @endif


  {!! Form::open(array('route' => 'roles.store','method'=>'POST')) !!}
  <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="form-group">
              <strong>نقش:</strong>
              {!! Form::text('name', null, array('placeholder' => 'نقش','class' => 'form-control')) !!}
          </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="form-group">
              <strong>صلاحیت ها:</strong>
              <br/>
              @foreach($permission as $value)
                  <label>{{ Form::checkbox('permission[]', $value->id, false, array('class' => 'name')) }}
                  {{ $value->name }}</label>
              <br/>
              @endforeach
          </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12 text-center">
          <button type="submit" class="btn btn-primary">ثبت</button>
      </div>
  </div>
  {!! Form::close() !!}

@endsection
