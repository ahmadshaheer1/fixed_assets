
<!DOCTYPE html>
<html lang="en" >
    <!-- begin::Head -->
    <head>
        <meta charset="utf-8" />

        <title>Fixed Assets Management System </title>
        <meta name="description" content="Latest updates and statistic charts">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

        <!--begin::Web font -->
        <link rel="apple-touch-icon" sizes="57x57" href="{{asset('public/assets/app/media/img/logos/icon/apple-icon-57x57.png')}}">
        <link rel="apple-touch-icon" sizes="60x60" href="{{asset('public/assets/app/media/img/logos/icon/apple-icon-60x60.png')}}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{asset('public/assets/app/media/img/logos/icon/apple-icon-72x72.png')}}">
        <link rel="apple-touch-icon" sizes="76x76" href="{{asset('public/assets/app/media/img/logos/icon/apple-icon-76x76.png')}}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{asset('public/assets/app/media/img/logos/icon/apple-icon-114x114.png')}}">
        <link rel="apple-touch-icon" sizes="120x120" href="{{asset('public/assets/app/media/img/logos/icon/apple-icon-120x120.png')}}">
        <link rel="apple-touch-icon" sizes="144x144" href="{{asset('public/assets/app/media/img/logos/icon/apple-icon-144x144.png')}}">
        <link rel="apple-touch-icon" sizes="152x152" href="{{asset('public/assets/app/media/img/logos/icon/apple-icon-152x152.png')}}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{asset('public/assets/app/media/img/logos/icon/apple-icon-180x180.png')}}">
        <link rel="icon" type="image/png" sizes="192x192"  href="{{asset('public/assets/app/media/img/logos/icon/android-icon-192x192.png')}}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{asset('public/assets/app/media/img/logos/icon/favicon-32x32.png')}}">
        <link rel="icon" type="image/png" sizes="96x96" href="{{asset('public/assets/app/media/img/logos/icon/favicon-96x96.png')}}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{asset('public/assets/app/media/img/logos/icon/favicon-16x16.png')}}">
        <link rel="manifest" href="{{asset('public/assets/app/media/img/logos/icon/manifest.json')}}">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        {{-- <script>
          WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
        </script> --}}
        <link href="{{asset('public/assets/vendors/base/vendors.bundle.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('public/assets/demo/demo12/base/style.bundle.css')}}" rel="stylesheet" type="text/css" />
        <!--end::Global Theme Styles -->
        <!--begin::Page Vendors Styles -->
        <link href="{{asset('public/assets/vendors/custom/fullcalendar/fullcalendar.bundle.css')}}" rel="stylesheet" type="text/css" />
        <!--end::Page Vendors Styles -->
        <link rel="shortcut icon" href="{{asset('public/assets/demo/demo12/media/img/logo/favicon.png')}}" />
    </head>
    <body  class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

<div class="m-grid m-grid--hor m-grid--root m-page">
				<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-3" id="m_login" style="background-image: url({{asset('public/images/bg-3.jpg')}});">
        	<div class="m-grid__item m-grid__item--fluid	m-login__wrapper pt-5">
        		<div class="m-login__container">
        			<div class="m-login__logo mb-0">
        				<a href="#">
                  {{-- <img style="width:30%;" src="{{asset('public/images/login_logo.png')}}"> --}}
        				<img class="mt-3" style="width:100%;" src="{{asset('public/images/login_logo.png')}}">
        				</a>
        			</div>
        			<div class="m-login__signin">
        				<div class="m-login__head">
        					<h3 class="m-login__title">Sign In To Fixed Assets</h3>
        				</div>
                {{-- <form method="POST" action="{{ route('login') }}">
                    @csrf

                    <div class="form-group row">
                        <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-6 offset-md-4">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                <label class="form-check-label" for="remember">
                                    {{ __('Remember Me') }}
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-8 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Login') }}
                            </button>

                            @if (Route::has('password.request'))
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            @endif
                        </div>
                    </div>
                </form> --}}
        				<form class="m-login__form m-form" method="POST" action="{{ route('login') }}">
                  @csrf
        					<div class="form-group m-form__group">
        						<input tid="email" type="email" placeholder="Email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} m-input" name="email" value="{{ old('email') }}" required autofocus autocomplete="off">
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
        					</div>
        					<div class="form-group m-form__group">
        						<input type="password" placeholder="Password"id="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} m-input m-login__form-input--last" name="password" required>
                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
        					</div>
        					<div class="row m-login__form-sub">
        						<div class="col m--align-left m-login__form-left">
        							<label class="m-checkbox  m-checkbox--light">
        							<input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}> Remember me
        							<span></span>
        							</label>
        						</div>
        						{{-- <div class="col m--align-right m-login__form-right">
        							<a href="javascript:;" id="m_login_forget_password" class="m-link">Forget Password ?</a>
        						</div> --}}
        					</div>
        					<div class="m-login__form-action">
        						<button id="m_login_signin_submit" class="btn m-btn--pill    btn-primary  m-login__btn">Sign In</button>
        					</div>
        				</form>
        			</div>
        			{{-- <div class="m-login__forget-password">
        				<div class="m-login__head">
        					<h3 class="m-login__title">Forgotten Password ?</h3>
        					<div class="m-login__desc">Enter your email to reset your password:</div>
        				</div>
        				<form class="m-login__form m-form" action="">
        					<div class="form-group m-form__group">
        						<input class="form-control m-input" type="text" placeholder="Email" name="email" id="m_email" autocomplete="off">
        					</div>
        					<div class="m-login__form-action">
        						<button id="m_login_forget_password_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary">Request</button>&nbsp;&nbsp;
        						<button id="m_login_forget_password_cancel" class="btn btn-outline-focus m-btn m-btn--pill m-btn--custom  m-login__btn">Cancel</button>
        					</div>
        				</form>
        			</div> --}}
        		</div>
        	</div>
        </div>


</div>
<!-- end:: Page -->


<!--begin::Global Theme Bundle -->
<script src="{{asset('public/assets/vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
<script src="{{asset('public/assets/demo/demo12/base/scripts.bundle.js')}}" type="text/javascript"></script>
<!--end::Global Theme Bundle -->
<!--begin::Page Vendors -->
<script src="{{asset('public/assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>
<!--end::Page Vendors -->
<!--begin::Page Scripts -->
<script src="{{asset('public/assets/app/js/dashboard.js')}}" type="text/javascript"></script>
      <!--end::Page Scripts -->

  </body>
<!-- end::Body -->
</html>
<style>

.m-login__btn{
    background-color: #2e69b3;
}
.m-login.m-login--2.m-login-2--skin-3 .m-login__container .m-login__form .form-control {
    color: #ffffff;
    background: #2a479d;
}
.m-login.m-login--2.m-login-2--skin-3 .m-login__container .m-login__form .form-control:focus {
    color: #fff;
}
</style>