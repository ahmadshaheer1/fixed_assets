
<table class="table table-striped- table-bordered table-hover table-checkable " id="m_table_1">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">{{trans('global.name')}}</th>
        <th scope="col">{{trans('global.actions')}}</th>
      </tr>
    </thead>
    <tbody>
        @php
            if(count($motamed_type) ==0){
                echo '<tr class="odd"><th valign="top" colspan="6" class="dataTables_empty text-center">معلومات مورد نظر دریافت نشد</th></tr>';
            }
        @endphp
        @foreach ($motamed_type as $index => $item)
            <tr id="{{$item->id}}_tr">
                <th scope="row">{{$item->id}}</th>
                <td>{{$item->name}} </td>
                <td>
                    <a class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" href="{{ route('motamed_type.edit',encrypt($item->id)) }}"><i class="la la-edit"></i></a>
                        <a href="javascript:void(0)" onclick="deleteRecord('{{route('motamed_type.destroy', encrypt($item->id))}}','{{$item->id}}_tr');" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill ">
                            <i class="fa fa-trash"></i>
                    </a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

{!! $motamed_type->links()!!}
