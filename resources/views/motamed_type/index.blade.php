@extends('layouts.master')
@section('title',trans('global.motamed_type'))
@section('content')
{{-- jquery-datatable --}}

<div class="row m--margin-bottom-20">
    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
      <label>{{trans('global.name')}}:</label>
      <input type="text"  id="name" class="form-control m-input" placeholder="{{trans('global.name')}}" data-col-index="0">
    </div>
    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
        <button onclick="serach()" class="  btn btn-info btn-sm">{{trans('global.search')}}<i class="la la-search"></i></button>
    </div>
</div>
  

  <div class="m-separator m-separator--md m-separator--dashed"></div>
  
<div id="data_table" class="table-responsive">
  @include('motamed_type.motamed_type_list')
</div>
@endsection
@push('create-button')
<a href="{{ route('motamed_type.create') }}" class="btn btn-success m-btn m-btn--custom ">
    <span>
    <span>اضافه نمودن</span>
    <i class="la la-plus"></i>
    </span>
</a>
@endpush
@push('custom-js')
<script>
  $.ajaxSetup({
  headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
  });

  $(document).ready(function(){
    
    $(document).on('click','.pagination a',function(event){
      event.preventDefault();
      var page = $(this).attr('href').split('page=')[1];
      get_data(page);

    });
    function get_data(page){
      $.ajax({
          url:'{{url("")}}/get_motamed_type_allotments_ajax?page='+page,
          beforeSend: function() {
        // setting a timeout
          $("#data_table").prepend('<div class="m-blockui "><span>لطفا منتظر باشید </span><span><div class="m-loader  m-loader--brand m-loader--lg"></div></span></div>');
          },
          success:function(data){

              $("#data_table").html(data);
          }
      });
    }   
  });
  function serach(){
      var name = $('#name').val();
      $.ajax({
          type:"post",
          url: "{{ route('get_motamed_type_search') }}",
          data:{
            'name' : name,
          },
          dataType: "text",
          beforeSend: function() {
        // setting a timeout
          $("#data_table").prepend('<center><div class="m-blockui justify-content-center"><span>لطفا منتظر باشید </span><span><div class="m-loader  m-loader--brand m-loader--lg"></div></span></div></center>');
          },
          success:function(data){
            $("#data_table").html(data);
          }

      });
    }
</script>

@endpush
@push('custom-css')
    <style>
      #data_table .m-blockui {
        color: #5867dd;
        text-align: center !important;
        position: fixed;
        top: 30%;
        right: 50%;
        display: inline-table;
      }
    </style>  
@endpush
