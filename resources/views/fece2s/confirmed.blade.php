@extends('layouts.master')
@section('title','ف س ۲ تایید شده')
@section('content')

<table id="example" class="table table-striped table-bordered jquery-datatable" style="width:100%">
    <thead>
        <tr>
            <th>#</th>
            <th>شماره</th>
            <th>تاریخ</th>
            <th>ریاست</th>
            <th>توضیحات</th>
            <th>حالت</th>
            <th>عملیات</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($fece2s as $item)
        @php
            $i=1;
        @endphp
        <tr id='{{$item->id}}_tr'>
            <td>{{$i++}}</td>
            <td>{{$item->number}}</td>
            <td>{{$item->date}}</td>
            <td>{{$item->department->name_dr}}</td> 
            <td>{{str_limit($item->summary,50)}}</td>
            <td>
                @if ($item->status == 0)
                <div class="badge badge-secondary">در حال پروسس</div>
                @elseif($item->status == 1)
                    <div class="badge badge-success">تایید شده</div>
                @else
                    <div class="badge badge-danger" title="{{$item->reject_remark}}">رد شده</div>
                @endif
            </td>
            
            <td>
                <a href="{{route('fece2s.show',$item->id)}}"
                    class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air">
                    <i class="la la-eye"></i>
                </a>
              
            </td>
        </tr>
        @endforeach
</table>

@endsection


