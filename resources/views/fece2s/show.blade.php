@extends('layouts.master')
@section('title','نمایش ف س ۲')
@section('content')
<div class="m-portlet__body" id="app">
    <div class="row">
        <div class="col-md-3">
            <p class="font-weight-bold">فورم ف س (۲)</p>
            <p class="font-weight-bold">سند ارسال و وصول</p>
        </div>
    </div>
    <table style="border-collapse: collapse; width: 100%; " border="1"  dir="ltr">
        <tbody>
        
        <tr height="40">
            <td  colspan="3" rowspan="2">
                <span>نمبر ارسالی: {{$fece2->number}}</span>
                <div>
                    <span>تاریخ: {{$fece2->date}}</span>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                    <span>مطابق: {{$fece2->date}}</span>
                </div>
                <div>:نمبر خریداری یا درخواست </div>
            </td>
            <td  colspan="3" >
                <span>ارسال گردیده از:  {{$fece2->department->name_dr}}</span>
            </td>
        </tr>
        <tr height="40">
            <td  colspan="3">
                <span>ارسال گردیده به: {{$fece2->department->name_dr}}</span>
            </td>
        </tr>
        <tr>
            <td  colspan="2" rowspan="2">ارسال گردیده از طریق وزن</td>
            <td>&nbsp;</td>
            <td  colspan="3" height="80">
                <span> :ملاحظه شد ریاست خدمات</span>
                &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                <span> منظوری آمر اعطا</span>
            </td>
        </tr>
        <tr >
            <td >&nbsp;</td>
            <td colspan="3" height="30">
                <span>امضا</span> &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                
                <span>تاریخ</span>
            </td>
        </tr>
        <tr >
            <td align="center">برای وضع ظاهری علامه استعمال شود</td>
            <td align="center">مقدار و اندازه ارسال شده</td>
            <td align="center">قیمت مجموعی</td>
            <td align="center">مقدار و اندازه اسال شده</td>
            <td align="center">تشریحات اجناس ارسال شده</td>
            <td height="40" align="center">نمبر صندوق پسته </td>
        </tr>
        <tr >
            <td height="140" >&nbsp;</td>
            <td >&nbsp;</td>
            <td >&nbsp;</td>
            <td >&nbsp;</td>
            <td >&nbsp;</td>
            <td >&nbsp;</td>
        </tr>
        <tr >
            <td  colspan="6" height="140">   
                {{-- <span>هدایات</span> --}}
            <pre class="custom-font no-border">
                هدایات</pre>
            </td>
        </tr>
        <tr >
            <td  colspan="6" height="100">
                <pre class="custom-font no-border">

                    مجموعه اقلام و اصله:                                                                     تسلیم شده توسط                                                                                               محل امضا 
                    
                    تاریخ رسید:                                                                  ملاحظه شده توسط نماینده مرجع مربوطه  
                </pre>
            </td>
        </tr>
        </tbody>
        </table>
        <div class="row">
            @if($fece2->file)
            <div class="row no-print">
                <div class="col-md-3 mb-2 pl-0">
                    <div class="update-image-wrapper">
                        <img width="200"  height="200" src="{{asset('public/images/pdf-2.png')}}" alt="" />

                        <div class="overlay">
                            <div class="text">
                                <a href="{{asset($fece2->file)}}" data-fancybox data-caption="{{$fece2->summary}}">
                                    نمایش
                                </a>
                                @if ($fece2->status == '0')
                                <a href="javascript:void(0)" onclick="$('input[name=image]').click();">
                                      تصحیح
                                </a>        
                                @endif
                                <form action="{{route('fece2_replace_attached_file')}}" id="image_form" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <input type="file" class="custom-file-input" id="image" multiple name="image" onchange="$('#image_form').submit();" ref="file">
                                    <input type="hidden" id="fece2_id" name="id" value="{{$fece2->id}}">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        </div>
        <div class="no-print mt-1">
            <button type="button" class="btn btn-brand" onclick="printFece2()">چاپ</button>
            @if ($fece2->status == 0 && $fece2->meem7 != null) 
                <a href="{{route('fece2_conformation',['id'=>$fece2->id,'status'=>'1'])}}" class="btn m-btn--square  btn-success" >تایید ف س ۲</a>
                <a href="#" onclick="rejectFece2({{$fece2->id}})" class="btn m-btn--square  btn-danger" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">رد</a>
            @endif
            <a href="{{route('fece2s.index')}}" class="btn btn-info">لغو</a>
        </div>
{{-- reject fece2 model  --}}
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">توضیحات رد ف س ۲</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form method="post" action="{{route('reject_fece2',$fece2->id)}}">
            @csrf
            <div class="form-group">
              <label for="message-text" class="col-form-label">توضیحات</label>
              <textarea class="form-control" id="message-text" name="reject_remark"></textarea>
            </div>
        </div>
        <div class="modal-footer float-right">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>
          <button type="submit" class="btn btn-primary">ثبت</button>
        </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('custom-css')
    <style>
        table{
            border: 2px solid black;
            border-collapse: collapse;
        }
        .custom-font {
            font-family: inherit; 
            font-size: inherit; 
        } 
        @media print {
            .no-border {
                border: none !important;
            }
        }
    </style>
@endpush

@push('custom-js')
  <script>
    function printFece2(){
        window.print()

    }
    window.onafterprint = function(){
          window.location.href="{{route('fece2s.index')}}";
      }
 </script>
@endpush

