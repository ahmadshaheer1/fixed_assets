@extends('layouts.master')
@section('title', trans('global.create_fece2'))
@section('content') 
<form class="form" action="{{route('fece2s.store')}}" method="POST" id="app">
    @csrf
        <div class="form-group m-form__group row ">
        <div class="form-group col-md-6 {{$errors->has('number') ? 'has-danger' : ''}}">
            <label for="fece2_number">{{trans('global.fece2_number')}}</label>
            <input type="number" name="number" value="{{old('number')}}" class="form-control m-input m-input--square" id="number" placeholder="نمبر ارسالی را وارید نماید.">
            @if($errors->has('number'))
                <div class="form-control-feedback">{{$errors->first('number')}}</div>
            @endif
        </div>
            <div class="form-group col-md-3 {{$errors->has('date') ? 'has-danger' : ''}}">
            <label for="inputPassword4">{{trans('global.fece2_date')}}</label>
                <div class="input-group date">
                <input type="text" name="date" value="{{old('date')}}" class="form-control persian_date" placeholder="تاریخ انتخاب کنید.">
                <div class="input-group-append">
                    <span class="input-group-text">
                    <i class="la la-calendar"></i>
                    </span>
                </div>
            </div>
            @if($errors->has('date'))
                <div class="form-control-feedback">{{$errors->first('date')}}</div>
            @endif
            </div>
        <div>
    </div>
</div>
<div class="form-group m-form__group row" >
    <div class="col-md-6">
        <div class="row items_list {{$errors->has('list') ? 'has-danger' : ''}}">

            <div class="form-group col-md-12">
                <label for="inputState">{{trans('global.sub_category')}}</label>
                <select id="sub_category_id" onchange="submitItemSpecifications()" name="sub_category_id" ref="item_details_id" class="form-control select2">
                    <option value="none" selected>یگ گزینه را انتخاب نماید  </option>
 
                        <option v-for="item in sub_category" :value="item.id" 
                     
                         >@{{item.name_dr}}</option>
                </select>
            </div>
        </div>
        <div class="row mr-0" id="keys_response">
        </div>
        <div class="row items_list {{$errors->has('item_details_id') ? 'has-danger' : ''}}">
            <div class="form-group col-md-12">
                <label for="inputState">{{trans('global.item')}}</label>
                <select id="item_details" name="item_details_id" ref="item_details_id"  class="form-control select2">
                    <option value="none" selected>یگ گزینه را انتخاب نماید.</option>
                     
                </select>
            </div>
        </div>
       
    </div>  
    <div class="form-group col-md-6 {{$errors->has('department_id') ? 'has-danger' : ''}}">
        <label for="department">{{trans('global.requesting_department')}}</label>
        <select class="form-control select2" name="department_id" id="department">
            @foreach ($departments as $item)
                <option value="{{$item['id']}}" {{old('department_id') == $item['id'] ? 'selected' : ''}}>{{$item['name_dr']}}</option>
            @endforeach
        </select>
        @if($errors->has('department_id'))
            <div class="form-control-feedback">{{$errors->first('department_id')}}</div>
        @endif
    </div>  
</div>
<div class="form-group m-form__group row" >
    <div class="form-group col-md-8">
        <label for="inputState">{{trans('global.item_details_specifications')}}</label>
        <select id="item_details_fece2" name="item_details_specifications_id"   class="form-control select2">
            <option value="none" selected>یگ گزینه را انتخاب نماید.</option>
               
        </select>
    </div>
    </div>
<div class="form-group m-form__group row {{$errors->has('summary') ? 'has-danger' : ''}}">
    <div class="form-group col-md-12">
        <label for="inputAddress2">هدایات</label>
        <textarea class="form-control" name="summary" id="" col="20" rows="7">{{old('text')}}</textarea>
            @if($errors->has('summary'))
                <div class="form-control-feedback">{{$errors->first('text')}}</div>
            @endif
    </div>
</div>
    <button type="submit" class="btn btn-primary">ذخیره</button>
    <a type="button" href="{{url('fece9/index')}}" class="btn btn-default">برگشت</a>
    <button type="reset" class="btn btn-danger">حذف کردن</button>
    </form>
</div>


@endsection
@push('custom-css')
    <style>
        #keys_response .col-md-3, #keys_response .col-md-2, #keys_response .col-md-1 {
            padding-left: 5px;
            padding-right: 5px;
        }
    </style>
@endpush

@push('custom-js')
    <script>
        var app = new Vue({
            el: '#app',
            data: {
                message: 'Hello Vue!',
                list: {!! old('list') ? old('list') : '[]'  !!},
                sub_category:{!!$item_details!!},
            },
            methods: {
            

            },
        })


        $('#sub_category_id').change(function() {
            sub_category_id = $(this).val();
            getSubCategoryKeys(sub_category_id);
        })



        function getSubCategoryKeys(sub_category_id, data) {
            $.ajax({
                method:'GET',
                url: '{{url("get_sub_categories_select_options")}}/'+sub_category_id,
                data: {data},
                success: function(resp) {
                    $('#keys_response').html(resp);
                }
            })
        }


        function submitItemSpecifications() {
            col1 = $('#col1').val();
            col2 = $('#col2').val();
            col3 = $('#col3').val();
            col4 = $('#col4').val();
            col5 = $('#col5').val();
            col6 = $('#col6').val();
            col7 = $('#col7').val();
            col8 = $('#col8').val();
            col9 = $('#col9').val();
            col10 = $('#col10').val();
            col11 = $('#col11').val();
            col12 = $('#col12').val();
            sub_category_id = {!! old('sub_category_id') ? old('sub_category_id') : "$('#sub_category_id').val();" !!}

            $.ajax({
                method: 'get',
                url: "{{url('get_item_details_by_specifications')}}/"+sub_category_id,
                data: {'col1': col1, 'col2': col2, 'col3': col3, 'col4': col4,'col5': col5,'col6': col6,'col7': col7,'col8': col8
                ,'col9': col9,'col10': col10,'col11': col11 ,'col12': col12
            },
                success: function(resp) {
                    console.log(resp);
                    $('#item_details').html('');
                    $.each(resp, populateOptions)
                }
            })
            $('#modal_sub_category_id').val(sub_category_id);
          
        }

        function populateOptions(index, item) {
            $('#item_details').append('<option value="'+item.id+'">'+item.name_dr+'</option>');
        }
        $('#item_details').on('click', function() {
                $(this).trigger('change');
            });


        $(function() {
           {!! old('sub_category_id') ? "submitItemSpecifications(); getSubCategoryKeys(".old('sub_category_id').");" : '' !!}
        })
       


        $('#item_details').on('change', function() {
        var selectedValue = $(this).val();
        var type = 'fece2';
        $.ajax({
            url: "{{url('get_item_details_specifications_axios')}}/"+selectedValue,
            method: 'GET',
            data: { item_details_id: selectedValue ,type},
            success: function(response) {
                $('#item_details_fece2').html('');
                $.each(response, populateOptionsFece2)
            },
            error: function(xhr, status, error) {
            console.error(error);
            }
        });
        });
            function populateOptionsFece2(index, item) {
                $('#item_details_fece2').append(`<option value=${item.id}>${item.col1 ? `${item.sub_category_keys.col1}  : ${item.col1} | ` : ''}
                        ${item.col2 ? `${item.sub_category_keys.col2}  : ${item.col2} |` : ''}
                        ${item.col3 ? `${item.sub_category_keys.col3}  : ${item.col3} |` : ''}
                        ${item.col4 ? `${item.sub_category_keys.col4}  : ${item.col4} |` : ''}
                        ${item.col5 ? `${item.sub_category_keys.col5}  : ${item.col5} |` : ''}
                        ${item.col6 ? `${item.sub_category_keys.col6}  : ${item.col6} |` : ''}
                        ${item.col7 ? `${item.sub_category_keys.col7}  : ${item.col7} |` : ''}
                        ${item.col8 ? `${item.sub_category_keys.col8}  : ${item.col8} |` : ''}
                        ${item.col9 ? `${item.sub_category_keys.col9}  : ${item.col9} |` : ''}
                        ${item.col10 ? `${item.sub_category_keys.col10}  : ${item.col10} |` : ''}
                        ${item.col11 ? `${item.sub_category_keys.col11}  : ${item.col11} |` : ''}
                        ${item.col12 ? `${item.sub_category_keys.col12}  : ${item.col12} |` : ''}
                </option>`)  
        }

        
    </script>

@endpush
