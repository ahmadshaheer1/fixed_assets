@extends('layouts.master')
@section('title',trans('global.meem7s'))
@section('content')
<table class="table table-striped- table-bordered table-hover table-checkable jquery-datatable" id="m_table_1">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th>شماره</th>
        <th>تاریخ</th>
        <th>اداره درخواست کننده</th>
        {{-- <th>حالت</th> --}}
        <th scope="col">{{trans('global.actions')}}</th>
      </tr>
    </thead>
    <tbody>

        @foreach ($fece9s as $key => $item)
            <tr id="{{$item->id}}_tr">
                <th scope="row">{{++$key}}</th>
                <td>{{$item->number}}</td>
                <td>{{$item->date}}</td>
                <td>{{$item->department->$name}}</td>
                {{-- <td class="text-center">
                    @if (isset($item->meem7) && $item->meem7->status == 0)
                        <i class="fa fa-times text-danger"></i>
                    @else
                        <i class="fa fa-check text-success"></i>

                    @endif
                </td> --}}
                <td>

                  <a href="{{route('fece9s.show',$item->id)}}" title="تصحیح" class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air">
                      <i class="la la-eye"></i>
                  </a>
                  <a href="{{route('meem7.create_meem7',$item->id)}}" title="اضافه نمودن م ۷" class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air">
                      <i class="la la-plus"></i>
                  </a>
                </td>
            </tr>
        @endforeach
    </tbody>
  </table>
@endsection
