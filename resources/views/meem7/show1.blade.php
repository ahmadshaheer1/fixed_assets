@extends('layouts.master')
@section('title','صفحه اصلی')
@section('content')
<div id="printableArea">
    <div class="row">
        <div class="col-md-12 text-center text-dark">
            <h4>فورم م۷ </h4><br>
            <h5>فورم راپور رسیدات</h5>
        </div>
    </div>
    <br>
    <div class="row mb-3">
        <table class="table">
            <thead>
                <tr>
                    <th>1-	</th> 
                    <th colspan="3">نمبر راپور : {{$meem7->number}}</th>
                    <th>4-	</th>
                    <th colspan="2">اداره
                        :
                         {{$parameter['department']['name_dr']}}
                    </th>
                </tr>
                <tr>
                    <th>2-	</th>
                    <th colspan="3">تاریخ:‌ {{$meem7->date}}</th>
                    <th>5-	</th>
                    <th colspan="2">محل تسلیمی:
                         {{$meem7->receiverEmployee->name_dr}}
                         -- ولد :
                         {{$meem7->receiverEmployee->father_name_dr}}

                        </th>
                </tr>
                <tr>
                    <th>3-	</th>
                    <th colspan="3">   به اساس درخواست نمبر :	
                      
                        &nbsp;{{$meem7_fece9->first()->fece9->number}}
                        الی 
                        &nbsp;{{$meem7_fece9->orderBy('id', 'desc')->first()->fece9->number}}

                        مورخ
                        &nbsp;{{$meem7_fece9->first()->fece9->date}}
                        الی
                        &nbsp;{{$meem7_fece9->orderBy('id', 'desc')->first()->fece9->date}}
                        ضرورت
                        {{$parameter['department']['name_dr']}}

                        </th>
                    <th>6-	</th>
                    <th colspan="2">اخذ گردید از: 
                        {{$meem7->contract->contractorCompany->company_name}}

                    </th>
                </tr>
                <tr class="with_background">
                    <th>شماره</th>
                    <th>مقدار	</th>
                    <th>واحد	</th>
                    <th>تفصیلات	</th>
                    <th>فیات	</th>
                    <th>مبلغ</th>
                    <th>ملاحظات</th>
                </tr>
                <tr class="with_background">
                    <th></th>
                    <th>7</th>
                    <th>8</th>
                    <th>9</th>
                    <th>10</th>
                    <th>11</th>
                    <th>12</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $total = 0;
                    $counter = 0;
                @endphp
                {{-- {{$meem7->fece9->itemDetails}} --}}
                {{-- @foreach ($meem7->fece9->itemDetails->groupBy('name_dr') as $index => $item)
                    @foreach ($item as $number => $details)
                        <tr>
                            <td>{{++$number}}</td>
                            <td>{{$details->pivot->quantity}}</td>
                            <td>
                                {{$details->unit->name_dr}}
                                
                            </td>
                            <td>{{$details->name_dr}} 
                            </td>
                            <td>
                                @php
                                    $price = App\ContractItems::where('item_details_id',$details->id)->where('contract_id',$meem7->contract_id)->first()->price_per_item;

                                @endphp
                                {{$price}}
                            </td>
                            <td>
                                {{$sub_total = ($price * $details->pivot->quantity)}}
                            </td>
                            @php
                                $total += $sub_total;
                                $counter += 1;
                            @endphp

                            <td></td>
                        </tr>
                    @endforeach
                @endforeach --}}
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>جمله شد</td>
                    <td>
                        {{$total}}
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td>هیئت معاینه</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>ملاحظه شد </td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>
                         قرارشـرح فوق تعداد  
                            {{$counter}} 
                            {{-- قلم جـــنس متـــذکره به اســــاس درخواســـت نمبر فوق که دارای امــریه مقام بوده در وجه اشخاص متذکره حواله و اسناد غرض ویزه بمدیریت محترم کنترول ارسال است. با احترام --}}
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>

            </tbody>
            <tfoot>
                <tr>
                    <td colspan="7">
                        13-
                        تصدیق میدارم که اجناس یا خدمات فوق الذکر را تسلیم شدم اجناس مذکور را به غور وزن، حساب یا اندازه نمودم خدمات مذکور قناعت بخش و اجناس آن ب استثنای اقلامیکه در ستون ملاحظات ذکر شده مکمل و سالم بوده است.

                    </td>
                  
                </tr>
                <tr>
                    <td colspan="4">14</td>
                  
                    <td colspan="3">15
                        &nbsp;
                        &nbsp;
                        هویت مهر و امضاء 
                    {{$meem7->receiverEmployee->name_dr}}
                    </td>
                </tr>
           
            </tfoot>
        </table>
        <p>
            از امضاء فوق (          ) تصدیق میگردد
        </p>
    <br>
        <div class="row no-print">
                <div class="col-lg-3">
                    <button type="button" class="btn btn-brand" onclick="printmeem7()">چاپ</button>

                </div>
        </div>
    </div>
</div>


@endsection
@push('custom-css')
<style>
    table.table tr th, table.table tr td {
        border: 1px solid #777;
    }
    table .with_background {
        background: gainsboro !important;

    }
    table .with_background th{
        font-weight: bold;
        white-space: nowrap;
    }
    table tbody tr td {
        border-bottom: 0px !important;
        border-top: 0px !important;
    }
     /* image slide hover styles */

     .update-image-wrapper {
            position: relative;
            /* width: 50%; */
        }

        .image {
            display: block;
            width: 100%;
            height: auto;
        }

        .overlay {
            position: absolute;
            bottom: 0;
            left: 0;
            right: 0;
            background-color: #008cba5e;
            overflow: hidden;
            width: 100%;
            height: 0;
            transition: .5s ease;
        }

        .update-image-wrapper:hover .overlay {
            height: 100%;
        }

        .text {
            color: white;
            font-size: 20px;
            position: absolute;
            top: 50%;
            left: 50%;
            -webkit-transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
            text-align: center;
        }

        .text a {
            background: #fff;
            padding: 5px 10px;
            margin: 3px;
            font-size: .7em;
        }

        .text a:hover {
            text-decoration: none;
        }

</style>

@endpush
@push('custom-js')
  <script>

  //print meem7
    function printmeem7(){

      window.print()

    }
    window.onafterprint = function(){
        // window.location.href="{{route('meem7.index')}}";
        // window.close();
    }
  </script>
@endpush
 