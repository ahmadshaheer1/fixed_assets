@extends('layouts.master')
@section('title',trans('global.meem7s'))
@section('content')
<span id="meem7">


    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item" @click="sort(0)">
            <a class="nav-link active" data-toggle="tab" href="#m_tabs_1_1">
                <i class="la la-angle-double-down"></i>غیر مصرفی
            </a>
        </li>
        <li class="nav-item"  @click="sort(1)">
            <a class="nav-link" data-toggle="tab" href="#m_tabs_1_2">
                <i class="la la-check"></i> مصرفی
            </a>
        </li>
      
    </ul>
    <div class="row" v-if="type==1">
        <div class="text-center col-lg-12">
            <a href="#" class="btn btn-outline-primary mr-3" :class="fece9_type==1 ? 'active' : '' " @click="sort(1,1)">ذخیروی <i class="flaticon-tool"></i></a>
            <a href="#" class="btn btn-outline-primary mr-3" :class="fece9_type==2 ? 'active' : '' "  @click="sort(1,2)">غیر ذخیروی  <i class="flaticon-grid-menu-v2"></i></a>
        </div>
    </div>
<table class="table table-striped- table-bordered table-hover table-checkable jquery-datatable" id="m_table_1">
    <thead>
      <tr v-if="fece9_type ==2">
        <th scope="col">#</th>
        <th width="200">تاریخ</th>
        <th width="300">اداره درخواست کننده</th>
        <th width="80">تعداد ف س۹ ها</th>
        <th scope="col">{{trans('global.actions')}}</th>
      </tr>
      <tr v-else>
        <th scope="col">#</th>
        <th>شماره</th>
        <th>تاریخ</th>
        <th>اداره درخواست کننده</th>
        <th scope="col">{{trans('global.actions')}}</th>
      </tr>
    </thead>
    <tbody v-if="fece9_type == 1">
     <tr v-for="(item , index) in fece9s_loop">
                
                    <th scope="row">@{{++index}}</th>
                    <td>@{{item.number}}</td>
                    <td>@{{item.date}}</td>
                    <td>@{{item.department.name_dr}}</td>
                  
                    <td>

                    <a :href="`{{url('fece9s/')}}/`+item.id" title="تصحیح" class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air">
                        <i class="la la-eye"></i>
                    </a>
                    <a  :href="`{{url('create_meem7')}}/`+item.id" title="اضافه نمودن م ۷" class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air">
                        <i class="la la-plus"></i>
                    </a>
                    </td>                
                
            </tr>

    </tbody>
    <tbody v-else-if="fece9_type == 2">
     <tr v-for="(item , index) in fece9s_loop">
                
                    <th scope="row">@{{++index}}</th>
                    
                    <td> سال - @{{item.year}} ماه -@{{item.month}}</td>
                    <td>@{{item.department.name_dr}}</td>
                    <td><span class="badge badge-info badge-circle font-13">@{{item.count}}</span></td>
                  
                    <td>

                   
                    <a  :href="`{{url('create_meem7_ghair_zakhirawi')}}/`+item.department.id+`/`+item.month+`/`+fece9_type" title="اضافه نمودن م ۷" class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air">
                        <i class="la la-plus"></i>
                    </a>
                    </td>                
                
            </tr>

    </tbody>
    <tbody v-else>
     <tr v-for="(item , index) in fece9s_loop">
                
                    <th scope="row">@{{++index}}</th>
                    <td>@{{item.number}}</td>
                    <td>@{{item.date}}</td>
                    <td>@{{item.department.name_dr}}</td>
                  
                    <td>

                    <a :href="`{{url('fece9s/')}}/`+item.id" title="تصحیح" class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air">
                        <i class="la la-eye"></i>
                    </a>
                    <a  :href="`{{url('create_meem7')}}/`+item.id" title="اضافه نمودن م ۷" class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air">
                        <i class="la la-plus"></i>
                    </a>
                    </td>                
                
            </tr>

    </tbody>
  </table>
</span>
@endsection


@push('custom-js')
<script>
    item_app = new Vue({
            el: '#meem7',
            data: {
                fece9s: {!!$fece9s!!},
                type: 0,
                fece9s_loop:[],
                fece9_type:1,
                ghair_zakhirawi_fece9 : {!!$ghair_zakhirawi_fece9!!}
            },
            methods: {              
                sort(type,fece9_type=1){
                   
                    this.type = type;
                    this.fece9_type = fece9_type;

                    if(type ==0){
                        this.fece9s_loop =  this.fece9s.filter(item => item.fece9_type == 0 || item.fece9_type ==3)
                    }else{
                        this.fece9s_loop =  this.fece9s.filter(item => item.fece9_type == 1)

                        if(fece9_type==2){
                            this.fece9s_loop = this.ghair_zakhirawi_fece9;
                        }
                    }

                    

                }
             },
            mounted() {
                this.sort(this.type)
            },
        });


</script>
@endpush
@push('custom-css')
<style>

.font-13{
    font-size: 13px;
}
</style>
@endpush
