@extends('layouts.master')
@section('title', trans('global.create_meem7'))
@section('content')
<div id="create_meem7">
    @if(!isset($meem7))

        @if(!isset($fece9_type))    
            <div class="accordion accordion-solid accordion-toggle-plus" id="accordionExample6" v-if="fece9.fece9_type ==1">
                <div class="card">
                    <div class="card-header" id="headingOne6">
                    <div class="card-title font-weight-bold" data-toggle="collapse" data-target="#collapseOne6">
                    <i class="flaticon-pie-chart-1"></i> اجناس درخواست شده
                    </div>
                    </div>
                    <div id="collapseOne6" class="collapse " data-parent="#accordionExample6">
                    <div class="card-body">
                    <table class="table" >
                        <thead>
                            <tr class="text-left text-uppercase">
                                <th  class="font-weight-bold">شماره</th>
                                <th  class="font-weight-bold"> نام جنس</th>
                                <th  class="font-weight-bold">مقدار</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="(item, index) in fece9.item_details">
                                <td class="pl-0 py-8">
                                        @{{++index}}
                                </td>
                                <td class="pl-0 py-8">
                                        @{{item.name_dr}}
                                </td>
                                <td>
                                    <span class="label label-inline label-light-primary font-weight-bold"> @{{item.pivot['quantity']}} @{{item.unit.name_dr}} </span>
                                </td>
                            </tr>   
                        </tbody>
                    </table>
                    </div>
                    </div>
                </div>
            </div>
            <form class="m-form m-form--fit m-form--label-align-right" id="meem7_form" method="POST" enctype="multipart/form-data" action="{{route('meem7.store')}}">
                @csrf
                <input type="hidden" name="fece9_id" value="{{$fece9->id}}">
                <input type="hidden" name="status" value="0">
                <div class="m-portlet__body">
                <div class="form-group row ">
                    <div class="form-group col-md-6 m-form__group {{$errors->has('number') ? 'has-danger' : ''}}">
                    <label for="fece5_number">{{trans('global.meem7_number')}}</label>
                        <input type="number" name="number" value="{{old('number')}}" class="form-control m-input m-input--square" id="number" placeholder="{{trans('global.meem7_number')}}">
                        @if($errors->has('number'))
                            <div class="form-control-feedback">{{$errors->first('number')}}</div>
                        @endif
                    </div>
                    <div class="form-group col-md-6 pt-0 m-form__group {{$errors->has('date') ? 'has-danger' : ''}}">
                    <label for="inputPassword4">{{trans('global.meem7_date')}}</label>
                    <div class="input-group date">
                        <input type="text" name="date" value="{{old('date')}}" class="form-control persian_date" placeholder="{{trans('global.meem7_date')}}">
                            <div class="input-group-append">
                            <span class="input-group-text">
                                <i class="la la-calendar"></i>
                            </span>
                            </div>
                    </div>
                    @if($errors->has('date'))
                        <div class="form-control-feedback">{{$errors->first('date')}}</div>
                    @endif
                    </div>
                </div>
                <div class="form-group row ">
                    <div class="form-group col-md-6 m-form__group {{$errors->has('akhz_az_wajh_employee_id') ? 'has-danger' : ''}}" v-if="fece9.fece9_type !=1">
                        <label for="akhz_az_wajh_employee_id">{{trans('global.dar_wajh')}}</label>
                        <select class="form-control employee" name="akhz_az_wajh_employee_id" id="akhz_az_wajh_employee_id">
                            <option value="">{{trans('global.dar_wajh')}}</option>
                            {{-- @foreach ($employees as $item)
                                <option value="{{$item['id']}}" {{old('akhz_az_wajh_employee_id') == $item['id'] ? 'selected' : ''}}>{{$item['name_dr']}}</option>
                            @endforeach --}}
                        </select>
                        @if($errors->has('akhz_az_wajh_employee_id'))
                            <div class="form-control-feedback">{{$errors->first('akhz_az_wajh_employee_id')}}</div>
                        @endif
                    </div>
                    <div class="form-group col-md-6 m-form__group {{$errors->has('contract_id') ? 'has-danger' : ''}}" v-else>
                        <input class="form-control" type="hidden" name="masrafi" value="1">
                        <label for="contract_id">{{trans('global.akhz_az_qarardad')}}</label>
                        <select class="form-control" name="contract_id" >
                            <option value="">{{trans('global.akhz_az_qarardad')}}</option>
                            @foreach ($contracts as $item)
                                <option value="{{$item['id']}}" {{old('contract_id') == $item['id'] ? 'selected' : ''}}>{{$item->contractorCompany->company_name}}</option>
                            @endforeach
                        </select>
                        @if($errors->has('contract_id'))
                            <div class="form-control-feedback">{{$errors->first('contract_id')}}</div>
                        @endif
                    </div>
                        <div class="form-group col-md-6 pt-0 m-form__group {{$errors->has('department_id') ? 'has-danger' : ''}}">
                            <label for="department">{{trans('global.requesting_department')}}</label>
                            <select class="form-control select2" name="department_id" id="department">
                                <option value="">{{trans('global.requesting_department')}}</option>
                                @foreach ($departments as $item)
                                    <option value="{{$item['id']}}" {{$fece9->department_id==$item->id ? 'selected' : ''}}{{old('department_id') == $item['id'] ? 'selected' : ''}}> {{$item['name_dr']}}</option>
                                @endforeach
                            </select>
                            @if($errors->has('department_id'))
                                <div class="form-control-feedback">{{$errors->first('department_id')}}</div>
                            @endif
                        </div>
                </div>
                <div class="form-group row ">
                        <div class="form-group col-md-6 m-form__group {{$errors->has('mahal_taslimi') ? 'has-danger' : ''}}">
                            <label for="fece9_mahal_taslimi">{{trans('global.mahal_taslimi')}}</label>
                            <select class="form-control employee" name="mahal_taslimi" id="mahal_taslimi">
                                <option value="" selected disabled>محل تسلیمی</option>
                                {{-- @foreach ($employees as $item)
                                    <option value="{{$item['id']}}" {{old('mahal_taslimi') == $item['id'] ? 'selected' : ''}}>دیپو {{$item['name_dr']}} </option>
                                @endforeach --}}
                            </select>

                            @if($errors->has('mahal_taslimi'))
                            <div class="form-control-feedback">{{$errors->first('mahal_taslimi')}}</div>
                            @endif
                        </div>
                        <div class="form-group m-form__group col-md-6 pt-0">
                            <label for="department_description">{{trans('global.description')}}</label>
                            <textarea name="description" class="form-control" id="department_description"placeholder="{{trans('global.description')}}">{{old('description')}}</textarea>
                        </div>
                    </div>
                    <div class="form-group row ">
                        <div class="form-group col-md-6 m-form__group {{$errors->has('maktoob_number') ? 'has-danger' : ''}}">
                            <label for="maktoob_number">{{trans('global.maktoob_number')}}</label>
                            <input type="number" name="maktoob_number" value="{{old('maktoob_number')}}" class="form-control m-input m-input--square" id="maktoob_number" placeholder="{{trans('global.maktoob_number')}}">
                            @if($errors->has('maktoob_number'))
                                <div class="form-control-feedback">{{$errors->first('maktoob_number')}}</div>
                            @endif
                        </div>
                        <div class="form-group col-md-6 m-form__group pt-0 {{$errors->has('maktoob_date') ? 'has-danger' : ''}}">
                            <label for="maktoob_date">{{trans('global.maktoob_date')}}</label>
                            <input type="text" name="maktoob_date" value="{{old('maktoob_date')}}" class="form-control m-input m-input--square persian_date" id="maktoob_date" placeholder="{{trans('global.maktoob_date')}}">
                            @if($errors->has('maktoob_date'))
                                <div class="form-control-feedback">{{$errors->first('maktoob_date')}}</div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row" v-if="fece9.fece9_type !=1">
                            <div class="form-group col m-form__group pt-0 {{$errors->has('meem3_file') ? 'has-danger' : ''}}">
                                    <label for="exampleInputEmail1">{{trans('global.meem3_file')}}</label>
                                    <div></div>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="image" id="customFile">
                                        <label class="custom-file-label" for="customFile">فایل را انتخاب نمائید</label>
                                    </div>
                                @if($errors->has('meem3_file'))
                                    <div class="form-control-feedback">{{$errors->first('meem3_file')}}</div>
                                @endif
                            </div>

                    </div>
                    <div class="form-group row ">
                        <div class="form-group col-md-6 m-form__group {{$errors->has('maktoob_number') ? 'has-danger' : ''}}">
                            <button class="btn btn-success">{{trans('global.meem7_submit')}}</button>
                        </div>
                    </div>
                </div>

            </form>
        @else
            <div v-for="(item, fece9_index) in fece9">
                <div class="accordion accordion-solid accordion-toggle-plus"   :id="`accordion`+fece9_index">
                    <div class="card">
                        <div class="card-header" id="headingOne6">
                        <div class="card-title font-weight-bold" data-toggle="collapse" :data-target="`#collapseOne6`+fece9_index">
                        <i class="flaticon-pie-chart-1"></i>  اجناس درخواست شده توسط ف س ۹ نمبر
                                            <u> @{{item.number}}</u> مورخ @{{item.date}}
                                            @{{item.department.name_dr}}
                        </div>
                        </div>
                        <div :id="'collapseOne6'+fece9_index" class="collapse " :data-parent="`#accordion`+fece9_index">
                        <div class="card-body">
                        <table class="table" >
                            <thead>
                                <tr class="text-left text-uppercase">
                                    <th  class="font-weight-bold">شماره</th>
                                    <th  class="font-weight-bold"> نام جنس</th>
                                    <th  class="font-weight-bold">مقدار</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(item_detail, index) in item.item_details">
                                    <td class="pl-0 py-8">
                                            @{{++index}}
                                    </td>
                                    <td class="pl-0 py-8">
                                            @{{item_detail.name_dr}}
                                    </td>
                                    <td>
                                        <span class="label label-inline label-light-primary font-weight-bold"> @{{item_detail.pivot['quantity']}} @{{item_detail.unit.name_dr}} </span>
                                    </td>
                                </tr>   
                            </tbody>
                        </table>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <form class="m-form m-form--fit m-form--label-align-right" id="meem7_form" method="POST" enctype="multipart/form-data" action="{{route('meem7.store')}}">
                @csrf
                <input type="hidden" name="fece9_id" value="">
                <input type="hidden" name="fece9_type" value="{{$fece9_type}}">
                <input type="hidden" name="meem7_type" value="1">

                <input class="form-control" type="hidden" name="masrafi" value="1">

                <input type="hidden" name="status" value="0">
                <div class="m-portlet__body">
                <div class="form-group row ">
                    <div class="form-group col-md-6 m-form__group {{$errors->has('number') ? 'has-danger' : ''}}">
                    <label for="fece5_number">{{trans('global.meem7_number')}}</label>
                        <input type="number" name="number" value="{{old('number')}}" class="form-control m-input m-input--square" id="number" placeholder="{{trans('global.meem7_number')}}">
                        @if($errors->has('number'))
                            <div class="form-control-feedback">{{$errors->first('number')}}</div>
                        @endif
                    </div>
                    <div class="form-group col-md-6 pt-0 m-form__group {{$errors->has('date') ? 'has-danger' : ''}}">
                    <label for="inputPassword4">{{trans('global.meem7_date')}}</label>
                    <div class="input-group date">
                        <input type="text" name="date" value="{{old('date')}}" class="form-control persian_date" placeholder="{{trans('global.meem7_date')}}">
                            <div class="input-group-append">
                            <span class="input-group-text">
                                <i class="la la-calendar"></i>
                            </span>
                            </div>
                    </div>
                    @if($errors->has('date'))
                        <div class="form-control-feedback">{{$errors->first('date')}}</div>
                    @endif
                    </div>
                </div>
                <div class="form-group row ">
                
                    <div class="form-group col-md-6 m-form__group {{$errors->has('contract_id') ? 'has-danger' : ''}}">
                        <label for="contract_id">{{trans('global.akhz_az_qarardad')}}</label>
                        <select class="form-control" name="contract_id" >
                            <option value="">{{trans('global.akhz_az_qarardad')}}</option>
                            @foreach ($contracts as $item)
                                <option value="{{$item['id']}}" {{old('contract_id') == $item['id'] ? 'selected' : ''}}>{{$item->contractorCompany->company_name}}</option>
                            @endforeach
                        </select>
                        @if($errors->has('contract_id'))
                            <div class="form-control-feedback">{{$errors->first('contract_id')}}</div>
                        @endif
                    </div>
                        <div class="form-group col-md-6 pt-0 m-form__group {{$errors->has('department_id') ? 'has-danger' : ''}}">
                            <label for="department">{{trans('global.requesting_department')}}</label>
                            <select class="form-control select2" name="department_id" id="department">
                                <option value="">{{trans('global.requesting_department')}}</option>
                               
                                    <option :value="fece9[0].department.id" selected > @{{fece9[0].department.name_dr}}</option>
                               
                            </select>
                            @if($errors->has('department_id'))
                                <div class="form-control-feedback">{{$errors->first('department_id')}}</div>
                            @endif
                        </div>
                </div>
                <div class="form-group row ">
                        <div class="form-group col-md-6 m-form__group {{$errors->has('mahal_taslimi') ? 'has-danger' : ''}}">
                            <label for="fece9_mahal_taslimi">{{trans('global.mahal_taslimi')}}</label>
                            <select class="form-control employee" name="mahal_taslimi" id="mahal_taslimi">
                                <option value="" selected disabled>محل تسلیمی</option>
                                {{-- @foreach ($employees as $item)
                                    <option value="{{$item['id']}}" {{old('mahal_taslimi') == $item['id'] ? 'selected' : ''}}>دیپو {{$item['name_dr']}} </option>
                                @endforeach --}}
                            </select>

                            @if($errors->has('mahal_taslimi'))
                            <div class="form-control-feedback">{{$errors->first('mahal_taslimi')}}</div>
                            @endif
                        </div>
                        <div class="form-group m-form__group col-md-6 pt-0">
                            <label for="department_description">{{trans('global.description')}}</label>
                            <textarea name="description" class="form-control" id="department_description"placeholder="{{trans('global.description')}}">{{old('description')}}</textarea>
                        </div>
                    </div>
                    <div class="form-group row ">
                        <div class="form-group col-md-6 m-form__group {{$errors->has('maktoob_number') ? 'has-danger' : ''}}">
                            <label for="maktoob_number">{{trans('global.maktoob_number')}}</label>
                            <input type="number" name="maktoob_number" value="{{old('maktoob_number')}}" class="form-control m-input m-input--square" id="maktoob_number" placeholder="{{trans('global.maktoob_number')}}">
                            @if($errors->has('maktoob_number'))
                                <div class="form-control-feedback">{{$errors->first('maktoob_number')}}</div>
                            @endif
                        </div>
                        <div class="form-group col-md-6 m-form__group pt-0 {{$errors->has('maktoob_date') ? 'has-danger' : ''}}">
                            <label for="maktoob_date">{{trans('global.maktoob_date')}}</label>
                            <input type="text" name="maktoob_date" value="{{old('maktoob_date')}}" class="form-control m-input m-input--square persian_date" id="maktoob_date" placeholder="{{trans('global.maktoob_date')}}">
                            @if($errors->has('maktoob_date'))
                                <div class="form-control-feedback">{{$errors->first('maktoob_date')}}</div>
                            @endif
                        </div>
                    </div>
              
                    <div class="form-group row ">
                        <div class="form-group col-md-6 m-form__group {{$errors->has('maktoob_number') ? 'has-danger' : ''}}">
                            <button class="btn btn-success">{{trans('global.meem7_submit')}}</button>
                        </div>
                    </div>
                </div>

            </form>
            
        @endif



    @else

    <div class="row m-row--no-padding m-row--col-separator-xl">
        <div class="col-md-12 col-lg-6 col-xl-3 meem7_descriptions">
            <div class="m-widget24__item">
                <h4 class="m-widget24__title">
                    {{trans('global.meem7_number')}}
                </h4>
                <span class="m-widget24__stats m--font-brand pull-left">
                        {{$meem7->number}}
                        
                </span>

            </div>
        </div>
        <div class="col-md-12 col-lg-6 col-xl-3 meem7_descriptions">
            <div class="m-widget24__item">
                <h4 class="m-widget24__title">
                    {{trans('global.meem7_date')}}
                </h4>
                <span class="m-widget24__stats m--font-brand pull-left">
                        {{$meem7->date}}
                </span>

            </div>
        </div>
        <div class="col-md-12 col-lg-6 col-xl-3 meem7_descriptions">
            <div class="m-widget24__item">
                <h4 class="m-widget24__title">
                    {{trans('global.requesting_department')}}
                </h4>
                <span class="m-widget24__stats m--font-brand pull-left">
                    {{$meem7->department->name_dr}}
                </span>

            </div>
        </div>
        <div class="col-md-12 col-lg-6 col-xl-3 meem7_descriptions">
            <div class="m-widget24__item">
                <h4 class="m-widget24__title">
                    {{trans('global.akhz_as_wajh')}}
                </h4>
                <span class="m-widget24__stats m--font-brand pull-left">
                    {{$meem7->getResposnableEmployee->name_dr}}
                </span>
            </div>
        </div>
        <div class="col-md-12 col-lg-6 col-xl-3 meem7_descriptions">
            <div class="m-widget24__item">
                <h4 class="m-widget24__title">
                        {{trans('global.mahal_taslimi')}}
                    </h4>
                <span class="m-widget24__stats m--font-brand pull-left">
                        {{$meem7->mahal_taslimi}}
                </span>

            </div>
        </div>
        <div class="col-md-12 col-lg-6 col-xl-3 meem7_descriptions">
            <div class="m-widget24__item">
                <h4 class="m-widget24__title">
                        {{trans('global.maktoob_number')}}
                </h4>
                <span class="m-widget24__stats m--font-brand pull-left">
                    {{$meem7->maktoob_number}}
                </span>

            </div>
        </div>
        <div class="col-md-12 col-lg-6 col-xl-3 meem7_descriptions">
            <div class="m-widget24__item">
                <h4 class="m-widget24__title">
                        {{trans('global.maktoob_date')}}
                </h4>
                <span class="m-widget24__stats m--font-brand pull-left">
                    {{$meem7->maktoob_date}}
                </span>
            </div>
        </div>
        <div class="col-md-12 col-lg-6 col-xl-3 meem7_descriptions">
            <div class="m-widget24__item">
                <h4 class="m-widget24__title">
                        {{trans('global.description')}}
                </h4>
                <span class="m-widget24__stats m--font-brand pull-left">
                    {{$meem7->description}}
                </span>
            </div>
        </div>
        <div class="col-md-12 col-lg-6 col-xl-3 meem7_descriptions">
            <div class="m-widget24__item">
                <h4 class="m-widget24__title">
                        {{trans('global.meem3_file')}}
                </h4>
                <span class="m-widget24__stats m--font-brand pull-left">
                    <a href="{{asset($meem7->meem3_file)}}"  data-fancybox><i class="flaticon-attachment"></i></a>
                </span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h4 class="p-2 font-weight-bold">
                {{trans('global.item_details')}}
            </h4>
            <div v-for="(item_details_specifications, item_details_id) in items">
                @{{existing_items_extra_specifications[item_details_id].name_dr}}
                <table class="table table-bordered m-2">
                    <thead>
                        <tr>
                            <th>شماره</th>
                            <th>قیمت</th>
                            <th>واحد پولی</th>
                            <th>حالت جنس</th>
                            <th v-if="existing_items_extra_specifications[item_details_id].col1" >@{{existing_items_extra_specifications[item_details_id].col1}}</th>
                            <th v-if="existing_items_extra_specifications[item_details_id].col2" >@{{existing_items_extra_specifications[item_details_id].col2}}</th>
                            <th v-if="existing_items_extra_specifications[item_details_id].col3" >@{{existing_items_extra_specifications[item_details_id].col3}}</th>
                            <th v-if="existing_items_extra_specifications[item_details_id].col4" >@{{existing_items_extra_specifications[item_details_id].col4}}</th>
                            <th v-if="existing_items_extra_specifications[item_details_id].col5" >@{{existing_items_extra_specifications[item_details_id].col5}}</th>
                            <th v-if="existing_items_extra_specifications[item_details_id].col6" >@{{existing_items_extra_specifications[item_details_id].col6}}</th>
                            <th v-if="existing_items_extra_specifications[item_details_id].col7" >@{{existing_items_extra_specifications[item_details_id].col7}}</th>
                            <th v-if="existing_items_extra_specifications[item_details_id].col8" >@{{existing_items_extra_specifications[item_details_id].col8}}</th>
                            <th v-if="existing_items_extra_specifications[item_details_id].col9" >@{{existing_items_extra_specifications[item_details_id].col9}}</th>
                            <th v-if="existing_items_extra_specifications[item_details_id].col10" >@{{existing_items_extra_specifications[item_details_id].col10}}</th>
                            <th v-if="existing_items_extra_specifications[item_details_id].col11" >@{{existing_items_extra_specifications[item_details_id].col11}}</th>
                            <th v-if="existing_items_extra_specifications[item_details_id].col12" >@{{existing_items_extra_specifications[item_details_id].col12}}</th>
                            <th v-if="extra_specifications.distribution_type =='1'">تعداد</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(item, index) in item_details_specifications">
                            <td>@{{++index}}</td>
                            <td>@{{item.price}}</td>
                            <td>
                                <span v-if="item.currency == 0">افغانی</span>
                                <span v-else-if="item.currency == 1">دالر</span>
                            <td>
                                <span v-if="item.condition == 0">جدید</span>
                                <span v-else-if="item.condition == 1">مستعمل</span>
                                <span v-else-if="item.condition == 2">داغمه</span>
                                <span v-else-if="item.condition == 3">غیر فعال</span>
                            </td>
                            <td v-if="existing_items_extra_specifications[item_details_id].col1">@{{item.col1}}</td>
                            <td v-if="existing_items_extra_specifications[item_details_id].col2">@{{item.col2}}</td>
                            <td v-if="existing_items_extra_specifications[item_details_id].col3">@{{item.col3}}</td>
                            <td v-if="existing_items_extra_specifications[item_details_id].col4">@{{item.col4}}</td>
                            <td v-if="existing_items_extra_specifications[item_details_id].col5">@{{item.col5}}</td>
                            <td v-if="existing_items_extra_specifications[item_details_id].col6">@{{item.col6}}</td>
                            <td v-if="existing_items_extra_specifications[item_details_id].col7">@{{item.col7}}</td>
                            <td v-if="existing_items_extra_specifications[item_details_id].col8">@{{item.col8}}</td>
                            <td v-if="existing_items_extra_specifications[item_details_id].col9">@{{item.col9}}</td>
                            <td v-if="existing_items_extra_specifications[item_details_id].col10">@{{item.col10}}</td>
                            <td v-if="existing_items_extra_specifications[item_details_id].col11">@{{item.col11}}</td>
                            <td v-if="existing_items_extra_specifications[item_details_id].col12">@{{item.col12}}</td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
    <div id="accordion" class="mt-2">

        @foreach ($meem7->fece9->itemDetails()->where('status', 0)->get() as $index => $item)
            <div class="card">

                <div class="card-header" @click="getSubCategorySpecificationsKeys({{$item->sub_category_id}}, {{$item->pivot->quantity}}, {{$item->id}}, {{isset($item->subCategory->subCategoryKeys) ? $item->subCategory->subCategoryKeys->id : ''}})" id="headingOne">
                    <h5 class="mb-0">
                        <button class="btn btn-link" data-toggle="collapse" data-target="#collapse{{$index}}" aria-expanded="false" aria-controls="collapse{{$index}}">
                        <span class="mr-2">کتگوری فرعی :</span><span class="text-dark pr-1">({{$item->name_dr}})</span>   <span class="mr-2">تعداد درخواست شده:</span><span class="text-dark pr-1">({{$item->pivot->quantity}})</span>  <span class="mr-2">واحد:</span> <span class="text-dark pr-1">({{$item->unit->name_dr}})</span>
                        </button>
                    </h5>
                    <span class="text-danger" v-if="error==true">لطفآ فورم را مکمل خانه پُری نمائید !!!</span>
                </div>

                <div id="collapse{{$index}}" class="collapse" aria-labelledby="heading{{$index}}" data-parent="#accordion">
                    <div class="card-body">
                        <form class="form-inline row">
                            <table class="table">
                                <tr  v-for="(item, index) in specifications">
                                    <th style="line-height:3;font-weight:bold">@{{++index}}</th>
                                    <th class="px-1">
                                        <div class="input-group">
                                            <input type="text"  required v-model="item.price" class="form-control"  placeholder="{{trans('global.price')}}">
                                            <div v-if="index == 1" class="input-group-append">
                                                <span class="input-group-text">همه</span>
                                                <span class="input-group-text">
                                                    <label class="m-checkbox m-checkbox--single m-checkbox--state m-checkbox--state-primary">
                                                        <input type="checkbox" @change="setMasterValue('price', item.price, $event)">
                                                        <span></span>
                                                    </label>

                                                </span>
                                            </div>
                                        </div>
                                    </th>

                                    <th class="px-1"  width="150">
                                        <div :class="['input-group', index > 1 ? 'w-100' : '']">
                                            {{-- <input type="text"  required class="form-control"  placeholder="{{trans('global.price')}}"> --}}
                                            <select v-model="item.currency" class="form-control"  >
                                                <option value="">{{trans('global.currency')}}</option>
                                                <option value="0">افغانی</option>
                                                <option value="1">دالر</option>
                                            </select>
                                            <div v-if="index == 1" class="input-group-append">
                                                <span class="input-group-text">همه</span>
                                                <span class="input-group-text">
                                                    <label class="m-checkbox m-checkbox--single m-checkbox--state m-checkbox--state-primary">
                                                        <input type="checkbox" @change="setMasterValue('currency', item.currency, $event)">
                                                        <span></span>
                                                    </label>

                                                </span>
                                            </div>

                                    </th>
                                    <th class="px-1" v-if="extra_specifications.col1">
                                        <div class="input-group">
                                            <input type="text"  required v-model="item.col1" class="form-control"  :placeholder="extra_specifications.col1">
                                            <div v-if="index == 1" class="input-group-append">
                                                <span class="input-group-text">همه</span>
                                                <span class="input-group-text">
                                                    <label class="m-checkbox m-checkbox--single m-checkbox--state m-checkbox--state-primary">
                                                        <input type="checkbox" @change="setMasterValue('col1', item.col1, $event)">
                                                        <span></span>
                                                    </label>

                                                </span>
                                            </div>
                                        </div>
                                    </th>
                                    <th class="px-1"  v-if="extra_specifications.col2">
                                        <div class="input-group">
                                            <input type="text"  required v-model="item.col2" class="form-control"  :placeholder="extra_specifications.col2">
                                            <div v-if="index == 1" class="input-group-append">
                                                <span class="input-group-text">همه</span>
                                                <span class="input-group-text">
                                                    <label class="m-checkbox m-checkbox--single m-checkbox--state m-checkbox--state-primary">
                                                        <input type="checkbox" @change="setMasterValue('col2', item.col2, $event)">
                                                        <span></span>
                                                    </label>

                                                </span>
                                            </div>
                                        </div>
                                    </th>
                                    <th class="px-1"  v-if="extra_specifications.col3">
                                        <div class="input-group">
                                            <input type="text"  required v-model="item.col3" class="form-control"  :placeholder="extra_specifications.col3">
                                            <div v-if="index == 1" class="input-group-append">
                                                <span class="input-group-text">همه</span>
                                                <span class="input-group-text">
                                                    <label class="m-checkbox m-checkbox--single m-checkbox--state m-checkbox--state-primary">
                                                        <input type="checkbox" @change="setMasterValue('col3', item.col3, $event)">
                                                        <span></span>
                                                    </label>

                                                </span>
                                            </div>
                                        </div>
                                    </th>
                                    <th class="px-1"  v-if="extra_specifications.col4">
                                        <div class="input-group">
                                            <input type="text"  required v-model="item.col4" class="form-control"  :placeholder="extra_specifications.col4">
                                            <div v-if="index == 1" class="input-group-append">
                                                <span class="input-group-text">همه</span>
                                                <span class="input-group-text">
                                                    <label class="m-checkbox m-checkbox--single m-checkbox--state m-checkbox--state-primary">
                                                        <input type="checkbox" @change="setMasterValue('col4', item.col4, $event)">
                                                        <span></span>
                                                    </label>

                                                </span>
                                            </div>
                                        </div>
                                    </th>
                                    <th class="px-1"  v-if="extra_specifications.col5">
                                        <div class="input-group">
                                            <input type="text"  required v-model="item.col5" class="form-control"  :placeholder="extra_specifications.col5">
                                            <div v-if="index == 1" class="input-group-append">
                                                <span class="input-group-text">همه</span>
                                                <span class="input-group-text">
                                                    <label class="m-checkbox m-checkbox--single m-checkbox--state m-checkbox--state-primary">
                                                        <input type="checkbox" @change="setMasterValue('col5', item.col5, $event)">
                                                        <span></span>
                                                    </label>

                                                </span>
                                            </div>
                                        </div>
                                    </th>
                                    <th class="px-1"  v-if="extra_specifications.col6">
                                        <div class="input-group">
                                            <input type="text"  required v-model="item.col6" class="form-control"  :placeholder="extra_specifications.col6">
                                            <div v-if="index == 1" class="input-group-append">
                                                <span class="input-group-text">همه</span>
                                                <span class="input-group-text">
                                                    <label class="m-checkbox m-checkbox--single m-checkbox--state m-checkbox--state-primary">
                                                        <input type="checkbox" @change="setMasterValue('col6', item.col6, $event)">
                                                        <span></span>
                                                    </label>

                                                </span>
                                            </div>
                                        </div>
                                    </th>
                                    <th class="px-1"  v-if="extra_specifications.col7">
                                        <div class="input-group">
                                            <input type="text"  required v-model="item.col7" class="form-control"  :placeholder="extra_specifications.col7">
                                            <div v-if="index == 1" class="input-group-append">
                                                <span class="input-group-text">همه</span>
                                                <span class="input-group-text">
                                                    <label class="m-checkbox m-checkbox--single m-checkbox--state m-checkbox--state-primary">
                                                        <input type="checkbox" @change="setMasterValue('col7', item.col7, $event)">
                                                        <span></span>
                                                    </label>

                                                </span>
                                            </div>
                                        </div>
                                    </th>
                                    <th class="px-1"  v-if="extra_specifications.col8">
                                        <div class="input-group">
                                            <input type="text"  required v-model="item.col8" class="form-control"  :placeholder="extra_specifications.col8">
                                            <div v-if="index == 1" class="input-group-append">
                                                <span class="input-group-text">همه</span>
                                                <span class="input-group-text">
                                                    <label class="m-checkbox m-checkbox--single m-checkbox--state m-checkbox--state-primary">
                                                        <input type="checkbox" @change="setMasterValue('col8', item.col8, $event)">
                                                        <span></span>
                                                    </label>

                                                </span>
                                            </div>
                                        </div>
                                    </th>
                                    <th class="px-1"  v-if="extra_specifications.col9">
                                        <div class="input-group">
                                            <input type="text"  required v-model="item.col9" class="form-control"  :placeholder="extra_specifications.col9">
                                            <div v-if="index == 1" class="input-group-append">
                                                <span class="input-group-text">همه</span>
                                                <span class="input-group-text">
                                                    <label class="m-checkbox m-checkbox--single m-checkbox--state m-checkbox--state-primary">
                                                        <input type="checkbox" @change="setMasterValue('col9', item.col9, $event)">
                                                        <span></span>
                                                    </label>

                                                </span>
                                            </div>
                                        </div>
                                    </th>
                                    <th class="px-1"  v-if="extra_specifications.col10">
                                        <div class="input-group">
                                            <input type="text"  required v-model="item.col10" class="form-control"  :placeholder="extra_specifications.col10">
                                            <div v-if="index == 1" class="input-group-append">
                                                <span class="input-group-text">همه</span>
                                                <span class="input-group-text">
                                                    <label class="m-checkbox m-checkbox--single m-checkbox--state m-checkbox--state-primary">
                                                        <input type="checkbox" @change="setMasterValue('col10', item.col10, $event)">
                                                        <span></span>
                                                    </label>

                                                </span>
                                            </div>
                                        </div>
                                    </th>
                                    <th class="px-1"  v-if="extra_specifications.col11">
                                        <div class="input-group">
                                            <input type="text"  required v-model="item.col11" class="form-control"  :placeholder="extra_specifications.col11">
                                            <div v-if="index == 1" class="input-group-append">
                                                <span class="input-group-text">همه</span>
                                                <span class="input-group-text">
                                                    <label class="m-checkbox m-checkbox--single m-checkbox--state m-checkbox--state-primary">
                                                        <input type="checkbox" @change="setMasterValue('col11', item.col11, $event)">
                                                        <span></span>
                                                    </label>

                                                </span>
                                            </div>
                                        </div>
                                    </th>
                                    <th class="px-1"  v-if="extra_specifications.col12">
                                        <div class="input-group">
                                            <input type="text"  required v-model="item.col12" class="form-control"  :placeholder="extra_specifications.col12">
                                            <div v-if="index == 1" class="input-group-append">
                                                <span class="input-group-text">همه</span>
                                                <span class="input-group-text">
                                                    <label class="m-checkbox m-checkbox--single m-checkbox--state m-checkbox--state-primary">
                                                        <input type="checkbox" @change="setMasterValue('col12', item.col12, $event)">
                                                        <span></span>
                                                    </label>

                                                </span>
                                            </div>
                                        </div>
                                    </th>
                                    <th class="px-1">
                                        <div  v-if="extra_specifications.distribution_type =='1'" class="form-group">
                                                <div class="input-group">
                                                    <input type="text" v-model="item.item_amount" class="form-control"  placeholder="تعداد">
                                                    <div v-if="index == 1 && !edit" class="input-group-append">
                                                        {{-- <span class="input-group-text">*</span> --}}
                                                        <span class="input-group-text">
                                                            <label class="m-checkbox m-checkbox--single m-checkbox--state m-checkbox--state-primary">
                                                                <input type="checkbox" @change="setMasterValue('distribution_type', item.distribution_type, $event)">
                                                                <span></span>
                                                            </label>
                    
                                                        </span>
                                                    </div>
                                                </div>
                                        </div>
                                    </th>
                                </tr>
                            </table>
                        </form>
                        <div class="row">
                            <div class="col-md-6 p-1" v-if="specifications.length > 0">
                                <button type="button" @click="submitSpecifications" class="btn btn-primary mb-2">{{trans('global.add_record')}}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    @if ($meem7->fece9->itemDetails()->where('status', 0)->count() == 0)
        <div class="m-form__actions my-2">
            <a href="{{route('meem7.index')}}" class="btn btn-primary">{{trans('global.submit')}}</a>
            <button type="reset" class="btn btn-danger">{{trans('global.cancel')}}</button>
        </div>
    @endif
    @endif
</div>
@endsection
@push('custom-js')

    <script>
item_app = new Vue({
            el: '#create_meem7',
            data: {
                total_forms: '',


                // extra specifications
                extra_specifications: '',

                sub_category_key_id: '',
                item_details_id: '',
                fece9_id: '{{isset($fece9->id) ? $fece9->id : ''}}',
                status:'',
                condition:'',
                fece9: {!! isset($fece9) ? $fece9 : $fece9s !!},

                // master value
                master: {
                    price: '',
                    currency: '',
                    col1: '',
                    col2: '',
                    col3: '',
                    col4: '',
                    col5: '',
                    col6: '',
                    col7: '',
                    col8: '',
                    col9: '',
                    col10: '',
                    col11: '',
                    col12: '',
                },

                // items specifications
                specifications: [

                ],
                error: false,
                error_messages: [],
                mahal_taslimi:'{{isset($meem7->mahal_taslimi) ? $meem7->mahal_taslimi : ''}}',


                // displaying items
                items: {},
                existing_items_extra_specifications:[],
                meem7_id: '{{isset($meem7->id) ? $meem7->id :''}}',


            },
            methods: {
          
                getSubCategorySpecificationsKeys(sub_category_id, total_quantity, item_details_id,  sub_categories_key_id='') {
                    this.getExtraSpecifications(sub_category_id)
                    this.setItemDetailsId(item_details_id);
                    this.setSubCategoryKeyId(sub_categories_key_id);
                    this.generateForms(total_quantity);
                },
                getExtraSpecifications(sub_category_id) {
                    axios.get("{{url('get_sub_categories_keys')}}/"+sub_category_id)
                        .then(res => {
                            console.log(res.data);
                            this.extra_specifications = res.data;
                        })
                        .catch(err => {
                            console.log(err);

                        });
                },
                generateForms(quantity) {
                    this.specifications=[];
                    this.total_forms = quantity;
                    for(i = 0 ; i < this.total_forms ; i++) {
                   
                        this.specifications.push(
                            {
                                price: this.master.price ? this.master.price : '',
                                currency: this.master.currency ? this.master.currency : '',
                                sub_categories_key_id: this.sub_categories_key_id,
                                item_details_id: this.item_details_id,
                                status: '0',
                                condition: '0',
                                sub_categories_key_id: this.sub_category_key_id,
                                col1: this.master.col1 ? this.master.col1 : '',
                                col2: this.master.col2 ? this.master.col2 : '',
                                col3: this.master.col3 ? this.master.col3 : '',
                                col4: this.master.col4 ? this.master.col4 : '',
                                col5: this.master.col5 ? this.master.col5 : '',
                                col6: this.master.col6 ? this.master.col6 : '',
                                col7: this.master.col7 ? this.master.col7 : '',
                                col8: this.master.col8 ? this.master.col8 : '',
                                col9: this.master.col9 ? this.master.col9 : '',
                                col10: this.master.col10 ? this.master.col10 : '',
                                col11: this.master.col11 ? this.master.col11 : '',
                                col12: this.master.col12 ? this.master.col12 : '',
                                meem7_id:this.meem7_id,

                            }
                        );
                    }
                },
                setItemDetailsId(item_details_id) {
                    this.item_details_id = item_details_id;
                },
                setSubCategoryKeyId(sub_category_key_id) {
                    if(sub_category_key_id !== '') {
                        this.sub_category_key_id = sub_category_key_id;
                    }
                },
                setMasterValue(type, value, event) {
                    this.master[type] = event.target.checked ? value : '';

                    for(i = 1 ; i < this.specifications.length ; i++) {
                        this.specifications[i][type] = this.master[type];
                    }
                },
                submitSpecifications() {
                    axios.post("{{route('item_details_specifications.store')}}", {
                        'fece9_id': this.fece9_id,
                        'item_details_id': this.item_details_id,
                        'specifications': this.specifications,
                        'sub_categories_key_id': this.sub_category_key_id,
                        
                    }) 
                    .then(res => {
                        this.success = true;
                        window.location.reload()
                    })
                    .catch(err => {
                        console.log(err);
                        this.error = true;
                        this.error_messages = err.response.data.errors;
                    });
                },
                getMeem7ItemDetailsSpecifications(meem7_id) {
                    axios.get("{{url('meem7/get_meem7_item_details_specifications')}}/"+meem7_id)
                    .then(res => {
                        this.items = res.data;
                    })
                    .catch(err => {

                    });
                },
                getMeem7ExtraSpecifications(meem7_id) {
                    axios.get("{{url('meem7/get_meem7_extra_specifications')}}/"+meem7_id)
                    .then(res => {
                        this.existing_items_extra_specifications = res.data;
                    })
                    .catch(err => {

                    });
                },





            },
            mounted() {

                if(this.meem7_id) {
                    this.getMeem7ItemDetailsSpecifications(this.meem7_id);
                    this.getMeem7ExtraSpecifications(this.meem7_id);
                }


            },
        });

        //disabled and enabled departement
        $('#department').prop('disabled', true);
        $('#meem7_form').on('submit', function() {
            $('#department').prop('disabled', false);
        });



        $(document).ready(function(){
            $(".employee").select2({
            ajax: { 
            url: '{{route("get_employee")}}',
            type:"post",
            dataType: 'json',
            delay: 250,
            data: function (params) {
            return {
                searchTerm: params.term // search term
            };
            },
            processResults: function (response) {
                return {
                results: response
                };
            },
            cache: true
            }
            });
        });
    </script>

@endpush


@push('custom-css')
    <style>
   #headingOne6:hover {
        cursor: pointer !important;
    }
    </style>
@endpush
