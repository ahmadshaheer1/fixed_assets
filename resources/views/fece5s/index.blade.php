@extends('layouts.master')
@section('title','صفحه اصلی')
@section('content')
  <table id="example" class="table table-striped table-bordered jquery-datatable" style="width:100%">
        <thead>
            <tr>
                <th>#</th>
                <th>شماره</th>
                <th>تاریخ</th>
                <th>ریاست</th>
                <th>اپلود فایل</th>
                <th>توضیحات</th>
                <th>حالت</th>
                <th>عملیات</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($fece5s as $item)
            @php
            $i=1;
            @endphp
            <tr id='{{$item->id}}_tr'>
            <td>{{$i++}}</td>
            <td>{{$item->number}}</td>
            <td>{{$item->date}}</td>
            <td>{{$item->department_id}}</td>
            <td>
                @if ($item->processed_fece5_file == Null)
                    <a href="javascript:void(0)" data-toggle="modal" data-target="#upload_modal" onclick="$('input[name=id]').val({{$item->id}})" class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air">
                        <i class="fa fa-upload"></i>
                    </a>
                @else
                    <a href="javascript:void(0)" class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air">
                        <i class="fa fa-check"></i>
                    </a>
                @endif
            </td>

            <td>{{str_limit($item->description,50)}}</td>
            <td>
                @if ($item->status == 0)
                    <div class="badge badge-secondary">در حال پروسس</div>
                @elseif($item->status == 1)
                    <div class="badge badge-success">تایید شده</div>
                @else
                    <div class="badge badge-danger" title="{{$item->reject_remark}}">رد شده</div>
                @endif
            </td>
            <td>
                <a href="{{route('fece5s.show',$item->id)}}" class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air">
                    <i class="la la-eye"></i>
                </a>

            </td>
            </tr>
        @endforeach
    </table>

    {{-- Start of the Modal  --}}
    <!-- Modal -->
    <div class="modal fade" id="upload_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">اپلود نمودن ف س ۵ طی مراحل شده</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('fece5s.attach_file_and_send')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="form-group m-form__group col-lg-12">
                        <div></div>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="image" multiple name="image" ref="file">
                            <label class="custom-file-label" for="customFile">انتخاب فایل</label>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="id" value="">
                <div class="modal-footer">
                    <div class="row">

                        <div class="col-md-6">
                            <div class="btn-group m-btn-group" role="group" aria-label="...">
                                <button type="submit" class="btn btn-brand">ارسال <i class="la la-paperclip"></i></button>
                                <button type="button" class="btn btn btn-info" data-dismiss="modal">لغو</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
        {{-- End of the Modal  --}}
@endsection

