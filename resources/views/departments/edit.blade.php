@extends('layouts.master')
@section('title', trans('global.create_department'))
@section('content')

<form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{route('departments.update', $department->id)}}">
    @csrf
    @method('PUT')
    <div class="m-portlet__body">
        <div class="form-group m-form__group {{$errors->has($name) ? 'has-danger' : ''}}">
            <label for="department_name">{{trans('global.name')}}</label>
            <input name="{{$name}}" value="{{$department->$name}}" type="text" class="form-control m-input m-input--square" id="department_name" aria-describedby="emailHelp" placeholder="{{trans('global.name')}}">
            @if($errors->has($name))
                <div class="form-control-feedback">{{$errors->first($name)}}</div>
            @endif
        </div>
        <div class="form-group m-form__group {{$errors->has('type') ? 'has-danger' : ''}}">
            <label for="department_type">{{trans('global.type')}}</label>
            <select class="custom-select form-control" name="type">
                <option value="">نوعیت</option>
                <option value="0" {{$department->type == '0' ? 'selected' : '' }}>اصلی</option>
                <option value="1" {{$department->type == '1' ? 'selected' : '' }}>فرعی</option>
            </select>
            @if($errors->has('type'))
                <div class="form-control-feedback">{{$errors->first('type')}}</div>
            @endif
        </div>
        <div class="form-group m-form__group">
            <label for="department_description">{{trans('global.description')}}</label>
            <textarea name="description" class="form-control" id="department_description"placeholder="{{trans('global.description')}}">{{$department->description}}</textarea>
        </div>
    </div>
    <div class="m-portlet__foot m-portlet__foot--fit">
        <div class="m-form__actions">
            <button type="submit" class="btn btn-primary">{{trans('global.submit')}}</button>
            <button type="reset" class="btn btn-danger">{{trans('global.cancel')}}</button>
        </div>
    </div>
</form>
@endsection
