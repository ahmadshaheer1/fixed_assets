@extends('layouts.master')
@section('title', trans('global.contractor_company_add'))
@section('content')
<style>
    .m-form.m-form--group-seperator-dashed .m-form__group{
        border-bottom: none;
        padding:10px;
    }
    .m-portlet .m-portlet__foot:not(.m-portlet__no-border){
        border:none;
    }
</style>
<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" method="POST" action="{{route('contractor_company.store')}}" enctype="multipart/form-data">
    @csrf
    <div class="m-portlet__body">
        <div class="form-group m-form__group row ">
            <div class="col-lg-6 {{$errors->has('company_name') ? 'has-danger' : ''}}">
                <label>{{trans('global.company_name')}}</label>
                <input type="text" name="company_name" value="{{old('company_name')}}" class="form-control m-input" placeholder="{{trans('global.company_name')}}">
                @if($errors->has('company_name'))
                    <div class="form-control-feedback">{{$errors->first('company_name')}}</div>
                @endif    
            </div>
            <div class="col-lg-6 {{$errors->has('license_number') ? 'has-danger' : ''}}">
                <label>{{trans('global.license_number')}}</label>
                <input type="number" name="license_number" value="{{old('license_number')}}" class="form-control m-input" placeholder="{{trans('global.license_number')}}">
                @if($errors->has('license_number'))
                    <div class="form-control-feedback">{{$errors->first('license_number')}}</div>
                @endif      
        </div>
        </div>
        <div class="form-group m-form__group row ">
            <div class="col-lg-6 {{$errors->has('tin') ? 'has-danger' : ''}}">
                <label>{{trans('global.tin')}}</label>
                <input type="number" name="tin" value="{{old('tin')}}" class="form-control m-input" placeholder="{{trans('global.tin')}}">
                @if($errors->has('tin'))
                    <div class="form-control-feedback">{{$errors->first('tin')}}</div>
                @endif    
            </div>
            <div class="col-lg-6 {{$errors->has('number') ? 'has-danger' : ''}}">
                <label>{{trans('global.number')}}</label>
                <input type="number" name="number" value="{{old('number')}}" class="form-control m-input" placeholder="{{trans('global.number')}}">
                @if($errors->has('number'))
                    <div class="form-control-feedback">{{$errors->first('number')}}</div>
                @endif    
            </div>
        </div>
        <div class="form-group m-form__group row ">
            <div class="col-lg-6 {{$errors->has('email') ? 'has-danger' : ''}}">
                <label>{{trans('global.email')}}</label>
                <input type="email" name="email" value="{{old('email')}}" class="form-control m-input" placeholder="{{trans('global.email')}}">
                @if($errors->has('email'))
                    <div class="form-control-feedback">{{$errors->first('email')}}</div>
                @endif    
            </div>
            <div class="col-lg-6 {{$errors->has('bank_account') ? 'has-danger' : ''}}">
                <label>{{trans('global.bank_account')}}</label>
                <input type="number" name="bank_account" value="{{old('bank_account')}}" class="form-control m-input" placeholder="{{trans('global.bank_account')}}">
                @if($errors->has('bank_account'))
                    <div class="form-control-feedback">{{$errors->first('bank_account')}}</div>
                @endif    
            </div>
        </div>
        <div class="form-group m-form__group row "> 
                
            <div class="form-group col m-form__group pt-0 {{$errors->has('company_file') ? 'has-danger' : ''}}">
                    <label for="exampleInputEmail1">{{trans('global.company_file')}}</label>
                    <div></div>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" name="image" id="customFile">
                        <label class="custom-file-label" for="customFile">فایل را انتخاب نمائید</label>
                    </div>
                @if($errors->has('company_file'))
                    <div class="form-control-feedback">{{$errors->first('company_file')}}</div>
                @endif
            </div>   
        </div>   
        <div class="m-portlet__foot m-portlet__foot--fit">
            <div class="m-form__actions">
                <button type="submit" class="btn btn-primary">{{trans('global.submit')}}</button>
                <button type="reset" class="btn btn-danger">{{trans('global.cancel')}}</button>
            </div>
        </div>
    </div>

</form>

@endsection
