@extends('layouts.master')
@section('title',trans('global.external_departments'))
@section('content')
<table class="table table-striped- table-bordered table-hover table-checkable jquery-datatable" id="m_table_1">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">{{trans('global.name')}}</th>
        <th scope="col">{{trans('global.actions')}}</th>
      </tr>
    </thead>
    <tbody>

        @foreach ($data as $key => $item)
            <tr id="{{$item->id}}_tr">
                <th scope="row">{{++$key}}</th>
                <td>{{$item->$name}}</td>
                <td>
                    <a class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" href="{{ route('external_departments.edit',encrypt($item->id)) }}"><i class="la la-edit"></i></a>
                    <a href="javascript:void(0)" onclick="deleteRecord('{{route('external_departments.destroy', encrypt($item->id))}}','{{$item->id}}_tr');" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill ">
                        <i class="fa fa-trash"></i>
                    </a>
                </td>
            </tr>
        @endforeach
    </tbody>
  </table>
@endsection
@push('create-button')
<a href="{{ route('external_departments.create') }}" class="btn btn-success m-btn m-btn--custom ">
    <span>
    <span>اضافه نمودن</span>
    <i class="la la-plus"></i>
    </span>
</a>
@endpush
