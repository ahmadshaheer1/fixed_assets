@extends('layouts.master')
@section('title', trans('global.edit_external_department'))
@section('content')

<form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{route('external_departments.update', encrypt($external_department->id))}}">
    @csrf
    @method('PUT')
    <div class="m-portlet__body">
        <div class="form-group m-form__group {{$errors->has('name_dr') ? 'has-danger' : ''}}">
            <label for="department_name">{{trans('global.name')}}</label>
            <input name="name_dr" value="{{ $external_department->name_dr }}" type="text" class="form-control m-input m-input--square" id="department_name" aria-describedby="emailHelp" placeholder="{{trans('global.name_dr')}}">
            @if($errors->has('name_dr'))
                <div class="form-control-feedback">{{$errors->first('name_dr')}}</div>
            @endif
        </div>
        <div class="form-group m-form__group {{$errors->has('name_en') ? 'has-danger' : ''}}">
            <label for="department_name">{{trans('global.name')}}</label>
            <input name="name_en" value="{{ $external_department->name_en }}"  type="text" class="form-control m-input m-input--square" id="department_name" aria-describedby="emailHelp" placeholder="{{trans('global.name_en')}}">
            @if($errors->has('name_en'))
                <div class="form-control-feedback">{{$errors->first('name_en')}}</div>
            @endif
        </div>
        <div class="form-group m-form__group {{$errors->has('name_pa') ? 'has-danger' : ''}}">
            <label for="department_name">{{trans('global.name')}}</label>
            <input name="name_pa" value="{{ $external_department->name_pa }}"  type="text" class="form-control m-input m-input--square" id="department_name" aria-describedby="emailHelp" placeholder="{{trans('global.name_pa')}}">
            @if($errors->has('name_pa'))
                <div class="form-control-feedback">{{$errors->first('name_pa')}}</div>
            @endif
        </div>
    </div>
    <div class="m-portlet__foot m-portlet__foot--fit">
        <div class="m-form__actions">
            <button type="submit" class="btn btn-primary">{{trans('global.submit')}}</button>
            <button type="reset" class="btn btn-danger">{{trans('global.cancel')}}</button>
        </div>
    </div>
</form>
@endsection
