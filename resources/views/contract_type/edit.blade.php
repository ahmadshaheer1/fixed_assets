@extends('layouts.master')
@section('title', trans('global.edit_contract_type'))
@section('content')

<form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{route('contract_type.update', $contractType->id)}}">
    @csrf
    @method('PUT')
    <div class="m-portlet__body">
        <div class="form-group m-form__group {{$errors->has('name_dr') ? 'has-danger' : ''}}">
            <label for="name_dr">{{trans('global.name')}}</label>
            <input name="name_dr" value="{{$contractType->name_dr}}" type="text" class="form-control m-input m-input--square" id="name_dr" aria-describedby="emailHelp" placeholder="{{trans('global.name')}}">
            @if($errors->has('name_dr'))
                <div class="form-control-feedback">{{$errors->first('name_dr')}}</div>
            @endif
        </div>
   
        <div class="form-group m-form__group">
            <label for="department_description">{{trans('global.description')}}</label>
            <textarea name="description" class="form-control" id="department_description"placeholder="{{trans('global.description')}}">{{$contractType->description}}</textarea>
        </div>
    </div>
    <div class="m-portlet__foot m-portlet__foot--fit">
        <div class="m-form__actions">
            <button type="submit" class="btn btn-primary">{{trans('global.submit')}}</button>
            <button type="reset" class="btn btn-danger">{{trans('global.cancel')}}</button>
        </div>
    </div>
</form>
@endsection
