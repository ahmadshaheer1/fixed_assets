@extends('layouts.master')
@section('title',trans('global.contract_type'))
@section('content')
<table class="table table-striped- table-bordered table-hover table-checkable jquery-datatable" id="m_table_1">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">{{trans('global.name')}}</th>
        <th scope="col">{{trans('global.description')}}</th>
        <th scope="col">{{trans('global.actions')}}</th>
      </tr>
    </thead>
    <tbody>

        @foreach ($contract_types as $key => $item)
            <tr id="{{$item->id}}_tr">
                <th scope="row">{{++$key}}</th>
                <td>{{$item->$name}}</td>
                <td>{{str_limit(strip_tags($item->description), 150, ' ...')}}</td>
                <td>
                    {{-- <a class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" href="{{ route('contract_type.show',$item->id) }}"><i class="la la-eye"></i></a> --}}
                    <a class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" href="{{ route('contract_type.edit',$item->id) }}"><i class="la la-edit"></i></a>
                    {{-- <a href="javascript:void(0)" onclick="deleteRecord('{{route('contract_type.destroy', $item->id)}}','{{$item->id}}_tr');" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill ">
                        <i class="fa fa-trash"></i>
                    </a> --}}
                </td>
            </tr>
        @endforeach
    </tbody>
  </table>
@endsection
@push('create-button')
<a href="{{ route('contract_type.create') }}" class="btn btn-success m-btn m-btn--custom ">
    <span>
    <span>اضافه نمودن</span>
    <i class="la la-plus"></i>
    </span>
</a>
@endpush
