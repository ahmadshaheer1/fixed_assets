@extends('layouts.master')
@section('title',' یوزر')
@section('content')
  <table class="table table-bordered jquery-datatable">
    <thead>
     <tr>
       <th>شماره</th>
       {{-- <th>اسم</th> --}}
       <th>صلاحیت</th>
       <th width="280px">عملیات</th>
     </tr>
   </thead>
   @foreach ($permissions as $key => $permission)
     <tbody>
        <tr>
          <td>{{ ++$key }}</td>
          {{-- <td>{{ $permission->name }}</td> --}}
          <td>{{ $permission->name_dr }}</td>
          <td>
            {{-- @can('permissions-edit') --}}
                <a class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" href="{{ route('permissions.edit',$permission->id) }}"><i class="la la-edit"></i></a>
            {{-- @endcan --}}
            {{-- @can('permissions-delete') --}}
                {{-- {!! Form::open(['method' => 'DELETE','route' => ['permissions.destroy', $permission->id],'style'=>'display:inline']) !!}
                    {!! Form::button('<i class="fa fa-trash"></i>', ['type'=>'submit','class' => 'm-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill ']) !!}
                {!! Form::close() !!} --}}
            {{-- @endcan --}}

          </td>
        </tr>
  </tbody>
   @endforeach
  </table>




@endsection
 @push('create-button')
    {{-- @can('permissions-create') --}}
        <a href="{{ route('permissions.create') }}" class="btn btn-success m-btn m-btn--custom ">
            <span>
            <span>اضافه نمودن صلاحیت</span>
            <i class="la la-plus"></i>
            </span>
        </a>
        <a href="{{ route('roles.index') }}" class="btn btn-info m-btn m-btn--custom ">
            <span>
            <span>نقش</span>
            <i class="la la-key"></i>
            </span>
        </a>
        <a href="{{ route('users.index') }}" class="btn btn-primary m-btn m-btn--custom ">
            <span>
            <span>یوزر ها</span>
            <i class="la la-user"></i>
            </span>
        </a>
    {{-- @endcan --}}
@endpush
