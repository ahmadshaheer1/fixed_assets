@extends('layouts.master')
@section('title',' یوزر')
@section('content')
  <table class="table table-bordered jquery-datatable">
    <thead>
     <tr>
       <th>شماره</th>
       {{-- <th>اسم</th> --}}
       <th>ایمیل</th>
       <th>نقش ها</th>
       <th width="280px">عملیات</th>
     </tr>
   </thead>
   @foreach ($data as $key => $user)
     <tbody>
        <tr>
          <td>{{ ++$i }}</td>
          {{-- <td>{{ $user->name }}</td> --}}
          <td>{{ $user->email }}</td>
          <td>
            @if(!empty($user->getRoleNames()))
              @foreach($user->getRoleNames() as $v)
                 <label class="badge badge-success">{{ $v }}</label>
              @endforeach
            @endif
          </td>
          <td>
            {{-- @can('users-show') --}}
                <a class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" href="{{ route('users.show',$user->id) }}"><i class="la la-eye"></i></a>
            {{-- @endcan --}}
            {{-- @can('users-edit') --}}
                <a class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" href="{{ route('users.edit',$user->id) }}"><i class="la la-edit"></i></a>
            {{-- @endcan --}}
            {{-- @can('users-delete') --}}
                {!! Form::open(['method' => 'DELETE','route' => ['users.destroy', $user->id],'style'=>'display:inline']) !!}
                    {!! Form::button('<i class="fa fa-trash"></i>', ['type'=>'submit','class' => 'm-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill ']) !!}
                {!! Form::close() !!}
            {{-- @endcan --}}

          </td>
        </tr>
  </tbody>
   @endforeach
  </table>


  {!! $data->render() !!}



@endsection
 @push('create-button')
    {{-- @can('users-create') --}}
        <a href="{{ route('users.create') }}" class="btn btn-success m-btn m-btn--custom ">
            <span>
            <span>اضافه نمودن یوزر</span>
            <i class="la la-plus"></i>
            </span>
        </a>
        <a href="{{ route('roles.index') }}" class="btn btn-info m-btn m-btn--custom ">
            <span>
            <span>نقش</span>
            <i class="la la-key"></i>
            </span>
        </a>
        <a href="{{ route('permissions.index') }}" class="btn btn-primary m-btn m-btn--custom ">
            <span>
            <span>صلاحیت</span>
            <i class="la la-key"></i>
            </span>
        </a>
    {{-- @endcan --}}
@endpush
