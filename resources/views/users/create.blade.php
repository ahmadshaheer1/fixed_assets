@extends('layouts.master')
@section('title', ' ایجاد یوزر')
@section('content')
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif



    {!! Form::open(['route' => 'users.store', 'method' => 'POST']) !!}
    <div class="row">
        {{-- <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="form-group">
              <strong>نام</strong>
              {!! Form::text('name', null, array('placeholder' => 'نام','class' => 'form-control')) !!}
          </div>
      </div> --}}
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group m-form__group {{ $errors->has('employee_id') ? 'has-danger' : '' }}">
                <label class="form-control-label" for="employee_id">کارمند</label>
                <select class="form-control select2" name="employee_id">
                    <option value="">--انتخاب--</option>
                    @foreach ($employees as $value)
                        <option {{ old('employee_id') == $value->id ? 'selected' : '' }} value="{{ $value->id }}">
                            {{ $value->name_dr }} ولد {{ $value->father_name_dr }}</option>
                    @endforeach
                </select>
                <div class="form-control-feedback">{{ $errors->first('employee_id') }}</div>
            </div>
            <div class="form-group">
                <strong>ایمیل</strong>
                {!! Form::text('email', null, ['placeholder' => 'ایمیل', 'class' => 'form-control']) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>پسورد</strong>
                {!! Form::password('password', ['placeholder' => 'پسورد', 'class' => 'form-control']) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>تاییدی پسورد</strong>
                {!! Form::password('confirm-password', ['placeholder' => 'تاییدی پسورد', 'class' => 'form-control']) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>نوعیت یوزر</strong>
                <div class="form-group-inline">
                    {!! Form::radio('user_type', '0', false, ['id' => 'ajnas']) !!}
                    {!! Form::label('ajnas', 'اجناس') !!}

                    {!! Form::radio('user_type', '1', false, ['id' => 'transport']) !!}
                    {!! Form::label('transport', 'ترانسپورت') !!}
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>صلاحیت</strong>
                {!! Form::select('roles[]', $roles, [], ['class' => 'form-control', 'multiple']) !!}
            </div>
        </div>
        <div class="row">
            <div class="col-2">
            </div>
            <div class="col-12">
                <button type="submit" class="btn btn-primary">ثبت</button>
                <button type="reset" class="btn btn-secondary">تنظیم مجدد</button>
                <a href="{{ route('users.index') }}" class="btn btn-danger">برگشت</a>
            </div>
        </div>
    </div>
    {!! Form::close() !!}

@endsection
