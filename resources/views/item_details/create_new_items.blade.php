@extends('layouts.master')
@section('title', trans('global.item_details'))
@section('content')
<form class="m-form m-form--fit m-form--label-align-right" method="POST" id="app" @submit.prevent="submitForm" action="{{route('item_details.store')}}">
        @csrf
    <div v-if="success" class="m-alert m-alert--icon m-alert--outline alert alert-success alert-dismissible fade show" role="alert">
        <div class="m-alert__icon">
            <i class="la la-check"></i>
        </div>
        <div class="m-alert__text">
            @{{message}}
        </div>
        <div class="m-alert__close">
            <button style="line-height: 3;" type="button" class="close" data-dismiss="alert" aria-label="Close">
            </button>
        </div>
    </div>
    <div class="m-portlet__body" >

        <div class="row py-1">
            <div :class="['col', errors.{{$name}} ? 'has-danger' : '']">
                <label for="">نام جنس</label>
                <input type="text" v-model="{{$name}}" class="form-control">
                <div v-if="error" class="form-control-feedback">@{{errors.{!! $name !!}}}</div>

            </div>
            <div :class="['col', errors.main_category_id ? 'has-danger' : ''] ">
                <label for="exampleSelect1">{{trans('global.main_categories')}}</label>
                <div class="input-group">
                    <div class="input-group-prepend pr-2">
                        <button class="btn btn-success" v-on:click="populateModal('main_categories')" id="main_categories" type="button">
                            <i class="fa fa-plus"></i>
                        </button>
                    </div>
                    <select class="form-control m-input" v-model="main_category_id" @change="getRelatedCategories"  id="main_category_id" >
                        <option v-for="item in main_categories" :value="item.id" >@{{item.name_dr}} --- @{{item.id}}</option>
                    </select>
                </div>
                <div v-if="error" class="form-control-feedback">@{{errors.main_category_id}}</div>

            </div>
          </div>
        <div class="row py-1">
            <div :class="['col', errors.sub_category_id ? 'has-danger' : ''] ">
                <label for="exampleSelect1">{{ trans('global.sub_categories')}}</label>
                <div class="input-group">
                    <div class="input-group-prepend pr-2">
                        <button class="btn btn-success" v-on:click="populateModal('sub_categories')" id="sub_categories" type="button">
                            <i class="fa fa-plus"></i>
                        </button>
                    </div>
                    <select class="form-control m-input " v-model="sub_category_id" @change="getRelatedCategories" id="sub_category_id">
                        <option value="">{{ trans('global.sub_categories')}}</option>
                        <option v-for="item in sub_categories" :value="item.id" >@{{item.name_dr}} --- @{{item.id}}</option>
                    </select>
                </div>
                <div v-if="error" class="form-control-feedback">@{{errors.sub_category_id}}</div>

            </div>
          <div :class="['col', errors.end_category_id ? 'has-danger' : ''] ">
                <label for="exampleSelect1">{{trans('global.end_categories')}}</label>
                <div class="input-group">
                    <div class="input-group-prepend pr-2">
                        <button class="btn btn-success" v-on:click="populateModal('end_categories')" id="end_categories" type="button">
                            <i class="fa fa-plus"></i>
                        </button>
                    </div>
                    <select class="form-control m-input" v-model="end_category_id" @change="getRelatedCategories" id="end_category_id">
                        <option v-for="item in end_categories" :value="item.id" >@{{item.name_dr}} --- @{{item.id}}</option>

                    </select>
                </div>
                <div v-if="error" class="form-control-feedback">@{{errors.end_category_id}}</div>
            </div>
        </div>
        <div class="row py-1">
            <div :class="['col', errors.vendor_id ? 'has-danger' : ''] ">
                <label for="exampleSelect1">{{trans('global.vendors')}}</label>
                <div class="input-group">
                    <div class="input-group-prepend pr-2">
                        <button class="btn btn-success" v-on:click="populateModal('vendors')" id="vendors" type="button">
                            <i class="fa fa-plus"></i>
                        </button>
                    </div>
                    <select class="form-control m-input " v-model="vendor_id" id="vendor_id">
                        <option v-for="item in vendors" :value="item.id" >@{{item.name_dr}} --- @{{item.id}}</option>
                    </select>
                </div>
                <div v-if="error" class="form-control-feedback">@{{errors.vendor_id}}</div>

            </div>

            <div :class="['col', errors.unit_id ? 'has-danger' : ''] ">
                <label for="unit_id">{{trans('global.units')}}</label>
                <div class="input-group">
                    <div class ="input-group-prepend pr-2">
                        <button class="btn btn-success" v-on:click="populateModal('units')" id="units" type="button">
                            <i class="fa fa-plus"></i>
                        </button>
                    </div>

                    <select class="form-control m-input  " v-model="unit_id" id="unit_id">
                        <option v-for="item in units" :value="item.id" >@{{item.name_dr}} --- @{{item.id}}</option>
                    </select>
                </div>
                <div v-if="error" class="form-control-feedback">@{{errors.unit_id}}</div>

            </div>
        </div>
        <div class="row py-1">
            <div class="col">
                <div class="form-group">
                    <label for="exampleSelect1">{{trans('global.description')}}</label>
                    <textarea v-model="description" class="form-control"></textarea>
                </div>

            </div>
        </div>
    </div>
    <div class="m-portlet__foot m-portlet__foot--fit">
        <div class="m-form__actions">
            <button type="submit" class="btn btn-primary">{{trans('global.submit')}}</button>
            <button type="reset" class="btn btn-danger" @click="getCategories()">{{trans('global.cancel')}}</button>
        </div>
    </div>

</form>

<div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">

            </div>
        </div>
    </div>

@endsection
@push('custom-js')
    <script>
        axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        var app = new Vue({
            el: '#app',
            data: {
                // dependent dropdowns
                {{$name}}: '',
                description: '',
                main_category_id: '',
                sub_category_id: '',
                end_category_id: '',
                vendor_id: '',
                unit_id: '',

                // categories
                main_categories:[],
                sub_categories:[],
                end_categories:[],
                vendors:[],
                units:[],

                // main form validation
                error: false,
                success: false,
                errors: []
            },
            methods: {
                submitForm: function() {
                    axios.post("{{route('item_details.store')}}", {
                        '{{$name}}': this.{{$name}},
                        'description': this.description,
                        'main_category_id': this.main_category_id,
                        'sub_category_id': this.sub_category_id,
                        'end_category_id': this.end_category_id,
                        'vendor_id': this.vendor_id,
                        'unit_id': this.unit_id,
                    })
                    .then(res => {
                        this.success =  true;
                        this.message =  res.data.success;

                        // reset all inputs
                        this.{{$name}} = '';
                        this.description = '';
                        this.main_category_id = '';
                        this.sub_category_id = '';
                        this.end_category_id = '';
                        this.vendor_id = '';
                        this.unit_id = '';
                    })
                    .catch(err => {
                        this.error = true;
                        this.errors = err.response.data.errors;
                    })
                },
                getCategories: function() {
                    axios.get("{{url('get_categories')}}")
                    .then(res => {
                            this.main_categories = res.data.main_categories;
                            this.sub_categories = res.data.sub_categories;
                            this.end_categories = res.data.end_categories;
                            this.vendors = res.data.vendors;
                            this.units = res.data.units;
                        })
                        .catch(err => {
                            console.log(err);
                        });
                },
                getRelatedCategories: function() {
                    axios.post("{{url('get_related_categories')}}", {
                        'main_category_id': this.main_category_id,
                        'sub_category_id': this.sub_category_id,
                        'end_category_id': this.end_category_id,
                    })
                    .then(res => {
                        if(res.data.main_categories != null) {
                            this.main_categories = res.data.main_categories;
                        }
                        if(res.data.sub_categories != null) {
                            this.sub_categories = res.data.sub_categories;
                        }
                        if(res.data.end_categories != null) {
                            this.end_categories = res.data.end_categories;
                        }


                        console.log(res.data)
                    })
                    .catch(err => {
                        console.log(err);
                    });
                },
                populateModal: function(type, event) {
                    axios.post("{{url('get_setting_modal_view')}}", {
                        "type": type,
                        "main_category_id": this.main_category_id,
                        "sub_category_id": this.sub_category_id,
                        "end_category_id": this.end_category_id
                    })
                    .then(res => {
                        $('.modal-content').html(res.data);
                        $('#exampleModal').modal('show');
                    })
                    .catch(err => {
                        console.log(err);
                    });
                },
            },
            beforeMount() {
                this.getCategories();
            }
        });

        $('#exampleModal').on('hide.bs.modal', function(event) {
            app.getCategories();
        })

    </script>

@endpush 
