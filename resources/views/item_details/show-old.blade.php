@extends('layouts.master')
@section('title',' نمایش')
@section('content')
<div class="m-wizard m-wizard--1 m-wizard--info" id="m_wizard">

    <!--begin: Message container -->
    <div class="m-portlet__padding-x">

        <!-- Here you can put a message or alert -->
    </div>

    <!--end: Message container -->

    <!--begin: Form Wizard Head -->
    <div class="m-wizard__head m-portlet__padding-x">

        <!--begin: Form Wizard Progress -->
        <div class="m-wizard__progress">
            <div class="progress">
                <div class="progress-bar" role="progressbar" style="width: calc(66.6% + 26px);" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
        </div>

        <!--end: Form Wizard Progress -->

        <!--begin: Form Wizard Nav -->
        <div class="m-wizard__nav">
            <div class="m-wizard__steps">
                <div class="m-wizard__step m-wizard__step--current" m-wizard-target="m_wizard_form_step_1">
                    <div class="m-wizard__step-info">
                        <div class="m-wizard__step-number">
                            <span>
                                <i class="fa fa-check text-light"></i>
                            </span>
                        </div>
                        <div class="m-wizard__step-line">
                            <span></span>
                        </div>
                        <div class="m-wizard__step-label">
                            ثبت معلومات جنس
                        </div>
                    </div>
                </div>
                <div class="m-wizard__step m-wizard__step--current" m-wizard-target="m_wizard_form_step_1">
                    <div class="m-wizard__step-info">
                        <div class="m-wizard__step-number">
                            <span>
                                <i class="fa fa-check text-light"></i>
                            </span>
                        </div>
                        <div class="m-wizard__step-line">
                            <span></span>
                        </div>
                        <div class="m-wizard__step-label">
                            ثبت نوعیت مشخصات جنس
                        </div>
                    </div>
                </div>
                <div class="m-wizard__step   " m-wizard-target="m_wizard_form_step_3">
                    <div class="m-wizard__step-info">
                        <a href="#" class="m-wizard__step-number">
                            <span class="bg-info"><span>3</span></span>
                        </a>
                        <div class="m-wizard__step-line">
                            <span></span>
                        </div>
                        <div class="m-wizard__step-label">
                            اضافه نمودن اجناس
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--end: Form Wizard Nav -->
    </div>
</div>
<div v-cloak class="wrapper" id="create_item_details_specifications" v-if="extra_specifications.distribution_type !=2" >
    <div >
        <div class="row border-bottom pt-1">
            <div class="col-md-6">
                <label for="">
                    <h4>
                        {{trans('global.name')}} :
                    </h4>
                </label>
                <div class="d-inline-block text-muted pl-2">
                    <h5>
                        {{$item_details->$name}}
                    </h5>
                </div>
            </div>
            <div class="col-md-6">
                <label for="">
                    <h4>
                        {{trans('global.main_category')}} :
                    </h4>
                </label>
                <div class="d-inline-block text-muted pl-2">
                    <h5>
                        {{$item_details->mainCategory->$name}}
                    </h5>
                </div>
            </div>
        </div>
        <div class="row border-bottom pt-1">

            <div class="col-md-6">
                <label for="">
                    <h4>
                        {{trans('global.sub_category')}} :
                    </h4>
                </label>
                <div class="d-inline-block text-muted pl-2">
                    <h5>
                        {{$item_details->subCategory->$name}}
                    </h5>
                </div>
            </div>
            <div class="col-md-6">
                <label for="">
                    <h4>
                        {{trans('global.end_category')}} :
                    </h4>
                </label>
                <div class="d-inline-block text-muted pl-2">
                    <h5>
                        {{$item_details->endCategory->$name}}
                    </h5>
                </div>
            </div>
        </div>
        <div class="row border-bottom pt-1">

            <div class="col-md-6">
                <label for="">
                    <h4>
                        {{trans('global.unit')}} :
                    </h4>
                </label>
                <div class="d-inline-block text-muted pl-2">
                    <h5>
                        {{$item_details->unit->$name}}
                    </h5>
                </div>
            </div>
            <div class="col-md-6">
                <label for="">
                    <h4>
                        {{trans('global.vendor')}} :
                    </h4>
                </label>
                <div class="d-inline-block text-muted pl-2">
                    <h5>
                        {{$item_details->vendor->$name}}
                    </h5>
                </div>
            </div>
        </div>
        <div class="row border-bottom pt-1">
            <div class="col-md-6">
                <label for="">
                    <h4>
                        {{trans('global.description')}} :
                    </h4>
                </label>
                <div class="d-inline-block text-muted pl-2">
                    <h5>
                        {{$item_details->description}}
                    </h5>
                </div>
            </div>
        </div>
        <div class="row search_panel">
            <div class="col-md-6">
                <input type="text" class="form-control" v-model="search_text" placeholder="جستجو">
            </div>
            <div class="col-md-4">
                <select class="form-control" v-model="search_type">
                    <option value="0" selected disabled>انتخاب مشخصه</option>
                    <option v-if="extra_specifications.col1" value="col1" >@{{extra_specifications.col1}}</option>
                    <option v-if="extra_specifications.col2" value="col2">@{{extra_specifications.col2}}</option>
                    <option v-if="extra_specifications.col3" value="col3">@{{extra_specifications.col3}}</option>
                    <option v-if="extra_specifications.col4" value="col4">@{{extra_specifications.col4}}</option>
                </select>
            </div>
            <div class="col-md-2">
                <button type="button" class="btn btn-light btn-shadow font-weight-bold mr-2" v-bind:value="1" @click="searchItem()">جستجو <i class="fa fa-search"></i> </button>
                {{-- <a href="#" class="btn btn-light btn-shadow font-weight-bold mr-2">Light</a> --}}

            </div>
        </div>
        <div class="row" >
            <div class="col-md-12">
                
                <table class="table table-bordered m-2" >
                    <thead>
                        <tr>
                            <th width="100">انتخاب
                                <label class="m-checkbox" style="display:inline;margin:1.5em;">
                                    <input type="checkbox" class="checkbox" @change="checkAll($event.target.checked,{{$item_details->id}})">
                                    <span></span>
                                </label>
                            </th>
                            <th>شماره</th>
                            {{-- <th>سریال نمبر</th> --}}
                            <th> قیمت فی واحد</th>
                            <th>واحد پولی</th>
                            <th>قیمت مجموعی</th>
                            <th>حالت جنس</th>
                            <th v-if="extra_specifications.col1" >@{{extra_specifications.col1}}</th>
                            <th v-if="extra_specifications.col2" >@{{extra_specifications.col2}}</th>
                            <th v-if="extra_specifications.col3" >@{{extra_specifications.col3}}</th>
                            <th v-if="extra_specifications.col4" >@{{extra_specifications.col4}}</th>
                            <th>تعداد</th>

                            <th>اضافه شده از طریق</th>
                            <th>عملیات</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(item, index) in items " v-if="(index >= pageSettings.sIndex && index <= pageSettings.eIndex) && extra_specifications.distribution_type=='0' ">
                            <td>
                                <div class="m-checkbox-list" v-if="item.status == 0 ">
                                    <label class="m-checkbox" style="display:inline;margin:1.5em;"  v-if="item.meem7_id == null">
                                    <input type="checkbox"   :id="index"  class="checkbox" @change="getItemDetailsSpecifications($event.target.checked,index,item.id)">
                                        <span></span>
                                    </label>
                                </div>
                            </td>
                            <td>@{{ index + 1 }}</td>
                            {{-- <td>@{{item.serial_number}}</td> --}}
                            <td>@{{item.price}}</td>
                            <td>
                                <span v-if="item.currency == 0">افغانی</span>
                                <span v-else-if="item.currency == 1">دالر</span>
                            </td>
                            <td>
                                @{{item.price}}
                            </td>
                            <td>
                                <span v-if="item.condition == 0">جدید</span>
                                <span v-else-if="item.condition == 1">مستعمل</span>
                                <span v-else-if="item.condition == 2">داغمه</span>
                            </td>
                            <td v-if="extra_specifications.col1">@{{item.col1}}</td>
                            <td v-if="extra_specifications.col2">@{{item.col2}}</td>
                            <td v-if="extra_specifications.col3">@{{item.col3}}</td>
                            <td v-if="extra_specifications.col4">@{{item.col4}}</td>
                            <td>1</td>
                            {{-- <td v-if="extra_specifications.distribution_type=='1'">@{{item.item_amount}}</td> --}}
                            <td>
                                <span v-if="item.meem7_id == null">دیپو</span>
                                <span v-else>م۷</span>
                            </td>
                            <td class="" >
                                <a href="#" @click.prevent="editItemDetailsSpecifications(item.id)"  class="text-info p-2">
                                    <i class="fa fa-edit"></i>
                                </a>
                                @can('item_details_specifications_delete')
                                <a v-if="item.status == 0" href="#" @click.prevent="deleteItemDetailsSpecifications(item.id)" class="text-danger">
                                    <i  class="fa fa-trash"></i>
                                </a>
                                @endcan
                                <a href="#"  v-if="item.status != 0"data-toggle="modal" data-target="#allotment_history" title="تاریخچه جمع و قید جنس" @click="showItemAllotmentHistory(item.id)">
                                    <i  class="fa fa-eye"></i>
                                </a>
                                <a href="#"  v-if="item.status == 1" data-toggle="modal" data-target="#allotment_history" title="برگشت به دیپو" @click="editItemAllotment(item.id)">
                                    <i  class="fa fa-undo"></i>
                                </a>
                                {{-- <a href="#"  v-else data-toggle="modal" data-target="#allotment_history" title="اعاده جنس" @click="getItemDetailsSpecifications($event.target.checked,{{$index}},{{$item->item_details_specifications_id}})>
                                    <i  class="la la-cart-plus"></i>
                                </a> --}}
                            </td>
                        </tr>
                        <tr v-for="(item, index) in collective_items " v-if="(index >= pageSettings.sIndex && index <= pageSettings.eIndex)">
                            <td>
                                <div class="m-checkbox-list" v-if="item.item_details_specifications.status != 1 ">
                                    <label class="m-checkbox" style="display:inline;margin:1.5em;"  v-if="item.item_details_specifications.meem7_id == null && item.type == 1">
                                    <input type="checkbox"   :id="index"  class="checkbox" @change="getItemDetailsSpecifications($event.target.checked,index,item.item_details_specifications.id)">
                                        <span></span>
                                    </label>
                                </div>
                            </td>
                            <td>@{{ index + 1 }}</td>
                            {{-- <td>@{{item.serial_number}}</td> --}}
                            <td>@{{item.item_details_specifications.price}}</td>
                            <td>
                                <span v-if="item.item_details_specifications.currency == 0">افغانی</span>
                                <span v-else-if="item.item_details_specifications.currency == 1">دالر</span>
                            </td>
                            <td v-if="item.type == '0'">
                                @{{item.item_details_specifications.price * item.item_amount}}
                            </td>
                            <td v-else>@{{item.item_details_specifications.price * item.item_details_specifications.item_amount}}</td>
                            <td>
                                <span v-if="item.item_details_specifications.condition == 0">جدید</span>
                                <span v-else-if="item.item_details_specifications.condition == 1">مستعمل</span>
                                <span v-else-if="item.item_details_specifications.condition == 2">داغمه</span>
                            </td>
                            <td v-if="extra_specifications.col1">@{{item.item_details_specifications.col1}}</td>
                            <td v-if="extra_specifications.col2">@{{item.item_details_specifications.col2}}</td>
                            <td v-if="extra_specifications.col3">@{{item.item_details_specifications.col3}}</td>
                            <td v-if="extra_specifications.col4">@{{item.item_details_specifications.col4}}</td>
                            <td v-if="item.type == '0'">@{{item.item_amount}}</td>
                            <td v-else>@{{item.item_details_specifications.item_amount}}</td>
                            {{-- <td v-if="extra_specifications.distribution_type=='1'">@{{item.item_amount}}</td> --}}
                            <td>
                                <span v-if="item.item_details_specifications.meem7_id == null">دیپو</span>
                                <span v-else>م۷</span>
                            </td>
                            <td class="" >
                                <a href="#" @click.prevent="editItemDetailsSpecifications(item.item_details_specifications.id)"  class="text-info p-2">
                                    <i class="fa fa-edit"></i>
                                </a>
                                @can('item_details_specifications_delete')
                                <a v-if="item.item_details_specifications.status == 0 && item.type !=0" href="#" @click.prevent="deleteItemDetailsSpecifications(item.item_details_specifications.id)" class="text-danger">
                                    <i  class="fa fa-trash"></i>
                                </a>
                                @endcan
                                <a href="#"  v-if="item.type == 0"data-toggle="modal" data-target="#allotment_history" title="تاریخچه جمع و قید جنس" @click="showItemAllotmentHistory(item.item_details_specifications.id,item.id)">
                                    <i  class="fa fa-eye"></i>
                                </a>
                                <a href="#"  v-if="item.type == 0" data-toggle="modal" data-target="#allotment_history" title="تصحیح توزیع" @click="editItemAllotment(item.item_details_specifications.id,item.id)">
                                    <i  class="fa fa-undo"></i>
                                </a>
                                {{-- <a href="#"  v-else data-toggle="modal" data-target="#allotment_history" title="اعاده جنس" @click="getItemDetailsSpecifications($event.target.checked,{{$index}},{{$item->item_details_specifications_id}})>
                                    <i  class="la la-cart-plus"></i>
                                </a> --}}
                            </td>
                        </tr>
                    </tbody>
                </table>
               <div class="pagination_content">
                <pagination :data-list="items" 
					:rows-per-page="pageSettings.rowsPerPage" 
					:current-page="pageSettings.currentPage"
					v-on:update-page="onPageChanged"></pagination>

               </div>
            </div>
            
        </div>
        <div class="row">
            <div class="col-md-2 p-1">
                <div class="input-group mb-3"  v-if="!edit">
                        <input type="number" v-model="total_forms" value="" class="form-control">
                    <div class="input-group-append">
                        <button class="btn btn-success " @click="addNewForm">
                            <i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
            </div>
            <div class="col-md-10">
                <button v-if="specifications.length > 1" class="btn btn-success pull-left" @click="storeItemDetailsSpecifications()">
                    اضافه نمودن همه
                </button>

               
                        
                    <a href="#" data-toggle="modal" v-if="item_details_specifications.length>0" @click="clearModel()" data-target="#allotment_history" title="توزیع جنس به اساس کتاب" class="btn m-btn--square  btn-success ull-left"> <i class="la la-rotate-left"></i> توزیع جنس به اساس کتاب</a>
                        
                
            </div>
        </div>
        <div class="table-responsive-lg">
        <table class="table">
            <tr v-for="(item, index) in specifications">
                <th class="" style="line-height:3;font-weight:bold">
                   <span class=""> @{{++index}}</span>
                </th>
                <th class="px-1">
                    <div class="input-group">
                        <input type="number" v-model="item.price" class="form-control" placeholder=" {{trans('global.price')}}">
                        <div v-if="index == 1 && !edit" class="input-group-append">
                            {{-- <span class="input-group-text">*</span> --}}
                            <span class="input-group-text">
                                <label class="m-checkbox m-checkbox--single m-checkbox--state m-checkbox--state-primary">
                                    <input type="checkbox" @change="setMasterValue('price', item.price, $event)">
                                    <span></span>
                                </label>

                            </span>
                        </div>
                    </div>
                </th>
                <th class="px-1" width="150">
                    <div class="form-group">
                            <div class="input-group">
                                <select v-model="item.currency" class="form-control"  >
                                    <option value="">{{trans('global.currency')}}</option>
                                    <option value="0">افغانی</option>
                                    <option value="1">دالر</option>
                                </select>
                                <div v-if="index == 1 && !edit" class="input-group-append">
                                    {{-- <span class="input-group-text">*</span> --}}
                                    <span class="input-group-text">
                                        <label class="m-checkbox m-checkbox--single m-checkbox--state m-checkbox--state-primary">
                                            <input type="checkbox" @change="setMasterValue('currency', item.currency, $event)">
                                            <span></span>
                                        </label>

                                    </span>
                                </div>
                            </div>
                    </div>
                </th>

                <th class="px-1" width="150">
                    <div class="form-group">
                            <div class="input-group">
                                <select v-model="item.condition" class="form-control"  >
                                    <option value="">{{trans('global.condition')}}</option>
                                    <option value="0">جدید</option>
                                    <option value="1">مستعمل</option>
                                    <option value="2">داغمه</option>
                                </select>
                                <div v-if="index == 1 && !edit" class="input-group-append">
                                    {{-- <span class="input-group-text">*</span> --}}
                                    <span class="input-group-text">
                                        <label class="m-checkbox m-checkbox--single m-checkbox--state m-checkbox--state-primary">
                                            <input type="checkbox" @change="setMasterValue('condition', item.condition, $event)">
                                            <span></span>
                                        </label>

                                    </span>
                                </div>
                            </div>
                    </div>
                </th>
                <th class="px-1">
                    <div v-if="extra_specifications.col1" class="form-group">
                            <div class="input-group">
                                <input type="text" v-model="item.col1" class="form-control"  :placeholder="extra_specifications.col1">
                                <div v-if="index == 1 && !edit" class="input-group-append">
                                    {{-- <span class="input-group-text">*</span> --}}
                                    <span class="input-group-text">
                                        <label class="m-checkbox m-checkbox--single m-checkbox--state m-checkbox--state-primary">
                                            <input type="checkbox" @change="setMasterValue('col1', item.col1, $event)">
                                            <span></span>
                                        </label>

                                    </span>
                                </div>
                            </div>
                    </div>
                </th>
                <th class="px-1">
                    <div  v-if="extra_specifications.col2" class="form-group">
                        <div class="input-group">
                            <input type="text" v-model="item.col2" class="form-control"  :placeholder="extra_specifications.col2">
                            <div v-if="index == 1 && !edit" class="input-group-append">
                                {{-- <span class="input-group-text">*</span> --}}
                                <span class="input-group-text">
                                    <label class="m-checkbox m-checkbox--single m-checkbox--state m-checkbox--state-primary">
                                        <input type="checkbox" @change="setMasterValue('col2', item.col2, $event)">
                                        <span></span>
                                    </label>

                                </span>
                            </div>
                        </div>
                    </div>
                </th>
                <th class="px-1">
                    <div  v-if="extra_specifications.col3" class="form-group">
                            <div class="input-group">
                                <input type="text" v-model="item.col3" class="form-control"  :placeholder="extra_specifications.col3">
                                <div v-if="index == 1 && !edit" class="input-group-append">
                                    {{-- <span class="input-group-text">*</span> --}}
                                    <span class="input-group-text">
                                        <label class="m-checkbox m-checkbox--single m-checkbox--state m-checkbox--state-primary">
                                            <input type="checkbox" @change="setMasterValue('col3', item.col3, $event)">
                                            <span></span>
                                        </label>

                                    </span>
                                </div>
                            </div>
                    </div>
                </th>
                <th class="px-1">
                    <div  v-if="extra_specifications.col4" class="form-group">
                            <div class="input-group">
                                <input type="text" v-model="item.col4" class="form-control"  :placeholder="extra_specifications.col4">
                                <div v-if="index == 1 && !edit" class="input-group-append">
                                    {{-- <span class="input-group-text">*</span> --}}
                                    <span class="input-group-text">
                                        <label class="m-checkbox m-checkbox--single m-checkbox--state m-checkbox--state-primary">
                                            <input type="checkbox" @change="setMasterValue('col4', item.col4, $event)">
                                            <span></span>
                                        </label>

                                    </span>
                                </div>
                            </div>
                    </div>
                </th>
                <th class="px-1">
                    <div  v-if="extra_specifications.distribution_type =='1'" class="form-group">
                            <div class="input-group">
                                <input type="text" v-model="item.item_amount" class="form-control"  placeholder="تعداد">
                                <div v-if="index == 1 && !edit" class="input-group-append">
                                    {{-- <span class="input-group-text">*</span> --}}
                                    <span class="input-group-text">
                                        <label class="m-checkbox m-checkbox--single m-checkbox--state m-checkbox--state-primary">
                                            <input type="checkbox" @change="setMasterValue('distribution_type', item.distribution_type, $event)">
                                            <span></span>
                                        </label>

                                    </span>
                                </div>
                            </div>
                    </div>
                </th>
                <th class="" style="line-height:3" width="75">
                        <div v-if="!edit" class="btn-group">
                            <button class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" @click="storeItemDetailsSpecifications(index-1)" title="View">
                                    <i class="la la-plus"></i>
                            </button>
                            <button @click="removeForm(index)" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="View">
                                    <i class="la la-trash"></i>
                            </button>
                        </div>
                        <div v-else>
                            <button @click="updateItemDetailsSpecifications()" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="View">
                                    <i class="la la-edit"></i>
                            </button>
                        </div>
                </th>
            </tr>
        </table>
        </div>
    </div>


     <!-- Modal -->
     <div class="modal fade bd-example-modal-lg" id="allotment_history" tabindex="-1"   role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">تاریخچه جمع و قید جنس</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{-- Start of Alloted to new Employees --}}
                    <span class="p-5 text-danger" v-if="error">لطفآ تسلیم گیرنده گان را انتخاب نمائید</span>
                    <div  v-if="allotment_history.length > 0" class="m-scrollable" data-scrollable="true" data-height="380" data-mobile-height="300">

                        <!--Begin::Timeline 2 -->
                        <div  class="m-timeline-2">
                            <div class="m-timeline-2__items  m--padding-top-25 m--padding-bottom-30">
                                    <div v-for="(item, index) in allotment_history" class="m-timeline-2__item">
                                        <span class="m-timeline-2__item-time m--font-primary">@{{item.allotment_date}}</span>
                                        <div class="m-timeline-2__item-cricle">
                                            <i :class="[ item.status == 0 ? 'fa fa-genderless m--font-warning' : item.status == 2 ? 'fa fa-home m--font-danger icon-sm' :'fa fa-genderless m--font-success']"></i>
                                        </div>
                                        <div class="m-timeline-2__item-text  m--padding-top-5">
                                            <div class="font-weight-bold">
                                                @{{item.employee.name_dr}}
                                            </div>
                                            <div class="">
                                                @{{item.employee.current_position_dr}}
                                            </div>
                                            <div v-if="item.condition == 0" class="m--font-success">
                                                جدید
                                            </div>
                                            <div v-else-if="item.condition == 1" class="m--font-warning">
                                                مستعمل
                                            </div>
                                            <div v-else-if="item.condition == 2" class="m--font-danger">
                                                داغمه
                                            </div>
                                                <div v-if="item.status == 0" class="font-weight-bold m--font-info">
                                                    تاریخ اعاده:‌@{{item.return_date}}
                                                </div>

                                        </div>
                                    </div>
                            </div>
                        </div>
                        
                    </div>
                    {{-- End of Alloted to new Employees --}}

                    {{-- Start of Edit Allotment --}}
                    <div v-else-if="active_allotment.length > 0">
                        <div>
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>شماره</th>
                                        <th>جنس</th>
                                        <th>توضیحات</th>
                                        <th>حالت جنس </th>
                                        <th>{{trans('global.fece9_date')}}</th>
                                        <th>شخص تسلیم گیرنده</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for=" (item,index) in active_allotment">
                                        <td>@{{++index}}</td>
                                        <td>@{{item.item_details_specifications.item_details.name_dr}}</td>
                                        <td>
                                            <div class="ditails" style="display:none">
                                            <span class="m--font-primary" v-if="item.item_details_specifications.sub_category_keys.col1">
                                                    @{{item.item_details_specifications.sub_category_keys.col1}} :
                                            </span>
                                            <span class="m--font-info">
                                                @{{item.item_details_specifications.col1}} ||
                                            </span>
                                            <span class="m--font-primary" v-if="item.item_details_specifications.sub_category_keys.col2">
                                                    @{{item.item_details_specifications.sub_category_keys.col2}} :
                                            </span>
                                            <span class="m--font-info">
                                                @{{item.item_details_specifications.col2}} ||
                                            </span>
                                            <span class="m--font-primary" v-if="item.item_details_specifications.sub_category_keys.col3">
                                                    @{{item.item_details_specifications.sub_category_keys.col3}} :
                                            </span>
                                            <span class="m--font-info">
                                                @{{item.item_details_specifications.col3}} ||
                                            </span>
                                            <span class="m--font-primary" v-if="item.item_details_specifications.sub_category_keys.col4">
                                                    @{{item.item_details_specifications.sub_category_keys.col4}} :
                                            </span>
                                            <span class="m--font-info">
                                                @{{item.item_details_specifications.col4}}
                                            </span>
                                            </div>
                                            <a href="javascript:;" class="btn btn-xs green toggle_description">
                                                نمایش <i class="fa fa-plus"></i>
                                            </a>
                                        </td>
                                        <td>@{{item.allotment_date}}</td>

                                        <td width="150">

                                            <span v-if="item.condition==1">جدید</span>
                                            <span v-else-if="item.condition==0">داغمه</span>
                                            <span v-else-if="item.condition==2">مستعمل</span>
                                        </td>    
                                           
                                        <td>
                                            @{{item.employee.name_dr}} @{{item.employee.last_name}} -- ولد --  @{{item.employee.father_name_dr}}
                                        </td>

                                    </tr>
                                </tbody>
                            </table>
                            <div class="form-group row ">
                                <div class="form-group col-md-6 m-form__group p-4 ">
                                    <button class="btn btn-success save-button" @click="roleBackAllotement(active_allotment[0].id,active_allotment[0].item_details_specifications.id)"> انتقال به دیپو <i class="fa fa-archive"></i></button>
                                </div>
                                {{-- <div class="form-group col-md-6 m-form__group p-4 ">
                                    <button class="btn btn-primary save-button" @click="submitAllotements()"> توزیع به شخص دیگر  <i class="fa fa-share-alt"></i></button>
                                </div> --}}
                            </div>
                        
                        </div>
                        
                    </div> 
                    
                    {{-- Start of Edit Allotment --}}

                    <div v-else-if="item_details_specifications.length>0" class="m-portlet__body">
                        <div>
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>شماره</th>
                                        <th>جنس</th>
                                        <th>توضیحات</th>
                                        <th>
                                            تعداد
                                        </th>
                                        <th>حالت جنس
                                            <label class="m-checkbox m-checkbox--single m-checkbox--state m-checkbox--state-primary">
                                                <input type="checkbox" @change="selectStates($event.target.checked)">
                                                <span></span>
                                            </label>
                                        </th>
                                        <th>{{trans('global.fece9_date')}} 
                                            <label class="m-checkbox m-checkbox--single m-checkbox--state m-checkbox--state-primary">
                                                <input type="checkbox" @change="selectDates($event.target.checked)">
                                                <span></span>
                                            </label>
                                        </th>
                                        <th>دیپارتمنت</th>
                                        <th>شخص تسلیم گیرنده

                                            <label class="m-checkbox m-checkbox--single m-checkbox--state m-checkbox--state-primary">
                                                <input type="checkbox" @change="selectDates($event.target.checked)">
                                                <span></span>
                                            </label>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for=" (item,index) in item_details_specifications">
                                        <td>@{{++index}}</td>
                                        <td>@{{item.item_details.name_dr}}</td>
                                        <td>
                                            <div class="ditails" style="display:none">
                                            <span class="m--font-primary" v-if="item.sub_category_keys.col1">
                                                    @{{item.sub_category_keys.col1}} :
                                            </span>
                                            <span class="m--font-info">
                                                @{{item.col1}} ||
                                            </span>
                                            <span class="m--font-primary" v-if="item.sub_category_keys.col2">
                                                    @{{item.sub_category_keys.col2}} :
                                            </span>
                                            <span class="m--font-info">
                                                @{{item.col2}} ||
                                            </span>
                                            <span class="m--font-primary" v-if="item.sub_category_keys.col3">
                                                    @{{item.sub_category_keys.col3}} :
                                            </span>
                                            <span class="m--font-info">
                                                @{{item.col3}} ||
                                            </span>
                                            <span class="m--font-primary" v-if="item.sub_category_keys.col4">
                                                    @{{item.sub_category_keys.col4}} :
                                            </span>
                                            <span class="m--font-info">
                                                @{{item.col4}}
                                            </span>
                                            <span class="m--font-primary" v-if="item.sub_category_keys.distribution_type=='1'">
                                               تعداد موجود || 
                                            </span>
                                            <span class="m--font-info">
                                                @{{item.item_amount}}
                                            </span>
                                          
                                            </div>
                                            <a href="javascript:;" class="btn btn-xs green toggle_description">
                                                نمایش <i class="fa fa-plus"></i>
                                            </a>
                                        </td>
                                        <td width="150" v-if="item.sub_category_keys.distribution_type=='1'">
                                            <div class="input-group date">
                                                <input type="number"  v-model="select_all_amount" v-on:keyup="validate($event.target.value,item.item_amount)" name="select_all_amount" :id="'select_all_amount'+(index-1)"   :ref="'select_all_amount'+(index-1)" value="{{old('select_all_amount')}}" min="1" :max="item.item_amount" :class="['form-control', allotment_error == true ? 'is-invalid' : '']" placeholder="تعداد توزیع">
                                            </div>
                                        </td>
                                        <td width="150" else>
                                            <div class="input-group date">
                                                <input type="number" disabled  v-model="select_all_amount" v-on:keyup="validate($event.target.value,item.item_amount)" name="select_all_amount" :id="'select_all_amount'+(index-1)"   :ref="'select_all_amount'+(index-1)" value="{{old('select_all_amount')}}" min="1" :max="item.item_amount" :class="['form-control', allotment_error == true ? 'is-invalid' : '']" placeholder="تعداد توزیع">
                                            </div>
                                        </td>
                                        <td width="100">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <select class="form-control"  :id="'condition'+(index-1)" :ref="'condition'+(index-1)">
                                                        
                                                        <option value="0"  :selected="select_all_state==0">جدید</option>
                                                        <option value="1" :selected="select_all_state==1">مستعمل</option>
                                                        <option value="2" :selected="select_all_state==2">داغمه</option>
                                                    </select>
                                                    <!---->
                                                    {{-- @{{ item.condition==0 ? "selected" : ""}}
                                                    @{{ item.condition==1 ? "selected" : ""}}
                                                    @{{ item.condition==2 ? "selected" : ""}} --}}
                                                </div>
                                            </div>
                                        </td>    
                                        <td  width="120">
                                            <div class="input-group date">
                                                <input type="text"  v-model="select_all_date" name="date" :id="'date'+(index-1)"  onfocus='$(".persian_date").persianDatepicker();' :ref="'date'+(index-1)" value="{{old('date')}}" class="form-control persian_date" placeholder="تاریخ انتخاب کنید.">
                                            </div>
                                                        
                                        </td>
                                        <td width="250">
                                            <v-select
                                                
                                                label="name_dr" 
                                                :options="departments"
                                                item-text=""
                                                dir="rtl"
                                                v-model="department_id"
                                                :reduce="departments=> departments.id"
                                                @input="getEmployeeByDep($event)"
                                                                                    
                                            >
                                           
                                            </v-select>
                                        </td>    
                                        <td width="400">
                                            {{-- <select class="form-control select2"  @change="addItemDetailsSpecificationsToReturnList(index-1,item.id,$event.target.value)">
                                                    <option selected disabled>{{trans('global.receiver_employee')}}</option>
                                                    <option v-for="item in employees" :value="item.id" >@{{item.name_dr}}</option>

                                            </select> --}}
                                            <v-select
                                                
                                                label="name_dr" 
                                                :options="employees"
                                                item-text=""
                                                dir="rtl"
                                                v-model="receiver_id"
                                                :reduce="employees=> employees.id"
                                                @search="onSearch"
                                                @input="addItemDetailsSpecificationsToReturnList(index-1,item.id)"
                                                                                    
                                            >
                                            <template slot="no-options">
                                                لطفآ اسم را بنویسید        
                                            </template>
                                            <template slot="option" slot-scope="option">
                                                    @{{  option.name_dr }}

                                                    (    
                                                        @{{option.last_name}}
                                                    )
                                                    ولد -- 
                                                    @{{option.father_name_dr}}
                                                    
                                                    وظیفه -- 
                                                    @{{option.current_position_dr}}
                                                    
                                     
                                            </template>
                                            </v-select>
                                        </td>    

                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    
                        <div class="form-group row ">
                            <div class="form-group col-md-6 m-form__group ">
                                <button class="btn btn-success save-button" :disabled="allotment_error" @click="submitAllotements()"> {{trans('global.submit')}} <i class="la la-rotate-left"></i></button>
                            </div>
                        </div>
                    
                </div>
                
                    

                </div>
                {{-- <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div> --}}
            </div>
        </div>
    </div>
    <div class="m-form__actions"  v-if="extra_specifications.distribution_type !=2">
        <a href="{{route('item_details.create')}}" class="btn btn-primary">{{trans('global.item_details_create')}}</a>
        <a href="{{route('item_details.index')}}" class="btn btn-primary">{{trans('global.item_details_index')}}</a>
    </div>
</div>


@endsection
@push('custom-css')
<style>
.m-widget1 .m-widget1__item .m-widget1__number {

    color:#5b5b5d !important;
}
label.m-checkbox {
    display: inline;
    margin: 1.5em;
}
.m-invoice-2 .m-invoice__wrapper .m-invoice__body table tbody tr:first-child td {
    padding-top: 10px;
    padding-bottom: 10px;
}
.m-timeline-2:before {
    right: 5.89rem;
}
.m-timeline-2 .m-timeline-2__items .m-timeline-2__item .m-timeline-2__item-cricle {
    right: 5.1rem;
}
@media (min-width: 992px){
.modal-lg {
    max-width: 90%;
}
}
.modal .modal-content .modal-body {
    padding:0;
}
.m-timeline-2 .m-timeline-2__items .m-timeline-2__item .m-timeline-2__item-text{
    padding-right: 6rem;
}

.pagination {
  display: flex;
}
.pagination button {
  width: 2rem;
  height: 2rem;
  margin: 1rem 0;
  color: #5867dd;
  background-color: white;
  border: 1px solid #5867dd;
  cursor: pointer;
  display: inline-flex;
  align-items: center;
  justify-content: center;
}
.pagination button.pg-number {
  border-radius: 0;
}
.pagination button.pg-number:disabled {
  color: white;
  background-color: #5867dd;
}
.pagination button.pg-prev, .pagination button.pg-next {
  border-radius: 0;
}
.pagination button:first-child {
  width: 2.5rem;
  border-radius: 0 100px 100px 0;
}
.pagination button:last-child {
  width: 2.5rem;
  
  border-radius: 100px 0 0 100px;

}
.pagination button.pg-first:disabled, .pagination button.pg-last:disabled, .pagination button.pg-prev:disabled, .pagination button.pg-next:disabled {
  cursor: not-allowed;
}
.pagination button + button {
  border-left: 0;
}
.pagination {
    padding-right: 40%;
}
.search_panel{
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        padding: 1.4em;
        border: 1px solid #EBEDF3;
        border-radius: 0.42rem;
        background: #36a3f7;
}
button.pg-number,button.pg-next,button.pg-last,button.pg-prev,button.pg-first :hover{
    cursor: pointer;
}
.btn.btn-light.active, .btn.btn-light.focus, .btn.btn-light:focus, .btn.btn-light:hover:not(:disabled) {
    color: #36a3f7 !important;
}
.btn.btn-light{
    color: #36a3f7 !important;
}
[v-cloak] { display: none; }
.icon-sm{
    font-size: 1.2rem !important;
    right: -0.7rem !important;
}

</style>
@endpush
@push('custom-js')
<script>

        Vue.component('v-select', VueSelect.VueSelect);
                
        Vue.component('pagination', {
            template: "<div class=\"pagination\"><button class=\"pg-first\" v-bind:disabled=\"currentPage == 1\" v-bind:value=\"1\" v-on:click=\"PageOnClick\"><<</button><button class=\"pg-prev\" v-bind:disabled=\"currentPage == 1\" v-bind:value=\"currentPage - 1\" v-on:click=\"PageOnClick\"><</button><template v-for=\"page in showPage\"><button class=\"pg-number\" v-bind:value=\"page\" v-bind:disabled=\"currentPage == page\" v-on:click=\"PageOnClick\">@{{page}}</button>  </template><button class=\"pg-next\" v-bind:disabled=\"currentPage == totalPages\" v-bind:value=\"currentPage + 1\" v-on:click=\"PageOnClick\">></button><button class=\"pg-last\" v-bind:disabled=\"currentPage == totalPages\" v-bind:value=\"totalPages\" v-on:click=\"PageOnClick\">>></button></div>",
            props:['dataList', 'rowsPerPage', 'currentPage'],
            data: function () {
                return {
                    source: [],
                    showPage: [] 
                };
            },
            mounted(){
                this.source = this.dataList;
            },
            watch:{
                dataList: function (newVal, oldVal) {
                    this.ComputeShowPage(this.totalPages);
                    this.source = newVal;
                }
            },
            methods: {
                PageOnClick: function (evt) {
                    var page = Number(evt.target.getAttribute('value')),
                        totalPages = this.totalPages;
                    
                    this.$emit('update-page', page);
                    
                    if (totalPages <= 5) return;
                    var sPage, ePage;
                    if (page == totalPages) { sPage = page-4; ePage = totalPages; }
                    else if (page == 1 || page - 2 < 1) { sPage = 1; ePage = 5;}
                    else if (page + 2 > totalPages) { sPage = page - 3; ePage = totalPages; }
                    else { sPage = page - 2; ePage = page + 2; }

                    this.showPage = this.GenerateShowPageArray(sPage, ePage).slice();
                },
                GenerateShowPageArray: function (sPage, ePage){
                    var ary = [];
                    for (var i = sPage; i <= ePage; i++){
                        ary.push(i);
                    }
                    return ary;
                },
                ComputeShowPage: function (totalPages) {
                    var count = 1,
                        showPage = [];
                    while (count <= totalPages && count <= 5) {
                        showPage.push(count);
                        count++;
                    }
                    return showPage;
                }
            }, 
            computed: {
                totalRows: function () {
                    return this.source.length || 0;
                },
                totalPages: function () {
                    var total = Math.ceil(this.source.length / this.rowsPerPage);
                    this.showPage = this.ComputeShowPage(total);
                    return total;
                }
            }
        })
            
        item_app = new Vue({
            el: '#create_item_details_specifications',
            data: {
                // available items
                items: '',
                collective_items:'',


                // values for creating new items
                sub_category_id: {{$item_details->sub_category_id}},
                sub_categories_key_id: "{{isset($item_details->subCategory->subCategoryKeys) ? $item_details->subCategory->subCategoryKeys->id : ""}}",
                item_details_id: {{$item_details->id}},

                //variable for editing
                edit:false,
                edit_specifications:{},

                // validation
                error: false,
                success: false,
                allotment_error: false,

                // bulk assignment values
                master: {
                    price: '',
                    currency: '',
                    condition: '',
                    col1: '',
                    col2: '',
                    col3: '',
                    col4: '',
                    item_amount: '',
                },

                // new items specifications
                specifications: [

                ],

                // variables for extra specifications rendering
                extra_specifications: '',

                // form
                total_forms: 1,

                // allotment_history
                allotment_history: [],
                department_id:null,
                item_details_specifications:[],
                employees:[],
                departments:[],
                allotment_list:[],
                receiver_id:null,
                select_all_date:'',
                select_all_state:'',
                select_all_amount:1,
                
                active_allotment:[],
                //Pagination
               
                showStudent: 1,
                pageSettings: {
                    currentPage: 1,
                    rowsPerPage: 20,
                    sIndex: 0, 
                    eIndex: 0  
                },
                // search
                search_text:'',
                search_type:0,
                search_items:'',

 
             
            },
// Strat pagination functions
            watch: {
                        'pageSettings.currentPage': {
                            immediate: true,
                            handler: function (){
                                this.refreshDisplayIndex();
                            }
                        },
                        showStudent: function (){
                            this.pageSettings.currentPage = 1;
                        }
                    },
// End Pagination functions
            methods: {
                // Start pagination function
                refreshDisplayIndex: function (){
				this.pageSettings.sIndex = (this.pageSettings.currentPage - 1) * this.pageSettings.rowsPerPage;
				this.pageSettings.eIndex = this.pageSettings.currentPage * this.pageSettings.rowsPerPage - 1;
                    },
                    onPageChanged: function (page){
                        this.pageSettings.currentPage = page;
                    },
                    toggleTabs: function (){
                        this.showStudent = !this.showStudent;
                    },
                  
                // End Pagination functions



                storeItemDetailsSpecifications(index='') {
                    axios.post("{{route('item_details_specifications.store')}}", {
                        
                        'specifications': index !== '' ? this.specifications[index] : this.specifications
                    })
                    .then(res => {
                        if(index !== '') {
                            this.removeForm(index);
                        }
                        else {
                            this.specifications = [];
                        }
                        this.getAvailableItems();

                    })
                    .catch(err => {
                        console.log(err);
                    });
                },
                deleteItemDetailsSpecifications(id) {
                    axios.delete("{{url('item_details_specifications')}}/"+id)
                    .then(res => {
                        this.getAvailableItems();

                    })
                    .catch(err => {

                    });
                },
                editItemDetailsSpecifications(id) {
                    this.edit=true;
                    axios.get("{{url('item_details_specifications')}}/"+id+"/edit")
                    .then(res => {
                        if(this.specifications.length == 0) {
                            this.specifications.push(res.data);
                        }
                    })
                    .catch(err => {

                    });
                },
                updateItemDetailsSpecifications() {
                    id = this.specifications[0].id;
                    axios.put("{{url('item_details_specifications')}}/"+id, {
                        'specifications': this.specifications[0]
                    })
                    .then(res => {
                        this.edit = false;
                        this.specifications = [];
                        this.getAvailableItems();
                    })
                    .catch(err => {
                        console.log(err);
                    })
                },
                getAvailableItems() {
                    axios.get("{{url('item_details_specifications')}}/"+this.item_details_id)
                        .then(res => {
                            this.items = res.data.item_details;
                            this.search_items = res.data.item_details;
                            this.collective_items = res.data.item_balance;
                        })
                        .catch(err => {

                        })
                },
                getDepartments() {
                    axios.get("{{url('get_departments')}}")
                        .then(res => {
                            this.departments = res.data;
                        })
                        .catch(err => {

                        })
                },
                setMasterValue(type, value, event) {

                    this.master[type] = event.target.checked ? value : '';

                    for(i = 1 ; i < this.specifications.length ; i++) {
                        this.specifications[i][type] = this.master[type];
                    }

                },
                addNewForm() {
                    for(i = 0 ; i < this.total_forms; i++) {
                        this.specifications.push(
                            {
                                // serial_number: '',
                                price: this.master.price ? this.master.price : '',
                                currency: this.master.currency ? this.master.currency : '',
                                sub_categories_key_id: this.sub_categories_key_id,
                                item_details_id: this.item_details_id,
                                col1: this.master.col1 ? this.master.col1 : '',
                                col2: this.master.col2 ? this.master.col2 : '',
                                col3: this.master.col3 ? this.master.col3 : '',
                                col4: this.master.col4 ? this.master.col4 : '',
                                distribution_type: this.master.distribution_type ? this.master.distribution_type : '',
                                status: 0,
                                condition: this.master.condition ? this.master.condition : '',
                            }
                        );
                    }
                    this.getSpecificationsKeys();

                },
                removeForm(index) {
                    this.specifications.splice(index, 1);

                },
                getSpecificationsKeys() {

                    axios.get("{{url('get_sub_categories_keys')}}/"+this.sub_category_id)

                    .then(res => {
                        this.extra_specifications = res.data;
                    })
                    .catch(err => {
                        console.log(err);

                    })
                },
                showItemAllotmentHistory(item_details_specifications_id,item_balances_id=null) {

                    axios.get("{{url('allotments/get_item_details_specifications_allotment_history')}}/"+item_details_specifications_id+'/'+item_balances_id)
                        .then(res => {
                            this.allotment_history = res.data;
                            this.active_allotment = [];
                            // console.log(this.allotment_history);
                        })
                        .catch(err => {
                            console.log(err);
                        });

                },
                clearModel(){
                    this.active_allotment = [];
                    this.allotment_history = [];
                },
                editItemAllotment(item_details_specifications_id,item_balances_id=null) {
                    axios.get("{{url('allotments/get_item_details_specifications_allotment_active')}}/"+item_details_specifications_id+'/'+item_balances_id)
                        .then(res => {
                            this.active_allotment = res.data;
                            this.allotment_history = [];
                           
                        })
                        .catch(err => {
                            console.log(err);
                        });

                },
                roleBackAllotement(allotment_id,item_details_specifications_id) {
                    axios.get("{{url('allotments/role_back_allotment')}}/"+allotment_id+"/"+item_details_specifications_id)
                        .then(res => {
                            $('#allotment_history').modal('hide');
                            this.getAvailableItems();
                            this.active_allotment = [];
                        })
                        .catch(err => {
                            console.log(err);
                        });

                },



                getItemDetailsSpecifications(evt,index,item_details_specifications_id) {
                    //if check
                    
                    if(evt){
                    axios.get("{{url('allotments/get_item_details_specifications')}}/"+item_details_specifications_id)
                        .then(res => {
                            this.allotment_history=[];
                            this.item_details_specifications.push(res.data);
                        })
                        .catch(err => {
                            console.log(err);
                        });
                    }else{
                        
                        var rem = this.item_details_specifications.filter(item=>{return item.id==item_details_specifications_id})[0];

                        remove_id = this.item_details_specifications.indexOf(rem);
                        this.item_details_specifications.splice(remove_id,1);
                    }

                },
                checkAll(event,item_details_id){
                    if(event){
                        $('input.checkbox:not(input.checkbox:disabled)').prop('checked', true);
                        axios.get("{{url('get_item_details_specifications_axios')}}/"+item_details_id)
                            .then(res => {
                                this.allotment_history=[];
                               
                                // this.item_details_specifications.push(res.data);
                                for($i=0;$i<res.data.length;$i++){
                                 this.item_details_specifications.push(res.data[$i]);
                                }
                            })
                            .catch(err => {
                                console.log(err);
                            });
                    }else{
                        $('input.checkbox:not(input.checkbox:disabled)').prop('checked', false);
                        this.item_details_specifications = [];
                    }
                },
                getEmployeeByDep(department_id) {
                    axios.get("{{url('get_employees_by_dep')}}/"+department_id)
                        .then(res => {
                            this.employees = res.data;
                        })
                        .catch(err => {
                            console.log(err);

                        })

                },
                
                addItemDetailsSpecificationsToReturnList(index,item_details_specifications_id) {
                    date='date'+index;
                    condition='condition'+index;
                    select_all_amount='select_all_amount'+index;
                    var object = {};
                    object['item_details_specifications_id'] = item_details_specifications_id;
                    object['receiver_employee_id'] = this.receiver_id;
                    object['status'] = 1;
                    object['condition'] = this.$refs[condition][0].value;
                    object['allotment_date'] = this.$refs[date][0].value;
                    object['total_alloted'] = this.$refs[select_all_amount][0].value;
                    this.allotment_list[index] = object;
                    this.allotment_list_string=JSON.stringify(this.allotment_list[index] );
                },
                selectDates(event){
                    
                    if(event){
                        this.select_all_date = this.$refs.date0[0].value;
                    }else{
                        this.select_all_date = '';
                    }
                },
                selectStates(event){
                    
                    if(event){
                        this.select_all_state = this.$refs.condition0[0].value;
                    }else{
                        this.select_all_state = '';
                    }
                },
                validate(alloted_item,item_amount){
                    if(alloted_item > item_amount){
                        this.allotment_error = true;
                    }
                    else{
                        this.allotment_error = false;
                    }
                },
                submitAllotements(){
                    
                    if(this.item_details_specifications.length == this.allotment_list.length)
                    {
                        axios.post("{{route('allotments')}}",{
                                    'allotment_list' : this.allotment_list
                                    }
                                )           
                            .then(res => {
                                // console.log(res.data)
                                location.reload();
                            })
                            .catch(err => {
                                console.log(err);
                            });
                    }else{
                         this.error = true;
                    }
                    $('.save-button').disable();        
                },
                onSearch(search, loading) {
                    loading(true);
                    this.search(loading, search, this,this.department_id);
                    },
                search: _.debounce((loading, search, vm,department_id) => {

                    axios.post(`{{route("get_employee_axios")}}`,{
                                'name' : search,
                                'department_id' : department_id
                                })           
                        .then(res => {
                            
                            // res.json().then(json => (vm.employees = json.items));  
                            vm.employees = res.data.items;
                            // this.employees = res.data.items;                           
                            loading(false);
                        })
                        .catch(err => {
                            console.log(err);
                        });
                        
                    }, 350),

                    searchItem(){
                        this.getAllItemsForSearch();
                        search_by = this.search_type;
                        filter_data = this.search_items.filter(item => {
                            if(item[search_by]) {
                                return item[search_by].toLowerCase().indexOf(this.search_text.toLowerCase()) > -1
                            }
                    });

                    this.items = filter_data;
                   
                    },
                    getAllItemsForSearch(){
                        axios.get("{{url('item_details_specifications')}}/"+this.item_details_id)
                        .then(res => {
                            this.search_items = res.data;
                        })
                        .catch(err => {

                        })

                    },
                
            },
            mounted() {
                this.getAvailableItems();
                this.getDepartments();
                this.getSpecificationsKeys();
            },
        });




    </script>

@endpush

