<table class="table table-striped- table-bordered table-hover" id="m_table_1">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">{{trans('global.name')}}</th>
        {{-- <th scope="col">{{trans('global.serial_number')}}</th> --}}
        {{-- <th scope="col">{{trans('global.price')}}</th> --}}
        {{-- <th scope="col">{{trans('global.currency')}}</th> --}}
        <th scope="col">{{trans('global.main_categories')}}</th>
        <th scope="col">{{trans('global.sub_categories')}}</th>
        <th scope="col">{{trans('global.end_categories')}}</th>
        <th scope="col">{{trans('global.vendors')}}</th>
        <th scope="col">{{trans('global.units')}}</th>
        <th scope="col">{{trans('global.total_remaining_items')}}</th>
        <th scope="col">{{trans('global.total_alloted_items')}}</th>
        <th scope="col">{{trans('global.description')}}</th>
        <th scope="col">{{trans('global.actions')}}</th>
      </tr>
    </thead>
    <tbody>

        @foreach ($item_details as $key => $item)
            <tr id="{{$item->id}}_tr">
                <th scope="row">{{$item_details->firstItem()+$key}}</th>
                <td>{{$item->$name}}</td>
                {{-- <td>{{$item->serial_number}}</td> --}}
                {{-- <td>{{$item->price}}</td> --}}
                {{-- <td>{{$item->currency =='0' ? 'افغانی' :'دالر'}}</td> --}}
                <td>{{$item->mainCategory->$name}}</td>
                <td>{{$item->subCategory->$name}}</td>
                <td>{{$item->endCategory->$name}}</td>
                <td>{{$item->vendor->$name}}</td>
                <td>{{$item->unit->$name}}</td>
                @php
                    $total = getTotal($item->id);
                @endphp
                <td>{{$total['remaining']}}</td>
                <td>{{$total['alloted']}}</td>
                <td>{{str_limit(strip_tags($item->description), 150, ' ...')}}</td>
                <td>
                    {{-- <a class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" href="{{ route('main_categories.show',$item->id) }}"><i class="la la-eye"></i></a> --}}
                    <a href="{{route('item_details.show', $item->id)}}" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill ">
                        <i class="fa fa-eye"></i>
                    </a>
                    @can('item_details_edit')
                        <a class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" href="{{ route('item_details.edit',$item->id) }}"><i class="la la-edit"></i></a>
                    @endcan

                    @can('item_details_delete')
                        <a href="javascript:void(0)" onclick="deleteRecord('{{route('item_details.destroy', $item->id)}}','{{$item->id}}_tr');" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill ">
                            <i class="fa fa-trash"></i>
                        </a>
                    @endcan
                </td>
            </tr>
        @endforeach
    </tbody>
  </table>
  {!!$item_details->links()!!}
 
