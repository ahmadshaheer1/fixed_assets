@extends('layouts.master')
@section('title', trans('global.item_details'))
@section('content')
<form class="m-form m-form--fit m-form--label-align-right" method="POST" id="app" @submit.prevent="submitForm" action="{{route('item_details.store')}}">
        @csrf
    <div v-if="success" class="m-alert m-alert--icon m-alert--outline alert alert-success alert-dismissible fade show" role="alert">
        <div class="m-alert__icon">
            <i class="la la-check"></i>
        </div>
        <div class="m-alert__text">
            @{{message}}
        </div>
        <div class="m-alert__close">
            <button style="line-height: 3;" type="button" class="close" data-dismiss="alert" aria-label="Close">
            </button>
        </div>
    </div>
    <div class="m-portlet__body" >
        <div class="m-wizard m-wizard--1 m-wizard--info" id="m_wizard">

            <!--begin: Message container -->
            <div class="m-portlet__padding-x">

                <!-- Here you can put a message or alert -->
            </div>

            <!--end: Message container -->

            <!--begin: Form Wizard Head -->
            <div class="m-wizard__head m-portlet__padding-x">

                <!--begin: Form Wizard Progress -->
                <div class="m-wizard__progress">
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width: calc(0% + 26px);" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>

                <!--end: Form Wizard Progress -->

                <!--begin: Form Wizard Nav -->
                <div class="m-wizard__nav">
                    <div class="m-wizard__steps">
                        <div class="m-wizard__step m-wizard__step--current" m-wizard-target="m_wizard_form_step_1">
                            <div class="m-wizard__step-info">
                                <a href="#" class="m-wizard__step-number">
                                    <span class="bg-info"><span>1</span></span>
                                </a>
                                <div class="m-wizard__step-line">
                                    <span></span>
                                </div>
                                <div class="m-wizard__step-label">
                                    ثبت معلومات جنس
                                </div>
                            </div>
                        </div>
                        <div class="m-wizard__step" m-wizard-target="m_wizard_form_step_2">
                            <div class="m-wizard__step-info">
                                <div class="m-wizard__step-number">
                                    <span><span>2</span></span>
                                </div>
                                <div class="m-wizard__step-line">
                                    <span></span>
                                </div>
                                <div class="m-wizard__step-label">
                                    ثبت نوعیت مشخصات جنس
                                </div>
                            </div>
                        </div>
                        <div class="m-wizard__step" m-wizard-target="m_wizard_form_step_3">
                            <div class="m-wizard__step-info">
                                <div class="m-wizard__step-number">
                                    <span><span>3</span></span>
                                </div>
                                <div class="m-wizard__step-line">
                                    <span></span>
                                </div>
                                <div class="m-wizard__step-label">
                                    اضافه نمودن اجناس
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--end: Form Wizard Nav -->
            </div>
        </div>

        <div class="row py-1">

            <div :class="['col', errors.main_category_id ? 'has-danger' : ''] ">
                <label for="exampleSelect1">{{trans('global.main_categories')}}</label>
                <div class="input-group">
                    <div class="input-group-prepend pr-2">
                        <button class="btn btn-success" v-on:click="populateModal('main_categories')" id="main_categories" type="button">
                            <i class="fa fa-plus"></i>
                        </button>
                    </div>
                

                    <v-select
                    label="name_dr" 
                    :options="main_categories"
                    dir="rtl"
                    v-model="main_category_id"
                    :reduce="main_categories=> main_categories.id"
                    @input="getRelatedCategories();itemOperations('main_categories',$event)"
                    >

                        <template slot="no-options">
                            {{ trans('global.main_categories')}}        
                        </template>
                        <template slot="option" slot-scope="option">
                                @{{  option.name_dr }}
                            
                                (@{{option.nature == 0 ? 'غیر مصرفی' : 'مصرفی'}})
                        </template>
                    </v-select>
                    <div class="input-group-prepend pl-2" v-if="operations['main_categories']" >
                        @can('category_edit')
                        <a class="text-info pt-3 mx-2" href="#" @click="editModal('main_categories',operations['main_categories'])">
                            <i class="fa fa-edit"></i>
                        </a>
                        @endcan
                        @can('category_delete')
                        <a class="text-danger pt-3" href="#" @click="deleteItem('main_categories',operations['main_categories'])">
                            <i class="fa fa-trash"></i>
                        </a>
                        @endcan

                    </div>

                </div>
                <div v-if="error" class="form-control-feedback">@{{errors.main_category_id}}</div>

            </div>
            <div :class="['col', errors.sub_category_id ? 'has-danger' : ''] ">
                <label for="exampleSelect1">{{ trans('global.sub_categories')}}</label>
                <div class="input-group">
                    <div class="input-group-prepend pr-2">
                        <button class="btn btn-success" v-on:click="populateModal('sub_categories')" id="sub_categories" type="button">
                            <i class="fa fa-plus"></i>
                        </button>
                    </div>
          
  
                    <v-select
                    label="name_dr" 
                    :options="sub_categories"
                    dir="rtl"
                    v-model="sub_category_id"
                    :reduce="sub_categories=> sub_categories.id"
                    @input="getRelatedCategories();itemOperations('sub_categories',$event)"
                    >
                    
                    <template slot="no-options">
                            {{ trans('global.sub_categories')}}        
                        </template>
                        <template slot="option" slot-scope="option">
                                @{{  option.name_dr }} (@{{(option?.sub_category_keys?.distribution_type) =='0' ? 'انفرادی' : (option?.sub_category_keys?.distribution_type) =='1' ? 'جمعی' :  (option?.sub_category_keys?.distribution_type) =='2' ? 'به اساس قرارداد' : '---'}})
                        </template>
                    </v-select>
                    <div class="input-group-prtransepend " style="line-height: 3.5" v-if="operations['sub_categories']">
                        @can('category_edit')
                        <a class="text-info pt-3 mx-2" @click="editModal('sub_categories',operations['sub_categories'])" href="#">
                            <i class="fa fa-edit"></i>
                        </a>
                        @endcan
                        @can('category_delete')
                        <a class="text-danger pt-3" href="#" @click="deleteItem('sub_categories',operations['sub_categories'])">
                            <i class="fa fa-trash"></i>
                        </a>
                        @endcan
                    </div>
  
                </div>
                <div v-if="error" class="form-control-feedback">@{{errors.sub_category_id}}</div>
  
            </div>
          </div>
        <div class="row py-1">
          <div :class="['col', errors.end_category_id ? 'has-danger' : ''] ">
                <label for="exampleSelect1">{{trans('global.end_categories')}}</label>
                <div class="input-group">
                    <div class="input-group-prepend pr-2">
                        <button class="btn btn-success" v-on:click="populateModal('end_categories')" id="end_categories" type="button">
                            <i class="fa fa-plus"></i>
                        </button>
                    </div>
              
                    <v-select
                    label="name_dr" 
                    :options="end_categories"
                    dir="rtl"
                    v-model="end_category_id"
                    :reduce="end_categories=> end_categories.id"
                    @input="getRelatedCategories();itemOperations('end_categories',$event)"
                    >
                    <template slot="no-options">
                            {{ trans('global.end_categories')}}        
                        </template>
                        <template slot="option" slot-scope="option">
                                @{{  option.name_dr }}
                        </template>
                    </v-select>
                    <div class="input-group-prepend pl-2" v-if="operations['end_categories']">
                        @can('category_edit')
                            <a class="text-info pt-3 mx-2" href="#" @click="editModal('end_categories',operations['end_categories'])">
                                <i class="fa fa-edit"></i>
                            </a>
                        @endcan
                        @can('category_delete')    
                            <a class="text-danger pt-3" href="#" @click="deleteItem('end_categories',operations['end_categories'])">
                            <i class="fa fa-trash"></i>
                        @endcan    
                        </a>
                    </div>
                </div>
                <div v-if="error" class="form-control-feedback">@{{errors.end_category_id}}</div>
            </div>
            <div :class="['col', errors.vendor_id ? 'has-danger' : ''] ">
                <label for="exampleSelect1">{{trans('global.vendors')}}</label>
                <div class="input-group">
                    <div class="input-group-prepend pr-2">
                        <button class="btn btn-success" v-on:click="populateModal('vendors')" id="vendors" type="button">
                            <i class="fa fa-plus"></i>
                        </button>
                    </div>
                  
    
                    <v-select
                    label="name_dr" 
                    :options="vendors"
                    dir="rtl"
                    v-model="vendor_id"
                    :reduce="vendors=> vendors.id"
                    @input="getRelatedCategories();itemOperations('vendors',$event)"
                    >
                    <template slot="no-options">
                            {{ trans('global.vendors')}}        
                        </template>
                        <template slot="option" slot-scope="option">
                                @{{  option.name_dr }}
                        </template>
                    </v-select>
                    <div class="input-group-prepend pl-2" v-if="operations['vendors']">
                        @can('vendor_edit')
                        <a class="text-info pt-3 mx-2" href="#" @click="editModal('vendors',operations['vendors'])">
                            <i class="fa fa-edit"></i>
                        </a>
                        @endcan
                        @can('vendor_delete')
                        <a class="text-danger pt-3" href="#" @click="deleteItem('vendors',operations['vendors'])">
                            <i class="fa fa-trash"></i>
                        </a>
                        @endcan
                    </div>
    
                </div>
                <div v-if="error" class="form-control-feedback">@{{errors.vendor_id}}</div>
    
            </div>
        </div>
        <div class="row py-1">

            <div :class="['col', errors.unit_id ? 'has-danger' : ''] ">
                <label for="unit_id">{{trans('global.units')}}</label>
                <div class="input-group">
                    <div class ="input-group-prepend pr-2">
                        <button class="btn btn-success" v-on:click="populateModal('units')" id="units" type="button">
                            <i class="fa fa-plus"></i>
                        </button>
                    </div>

                    <v-select
                    label="name_dr" 
                    :options="units"
                    v-model="unit_id"
                    dir="rtl"
                    :reduce="units=> units.id"
                    @input="getRelatedCategories();itemOperations('units',$event)"
                    >
                    <template slot="no-options">
                            {{ trans('global.units')}}        
                        </template>
                        <template slot="option" slot-scope="option">
                                @{{  option.name_dr }}
                        </template>
                    </v-select>
                    <div class="input-group-prepend pl-2" v-if="operations['units']">
                        @can('unit_edit')
                        <a class="text-info pt-3 mx-2 " href="#" @click="editModal('units',operations['units'])">
                            <i class="fa fa-edit"></i>
                        </a>
                        @endcan
                        @can('unit_delete')
                        <a class="text-danger pt-3" href="#" @click="deleteItem('units',operations['units'])">
                            <i class="fa fa-trash"></i>
                        </a>
                        @endcan
                    </div>

                </div>
                <div v-if="error" class="form-control-feedback">@{{errors.unit_id}}</div>

            </div>

            <div :class="['col', errors.{{$name}} ? 'has-danger' : '']">
                <label for="">نام جنس</label>
                <input type="text" v-model="{{$name}}"   class="form-control">
               
                <div v-if="error" class="form-control-feedback">@{{errors.{!! $name !!}}}</div>

            </div>
        </div>
        <div class="row py-1">
            <div class="col">
                <div class="form-group">
                    <label for="exampleSelect1">{{trans('global.description')}}</label>
                    <textarea v-model="description" class="form-control"></textarea>
                </div>

            </div>
        </div>
    </div>
    <div class="m-portlet__foot m-portlet__foot--fit">
        <div class="m-form__actions">
            <button type="submit" class="btn btn-primary">
                {{trans('global.continue')}}
                <i class="la la-arrow-left"></i>
            </button>
            <button type="reset" class="btn btn-danger" @click="getCategories()">{{trans('global.cancel')}}</button>
            {{-- <a href="#" class="btn btn-info mx-3">
                {{trans('global.continue')}}
                <i class="la la-arrow-left"></i>
            </a> --}}
        </div>
    </div>

</form>

<div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">

            </div>
        </div>
    </div>

@endsection
@push('custom-css')
<style>
.v-select {
    width:80%;
}
</style>
@endpush
@push('custom-js')
    <script>
        Vue.component('v-select', VueSelect.VueSelect);
        axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        var app = new Vue({
            el: '#app',
            data: {
                // dependent dropdowns
                {{$name}}: '',
                description: '',
                main_category_id: '',
                sub_category_id: '',
                end_category_id: '',
                vendor_id: '',
                unit_id: '',

                // categories
                main_categories:[],
                sub_categories:[],
                end_categories:[],
                vendors:[],
                units:[],

                //show list
                show_data:[],
                // main form validation
                error: false,
                success: false,
                errors: [],

                // Items oprations  for displaying buttons after item is selected
                operations:{}


            },
            methods: {
                submitForm: function() {
                    axios.post("{{route('item_details.store')}}", {
                        '{{$name}}': this.{{$name}},
                        'description': this.description,
                        'main_category_id': this.main_category_id,
                        'sub_category_id': this.sub_category_id,
                        'end_category_id': this.end_category_id,
                        'vendor_id': this.vendor_id,
                        'unit_id': this.unit_id,
                    })
                    .then(res => {
                        this.success =  true;
                        this.message =  res.data.success;
                        if(res.data.sub_category_id) {
                            window.location.href = "{{url('sub_categories_keys/create/')}}/"+res.data.sub_category_id;
                        }
                        else {
     

                            window.location.href = "{{url('item_details/')}}/"+res.data.item_details_id;

                        }
                        console.log(res.data.sub_category_id);

                        // reset all inputs
                        this.{{$name}} = '';
                        this.description = '';
                        this.main_category_id = '';
                        this.sub_category_id = '';
                        this.end_category_id = '';
                        this.vendor_id = '';
                        this.unit_id = '';
                        this.errors = [];
                    })
                    .catch(err => {
                        this.error = true;
                        this.errors = err.response.data.errors;
                    })
                },
                getCategories: function() {
                    axios.get("{{url('get_categories')}}")
                    .then(res => {
                            this.main_categories = res.data.main_categories;
                            this.sub_categories = res.data.sub_categories;
                            this.end_categories = res.data.end_categories;
                            this.vendors = res.data.vendors;
                            this.units = res.data.units;
                        })
                        .catch(err => {
                            console.log(err);
                        });
                },
                getRelatedCategories: function() {
                    axios.post("{{url('get_related_categories')}}", {
                        'main_category_id': this.main_category_id,
                        'sub_category_id': this.sub_category_id,
                        'end_category_id': this.end_category_id,
                    })
                    .then(res => {
                        if(res.data.main_categories != null) {
                            this.main_categories = res.data.main_categories;
                            console.log(res.data.main_categories);
                        }
                        if(res.data.sub_categories != null) {
                            this.sub_categories = res.data.sub_categories;
                        }
                        if(res.data.end_categories != null) {
                            this.end_categories = res.data.end_categories;
                        }

                    })
                    .catch(err => {
                        console.log(err);
                    });
                },
                populateModal: function(type, event) {
                    axios.post("{{url('get_setting_modal_view')}}", {
                        "type": type,
                        "main_category_id": this.main_category_id,
                        "sub_category_id": this.sub_category_id,
                        "end_category_id": this.end_category_id
                    })
                    .then(res => {
                        $('.modal-content').html(res.data);
                        $('#exampleModal').modal('show');
                    })
                    .catch(err => {
                        console.log(err);
                    });
                },
                itemOperations:function(type,id){
                    this.operations[type]=id;
                    this.getCategories();
                },
                editModal:function(type,id){
                    route = "{{url('')}}/"+type+"/"+id+"/edit";
                    axios.get(route)
                    .then(res=>{
                        $('#exampleModal').modal('show');
                        $('.modal-content').html(res.data);

                    })
                    .catch(err=>{

                    });

                },
                deleteItem:function(type,id){
                    route = "{{url('')}}/"+type+"/"+id;
                    swal({
                        title: "ایا شما مطمین هستید ؟",
                        text: "شما قادر به دریافت این معلومات نمیباشد.",
                        type: "warning",
                        showCancelButton: !0,
                        confirmButtonText: "حذف",
                        cancelButtonText: "لغو !",
                        reverseButtons: !0
                    }).then(function(e) {
                        if(e.value)
                        {
                            axios.delete(route)
                                .then(res=>{
                                    console.log(res);
                                    app.getCategories();
                                })
                                .catch(err=>{
                                    console.log(err);

                                });
                        }
                    });



                },

            },
            beforeMount() {
                this.getCategories();
            }
        });

        $('#exampleModal').on('hide.bs.modal', function(event) {
            app.getCategories();
        })

    </script>

@endpush
