@extends('layouts.master')
@section('title', trans('global.edit_item_details'))
@section('content')
<form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{route('item_details.update',$item_details->id)}}">
    @csrf
    @method('PUT')

    <div class="m-portlet__body">
        <div class="row">
          <div class="col {{$errors->has($name) ? 'has-danger' : ''}}">
            <label for="">{{trans('global.name')}}</label>
            <input name="{{$name}}" value="{{$item_details->$name}}" type="text" class="form-control m-input m-input--square " id="main_category_name" aria-describedby="emailHelp" placeholder="{{trans('global.name')}}">
            @if($errors->has($name))
                <div class="form-control-feedback">{{$errors->first($name)}}</div>
            @endif
          </div>
          <div class="col {{$errors->has('main_category_id') ? 'has-danger' : ''}}">
              <label for="exampleSelect1">{{trans('global.main_categories')}}</label>
              <select class="form-control m-input  " name="main_category_id" id="main_category_id">
                  <option value="">{{trans('global.main_categories')}}</option>
                  @foreach ($main_categories as $value)
                    <option value="{{$value->id}}" {{$item_details->main_category_id == $value->id ? 'selected' :''}}>{{$value->$name}}</option>
                  @endforeach
              </select>
              @if($errors->has('main_category_id'))
                  <div class="form-control-feedback">{{$errors->first('main_category_id')}}</div>
              @endif
            </div>
          {{-- <div class="col {{$errors->has('currency') ? 'has-danger' : ''}}">
              <label for="exampleSelect1">{{trans('global.currency')}}</label>
              <select class="form-control m-input" name="currency">
                  <option value="">{{trans('global.currency')}}</option>
                    <option value="0" {{$item_details->currency =='0' ? 'selected' : '' }}>افغانی</option>
                    <option value="1" {{$item_details->currency =='1' ? 'selected' : '' }}>دالر</option>
              </select>
              @if($errors->has('currency'))
                  <div class="form-control-feedback">{{$errors->first('currency')}}</div>
              @endif
            </div> --}}
          
        </div>
        {{-- <div class="row">
          <div class="col">
            <label for="price">{{trans('global.price')}}</label>
            <input name="price" value="{{$item_details->price}}" type="text" class="form-control m-input m-input--square {{$errors->has('price') ? 'has-danger' : ''}}" id="main_category_name" aria-describedby="emailHelp" placeholder="{{trans('global.price')}}">
            @if($errors->has('price'))
                <div class="form-control-feedback">{{$errors->first('price')}}</div>
            @endif
          </div>

          <div class="col">
            <label for="serial_number">{{trans('global.serial_number')}}</label>
            <input name="serial_number" value="{{$item_details->serial_number}}" type="text" class="form-control m-input m-input--square {{$errors->has('serial_number') ? 'has-danger' : ''}}" id="main_category_name" aria-describedby="emailHelp" placeholder="{{trans('global.serial_number')}}">
            @if($errors->has('serial_number'))
                <div class="form-control-feedback">{{$errors->first('serial_number')}}</div>
            @endif
          </div>
            
          </div> --}}
        <div class="row">
            
            <div class="col {{$errors->has('sub_category_id') ? 'has-danger' : ''}}">
              <label for="exampleSelect1">{{ trans('global.sub_categories')}}</label>
              <select class="form-control m-input " name="sub_category_id" id="sub_category_id">
                <option value="">{{ trans('global.sub_categories')}}</option>
                @foreach ($sub_categories as $value)
                  <option value="{{$value->id}}" {{$item_details->sub_category_id == $value->id ? 'selected' :''}}>{{$value->$name}}</option>
                @endforeach
              </select>
              @if($errors->has('sub_category_id'))
                <div class="form-control-feedback">{{$errors->first('sub_category_id')}}</div>
              @endif
            </div>
            <div class="col {{$errors->has('end_category_id') ? 'has-danger' : ''}}">
                <label for="exampleSelect1">{{trans('global.end_categories')}}</label>
                <select class="form-control m-input" name="end_category_id" id="end_category_id">
                    <option value="">{{trans('global.end_categories')}}</option>
                    @foreach ($end_categories as $value)
                      <option value="{{$value->id}}" {{$item_details->end_category_id == $value->id ? 'selected' :''}}>{{$value->$name}}</option>
                    @endforeach
                </select>
                @if($errors->has('end_category_id'))
                    <div class="form-control-feedback">{{$errors->first('end_category_id')}}</div>
                @endif
              </div>
          </div>
        <div class="row">
          
          <div class="col {{$errors->has('vendor_id') ? 'has-danger' : ''}}">
            <label for="exampleSelect1">{{trans('global.vendors')}}</label>
            <select class="form-control m-input " name="vendor_id" id="vendor_id">
                <option value="">{{trans('global.vendors')}}</option>
                @foreach ($vendors as $value)
                  <option value="{{$value->id}}" {{$item_details->vendor_id == $value->id ? 'selected' :''}}>{{$value->$name}}</option>
                @endforeach
            </select>
            @if($errors->has('vendor_id'))
                <div class="form-control-feedback">{{$errors->first('vendor_id')}}</div>
            @endif
          </div>
          <div class="col {{$errors->has('unit_id') ? 'has-danger' : ''}}">
              <label for="exampleSelect1">{{trans('global.units')}}</label>
              <select class="form-control m-input  " name="unit_id" id="unit_id">
                  <option value="">{{trans('global.units')}}</option>
                  @foreach ($units as $value)
                    <option value="{{$value->id}}" {{$item_details->unit_id == $value->id ? 'selected' :''}}>{{$value->$name}}</option>
                  @endforeach
              </select>
              @if($errors->has('unit_id'))
                  <div class="form-control-feedback">{{$errors->first('unit_id')}}</div>
              @endif
            </div>
        </div>
        <div class="row" id="keys_response">
        </div>
        
      <div class="row">
        <div class="col">
          <label for="main_category_description">{{trans('global.description')}}</label>
          <textarea name="description" class="form-control" id="main_category_description"placeholder="{{trans('global.description')}}">{{old('description')}}</textarea>

        </div>
      </div>
    </div>
    <div class="m-portlet__foot m-portlet__foot--fit">
        <div class="m-form__actions">
            <button type="submit" class="btn btn-primary">{{trans('global.submit')}}</button>
            <button type="reset" class="btn btn-danger">{{trans('global.cancel')}}</button>
        </div>
    </div>
</form>

@endsection
@push('custom-js')
    <script>
        $('#sub_category_id').change(function() {
            sub_category_id = $(this).val();
            getSubCategoryKeys(sub_category_id);
        })

        $(function() {
            sub_category_id = $('#sub_category_id').val();
            getSubCategoryKeys(sub_category_id, {col1: "{{$item_details->col1}}", col2: "{{$item_details->col2}}" , col3: "{{$item_details->col3}}" , col4: "{{$item_details->col4}}",
            col5: "{{old('col5')}}", col6: "{{old('col6')}}",  col7: "{{old('col7')}}", col8: "{{old('col8')}}", col9: "{{old('col9')}}",
            col10: "{{old('col10')}}", col11: "{{old('col11')}}", col12: "{{old('col12')}}"
          });
        })

        function getSubCategoryKeys(sub_category_id, data) {
            console.log(data);
            $.ajax({
                method:'GET',
                url: '{{url("get_sub_categories_keys")}}/'+sub_category_id,
                data: {data},
                success: function(resp) {
                    $('#keys_response').html(resp);
                }
            })
        }
    </script>

@endpush
