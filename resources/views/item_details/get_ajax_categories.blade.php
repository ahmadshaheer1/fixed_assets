<select class="form-control m-input " name="{{$id}}" id="{{$id}}">
  <option value="">{{ trans('global.'.$option_name.'')}}</option>
  @foreach ($categories as $value)
    <option value="{{$value->id}}" {{old($id) == $value->id ? 'selected' : '' }}>{{$value->$name}}</option>
  @endforeach
</select>
