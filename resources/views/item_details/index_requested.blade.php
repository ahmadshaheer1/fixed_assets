@extends('layouts.master')
@section('title', trans('global.item_details_requested'))
@section('content')
<table class="table table-striped- table-bordered table-hover jquery-datatable" id="m_table_1">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">{{trans('global.user')}}</th>
        <th scope="col">{{trans('global.sub_category')}}</th>
        <th scope="col">{{trans('global.description')}}</th>
        <th scope="col">{{trans('global.actions')}}</th>
      </tr>
    </thead>
    <tbody>

        {{-- @foreach ($item_details as $key => $item) --}}
            {{-- <tr id="{{$item->id}}_tr">
                <th scope="row">{{++$key}}</th> --}}
                {{-- <td>{{$item->$name}}</td>
                <td>{{$item->subCategory->$name}}</td> 
                <td>{{str_limit(strip_tags($item->description), 150, ' ...')}}</td> --}}
                <tr>
                <th scope="row">1</th>
                <td>جمال</td>
                <td>کمپیوتر</td>
                <td>لطف نموده معلومات ذیل را درج سیستم نمائید</td>
                <td>
                    {{-- <a class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" href="{{ route('main_categories.show',$item->id) }}"><i class="la la-eye"></i></a> --}}
                  
                    <a class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" href="{{ route('requested_item_details',1) }}"><i class="la la-eye"></i></a>
                  
                </td>
            </tr>
        {{-- @endforeach --}}
    </tbody>
  </table>
@endsection

