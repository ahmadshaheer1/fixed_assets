@extends('layouts.master')
@section('title', trans('global.create_sub_categories_key'))
@section('content')
<div class="m-wizard m-wizard--1 m-wizard--info" id="m_wizard">

    <!--begin: Message container -->
    <div class="m-portlet__padding-x">

        <!-- Here you can put a message or alert -->
    </div>

    <!--end: Message container -->

    <!--begin: Form Wizard Head -->
    <div class="m-wizard__head m-portlet__padding-x">

        <!--begin: Form Wizard Progress -->
        <div class="m-wizard__progress">
            <div class="progress">
                <div class="progress-bar" role="progressbar" style="width: calc(33.3% + 26px);" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
        </div>

        <!--end: Form Wizard Progress -->

        <!--begin: Form Wizard Nav -->
        <div class="m-wizard__nav">
            <div class="m-wizard__steps ">
                <div class="m-wizard__step m-wizard__step--current" m-wizard-target="m_wizard_form_step_1">
                    <div class="m-wizard__step-info">
                        <div class="m-wizard__step-number">
                            <span>
                                <i class="fa fa-check text-light"></i>
                            </span>
                        </div>
                        <div class="m-wizard__step-line">
                            <span></span>
                        </div>
                        <div class="m-wizard__step-label"> 
                            ثبت معلومات جنس
                        </div>
                    </div>
                </div>
                <div class="m-wizard__step  m-wizard__step--current" m-wizard-target="m_wizard_form_step_2">
                    <div class="m-wizard__step-info">
                        <a href="#" class="m-wizard__step-number">
                            <span class="bg-info"><span>2</span></span>
                        </a>
                        <div class="m-wizard__step-line">
                            <span></span>
                        </div>
                        <div class="m-wizard__step-label">
                            ثبت نوعیت مشخصات جنس
                        </div>
                    </div>
                </div>
                <div class="m-wizard__step" m-wizard-target="m_wizard_form_step_3">
                    <div class="m-wizard__step-info">
                        <div class="m-wizard__step-number">
                            <span><span>3</span></span>
                        </div>
                        <div class="m-wizard__step-line">
                            <span></span>
                        </div>
                        <div class="m-wizard__step-label">
                            اضافه نمودن اجناس
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--end: Form Wizard Nav -->
    </div>
</div>
<form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{route('sub_categories_keys.store')}}">
    @csrf

    <div class="form-group m-form__group {{$errors->has('sub_category') ? 'has-danger' : ''}}">
        <label for="sub_category">{{trans('global.sub_category')}}</label>

        <select class="custom-select form-control" name="sub_category_id" required>

            <option value="" disabled>{{trans('global.sub_category')}}</option>
            @if($id != "")
                <option value="{{$id}}" {{ $test = $sub_categories->find($id) }} selected>{{$test->name_dr}}</option>
            @else
                @foreach ($sub_categories as $item)
                    <option value="{{$item->id}}" {{old('sub_category_id') == $item->id ? 'selected' : '' }}>{{$item->$name}}</option>
                @endforeach
            @endif



        </select>
        
        @if($errors->has('sub_category_id'))
            <div class="form-control-feedback">{{$errors->first('sub_category_id')}}</div>
        @endif
    </div>
    <div class="form-group m-form__group {{$errors->has('distribution_type') ? 'has-danger' : ''}}">
        <label for="">نوعیت توزیع</label>
        <select class="browser-default custom-select" v-model="distribution_type" name="distribution_type">
            <option value="0" selected>انفرادی</option>
            <option value="1">جمعی</option>
            <option value="2">به اساس قراداد</option>
        </select>
        @if($errors->has('distribution_type'))
            <div class="form-control-feedback">{{$errors->first('distribution_type')}}</div>
        @endif
    </div>
    <div class="form-group m-form__group {{$errors->has('item_type') ? 'has-danger' : ''}}">
        <label for="">نوعیت جنس</label>
        <select class="browser-default custom-select" v-model="item_type" name="item_type">
            <option value="0" selected>منقول</option>
            <option value="1">غیر منقول</option>
        </select>
        @if($errors->has('item_type'))
            <div class="form-control-feedback">{{$errors->first('item_type')}}</div>
        @endif
    </div>

    <div class="m-portlet__body">
        <div class="form-group m-form__group {{$errors->has('col1') ? 'has-danger' : ''}}" >
            <label for="sub_categories_key_name">{{trans('global.col1')}}</label>
            <input name="{{'col1'}}" value="{{old('col1')}}" type="text"  class="form-control m-input m-input--square"  aria-describedby="emailHelp" placeholder="{{trans('global.col1')}}">
            @if($errors->has('col1'))
                <div class="form-control-feedback">{{$errors->first('col1')}}</div>
            @endif
        </div>
        <div class="form-group m-form__group {{$errors->has('col2') ? 'has-danger' : ''}}">
            <label for="sub_categories_key_name">{{trans('global.col2')}}</label>
            <input name="{{'col2'}}" value="{{old('col2')}}" type="text" class="form-control m-input m-input--square"  aria-describedby="emailHelp" placeholder="{{trans('global.col2')}}">
            @if($errors->has('col2'))
                <div class="form-control-feedback">{{$errors->first('col2')}}</div>
            @endif
        </div>
        <div class="form-group m-form__group {{$errors->has('col3') ? 'has-danger' : ''}}">
            <label for="sub_categories_key_name">{{trans('global.col3')}}</label>
            <input name="{{'col3'}}" value="{{old('col3')}}" type="text" class="form-control m-input m-input--square"  aria-describedby="emailHelp" placeholder="{{trans('global.col3')}}">
            @if($errors->has('col3'))
                <div class="form-control-feedback">{{$errors->first('col3')}}</div>
            @endif
        </div>
        <div class="form-group m-form__group {{$errors->has('col4') ? 'has-danger' : ''}}">
            <label for="sub_categories_key_name">{{trans('global.col4')}}</label>
            <input name="{{'col4'}}" value="{{old('col4')}}" type="text" class="form-control m-input m-input--square"  aria-describedby="emailHelp" placeholder="{{trans('global.col4')}}">
            @if($errors->has('col4'))
                <div class="form-control-feedback">{{$errors->first('col4')}}</div>
            @endif
        </div>
        <div class="form-group m-form__group {{$errors->has('col5') ? 'has-danger' : ''}}">
            <label for="sub_categories_key_name">{{trans('global.col5')}}</label>
            <input name="{{'col5'}}" value="{{old('col5')}}" type="text" class="form-control m-input m-input--square"  aria-describedby="emailHelp" placeholder="{{trans('global.col5')}}">
            @if($errors->has('col5'))
                <div class="form-control-feedback">{{$errors->first('col5')}}</div>
            @endif
        </div>
        <div class="form-group m-form__group {{$errors->has('col6') ? 'has-danger' : ''}}">
            <label for="sub_categories_key_name">{{trans('global.col6')}}</label>
            <input name="{{'col6'}}" value="{{old('col6')}}" type="text" class="form-control m-input m-input--square"  aria-describedby="emailHelp" placeholder="{{trans('global.col6')}}">
            @if($errors->has('col6'))
                <div class="form-control-feedback">{{$errors->first('col6')}}</div>
            @endif
        </div>
        <div class="form-group m-form__group {{$errors->has('col7') ? 'has-danger' : ''}}">
            <label for="sub_categories_key_name">{{trans('global.col7')}}</label>
            <input name="{{'col7'}}" value="{{old('col7')}}" type="text" class="form-control m-input m-input--square"  aria-describedby="emailHelp" placeholder="{{trans('global.col7')}}">
            @if($errors->has('col7'))
                <div class="form-control-feedback">{{$errors->first('col7')}}</div>
            @endif
        </div>
        <div class="form-group m-form__group {{$errors->has('col8') ? 'has-danger' : ''}}">
            <label for="sub_categories_key_name">{{trans('global.col8')}}</label>
            <input name="{{'col8'}}" value="{{old('col8')}}" type="text" class="form-control m-input m-input--square"  aria-describedby="emailHelp" placeholder="{{trans('global.col8')}}">
            @if($errors->has('col8'))
                <div class="form-control-feedback">{{$errors->first('col8')}}</div>
            @endif
        </div>
        <div class="form-group m-form__group {{$errors->has('col9') ? 'has-danger' : ''}}">
            <label for="sub_categories_key_name">{{trans('global.col9')}}</label>
            <input name="{{'col9'}}" value="{{old('col9')}}" type="text" class="form-control m-input m-input--square"  aria-describedby="emailHelp" placeholder="{{trans('global.col9')}}">
            @if($errors->has('col9'))
                <div class="form-control-feedback">{{$errors->first('col9')}}</div>
            @endif
        </div>
        <div class="form-group m-form__group {{$errors->has('col10') ? 'has-danger' : ''}}">
            <label for="sub_categories_key_name">{{trans('global.col10')}}</label>
            <input name="{{'col10'}}" value="{{old('col10')}}" type="text" class="form-control m-input m-input--square"  aria-describedby="emailHelp" placeholder="{{trans('global.col10')}}">
            @if($errors->has('col10'))
                <div class="form-control-feedback">{{$errors->first('col10')}}</div>
            @endif
        </div>
        <div class="form-group m-form__group {{$errors->has('col11') ? 'has-danger' : ''}}">
            <label for="sub_categories_key_name">{{trans('global.col11')}}</label>
            <input name="{{'col11'}}" value="{{old('col11')}}" type="text" class="form-control m-input m-input--square"  aria-describedby="emailHelp" placeholder="{{trans('global.col11')}}">
            @if($errors->has('col11'))
                <div class="form-control-feedback">{{$errors->first('col11')}}</div>
            @endif
        </div>
        <div class="form-group m-form__group {{$errors->has('col12') ? 'has-danger' : ''}}">
            <label for="sub_categories_key_name">{{trans('global.col12')}}</label>
            <input name="{{'col12'}}" value="{{old('col12')}}" type="text" class="form-control m-input m-input--square"  aria-describedby="emailHelp" placeholder="{{trans('global.col12')}}">
            @if($errors->has('col12'))
                <div class="form-control-feedback">{{$errors->first('col12')}}</div>
            @endif
        </div>

    </div>
    <div class="m-portlet__foot m-portlet__foot--fit">
        <div class="m-form__actions">
            <button type="submit" class="btn btn-primary">{{trans('global.continue')}}</button>
            <button type="reset" class="btn btn-danger">{{trans('global.cancel')}}</button>
        </div>
    </div>
</form>
@endsection
