@extends('layouts.master')
@section('title', trans('global.sub_categories_keys'))
@section('content')
<table class="table table-striped- table-bordered table-hover table-checkable datatable jquery-datatable" id="m_table_1">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">{{trans('global.name')}}</th>
        <th scope="col">{{trans('global.col1')}}</th>
        <th scope="col">{{trans('global.col2')}}</th>
        <th scope="col">{{trans('global.col3')}}</th>
        <th scope="col">{{trans('global.col4')}}</th>
        <th scope="col">{{trans('global.col5')}}</th>
        <th scope="col">{{trans('global.col6')}}</th>
        <th scope="col">{{trans('global.col7')}}</th>
        <th scope="col">{{trans('global.col8')}}</th>
        <th scope="col">{{trans('global.col9')}}</th>
        <th scope="col">{{trans('global.col10')}}</th>
        <th scope="col">{{trans('global.col11')}}</th>
        <th scope="col">{{trans('global.col12')}}</th>
        <th scope="col">{{trans('global.actions')}}</th>
      </tr>
    </thead>
    <tbody>

        @foreach ($data as $key => $item)
            <tr id="{{$item->id}}_tr">
                <td scope="row">{{++$key}}</td>
                <td>{{$item->subCategory->name_dr}}</td>
                <td>{{$item->col1}}</td>
                <td>{{$item->col2}}</td>
                <td>{{$item->col3}}</td>
                <td>{{$item->col4}}</td>
                <td>{{$item->col5}}</td>
                <td>{{$item->col6}}</td>
                <td>{{$item->col7}}</td>
                <td>{{$item->col8}}</td>
                <td>{{$item->col9}}</td>
                <td>{{$item->col10}}</td>
                <td>{{$item->col11}}</td>
                <td>{{$item->col12}}</td>
                <td>

                  @can('sub_categories_key_edit')
                  <a class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" href="{{ route('sub_categories_keys.edit',$item->id) }}"><i class="la la-edit"></i></a>
                  @endcan
                  @can('sub_categories_key_delete')
                  <a href="javascript:void(0)" onclick="deleteRecord('{{route('sub_categories_keys.destroy', $item->id)}}','{{$item->id}}_tr');" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill ">
                      <i class="fa fa-trash"></i>
                  </a>
                  @endcan
                </td>
            </tr>
        @endforeach
    </tbody>
  </table>
@endsection
@push('create-button')
<a href="{{ route('sub_categories_keys.create') }}" class="btn btn-success m-btn m-btn--custom ">
    <span>
    <span>اضافه نمودن</span>
    <i class="la la-plus"></i>
    </span>
</a>
@endpush
