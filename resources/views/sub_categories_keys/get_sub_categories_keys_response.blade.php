<div class="form-group" id="">
    <legend class="">مشخصات</legend>
    <div class="input-group">
        <input type="text" v-model="specifications.extra_specifications.col1" class="form-control" :disabled="index != 0 && master.currency > 0" placeholder=" {{$keys->col1}}">
        <div v-if="index == 0" class="input-group-append">
            <span class="input-group-text">همه</span>
            <span class="input-group-text">
                <label class="m-checkbox m-checkbox--single m-checkbox--state m-checkbox--state-primary">
                    <input type="checkbox" @change="setMasterValue('currency', specifications.currency)">
                    <span></span>
                </label>

            </span>
        </div>
    </div>
    <div class="input-group">
        <input type="text" v-model="specifications.extra_specifications.col2" class="form-control" :disabled="index != 0 && master.currency > 0" placeholder=" {{$keys->col2}}">
        <div v-if="index == 0" class="input-group-append">
            <span class="input-group-text">همه</span>
            <span class="input-group-text">
                <label class="m-checkbox m-checkbox--single m-checkbox--state m-checkbox--state-primary">
                    <input type="checkbox" @change="setMasterValue('currency', specifications.currency)">
                    <span></span>
                </label>

            </span>
        </div>
    </div>
</div>
<script>
keys_app = new Vue({
        el: '#key_response',
        data: {

        },
        methods: {
        }
    });
</script>
