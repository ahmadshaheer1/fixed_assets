@extends('layouts.master')
@section('title', trans('global.create_sub_categories_key'))
@section('content')

<form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{route('sub_categories_keys.update', $sub_categories_keys->id)}}">
    @csrf
    @method('PUT')

    <div class="form-group m-form__group {{$errors->has('sub_category') ? 'has-danger' : ''}}">
            <label for="sub_category">{{trans('global.sub_category')}}</label>
            <select class="custom-select form-control" name="sub_category_id" required>
                <option value="" selected disabled>{{trans('global.sub_category')}}</option>

                @foreach ($sub_categories as $item) 
                    <option value="{{$item->id}}" {{$sub_categories_keys->sub_category_id == $item->id ? 'selected' : '' }}>{{$item->$name}}</option>
                @endforeach

            </select>
            @if($errors->has('sub_category_id'))
                <div class="form-control-feedback">{{$errors->first('sub_category_id')}}</div>
            @endif
    </div>
    <div class="form-group m-form__group {{$errors->has('distribution_type') ? 'has-danger' : ''}}">
        <label for="">نوعیت توزیع</label>
        <select class="browser-default custom-select" v-model="distribution_type" name="distribution_type" {{$edit_distribution_type>0 ? 'disabled' : ''}}>
            <option value="0"  {{$sub_categories_keys->distribution_type == 0 ? 'selected' : '' }}>انفرادی</option>
            <option value="1"  {{$sub_categories_keys->distribution_type == 1 ? 'selected' : '' }}>جمعی</option>
        </select>
        @if($errors->has('distribution_type'))
            <div class="form-control-feedback">{{$errors->first('distribution_type')}}</div>
        @endif
    </div>

    <div class="form-group m-form__group {{$errors->has('item_type') ? 'has-danger' : ''}}">
        <label for="">نوعیت جنس</label>
        <select class="browser-default custom-select" v-model="item_type" name="item_type">
            <option value="0"  {{$sub_categories_keys->item_type == 0 ? 'selected' : '' }}>منقول</option>
            <option value="1"  {{$sub_categories_keys->item_type == 1 ? 'selected' : '' }}>غیر منقول</option>
        </select>
        @if($errors->has('item_type'))
            <div class="form-control-feedback">{{$errors->first('item_type')}}</div>
        @endif
    </div>

    <div class="m-portlet__body">
        <div class="form-group m-form__group {{$errors->has('col1') ? 'has-danger' : ''}}" >
            <label for="sub_categories_key_name">{{trans('global.col1')}}</label>
            <input name="col1" value="{{$sub_categories_keys->col1}}" type="text" required class="form-control m-input m-input--square"  aria-describedby="emailHelp" placeholder="{{trans('global.col1')}}">
            @if($errors->has('col1'))
                <div class="form-control-feedback">{{$errors->first('col1')}}</div>
            @endif
        </div>
        <div class="form-group m-form__group {{$errors->has('col2') ? 'has-danger' : ''}}">
            <label for="sub_categories_key_name">{{trans('global.col2')}}</label>
            <input name="col2" value="{{$sub_categories_keys->col2}}" type="text" class="form-control m-input m-input--square"  aria-describedby="emailHelp" placeholder="{{trans('global.col2')}}">
            @if($errors->has('col2'))
                <div class="form-control-feedback">{{$errors->first('col2')}}</div>
            @endif
        </div>
        <div class="form-group m-form__group {{$errors->has('col3') ? 'has-danger' : ''}}">
            <label for="sub_categories_key_name">{{trans('global.col3')}}</label>
            <input name="col3" value="{{$sub_categories_keys->col3}}" type="text" class="form-control m-input m-input--square"  aria-describedby="emailHelp" placeholder="{{trans('global.col3')}}">
            @if($errors->has('col3'))
                <div class="form-control-feedback">{{$errors->first('col3')}}</div>
            @endif
        </div>
        <div class="form-group m-form__group {{$errors->has('col4') ? 'has-danger' : ''}}">
            <label for="sub_categories_key_name">{{trans('global.col4')}}</label>
            <input name="col4" value="{{$sub_categories_keys->col4}}" type="text" class="form-control m-input m-input--square"  aria-describedby="emailHelp" placeholder="{{trans('global.col4')}}">
            @if($errors->has('col4'))
                <div class="form-control-feedback">{{$errors->first('col4')}}</div>
            @endif
        </div>
        <div class="form-group m-form__group {{$errors->has('col5') ? 'has-danger' : ''}}">
            <label for="sub_categories_key_name">{{trans('global.col5')}}</label>
            <input name="col5" value="{{$sub_categories_keys->col5}}" type="text" class="form-control m-input m-input--square"  aria-describedby="emailHelp" placeholder="{{trans('global.col5')}}">
            @if($errors->has('col5'))
                <div class="form-control-feedback">{{$errors->first('col5')}}</div>
            @endif
        </div>
        <div class="form-group m-form__group {{$errors->has('col6') ? 'has-danger' : ''}}">
            <label for="sub_categories_key_name">{{trans('global.col6')}}</label>
            <input name="col6" value="{{$sub_categories_keys->col6}}" type="text" class="form-control m-input m-input--square"  aria-describedby="emailHelp" placeholder="{{trans('global.col6')}}">
            @if($errors->has('col6'))
                <div class="form-control-feedback">{{$errors->first('col6')}}</div>
            @endif
        </div>
        <div class="form-group m-form__group {{$errors->has('col7') ? 'has-danger' : ''}}">
            <label for="sub_categories_key_name">{{trans('global.col7')}}</label>
            <input name="col7" value="{{$sub_categories_keys->col7}}" type="text" class="form-control m-input m-input--square"  aria-describedby="emailHelp" placeholder="{{trans('global.col7')}}">
            @if($errors->has('col7'))
                <div class="form-control-feedback">{{$errors->first('col7')}}</div>
            @endif
        </div>
        <div class="form-group m-form__group {{$errors->has('col8') ? 'has-danger' : ''}}">
            <label for="sub_categories_key_name">{{trans('global.col8')}}</label>
            <input name="col8" value="{{$sub_categories_keys->col8}}" type="text" class="form-control m-input m-input--square"  aria-describedby="emailHelp" placeholder="{{trans('global.col8')}}">
            @if($errors->has('col8'))
                <div class="form-control-feedback">{{$errors->first('col8')}}</div>
            @endif
        </div>
        <div class="form-group m-form__group {{$errors->has('col9') ? 'has-danger' : ''}}">
            <label for="sub_categories_key_name">{{trans('global.col9')}}</label>
            <input name="col9" value="{{$sub_categories_keys->col9}}" type="text" class="form-control m-input m-input--square"  aria-describedby="emailHelp" placeholder="{{trans('global.col9')}}">
            @if($errors->has('col9'))
                <div class="form-control-feedback">{{$errors->first('col9')}}</div>
            @endif
        </div>
        <div class="form-group m-form__group {{$errors->has('col10') ? 'has-danger' : ''}}">
            <label for="sub_categories_key_name">{{trans('global.col10')}}</label>
            <input name="col10" value="{{$sub_categories_keys->col10}}" type="text" class="form-control m-input m-input--square"  aria-describedby="emailHelp" placeholder="{{trans('global.col10')}}">
            @if($errors->has('col10'))
                <div class="form-control-feedback">{{$errors->first('col10')}}</div>
            @endif
        </div>
        <div class="form-group m-form__group {{$errors->has('col11') ? 'has-danger' : ''}}">
            <label for="sub_categories_key_name">{{trans('global.col11')}}</label>
            <input name="col11" value="{{$sub_categories_keys->col11}}" type="text" class="form-control m-input m-input--square"  aria-describedby="emailHelp" placeholder="{{trans('global.col11')}}">
            @if($errors->has('col11'))
                <div class="form-control-feedback">{{$errors->first('col11')}}</div>
            @endif
        </div>
        <div class="form-group m-form__group {{$errors->has('col12') ? 'has-danger' : ''}}">
            <label for="sub_categories_key_name">{{trans('global.col12')}}</label>
            <input name="col12" value="{{$sub_categories_keys->col12}}" type="text" class="form-control m-input m-input--square"  aria-describedby="emailHelp" placeholder="{{trans('global.col12')}}">
            @if($errors->has('col12'))
                <div class="form-control-feedback">{{$errors->first('col12')}}</div>
            @endif
        </div>

    </div>
    <div class="m-portlet__foot m-portlet__foot--fit">
        <div class="m-form__actions">
            <button type="submit" class="btn btn-primary">{{trans('global.submit')}}</button>
            <button type="reset" class="btn btn-danger">{{trans('global.cancel')}}</button>
        </div>
    </div>
</form>
@endsection
