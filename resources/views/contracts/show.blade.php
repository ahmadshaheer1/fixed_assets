@extends('layouts.master')
@section('title', trans('global.add_contract'))
@section('content')

<div id="contract_show">

    <div class="contract_info" >
        <div class="row border-bottom pt-1">
           <div class="col-md-6">
              <label for="">
                 <h4>
                    نوعیت قرارداد :
                 </h4>
              </label>
              <div class="d-inline-block text-muted pl-2">
                 <h5>
                    @{{contract_info.contract_type['name_dr']}}
                 </h5>
              </div>
           </div>
           <div class="col-md-6">
              <label for="">
                 <h4>
                    نمبر تشخصیه :
                 </h4>
              </label>
              <div class="d-inline-block text-muted pl-2">
                 <h5>
                    @{{contract_info.identification_number}}
                 </h5>
              </div>
           </div>
        </div>
        <div class="row border-bottom pt-1">
           <div class="col-md-6">
              <label for="">
                 <h4>
                    اداره :
                 </h4>
              </label>
              <div class="d-inline-block text-muted pl-2">
                 <h5>
                    @{{contract_info.department['name_dr']}}
                 </h5>
              </div>
           </div>
           <div class="col-md-6">
              <label for="">
                 <h4>
                    شرکت های قراردادی :
                 </h4>
              </label>
              <div class="d-inline-block text-muted pl-2">
                 <h5>
                    @{{contract_info.contractor_company['company_name']}}
                 </h5>
              </div>
           </div>
        </div>
        <div class="row border-bottom pt-1">
           <div class="col-md-6">
              <label for="">
                 <h4>
                    سال :
                 </h4>
              </label>
              <div class="d-inline-block text-muted pl-2">
                 <h5>
                    @{{contract_info.year}}
                 </h5>
              </div>
           </div>
           <div class="col-md-6">
              <label for="">
                 <h4>
                    مقدار پول :
                 </h4>
              </label>
              <div class="d-inline-block text-muted pl-2">
                 <h5>
                    @{{contract_info.contract_amount}}
                 </h5>
              </div>
           </div>
        </div>
        <div class="row border-bottom pt-1">
           <div class="col-md-6">
              <label for="">
                 <h4>
                    تاریخ آغاز :
                 </h4>
              </label>
              <div class="d-inline-block text-muted pl-2">
                 <h5>
                    @{{contract_info.start_date}}
                 </h5>
              </div>
           </div>
           <div class="col-md-6">
              <label for="">
                 <h4>
                    تاریخ ختم :
                 </h4>
              </label>
              <div class="d-inline-block text-muted pl-2">
                 <h5>
                    @{{contract_info.end_date}}
                 </h5>
              </div>
           </div>
        </div>

        <hr style="border-bottom: 3px solid #3699ff;">

        <div class="row" >
            <div class="col-md-12">
                
                <table class="table table-bordered m-2" >
                    <thead>
                     
                            <tr>
                                <th>شماره</th>
                                <th>مشخصات اجناس</th>
                                <th>واحد</th>
                                <th>مقدار</th>
                                <th>مقدار فی واحد</th>
                                <th>  مقدار فی واحد به حروف</th>
                                <th>قیمت مجموعی</th>
                                <th>عملیات</th>
                            </tr>
                       
                    </thead>
                    <tbody>
                        <tr v-for="(item, index) in items ">
                           
                            <td>@{{ index + 1 }}</td>
                            {{-- <td>@{{item.serial_number}}</td> --}}
                            <td>@{{item.item_details.name_dr}}</td>
                            <td>@{{item.item_details.unit.name_dr}}</td>
                        
                            <td>
                                @{{item.price_per_item}}
                            </td>
                            
                            
                            <td>@{{item.item_amount}}</td>
                            <td>
                                @{{item.item_amount.toPersianLetter()}}
                            </td>
                            <td>@{{item.total_price}}</td>
                            
                            <td class="" >
                                <a href="#" @click.prevent="editItemDetailsSpecifications(item.id)"  class="text-info p-2">
                                    <i class="fa fa-edit"></i>
                                </a>
                                @can('item_details_specifications_delete')
                                <a v-if="item.status == 0" href="#" @click.prevent="deleteItemDetailsSpecifications(item.id)" class="text-danger">
                                    <i  class="fa fa-trash"></i>
                                </a>
                                @endcan
                              
                               
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">قیمت ارایه شده در آفر مبلغ : @{{contract_info.contract_amount.toPersianLetter()}}</td>
                            <td colspan="2">@{{contract_info.contract_amount}}</td>
                        </tr>
                        <tr>
                            <td colspan="6">مبلغ تخفیف داده شده: @{{contract_info.contract_discount.toPersianLetter()}} (در حدود @{{contract_info.contract_discount_perce}} %)</td>
                            <td colspan="2">@{{contract_info.contract_discount}}</td>
                        </tr>
                        <tr>
                            <td colspan="6" class="bold">قیمت نهایی قرارداد مبلغ: @{{total_contract_price.toPersianLetter()}}</td>
                            <td colspan="2" class="bold">@{{total_contract_price}}</td>
                        </tr>
                     
                    </tbody>
                </table>


                <div class="alert alert-custom alert-danger  fade show" role="alert" v-if="error_message">
                    <div class="alert-icon"><i class="flaticon-warning"></i></div>
                    <div class="alert-text"> قیمت اجناس قرارداد بیشتر از قیمت قرارداد میباشد</div>
                    <div class="alert-close">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true"><i class="ki ki-close"></i></span>
                        </button>
                    </div>
                </div>
            </div>
            
            
       
        </div>
        
        <hr style="border-bottom: 3px solid #3699ff;">
        <div class="row">
            <div class="col-md-2 p-1" v-if="final_price != total_contract_price">
                <div class="input-group mb-3"  v-if="!edit">
                        <input type="number" v-model="total_forms" value="" class="form-control">
                    <div class="input-group-append">
                        <button class="btn btn-success " @click="addNewForm">
                            <i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
            </div>
            <div class="col-md-4" v-if="final_price != total_contract_price">
                <span class="pull-left">
                    <div class="font-size-sm text-muted font-weight-bold">پول باقی مانده</div>
                    <h5 class="font-size-h4 font-weight-bolder text-dark">@{{contract_available_amount}}</h5>
                </span>
        

            </div>
            <div class="col-md-6">
                <button 
                v-if="contract_items.length > 1 && add_button_error" 
                class="btn btn-success pull-left" @click="storeItemDetailsSpecifications()">
                    اضافه نمودن همه
                </button>
            </div>
            <div class="col-md-6" v-if="final_price == total_contract_price">
                <a class="btn btn-success pull-left" href="{{route('contracts.index')}}">
                    تکمیل قرارداد
                </a>
            </div>
        </div>
        <div class="table-responsive-lg">
            <table class="table" v-if="contract_items.length && !edit ">
                <tr>
                    <th>شماره</th>
                    <th>مشخصات اجناس</th>
                    <th>مقدار</th>
                    <th>واحد</th>
                    <th>مقدار فی واحد</th>
                    <th>قیمت مجموعی</th>
                </tr>
                <tr v-for="(item, index) in contract_items">
                    <th class="" style="line-height:3;font-weight:bold">
                    <span class=""> @{{++index}}</span>
                    </th>
                


                    <th class="px-1"  width="200">
                        {{-- <div class="form-group"> --}}
                            <v-select
                                                    
                            label="name_dr" 
                            :options="item_details"
                            item-text=""
                            dir="rtl"
                            v-model="item.item_details"
                            :reduce="item_details=> item_details"
                                                                
                            >
                        
                            </v-select>
                                
                        {{-- </div> --}}
                    </th>
                    <th class="px-1">
                        <div  class="form-group">
                            <div class="input-group">
                                <input type="text" v-model="item.item_amount" class="form-control"  placeholder="مقدار">
                                
                            </div>
                        </div>
                    </th>
                    <th class="px-1">
                        <span v-if="item.item_details.unit">@{{item.item_details.unit['name_dr']}}</span>
                    </th>
                    <th class="px-1">
                        <div  class="form-group">
                                <div class="input-group">
                                    <input type="text" v-model="item.price_per_item" class="form-control"  placeholder="قیمت فی واحد" v-on:keyup="calculatePrice(index)">
                                   
                                </div>
                        </div>
                    </th>
                    <th class="px-1">
                        <div   class="form-group">
                                <div class="input-group">
                                    <input type="text" v-model="item.total_price" class="form-control"  placeholder="قیمت مجموعی" >
                                  
                                </div>
                        </div>
                    </th>
            
                    <th class="" style="line-height:3" width="75" >
                            <div v-if="!edit" class="btn-group">
                                
                                <button v-if="add_button_error" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" @click="storeItemDetailsSpecifications(index-1)" title="Add">
                                        <i class="la la-plus"></i>
                                </button>
                                <button @click="removeForm(index)" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="View">
                                        <i class="la la-trash"></i>
                                </button>
                            </div>
                            <div v-else>
                                <button
                                @click="updateItemDetailsSpecifications()"
                                class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="View">
                                        <i class="la la-edit"></i>
                                </button>
                            </div>
                    </th>
                </tr>
            </table>
        </div>

        <div class="table-responsive-lg" v-if="edit">
            <table class="table">
                <tr v-for="(item, index) in contract_items">
                    <th class="" style="line-height:3;font-weight:bold">
                    <span class=""> @{{++index}}</span>
                    </th>
               
                    <th class="px-1" width="150">
                        
                        <v-select
                                        
                        label="name_dr" 
                        :options="item_details"
                        item-text=""
                        dir="rtl"
                        v-model="item.item_details"
                        :reduce="item_details=> item_details"
                                                            
                        >
                        
                        </v-select>
                         
                    </th>
                    
                    <th class="px-1" width="150">
                        <div class="form-group">
                            <input type="text" v-model="item.item_amount" class="form-control"  placeholder="مقدار">
                        </div>
                    </th>
                    <th class="px-1" >
                       <span v-if="item.item_details.unit">@{{item.item_details.unit['name_dr']}}</span>
                    </th>
                    <th class="px-1">
                        <div class="form-group">
                                <div class="input-group">
                                    <input type="text" v-model="item.item_amount" class="form-control"  placeholder="مقدار">    
                                  
                                </div>
                        </div>
                    </th>
                    <th class="px-1">
                        <div class="form-group">
                            <div class="input-group">
                                    <input type="text" v-model="item.price_per_item" class="form-control"  placeholder="قیمت فی واحد" v-on:keyup="calculatePrice(index)">   
                   
                            </div>
                        </div>
                    </th>
                    <th class="px-1">
                        <div class="form-group">
                                <div class="input-group">
                                    <input type="text" v-model="item.total_price" class="form-control"  placeholder="قیمت مجموعی" > 
                                    
                                </div>
                        </div>
                    </th>
                    <th class="" style="line-height:3" width="75">
                            <div v-if="!edit" class="btn-group">
                                <button class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" @click="storeItemDetailsSpecifications(index-1)" title="View">
                                        <i class="la la-plus"></i>
                                </button>
                                <button @click="removeForm(index)" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="View">
                                        <i class="la la-trash"></i>
                                </button>
                            </div>
                            <div v-else>
                                <button @click="updateItemDetailsSpecifications()" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" title="View">
                                        <i class="la la-edit"></i>
                                </button>
                            </div>
                    </th>
                </tr>
            </table>
        </div>
     </div>
</div>

@endsection
@push('custom-css')
<style>
    .bold{
        font-size: 16px;
        font-weight: bold;  
    }
</style>
@endpush
@push('custom-js')
<script type="text/javascript" src="{{asset('public/js/numberToPersian.js')}}"></script>
<script>

Vue.component('v-select', VueSelect.VueSelect);
var app = new Vue({
            el: '#contract_show',
            data: {
                contract_info :JSON.parse('{!!json_encode($contract_info, 1)!!}') , 
                edit:false,
                total_forms:1,
                // bulk assignment values
                master: {
                    item_details: '',
                    item_details_id:'',
                    price_per_item: '',
                    total_price: '',
                    item_amount: '',
                    available_amount: '',
                    contract_id:'',
                },
                // new items contract_items
                contract_items: [

                ],
                item_details:[

                ],
                items:[

                ],
                total_price_sum: '',
                final_price:'',
                add_button_error: '',
                error_message:'',
                contract_available_amount:'',
                total_contract_price:'',
                
            },
            methods: {
                addNewForm() {
                    for(i = 0 ; i < this.total_forms; i++) {
                        this.contract_items.push(
                            {
                                item_details: this.master.item_details,
                                item_details_id:'',
                                item_amount: this.master.item_amount ,
                                price_per_item: this.price_per_item,
                                total_price: '',
                                available_amount:this.master.item_amount,
                                contract_id:this.contract_info.id,

                            }
                        );
                    }
                   

                },
                getItemDetails(){
                    axios.get("{{route('get_item_details_consumable')}}")

                        .then(res => {
                            this.item_details = res.data;
                        })
                        .catch(err => {
                            console.log(err);
                        })  
                },
                setMasterValue(type, value, event) {

                    this.master[type] = event.target.checked ? value : '';

                    for(i = 1 ; i < this.contract_items.length ; i++) {
                        this.contract_items[i][type] = this.master[type];
                    }

                },
                removeForm(index) {
                    var index= index - 1;
                    this.contract_items.splice(index, 1);

                },
                calculatePrice(index) {
                    var index= index - 1;
                    if(this.contract_items[index].price_per_item){

                        this.contract_items[index].total_price = this.contract_items[index].item_amount * this.contract_items[index].price_per_item
                    }
                },
                storeItemDetailsSpecifications(index='') {
                    axios.post("{{route('contract_items.store')}}", {
                        
                        'contract_items': index !== '' ? this.contract_items[index] : this.contract_items
                    })
                    .then(res => {
                        if(index !== '') {
                            this.removeForm(index);
                        }
                        else {
                            this.contract_items = [];
                        }
                        this.getAvailableItems();

                    })
                    .catch(err => {
                        console.log(err);
                    });
                },
                getAvailableItems() {
                    axios.get("{{url('contract_items')}}/"+this.contract_info.id)
                        .then(res => {
                            this.items = res.data.item_details;
                            this.total_price_sum = res.data.total_price_sum;
                            this.final_price = this.total_price_sum;
                            this.total_contract_price = this.contract_info.contract_amount - this.contract_info.contract_discount;
                          
                        })
                        .catch(err => {

                        })
                },
                editItemDetailsSpecifications(id) {
                    
                    this.edit=true;
                    axios.get("{{url('contract_items')}}/"+id+"/edit")
                    .then(res => {
                        if(this.contract_items.length == 0) {
                            this.contract_items.push(res.data);
                        }
                    })
                    .catch(err => {

                    });
                },
                updateItemDetailsSpecifications() {
                    id = this.contract_items[0].id;
                    axios.put("{{url('contract_items')}}/"+id, {
                        'contract_items': this.contract_items[0]
                    })
                    .then(res => {
                        this.edit = false;
                        this.contract_items = [];
                        this.getAvailableItems();
                    })
                    .catch(err => {
                        console.log(err);
                    })
                },
                checkedContractAmount(){
                    
                    var current_item_total = this.total_contract_price - this.final_price
                    this.contract_available_amount = current_item_total ;
                
                    if(this.contract_items.length > 0 ){
                        
                        var items_total = 0
                        
                        for(var i = 0; i < this.contract_items.length; i++) {
     
                            items_total += parseFloat(this.contract_items[i].total_price)
                        }
                        
                        // this.contract_available_amount = current_item_total - items_total;
                        
                        if(items_total <= current_item_total){
                            
                            this.add_button_error = true;
                            if(items_total>0)
                                this.error_message = false
                        }else{
                            this.add_button_error =  false;
                            if(items_total>0)
                                this.error_message = true
     
                        }
                    }else{
                      
                        this.add_button_error = true;
                    }

                }
                
            },
            mounted(){
                this.getItemDetails();
                this. getAvailableItems();
            },
            updated(){
                this.checkedContractAmount()
            }
           
        })

</script>
@endpush
