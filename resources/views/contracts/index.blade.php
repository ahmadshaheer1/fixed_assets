@extends('layouts.master')
@section('title',trans('global.contracts'))
@section('content')
<table  id="example" class="table table-striped table-bordered jquery-datatable" style="width:100%">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">{{trans('global.contract_type')}}</th>
        <th scope="col">{{trans('global.identification_number')}}</th>
        <th scope="col">{{trans('global.year')}}</th>
        <th scope="col">{{trans('global.department')}}</th>
        <th scope="col">{{trans('global.contractor_company')}}</th>
        <th scope="col">{{trans('global.contract_amount')}}</th>
        <th scope="col">{{trans('global.actions')}}</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($contracts as $contract)  
        <tr>
          <td>{{$loop->iteration}}</td>
          <td>
            {{$contract->contractType->name_dr}}
          </td>
          <td>
            {{$contract->identification_number}}
          </td>
          <td>
            {{$contract->year}}
          </td>
          <td>
            {{$contract->department->name_dr}}
          </td>
          <td>
            {{$contract->contractorCompany->company_name}}
          </td>
          <td>
            {{$contract->contract_amount}}
          </td>
          <td>
            <a class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" href="{{ route('contracts.show_item',$contract->id) }}"><i class="icon-xl fas fa-chart-bar"></i></a>
            <a class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" href="{{ route('contracts.show',$contract->id) }}"><i class="la la-eye"></i></a>
            <a class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" href="{{ route('contracts.edit',$contract->id) }}"><i class="la la-edit"></i></a>
            <a href="javascript:void(0)" onclick="deleteRecord('{{route('contracts.destroy', $contract->id)}}','{{$contract->id}}_tr');" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill ">
                <i class="fa fa-trash"></i>
            </a>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
  {{-- {!!$contracts->links()!!} --}}
@endsection
@push('create-button')
<a href="{{ route('contracts.create') }}" class="btn btn-success m-btn m-btn--custom ">
    <span>
    <span>اضافه نمودن</span>
    <i class="la la-plus"></i>
    </span>
</a>
@endpush
