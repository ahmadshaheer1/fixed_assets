@extends('layouts.master')
@section('title', trans('global.add_contract'))
@section('content')

<div id="contract_show">

    <div class="contract_info" >
        <div class="row border-bottom pt-1">
           <div class="col-md-6">
              <label for="">
                 <h4>
                    نوعیت قرارداد :
                 </h4>
              </label>
              <div class="d-inline-block text-muted pl-2">
                 <h5>
                    @{{contract_info.contract_type['name_dr']}}
                 </h5>
              </div>
           </div>
           <div class="col-md-6">
              <label for="">
                 <h4>
                    نمبر تشخصیه :
                 </h4>
              </label>
              <div class="d-inline-block text-muted pl-2">
                 <h5>
                    @{{contract_info.identification_number}}
                 </h5>
              </div>
           </div>
        </div>
        <div class="row border-bottom pt-1">
           <div class="col-md-6">
              <label for="">
                 <h4>
                    اداره :
                 </h4>
              </label>
              <div class="d-inline-block text-muted pl-2">
                 <h5>
                    @{{contract_info.department['name_dr']}}
                 </h5>
              </div>
           </div>
           <div class="col-md-6">
              <label for="">
                 <h4>
                    شرکت های قراردادی :
                 </h4>
              </label>
              <div class="d-inline-block text-muted pl-2">
                 <h5>
                    @{{contract_info.contractor_company['company_name']}}
                 </h5>
              </div>
           </div>
        </div>
        <div class="row border-bottom pt-1">
           <div class="col-md-6">
              <label for="">
                 <h4>
                    سال :
                 </h4>
              </label>
              <div class="d-inline-block text-muted pl-2">
                 <h5>
                    @{{contract_info.year}}
                 </h5>
              </div>
           </div>
           <div class="col-md-6">
              <label for="">
                 <h4>
                    مقدار پول :
                 </h4>
              </label>
              <div class="d-inline-block text-muted pl-2">
                 <h5>
                    @{{contract_info.contract_amount}}
                 </h5>
              </div>
           </div>
        </div>
        <div class="row border-bottom pt-1">
           <div class="col-md-6">
              <label for="">
                 <h4>
                    تاریخ آغاز :
                 </h4>
              </label>
              <div class="d-inline-block text-muted pl-2">
                 <h5>
                    @{{contract_info.start_date}}
                 </h5>
              </div>
           </div>
           <div class="col-md-6">
              <label for="">
                 <h4>
                    تاریخ ختم :
                 </h4>
              </label>
              <div class="d-inline-block text-muted pl-2">
                 <h5>
                    @{{contract_info.end_date}}
                 </h5>
              </div>
           </div>
        </div>


     </div>
     <div class="row" >
        <div class="col-md-12">
            <div class="accordion accordion-solid accordion-toggle-plus" id="accordionExample6" v-for="(item, index) in contract_info.contract_items">
                <div class="card">
                    <div class="card-header" id="headingOne6">
                    <div class="card-title font-weight-bold" data-toggle="collapse" :data-target="'#collapseOne6'+index">
                    <a href="javascript:void(0)" @click="getContractItems(item.item_details.id)">   <i class="flaticon-pie-chart-1"></i> @{{item.item_details.name_dr}} 
                        <span class="">مقدار قرارداد : @{{item.item_amount}} @{{item.item_details.unit.name_dr}}</span>
                        <span class="">مقدار موجود : @{{item.available_amount}} @{{item.item_details.unit.name_dr}}</span>
                        <span class="">مقدار توزیع شده : @{{(item.item_amount - item.available_amount)}} @{{item.item_details.unit.name_dr}}</span>
                        {{-- <span class="">مقدار قرارداد : @{{item.item_details.unit.name_dr}}</span> --}}
                    </a>
                    </div>
                    </div>
                    <div :id="'collapseOne6'+index" class="collapse " data-parent="#accordionExample6">
                    <div class="card-body">
                        <table class="table table-bordered m-2" >
                            <thead>
                             
                                    <tr>
                                        <th>شماره</th>
                                        <th>نام جنس</th>
                                        <th>ف س ۹</th>
                                        <th>م ۷</th>
                                        <th>  مقدار توزیع</th>
                                        <th>در جمع</th>
                                    </tr>
                               
                            </thead>
                            <tbody>
                                <tr v-for="(contract_item, contract_item_index) in contract_items ">
                                   
                                   
                                    <td>@{{++contract_item_index}}</td>
                                    <td>@{{contract_item.item_details.name_dr}}</td>
                                    <td>نمبر @{{contract_item.fece9.number}} - تاریخ @{{contract_item.fece9.request_date}}</td>
                                    <td>نمبر @{{contract_item.meem7.number}} - تاریخ @{{contract_item.meem7.date}}</td>
                                    <td>@{{contract_item.quantity}}</td>
                                    
                                    <td>@{{contract_item.meem7.receiver_employee.name_dr}} @{{contract_item.meem7.receiver_employee.last_name}} @{{contract_item.meem7.receiver_employee.current_position_dr}}</td>
                                </tr>                 
                            </tbody>
                        </table>
                    </div>
                    </div>
                </div>
            </div>

        </div>
        
        
   
    </div>
</div>

@endsection
@push('custom-css')
<style>
    .bold{
        font-size: 16px;
        font-weight: bold;  
    }
</style>
@endpush
@push('custom-js')
<script type="text/javascript" src="{{asset('public/js/numberToPersian.js')}}"></script>
<script>

Vue.component('v-select', VueSelect.VueSelect);
var app = new Vue({
            el: '#contract_show',
            data: {
                contract_info :JSON.parse('{!!json_encode($contract_info, 1)!!}') , 
                contract_item_balance :JSON.parse('{!!json_encode($contract_item_balance, 1)!!}') , 
                contract_items:[],
      
                
            },
            methods: {
                getContractItems(item_id){
                    if(!this.contract_items.length){
                        this.contract_items = this.contract_item_balance.filter(item => item.item_details_id == item_id);
                    }else{
                        this.contract_items = [];
                    }

                    }
                
            },
            mounted(){
            
            },
            updated(){
               
            }
           
        })

</script>
@endpush
