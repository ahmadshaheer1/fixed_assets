<style>
    .m-aside-menu.m-aside-menu--skin-dark .m-menu__nav>.m-menu__item.m-menu__item--expanded>.m-menu__heading, .m-aside-menu.m-aside-menu--skin-dark .m-menu__nav>.m-menu__item.m-menu__item--expanded>.m-menu__link {
        background-color: #1d5e7d;
    }
    .m-aside-menu.m-aside-menu--skin-dark .m-menu__nav>.m-menu__item.m-menu__item--expanded>.m-menu__heading .m-menu__link-icon, .m-aside-menu.m-aside-menu--skin-dark .m-menu__nav>.m-menu__item.m-menu__item--expanded>.m-menu__link .m-menu__link-icon, .m-aside-menu.m-aside-menu--skin-dark .m-menu__nav>.m-menu__item.m-menu__item--expanded>.m-menu__heading .m-menu__ver-arrow, .m-aside-menu.m-aside-menu--skin-dark .m-menu__nav>.m-menu__item.m-menu__item--expanded>.m-menu__link .m-menu__ver-arrow {
        color: #04acff;
    }
    .m-body .m-content {
        padding: 15px;
    }
    .m-aside-menu.m-aside-menu--skin-dark .m-menu__nav>.m-menu__item>.m-menu__heading .m-menu__link-text, .m-aside-menu.m-aside-menu--skin-dark .m-menu__nav>.m-menu__item>.m-menu__link .m-menu__link-text {
        font-size: 1.2em;
    }
    .has-danger .form-control {
        border-color: #f4516c;
    }
    .dataTables_filter {
        float: left;
    }
    .m-form .m-form__section.m-form__section--label-align-right .m-form__group>label, .m-form.m-form--label-align-right .m-form__group>label {
        font-weight: bold;
    }
    .has-danger .select2-container--bootstrap4 .select2-selection {
        border-color: #f4516c;
        border-radius: 0px !important;
    }
    .select2-container--bootstrap4 .select2-selection {
        border-radius: 0;
    }

    .table-fece9{
        width: 100%;
        border: 3px groove gray;;
    }
    .table-fece9 th,.table-fece9 td {
        padding: 5px;
        text-align: center;
        border: 1px solid;
    }

    {{-- fece9 9 view  --}}
    .fs9-invoice__logo
    {
        padding-top: 0rem !important;
    }

    .m-invoice-2 .m-invoice__wrapper .m-invoice__head .m-invoice__container .m-invoice__items {
        border-bottom: 1px solid #ebedf2;
    }

    .table-fees5{
      width: 100%;
      border: 1px solid black;
    }
    .table-fees5 th,.table-fees5 td {
      padding: 5px;
      text-align: center;
      border: 1px solid;
    }
    .bggray {
      background-color: gainsboro !important;
    }
    .no-border {
        border-top: 1px solid white !important;
        border-bottom: 1px solid white !important;
    }

    .image_attachment
    {
        width: 100px !important;
    }
    #test123{
    position: relative;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    width: 100%;
    pointer-events: auto;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid rgba(0,0,0,0.2);
    outline: 0;
    box-shadow: 0px 0px 15px 1px rgb(69 65 78 / 20%);
    
  }

    /* Disterbution Ticket Details  */
    .dist-m-demo__preview
    {
        overflow: hidden;
        margin-top: 20px;
        margin-bottom: 20px;
    }
    span.m-widget24__stats.m--font-brand.pull-left {
        font-size: 1.2em;
    }
    @media (min-width: 1200px) {
        .row.m-row--col-separator-xl>div {
            border-left: 1px solid #ebedf2 !important;
            border-bottom: 1px solid #ebedf2 !important;
            padding: 10px;
        }
    }
    @media print {
    .no-print{
        display: none !important;

    }


    @page{
        /*margin:-10px !important;*/
        margin-left: -10px;
        margin-right: -10px;
        margin-top: -30px;
        margin-bottom:-30px;
      }




 

</style>
