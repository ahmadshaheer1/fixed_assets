@php
    $status = Auth::user()->status;
@endphp
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">

  <!-- BEGIN: Aside Menu -->
  <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " m-menu-vertical="1" m-menu-scrollable="0" m-menu-dropdown-timeout="500">
    <ul class="m-menu__nav ">
      <li class="m-menu__section m-menu__section--first">
        <h4 class="m-menu__section-text">عمومی</h4>
        <i class="m-menu__section-icon flaticon-more-v2"></i>
      </li>
      <li class="m-menu__item {{ route::is('home') ? 'm-menu__item--open m-menu__item--expanded' : ''}}" aria-haspopup="true"><a href="{{route('home')}}" class="m-menu__link "><span class="m-menu__item-here"></span><i class="m-menu__link-icon flaticon-line-graph"></i><span class="m-menu__link-text">صفحه اصلی</span></a></li>
      @if($status !=1)
      <li class="m-menu__item {{ route::is('contracts*') ? 'm-menu__item--open m-menu__item--expanded' : ''}}" aria-haspopup="true"><a href="{{route('contracts.index')}}" class="m-menu__link "><span class="m-menu__item-here"></span><i class="m-menu__link-icon flaticon-line-graph"></i><span class="m-menu__link-text">{{trans('global.contracts')}}</span></a></li>
      <li class="m-menu__item {{ route::is('fece9*') ? 'm-menu__item--open m-menu__item--expanded' : ''}}" aria-haspopup="true"><a href="{{route('fece9s.index')}}" class="m-menu__link "><span class="m-menu__item-here"></span><i class="m-menu__link-icon flaticon-line-graph"></i><span class="m-menu__link-text">ف س ۹ ها</span></a></li>
      <li class="m-menu__item {{ route::is('fece5*') ? 'm-menu__item--open m-menu__item--expanded' : ''}}" aria-haspopup="true"><a href="{{route('fece5s.index')}}" class="m-menu__link "><span class="m-menu__item-here"></span><i class="m-menu__link-icon flaticon-line-graph"></i><span class="m-menu__link-text">ف س ۵</span></a></li>
      <li class="m-menu__item {{ route::is('fece2*') ? 'm-menu__item--open m-menu__item--expanded' : ''}}" aria-haspopup="true"><a href="{{route('fece2s.index')}}" class="m-menu__link "><span class="m-menu__item-here"></span><i class="m-menu__link-icon flaticon-line-graph"></i><span class="m-menu__link-text">ف س ۲ ها (ارسال و وصول)</span></a></li>
      <li class="m-menu__item {{ route::is('meem7*') ? 'm-menu__item--open m-menu__item--expanded' : ''}}" aria-haspopup="true"><a href="{{route('meem7.index')}}" class="m-menu__link "><span class="m-menu__item-here"></span><i class="m-menu__link-icon flaticon-line-graph"></i><span class="m-menu__link-text">م ۷</span></a></li>
      @endif
      <li class="m-menu__item {{ route::is('allotments.get_employees_allotments') ? 'm-menu__item--open m-menu__item--expanded' : ''}}" aria-haspopup="true"><a href="{{route('allotments.get_employees_allotments')}}" class="m-menu__link "><span class="m-menu__item-here"></span><i class="m-menu__link-icon flaticon-line-graph"></i><span class="m-menu__link-text">{{trans('global.employee_allotments')}}</span></a></li>
      <li class="m-menu__item {{ route::is('allotments.motamed_allotments') ? 'm-menu__item--open m-menu__item--expanded' : ''}}" aria-haspopup="true"><a href="{{route('allotments.motamed_allotments')}}" class="m-menu__link "><span class="m-menu__item-here"></span><i class="m-menu__link-icon flaticon-line-graph"></i><span class="m-menu__link-text">{{trans('global.motamed_allotments')}}</span></a></li>
      {{-- <li class="m-menu__item {{ route::is('external_employee.index') ? 'm-menu__item--open m-menu__item--expanded' : ''}}" aria-haspopup="true"><a href="{{route('external_employee.index')}}" class="m-menu__link "><span class="m-menu__item-here"></span><i class="m-menu__link-icon flaticon-line-graph"></i><span class="m-menu__link-text">{{trans('global.external_employee_allotments')}}</span></a></li> --}}

      @if($status !=1)
      <li class="m-menu__item {{ route::is('item_return_requests*') ? 'm-menu__item--open m-menu__item--expanded' : ''}}" aria-haspopup="true"><a href="{{route('item_return_requests.index')}}" class="m-menu__link "><span class="m-menu__item-here"></span><i class="m-menu__link-icon flaticon-line-graph"></i><span class="m-menu__link-text">{{trans('global.return_request')}}</span></a></li>
     @endif
      {{-- <li class="m-menu__item {{ route::is('end_items_details*') ? 'm-menu__item--open m-menu__item--expanded' : ''}}" aria-haspopup="true"><a href="{{route('end_items_details.index')}}" class="m-menu__link "><span class="m-menu__item-here"></span><i class="m-menu__link-icon flaticon-line-graph"></i><span class="m-menu__link-text">اجناس</span></a></li> --}}
     @can('report-list')
       
    
      <li class="m-menu__item  m-menu__item--submenu {{ route::is('item_report*') || route::is('item_report_details') ? 'm-menu__item--open m-menu__item--expanded' : ''}}" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><span class="m-menu__item-here"></span><i
           class="m-menu__link-icon flaticon-layers"></i><span class="m-menu__link-text">راپور ها</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
        <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
          <ul class="m-menu__subnav">
            <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true"><span class="m-menu__link"><span class="m-menu__item-here"></span><span class="m-menu__link-text">راپور ها</span></span></li>
           
            <li class="m-menu__item {{ route::is('item_report*') ? 'm-menu__item--active' : ''}}" aria-haspopup="true"><a href="{{route('item_report.index')}}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">راپور اجناس جمع قید شده</span></a></li>
            <li class="m-menu__item {{ route::is('item_report_details') ? 'm-menu__item--active' : ''}}" aria-haspopup="true"><a href="{{route('item_report_details')}}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">راپور اجناس</span></a></li> 
          </ul>
        </div>
      </li>
      @endcan
      <li class="m-menu__item  m-menu__item--submenu {{ route::is('units*') ||  route::is('departments*')||  route::is('contract_type*') || route::is('users*') || route::is('external_departments*') ||  route::is('roles*') || route::is('items_basic_informations') || route::is('main_categories*') || route::is('sub_categories*')  || route::is('end_categories*') || route::is('vendors*')|| route::is('sub_categories_keys*') || route::is('item_details*') || route::is('requested_items*') || route::is('contractor_company*') ? 'm-menu__item--open m-menu__item--expanded' : ''}}" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><span class="m-menu__item-here"></span><i
           class="m-menu__link-icon flaticon-layers"></i><span class="m-menu__link-text">تنظیمات</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
        <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
          <ul class="m-menu__subnav">
            <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true"><span class="m-menu__link"><span class="m-menu__item-here"></span><span class="m-menu__link-text">تنظیمات</span></span></li>
            @if($status !=1)
            <li class="m-menu__item {{ route::is('departments*') ? 'm-menu__item--active' : ''}}" aria-haspopup="true"><a href="{{route('departments.index')}}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">ادارات</span></a></li>
            @endif
            @if($status !=1)
          <li class="m-menu__item {{ route::is('external_departments*') ? 'm-menu__item--active' : ''}}" aria-haspopup="true"><a href="{{route('external_departments.index')}}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text"> ادارات بیرونی</span></a></li>
          @endif   
            {{-- <li class="m-menu__item {{ route::is('units*') ? 'm-menu__item--active' : ''}}" aria-haspopup="true" m-menu-link-redirect="1"><a href="{{route('units.index')}}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">واحد</span></a></li> --}}
            <li class="m-menu__item {{ route::is('items_basic_informations') || route::is('main_categories*') || route::is('sub_categories*') || route::is('end_categories*') || route::is('vendors*')|| route::is('sub_categories_keys*') || route::is('item_details*')  ? 'm-menu__item--active' : ''}}" aria-haspopup="true" m-menu-link-redirect="1"><a href="{{route('item_details.index')}}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">معلومات اساسی اجناس</span></a></li>
            {{-- <li class="m-menu__item  m-menu__item--submenu {{ route::is('units*') ||  route::is('departments*') || route::is('users*') ||  route::is('roles*') || route::is('items_basic_informations') || route::is('main_categories*') || route::is('sub_categories*') || route::is('end_categories*') ? 'm-menu__item--open m-menu__item--expanded' : ''}}" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><span class="m-menu__item-here"></span><i --}}
              <li class="m-menu__item {{ route::is('requested_items*')  ? 'm-menu__item--active' : ''}}" aria-haspopup="true" m-menu-link-redirect="1"><a href="{{route('requested_items.index')}}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">اجناس درخواست شده </span></span></a></li>
              <li class="m-menu__item {{ route::is('motamed/create')  ? 'm-menu__item--active' : ''}}" aria-haspopup="true" m-menu-link-redirect="1"><a href="{{route('motamed.create')}}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">{{trans('global.add_motamed')}}</span></span></a></li>
              <li class="m-menu__item {{ route::is('motamed/create')  ? 'm-menu__item--active' : ''}}" aria-haspopup="true" m-menu-link-redirect="1"><a href="{{route('motamed_type.index')}}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">{{trans('global.motamed_type')}}</span></span></a></li>
              @if($status !=1)
              <li class="m-menu__item {{ route::is('users*') || route::is('roles*') ? 'm-menu__item--active' : ''}}" aria-haspopup="true" m-menu-link-redirect="1"><a href="{{route('users.index')}}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">یوزر ها</span></a></li>
              @endif
              <li class="m-menu__item {{route::is('contractor_company*')  ? 'm-menu__item--active' : ''}}" aria-haspopup="true" m-menu-link-redirect="1"><a href="{{route('contractor_company.index')}}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">شرکت های قراردادی</span></a></li>
              <li class="m-menu__item {{route::is('contract_type*')  ? 'm-menu__item--active' : ''}}" aria-haspopup="true" m-menu-link-redirect="1"><a href="{{route('contract_type.index')}}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">نوعیت قراداد</span></a></li>
          </ul>
        </div>
      </li>

    </ul>
  </div>

  <!-- END: Aside Menu -->
</div>
