<header id="m_header" class="m-grid__item    m-header " m-minimize-offset="200" m-minimize-mobile-offset="200">
  <div class="m-container m-container--fluid m-container--full-height no-print">
    <div class="m-stack m-stack--ver m-stack--desktop ">

      <!-- BEGIN: Brand -->
      <div class="m-stack__item m-brand  m-brand--skin-dark pl-0">
        <div class="m-stack m-stack--ver m-stack--general">
          <div class="m-stack__item m-stack__item--middle m-brand__logo">
            <a href="home" class="m-brand__logo-wrapper">
              <img alt="" src="{{asset('public/images/logo5.png')}}" style="width: 180px;" />
            </a>
          </div>
          <div class="m-stack__item m-stack__item--middle m-brand__tools">

            <!-- BEGIN: Left Aside Minimize Toggle -->
            <a href="javascript:;" id="m_aside_left_minimize_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block  ">
              <span></span>
            </a>

            <!-- END -->

            <!-- BEGIN: Responsive Aside Left Menu Toggler -->
            <a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
              <span></span>
            </a>

            <!-- END -->

            <!-- BEGIN: Responsive Header Menu Toggler -->
            <a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
              <span></span>
            </a>

            <!-- END -->

            <!-- BEGIN: Topbar Toggler -->
            <a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
              <i class="flaticon-more"></i>
            </a>

            <!-- BEGIN: Topbar Toggler -->
          </div>
        </div>
      </div>

      <!-- END: Brand -->
      <div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">

        <!-- BEGIN: Horizontal Menu -->
        <button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark " id="m_aside_header_menu_mobile_close_btn"><i class="la la-close"></i></button>

        <!-- END: Horizontal Menu -->

        <!-- BEGIN: Topbar -->
        <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
          <div class="m-stack__item m-topbar__nav-wrapper">
            <ul class="m-topbar__nav m-nav m-nav--inline">
              <li class="m-nav__item m-dropdown m-dropdown--large m-dropdown--arrow m-dropdown--align-center m-dropdown--mobile-full-width m-dropdown--skin-light	m-list-search m-list-search--skin-light" m-dropdown-toggle="click" id="m_quicksearch"
               m-quicksearch-mode="dropdown" m-dropdown-persistent="1">
                <a href="#" class="m-nav__link m-dropdown__toggle">
                  <span class="m-nav__link-icon"><span class="m-nav__link-icon-wrapper"><i class="flaticon-search-1"></i></span></span>
                </a>
                <div class="m-dropdown__wrapper">
                  <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                  <div class="m-dropdown__inner ">
                    <div class="m-dropdown__header">
                      <form class="m-list-search__form">
                        <div class="m-list-search__form-wrapper">
                          <span class="m-list-search__form-input-wrapper">
                            <input id="m_quicksearch_input" autocomplete="off" type="text" name="q" class="m-list-search__form-input" value="" placeholder="جستجو...">
                          </span>
                          <span class="m-list-search__form-icon-close" id="m_quicksearch_close">
                            <i class="la la-remove"></i>
                          </span>
                        </div>
                      </form>
                    </div>
                    <div class="m-dropdown__body">
                      <div class="m-dropdown__scrollable m-scrollable" data-scrollable="true" data-height="300" data-mobile-height="200">
                        <div class="m-dropdown__content">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
              <li class="m-nav__item m-topbar__notifications m-dropdown m-dropdown--large m-dropdown--arrow m-dropdown--align-center 	m-dropdown--mobile-full-width" m-dropdown-toggle="click" m-dropdown-persistent="1">
                <a href="#" class="m-nav__link m-dropdown__toggle" id="m_topbar_notification_icon">
                  <span class="m-nav__link-icon">
                    <span class="m-nav__link-icon-wrapper"><i class="flaticon-alarm"></i></span>
                    <span class="m-nav__link-badge m-badge m-badge--danger">3</span>
                  </span>
                </a>
                <div class="m-dropdown__wrapper">
                  <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                  <div class="m-dropdown__inner">
                    <div class="m-dropdown__header m--align-center">
                      <span class="m-dropdown__header-title">پیام ها</span>
                      <span class="m-dropdown__header-subtitle">پیام های استفاده کننده گان</span>
                    </div>
                    {{-- <div class="m-dropdown__body">
                      <div class="m-dropdown__content">
                        <ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand" role="tablist">
                          <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link active" data-toggle="tab" href="#topbar_notifications_notifications" role="tab">
                              Alerts
                            </a>
                          </li>
                          <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link" data-toggle="tab" href="#topbar_notifications_events" role="tab">Events</a>
                          </li>
                          <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link" data-toggle="tab" href="#topbar_notifications_logs" role="tab">Logs</a>
                          </li>
                        </ul>
                        <div class="tab-content">
                          <div class="tab-pane active" id="topbar_notifications_notifications" role="tabpanel">
                            <div class="m-scrollable" data-scrollable="true" data-height="250" data-mobile-height="200">
                              <div class="m-list-timeline m-list-timeline--skin-light">
                                <div class="m-list-timeline__items">
                                  <div class="m-list-timeline__item">
                                    <span class="m-list-timeline__badge -m-list-timeline__badge--state-success"></span>
                                    <span class="m-list-timeline__text">12 new users registered</span>
                                    <span class="m-list-timeline__time">Just now</span>
                                  </div>
                                  <div class="m-list-timeline__item">
                                    <span class="m-list-timeline__badge"></span>
                                    <span class="m-list-timeline__text">System shutdown <span class="m-badge m-badge--success m-badge--wide">pending</span></span>
                                    <span class="m-list-timeline__time">14 mins</span>
                                  </div>
                                  <div class="m-list-timeline__item">
                                    <span class="m-list-timeline__badge"></span>
                                    <span class="m-list-timeline__text">New invoice received</span>
                                    <span class="m-list-timeline__time">20 mins</span>
                                  </div>
                                  <div class="m-list-timeline__item">
                                    <span class="m-list-timeline__badge"></span>
                                    <span class="m-list-timeline__text">DB overloaded 80% <span class="m-badge m-badge--info m-badge--wide">settled</span></span>
                                    <span class="m-list-timeline__time">1 hr</span>
                                  </div>
                                  <div class="m-list-timeline__item">
                                    <span class="m-list-timeline__badge"></span>
                                    <span class="m-list-timeline__text">System error - <a href="#" class="m-link">Check</a></span>
                                    <span class="m-list-timeline__time">2 hrs</span>
                                  </div>
                                  <div class="m-list-timeline__item m-list-timeline__item--read">
                                    <span class="m-list-timeline__badge"></span>
                                    <span href="" class="m-list-timeline__text">New order received <span class="m-badge m-badge--danger m-badge--wide">urgent</span></span>
                                    <span class="m-list-timeline__time">7 hrs</span>
                                  </div>
                                  <div class="m-list-timeline__item m-list-timeline__item--read">
                                    <span class="m-list-timeline__badge"></span>
                                    <span class="m-list-timeline__text">Production server down</span>
                                    <span class="m-list-timeline__time">3 hrs</span>
                                  </div>
                                  <div class="m-list-timeline__item">
                                    <span class="m-list-timeline__badge"></span>
                                    <span class="m-list-timeline__text">Production server up</span>
                                    <span class="m-list-timeline__time">5 hrs</span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="tab-pane" id="topbar_notifications_events" role="tabpanel">
                            <div class="m-scrollable" data-scrollable="true" data-height="250" data-mobile-height="200">
                              <div class="m-list-timeline m-list-timeline--skin-light">
                                <div class="m-list-timeline__items">
                                  <div class="m-list-timeline__item">
                                    <span class="m-list-timeline__badge m-list-timeline__badge--state1-success"></span>
                                    <a href="" class="m-list-timeline__text">New order received</a>
                                    <span class="m-list-timeline__time">Just now</span>
                                  </div>
                                  <div class="m-list-timeline__item">
                                    <span class="m-list-timeline__badge m-list-timeline__badge--state1-danger"></span>
                                    <a href="" class="m-list-timeline__text">New invoice received</a>
                                    <span class="m-list-timeline__time">20 mins</span>
                                  </div>
                                  <div class="m-list-timeline__item">
                                    <span class="m-list-timeline__badge m-list-timeline__badge--state1-success"></span>
                                    <a href="" class="m-list-timeline__text">Production server up</a>
                                    <span class="m-list-timeline__time">5 hrs</span>
                                  </div>
                                  <div class="m-list-timeline__item">
                                    <span class="m-list-timeline__badge m-list-timeline__badge--state1-info"></span>
                                    <a href="" class="m-list-timeline__text">New order received</a>
                                    <span class="m-list-timeline__time">7 hrs</span>
                                  </div>
                                  <div class="m-list-timeline__item">
                                    <span class="m-list-timeline__badge m-list-timeline__badge--state1-info"></span>
                                    <a href="" class="m-list-timeline__text">System shutdown</a>
                                    <span class="m-list-timeline__time">11 mins</span>
                                  </div>
                                  <div class="m-list-timeline__item">
                                    <span class="m-list-timeline__badge m-list-timeline__badge--state1-info"></span>
                                    <a href="" class="m-list-timeline__text">Production server down</a>
                                    <span class="m-list-timeline__time">3 hrs</span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="tab-pane" id="topbar_notifications_logs" role="tabpanel">
                            <div class="m-stack m-stack--ver m-stack--general" style="min-height: 180px;">
                              <div class="m-stack__item m-stack__item--center m-stack__item--middle">
                                <span class="">All caught up!<br>No new logs.</span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div> --}}
                  </div>
                </div>
              </li>
              <li class="m-nav__item m-topbar__quick-actions m-dropdown m-dropdown--skin-light m-dropdown--large m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push m-dropdown--mobile-full-width m-dropdown--skin-light" m-dropdown-toggle="click">
                <a href="#" class="m-nav__link m-dropdown__toggle">
                  <span class="m-nav__link-icon">
                    <span class="m-nav__link-icon-wrapper"><i class="flaticon-share"></i></span>
                    <span class="m-nav__link-badge m-badge m-badge--accent">2</span>
                  </span>
                </a>
                <div class="m-dropdown__wrapper">
                  <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                  <div class="m-dropdown__inner">
                    <div class="m-dropdown__header m--align-center">
                      <span class="m-dropdown__header-title">دسترسی سریع</span>
                      <span class="m-dropdown__header-subtitle">شارت کت</span>
                    </div>
                    <div class="m-dropdown__body m-dropdown__body--paddingless">
                      <div class="m-dropdown__content">
                        <div class="m-scrollable" data-scrollable="false" data-height="380" data-mobile-height="200">
                          <div class="m-nav-grid m-nav-grid--skin-light">
                            <div class="m-nav-grid__row">
                              <a href="{{route('item_details.index')}}" class="m-nav-grid__item">
                                <i class="m-nav-grid__icon flaticon-file"></i>
                                <span class="m-nav-grid__text">معلومات اساسی اجناس</span>
                              </a>
                              <a href="{{route('allotments.get_employees_allotments')}}" class="m-nav-grid__item">
                                <i class="m-nav-grid__icon flaticon-layer"></i>
                                <span class="m-nav-grid__text">{{ trans('global.employee_allotments') }}</span>
                              </a>
                            </div>
                            {{-- <div class="m-nav-grid__row">
                              <a href="#" class="m-nav-grid__item">
                                <i class="m-nav-grid__icon flaticon-folder"></i>
                                <span class="m-nav-grid__text">Create New Task</span>
                              </a>
                              <a href="#" class="m-nav-grid__item">
                                <i class="m-nav-grid__icon flaticon-clipboard"></i>
                                <span class="m-nav-grid__text">Completed Tasks</span>
                              </a>
                            </div> --}}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
              <li class="m-nav__item m-topbar__user-profile  m-dropdown m-dropdown--medium m-dropdown--arrow  m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" m-dropdown-toggle="click">
                <a href="#" class="m-nav__link m-dropdown__toggle">
                  <span class="m-topbar__userpic">
                    <img src="{{asset('public/images/user1.jpg')}}" class="m--img-rounded m--marginless m--img-centered" alt="" />
                  </span>
                  <span class="m-nav__link-icon m-topbar__usericon  m--hide">
                    <span class="m-nav__link-icon-wrapper"><i class="flaticon-user-ok"></i></span>
                  </span>
                  <span class="m-topbar__username m--hide">Nick</span>
                </a>
                <div class="m-dropdown__wrapper">
                  <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                  <div class="m-dropdown__inner">
                    <div class="m-dropdown__header m--align-center">
                      <div class="m-card-user m-card-user--skin-light">
                        <div class="m-card-user__pic">
                          <img src="{{asset('public/images/user1.jpg')}}" class="m--img-rounded m--marginless" alt="" />
                        </div>
                        <div class="m-card-user__details">
                          <span class="m-card-user__name m--font-weight-500">{{ isset(Auth::user()->employee) ? Auth::user()->employee->name_dr : '' }} </span>
                          <a href="" class="m-card-user__email m--font-weight-300 m-link"></a>{{ Auth::user()->email }} </a>
                        </div>
                      </div>
                    </div>
                    <div class="m-dropdown__body">
                      <div class="m-dropdown__content">
                        <ul class="m-nav m-nav--skin-light">
                          <li class="m-nav__section m--hide">
                            <span class="m-nav__section-text">Section</span>
                          </li>
                          <li class="m-nav__separator m-nav__separator--fit">
                            <a href="http://localhost/president_new_website/cms/edti_user" 
                            class="btn m-btn--pill  float-right   btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder"
                            data-toggle="modal" data-target="#m_modal_4">Edit Account</a>
                          </li>
                          <li class="m-nav__item">
                            <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();" class="btn m-btn--pill    btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder"> {{ __('Logout') }}</a>
                          
                        </li>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                           
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>
        
        <!-- END: Topbar -->
      </div>
    </div>
  </div>

</header>

<div class="modal fade" id="m_modal_4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div id="test123" class="modal-content-2">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">تغیر رمز</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="m-alert m-alert--outline alert alert-success alert-dismissible fade show" role="alert" v-if="success">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          </button>
          <strong>رمز شما موفقانه تغییر نمود</strong> 
        </div>
        <form>
          <div class="form-group" :class="err_pass ? 'has-danger':''">
            <label for="recipient-name" class="form-control-label">رمز فعلی:</label>
            <input type="password" class="form-control" v-model="password">
            <div id="password" class="form-control-feedback" v-if="err_pass">رمز فعلی تان اشتباه است</div>
          </div>
          <div class="form-group">
            <label for="recipient-name" class="form-control-label">رمز جدید:</label>
            <input type="password" class="form-control" v-model="new_password">
          </div>
          <div class="form-group" :class="err_new_pass ? 'has-danger': ''">
            <label for="recipient-name" class="form-control-label">تکرار رمز جدید:</label>
            <input type="password" class="form-control" v-model="conf_password">
            <div id="password-error" class="form-control-feedback" v-if="err_new_pass">رمز جدید تان باهم مطابقت نمی کند</div>
          </div>
      
        </form>
      </div>
      <div class="modal-footer">
       

        <button type="button" class="btn btn-primary" @click="checkPassword()">ذخیره </button>
      </div>
    </div>
  </div>
</div>



@push('custom-js')

<script>

  Vue.component('v-select', VueSelect.VueSelect);

  item_app = new Vue({
      el: '#m_modal_4',
      data: {
          password :'',
          new_password :'',
          conf_password :'',
          err_new_pass: false,
          err_pass: false,
          success: false,
      },

      methods: {
    
            
         
      checkPassword(){

        if(this.checkNewPassword()){
          axios.post(`{{route("check_password")}}`,{
                          'password' : this.password,
                          'new_password' : this.new_password,
                          'conf_password' : this.conf_password,
                          
                          })           
                  .then(res => {
                    if(res.data ==1){
                      this.success = true
                      this.err_pass
                      setTimeout(function() {
                          
                            $('#m_modal_4').modal('toggle')
                          
                      }, 500)
                    
                    }else if(res.data == 0){
                      this.err_pass =true
                    }
                      // vm.items = res.data.items;                     
                      
                  })
                  .catch(err => {
                      console.log(err);
                  });
        }else{
          this.err_new_pass = true;
        }

      
                  
      }
  ,
      checkNewPassword(){
        if(this.new_password === this.conf_password){
          this.err_new_pass = false
          return true
        }else{
          return false
        }   
      }

    
             
          
      },
  });



</script>
@endpush
