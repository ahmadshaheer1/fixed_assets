<script>
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
  // persian date picker
$(document).ready(function() {
$(".persian_date").persianDatepicker();
});


$(document).ready(function() {
  $('.jquery-datatable').DataTable( {
          "language": {
              "sEmptyTable":     "هیچ داده ای در جدول وجود ندارد",
              "sInfo":           "نمایش _START_ تا _END_ از _TOTAL_ مورد",
              "sInfoEmpty":      "نمایش 0 تا 0 از 0 مورد",
              "sInfoFiltered":   "(فیلتر شده از _MAX_ مورد)",
              "sInfoPostFix":    "",
              "sInfoThousands":  ",",
              "sLengthMenu":     "نمایش _MENU_ مورد",
              "sLoadingRecords": "در حال نمایش...",
              "sProcessing":     "در حال نمایش...",
              "sSearch":         "جستجو:",
              "sZeroRecords":    "موردی با این مشخصات پیدا نشد",
              "oPaginate": {
                  "sFirst":    "ابتدا",
                  "sLast":     "انتها",
                  "sNext":     "بعدی",
                  "sPrevious": "قبلی"
              },
              "oAria": {
                  "sSortAscending":  ": فعال سازی نمایش به صورت صعودی",
                  "sSortDescending": ": فعال سازی نمایش به صورت نزولی"
              }
          }
      });

} );

    // Delec A Record
function deleteRecord(serverURL,rowId)
{

  swal({
      title: "ایا شما مطمین هستید ؟",
      text: "شما قادر به دریافت این معلومات نمیباشد.",
      type: "warning",
      showCancelButton: !0,
      confirmButtonText: "حذف",
      cancelButtonText: "لغو !",
      reverseButtons: !0
  }).then(function(e) {
      if(e.value)
      {
        $.ajax({
            url: serverURL,
            method: 'DELETE',
            success: function(result) {
              $('#'+rowId).slideUp();
              swal("حذف شد",''+result.message+'', "success");
            }
        });
      }
  })
}

$(function () {
  $('.select2').each(function () {
    $(this).select2({
      // theme: 'bootstrap4',
      // width: 'style',
      // height: '100%',
      // placeholder: $(this).attr('placeholder'),
      // allowClear: Boolean($(this).data('allow-clear')),
    });
  });
});

function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
}

tinymce.init({
    selector: '.text-editor',
    height: 500,
    plugins: 'print preview fullpage searchreplace autolink directionality  visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount   imagetools  contextmenu colorpicker textpattern help',
    toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
    // image_advtab: true,
    templates: [
      { title: 'Test template 1', content: 'Test 1' },
      { title: 'Test template 2', content: 'Test 2' }
    ],
    content_css: [
      '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
      '//www.tinymce.com/css/codepen.min.css'
    ]
});


</script>
