<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>@yield('title') - سیستم اجناس</title>
    <meta name="description" content="Fixed Assets System">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

    <link href="{{asset('public/assets/vendors/base/vendors.bundle.rtl.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('public/assets/demo/demo12/base/style.bundle.rtl.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('public/css/select2.min.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{asset('public/css/select2.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('public/css/persianDatepicker-default.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('public/css/jquery.fancybox.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('public/css/select2-bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    {{-- vue select style --}}
    <link rel="stylesheet" href="{{asset('public/css/vue-select.css')}}">





    <link rel="shortcut icon" href="{{asset('public/assets/demo/demo12/media/img/logo/favicon.ico')}}" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <style>
        @font-face {
            font-family: 'persian_font';
            src:url("{{asset('public/fonts/aop_font.ttf')}}");
        }

        html,
        body,
        a,
        button.btn,
        table,
        th,
        td,
        h3,
        input,
        select,
        textarea,
        label {
            font-family: persian_font !important;
        }

        .m-radio>input:checked~span::after,
        .m-checkbox>input:checked~span::after {
            transform: rotate(40deg);
        }

        .m-invoice-2 .m-invoice__wrapper .m-invoice__body table tbody tr td:not(:first-child) {
            text-align: right !important;
        }

        .details {
            display: none;
        }
    </style>
    @include('layouts.partials._custom_css')
    @stack('custom-css')
</head>


<body
    class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

    <div class="m-grid m-grid--hor m-grid--root m-page">

        @include('layouts.partials.header')

        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body"><button
                class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i
                    class="la la-close"></i></button>
            @include('layouts.partials.sidebar')
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                <div class="m-content">
                    {{-- ajax submit success message --}}
                    <div class="m-alert m-alert--icon m-alert--outline alert alert-success alert-dismissible fade show"
                        id="ajax-success" style="display:none;" role="alert">
                        <div class="m-alert__icon">
                            <i class="la la-check"></i>
                        </div>
                        <div class="m-alert__text">
                            {{trans('global.create_successful')}}
                        </div>
                        <div class="m-alert__close">
                            <button style="line-height: 3;" type="button" class="close" data-dismiss="alert"
                                aria-label="Close">
                            </button>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                    <div class="m-alert m-alert--icon m-alert--outline alert alert-success alert-dismissible fade show"
                        id="success" role="alert">
                        <div class="m-alert__icon">
                            <i class="la la-check"></i>
                        </div>
                        <div class="m-alert__text">
                            {{ $message }}
                        </div>
                        <div class="m-alert__close">
                            <button style="line-height: 3;" type="button" class="close" data-dismiss="alert"
                                aria-label="Close">
                            </button>
                        </div>
                    </div>
                    @endif

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="m-portlet m-portlet--last m-portlet--head-lg m-portlet--responsive-mobile"
                                id="main_portlet">
                                <div class="m-portlet__head no-print">
                                    <div class="m-portlet__head-progress">

                                    </div>
                                    <div class="m-portlet__head-wrapper">
                                        <div class="m-portlet__head-caption">
                                            <div class="m-portlet__head-title">
                                                {{-- <span class="m-portlet__head-icon">
                                                        <i class="flaticon-map-location"></i>
                                                    </span> --}}
                                                <h3 class="m-portlet__head-text">
                                                    @yield('title')
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="m-portlet__head-tools" style="">
                                            <div class="create mr-2">
                                                @stack('create-button')

                                            </div>
                                            @php
                                            $segment1 = Request::segment(1);

                                            @endphp
                                            @foreach (getTabsForMenu(Request::route()->getName()) as $item)
                                            {{-- {{ $item['route'] == URL::current()}} --}}

                                            <a href="{{$item['route']}}"
                                                class="btn btn-outline-info m-1 m-btn--wide {{ $item['route'] == URL::current() ?'active' :''}}">{{$item['title']}}
                                                &nbsp;&nbsp; <i class="la la-arrow-left"></i></a>
                                            @endforeach

                                            @if (Route::is('*create') || Route::is('*edit') || Route::is('*show'))
                                            <a href="{{ URL::previous()}}" class="btn btn-secondary m-btn m-btn--custom"
                                                style="direction:ltr;">
                                                <span>
                                                    <i class="la la-reply"></i>
                                                    <span>بازگشت</span>
                                                </span>
                                            </a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="m-portlet__body">
                                    @yield('content')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.partials.footer')
    </div>
    {{-- @include('layouts.partials.quick_sidebar') --}}

    <div id="m_scroll_top" class="m-scroll-top">
        <i class="la la-arrow-up"></i>
    </div>

    <script src="{{asset('public/assets/vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/assets/demo/demo12/base/scripts.bundle.js')}}" type="text/javascript"></script>
    {{-- <script src="{{asset('public/assets/demo/default/custom/components/charts/morris-charts.js')}}"
    type="text/javascript"></script> --}}
    <script src="{{asset('public/js/datatable/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/js/datatable/dataTables.bootstrap4.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/js/datatable/dataTables.responsive.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/js/datatable/responsive.bootstrap4.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/js/persianDatepicker.js')}}"></script>
    <script src="{{asset('public/js/jquery.fancybox.min.js')}}"></script>

    <script src="{{asset('public/js/select2.min.js')}}" type="text/javascript"></script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.full.js" type="text/javascript"></script> --}}
    <script src="{{asset('public/js/tinymce/tinymce.min.js')}}" type="text/javascript"></script>

    <script src="{{asset('public/js/vue.js')}}"></script>
    <script src="{{asset('public/js/axios.min.js')}}"></script>


    {{-- Vue Select scripts --}}
    <!-- use the latest vue-select release -->
    <script src="{{asset('public/js/v-select/vue-select.js')}}"></script>
    <!-- or point to a specific vue-select release -->
    <script src="{{asset('public/js/v-select/lodash.min.js')}}"></script>




    @include('layouts.partials._custom_js')
    @stack('custom-js')

    <script>
        $( document ).on('click','.toggle_description',function() {
        // $('.toggle_description').click(function() {

            description = $(this).prev();
            description.toggle();
            if(description.css('display') == 'none') {
            $('.description').show();

            $(this).html('نمایش');
            $(this).removeClass('red').addClass('green');
            $(this).append('<i class="fa fa-plus"></i>');
            }
            else {
            $(this).html('پنهان');
            $('.description').hide();

            $(this).removeClass('blue').addClass('red');
                $(this).append('<i class="fa fa-times"></i>');
            // $(this).removeClass('btn-primary').addClass('btn-success');
        }

        });
    </script>
</body>

</html>
