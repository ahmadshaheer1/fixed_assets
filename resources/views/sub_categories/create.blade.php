
<div class="test" id="create_sub_categories">
    <div v-if="success" class="m-alert m-alert--icon m-alert--outline alert alert-success alert-dismissible fade show" role="alert">
        <div class="m-alert__icon">
            <i class="la la-check"></i>
        </div>
        <div class="m-alert__text">
            @{{message}}
        </div>
        <div class="m-alert__close">
            <button style="line-height: 3;" type="button" class="close" data-dismiss="alert" aria-label="Close">
            </button>
        </div>
    </div>
    <form class="m-form m-form--fit m-form--label-align-right sub_category_form"  @submit.prevent="createSubCategory">
        @csrf
        <div class="m-portlet__body">

            <div v-if="error" class="form-group m-form__group has-danger">
                    <div  class="form-control-feedback ">@{{error_messages.main_category_message}}</div>
            </div>

            <div :class="['form-group m-form__group', error_messages.{{$name}} ? 'has-danger': '']" id="name_dr">
                <label for="main_category_name">{{trans('global.name')}}</label>
                <input name="{{$name}}" v-model="{{$name}}" type="text" class="form-control m-input m-input--square" id="main_category_name" aria-describedby="emailHelp" placeholder="{{trans('global.name')}}">
                <div v-if="error" class="form-control-feedback">@{{error_messages.{!! $name !!}}}</div>
            </div>
            <div class="form-group m-form__group">
                <label for="main_category_description">{{trans('global.description')}}</label>
                <textarea name="description" v-model="description" class="form-control" id="main_category_description"placeholder="{{trans('global.description')}}">{{old('description')}}</textarea>
            </div>
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">
            <div class="m-form__actions">
                <button type="submit" class="btn btn-primary">{{trans('global.submit')}}</button>
                <button type="reset" class="btn btn-danger">{{trans('global.cancel')}}</button>
            </div>
        </div>
    </form>

</div>
<script>
        sub_category_app = new Vue({
            el: '#create_sub_categories',
            data: {
                main_category_id: {!! json_encode($data['main_category_id'], 1) !!},
                {{$name}}: '',
                description: '',

                error: false,

                error_messages: [

                ],

                success: false,
            },
            methods: {
                createSubCategory(event) {
                    if(this.error == false) {
                        axios.post("{{route('sub_categories.store')}}", {
                            "main_category_id": this.main_category_id,
                            "description": this.description,
                            "{{$name}}": this.{{$name}},
                        })
                        .then(res => {
                            this.success =  true;
                            this.message =  res.data.success;
                            $('#exampleModal').modal('hide')
                        })
                        .catch(err => {
                            this.error = true;
                            this.error_messages = err.response.data.errors;
                        })

                    }
                }
            },
            beforeMount() {
                if(typeof this.main_category_id != 'number'){
                    this.error = true;
                    this.error_messages.main_category_message = 'کتگوری اصلی باید از فورم قبلی انتخاب گردد!!!';
                }
            }
        });
    // $('.sub_category_form').submit(function(e) {

    //     e.preventDefault();
    //     $.ajax({
    //         method: "POST",
    //         data: $('.sub_category_form').serialize(),
    //         url: "{{route('sub_categories.store')}}",
    //         success: function(resp) {
    //             // $('.'+key).removeClass('has-danger');
    //             // $('.form-control-feedback').remove();
    //           $('#exampleModal').modal('hide');
    //           $("#ajax-success").show();
    //           $("#ajax-success").delay(2000).fadeOut();

    //         },
    //         error: function(data) {
    //             $.each(data.responseJSON.errors, function(key, value) {

    //                 $('#'+key).addClass('has-danger');
    //                 $('#'+key+" .form-control-feedback").html(value);
    //             });


    //                 // $('#'+key).addClass('has-danger');
    //                 // $('#'+key+" .form-control-feedback").html(value);
    //             console.log(data.responseJSON.errors);

    //         },
    //     })

    // });
</script>
