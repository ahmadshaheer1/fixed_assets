<div id="edit_‌‌sub_category">
    <div v-if="success" class="m-alert m-alert--icon m-alert--outline alert alert-success alert-dismissible fade show" role="alert">
        <div class="m-alert__icon">
            <i class="la la-check"></i>
        </div>
        <div class="m-alert__text">
            @{{message}}
        </div>
        <div class="m-alert__close">
            <button style="line-height: 3;" type="button" class="close" data-dismiss="alert" aria-label="Close">
            </button>
        </div>
    </div>
    <form class="m-form m-form--fit m-form--label-align-right" method="POST" @submit.prevent="editSubCategory">
        @csrf
        @method('PUT')

        <div :class="['form-group m-form__group', error_messages.main_category_id ? 'has-danger': '']">
                <label for="sub_category">{{trans('global.main_category')}}</label>
                <select class="custom-select form-control" name="main_category_id" v-model="main_category_id">
                    <option>{{trans('global.main_category')}}</option>

                        {{-- <option v-for="item in main_categories" :value="item.id">@{{item.name_dr}}</option> --}}
                        <option v-for="item in main_categories" :value="item.id" >@{{item.name_dr}}</option>

                </select>
                <div class="form-control-feedback">@{{error_messages.main_category_id}}</div>
        </div>
        <div class="m-portlet__body">
            <div :class="['form-group m-form__group', error_messages.name_dr ? 'has-danger': '']">
                <label for="sub_category_name">{{trans('global.name')}}</label>
                <input v-model="sub_category" type="text" class="form-control m-input m-input--square" id="sub_category_name" aria-describedby="emailHelp" placeholder="{{trans('global.name')}}">

                <div class="form-control-feedback">@{{error_messages.name_dr}}</div>
            </div>
            <div class="form-group m-form__group">
                <label for="sub_category_description">{{trans('global.description')}}</label>
                <textarea name="description" class="form-control" id="sub_category_description" v-model='description' placeholder="{{trans('global.description')}}"></textarea>
            </div>
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">
            <div class="m-form__actions">
                <button type="submit" class="btn btn-primary">{{trans('global.submit')}}</button>
                <button type="reset" class="btn btn-danger">{{trans('global.cancel')}}</button>
            </div>
        </div>
    </form>
</div>
<script>
        edit_‌‌sub_category=new Vue({
             el: "#edit_‌‌sub_category",
             data: {
               category_id: {{$sub_category->id}},
               main_category_id: {{$sub_category->main_category_id}},
               main_categories: JSON.parse('{!!json_encode($main_categories, 1)!!}'),
               sub_category: '{{$sub_category->$name}}',
               description: '{{$sub_category->description}}',
               error: false,
               error_messages: [],
               success: false,

             },
             methods: {
              editSubCategory(event){

                 axios.put("{{url('sub_categories')}}/"+this.category_id, {
                   "{{$name}}": this.sub_category,
                   'main_category_id': this.main_category_id,
                   'description': this.description,

                 })
                 .then(res => {
                   this.success = true;
                   this.message = res.data.success;
                   $('#exampleModal').modal('hide')


                 })
                 .catch(err => {
                   console.log(err.data);
                   this.error = true;
                   this.error_messages = err.response.data.errors;
                 })
               }


             }
           });
</script>
