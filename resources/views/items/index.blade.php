@extends('layouts.master')
@section('title','صفحه اصلی')
@section('content')
  <div class="m-portlet m-portlet--tab">
      {{-- <div class="m-portlet__head">
          <div class="m-portlet__head-caption">
              <div class="m-portlet__head-title">
                  <span class="m-portlet__head-icon m--hide">
                      <i class="la la-gear"></i>
                  </span>
                  <h3 class="m-portlet__head-text">
                      Pie Chart
                  </h3>
              </div>
          </div>
      </div> --}}
      <div class="m-portlet__body">
          <div id="m_morris_4" style="height:500px;"><svg height="500" version="1.1" width="978" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="overflow: hidden; position: relative; left: -0.59375px; top: -0.125px;"><desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.2.0</desc><defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><path fill="none" stroke="#0b62a4" d="M489.406,410A160,160,0,0,0,640.158398153737,303.60703732624245" stroke-width="2" opacity="0" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 0;"></path><path fill="#0b62a4" stroke="#ffffff" d="M489.406,413A163,163,0,0,0,642.9850056191196,304.6121692761095L710.8235847883012,328.73533607291864A235,235,0,0,1,489.406,485Z" stroke-width="3" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path fill="none" stroke="#3980b5" d="M640.158398153737,303.60703732624245A160,160,0,0,0,345.8434077979751,179.361610152587" stroke-width="2" opacity="0" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 0;"></path><path fill="#3980b5" stroke="#ffffff" d="M642.9850056191196,304.6121692761095A163,163,0,0,0,343.15160919418713,178.03714034294802L278.5484427032759,146.24986491161218A235,235,0,0,1,710.8235847883012,328.73533607291864Z" stroke-width="3" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path fill="none" stroke="#679dc6" d="M345.8434077979751,179.361610152587A160,160,0,0,0,489.35573451836956,409.99999210431656" stroke-width="2" opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 1;"></path><path fill="#679dc6" stroke="#ffffff" d="M343.15160919418713,178.03714034294802A163,163,0,0,0,489.354792040589,412.9999919562725L489.33060177755436,489.99998815647484A240,240,0,0,1,274.0621116969627,144.0424152288805Z" stroke-width="3" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="489.406" y="240" text-anchor="middle" font-family="&quot;Arial&quot;" font-size="15px" stroke="none" fill="#000000" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: Arial; font-size: 15px; font-weight: 800;" font-weight="800" transform="matrix(2.4189,0,0,2.4189,-694.8829,-352.5961)" stroke-width="0.4134114583333333"><tspan dy="5.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Mail-Order Sales</tspan></text><text x="489.406" y="260" text-anchor="middle" font-family="&quot;Arial&quot;" font-size="14px" stroke="none" fill="#000000" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: Arial; font-size: 14px;" transform="matrix(3.3333,0,0,3.3333,-1142.1849,-588)" stroke-width="0.3"><tspan dy="5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">20</tspan></text></svg></div>
      </div>
  </div>
@endsection
