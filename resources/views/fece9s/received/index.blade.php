@extends('layouts.master')
@section('title','ف س ۹ دریافت شده')
@section('content')
<div id="app">

    <table id="example" class="table table-striped table-bordered jquery-datatable" style="width:100%">
        <thead>
        <tr>
            <th>#</th>
            <th>شماره</th>
            <th>تاریخ</th>
            <th>ریاست</th>
            <th>توضیحات</th>
            <th>عملیات</th>
        </tr>
    </thead>
    <tbody>
        <tr v-for="(item, index) in fece9s" :id='item.id+`_tr`'>
            <td>@{{++index}}</td>
            <td>@{{item.number}}</td>
            <td>@{{item.date}}</td>
            <td>@{{item.department.name_dr}}</td>
            
            <td>@{{item.description}}</td>
            <td>
                <a :href="`{{url('fece9s')}}/`+item.id" title="نمایش ف س ۹"
                class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air">
                <i class="la la-eye"></i>
            </a>
            <a :href="`{{url('fece9s/received_show')}}/`+item.id" title="اضافه نمودن ف س ۵"
            class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air">
            <i class="la la-plus"></i>
        </a>
    </td>
</tr>
</table>
</div>

@endsection
@push('custom-js')
    <script>
        var app = new Vue({
            el: '#app',
            data: {

                fece9s:{!!$fece9s!!},

            },
            methods: {
               

            },
        })


    </script>

@endpush