@extends('layouts.master')
@section('title','ف س ۹ دریافت شده')
@section('content')

  <table id="example" class="table table-striped table-bordered jquery-datatable" style="width:100%">
        <thead>
            <tr>
                <th>#</th>
                <th>شماره</th>
                <th>تاریخ</th>
                <th>ریاست</th>
                <th>توضیحات</th>
                <th>عملیات</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($fece9s as $item)
            @php
            $i=1;
            @endphp
            <tr id='{{$item->id}}_tr'>
            <td>{{$i++}}</td>
            <td>{{$item->number}}</td>
            <td>{{$item->date}}</td>
            <td>{{$item->department->name_dr}}</td>

            <td>{{str_limit($item->description,50)}}</td>
            <td>
                    <a href="{{route('fece9s.show',$item->id)}}" title="نمایش ف س ۹" class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air">
                        <i class="la la-eye"></i>
                    </a>
                    <a href="{{route('fece9s.received_show',$item->id)}}" title="اضافه نمودن ف س ۵" class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air">
                        <i class="la la-plus"></i>
                    </a>



            </td>
            </tr>
        @endforeach
    </table>

@endsection
