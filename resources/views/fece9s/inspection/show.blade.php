@extends('layouts.master')
@section('title','نمایش ف س ۹')
@section('content')
<div class="m-portlet__body" id="inspection_show">
    <div class="container-fluid">
        <div class="row no-print mb-5 text-right">

            <div class="btn-group col-lg-6" role="group" aria-label="First group">
                <button type="button" class="m-btn btn btn-success" @click="approve()"><i
                        class="la la-check"></i></button>
                <button type="button" class="m-btn btn btn-primary" data-toggle="modal" data-target="#exampleModal"><i class="la la-edit"></i></button>
                <button type="button" class="m-btn btn btn-danger" @click="reject()" {{$fece9->inspection_approval == 3 ?'disabled' :''}}><i class="la la-close"></i></button>
            </div>
            <div class="col-lg-6" v-if="edit">

                <button type="button" class="btn btn-primary" @click="completeUpdate()">تائید <i class="fa fa-check"> </i></button>
            </div>


        </div>
        <div class="row">
            <div class="col-md-3">
                <p class="font-weight-bold">1. قرار شماره: {{$fece9->number}}</p>
                <p class="font-weight-bold">2. تاریخ: {{$fece9->date}}</p>
            </div>
            <div class="col-md-6">
                <h3 class="text-center">درخواست تحویلخانه</h3>
            </div>
            <div class="col-md-3">
                <p class="font-weight-bold">فورمه ف س ۹</p>
                <p class="font-weight-bold" style="white-space: nowrap;">3. اسم شعبه درخواست
                    دهنده:‌{{$fece9->department->name_dr}}</p>
            </div>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th>شماره اقلام </th>
                    <th>شماره ذخیره </th>
                    <th>تفصیلات جنسی خدمات </th>
                    <th>مقدار </th>
                    <th>واحد </th>
                    <th>معامله شد بحساب</th>
                </tr>
                <tr>
                    <th>5</th>
                    <th>6</th>
                    <th>7</th>
                    <th>8</th>
                    <th>9</th>
                    <th>10</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td></td>
                    <td></td>
                    <td>
                        <p class="font-weight-bold">
                            به معاونیت محترم خدماتی و حمایوی ریاست عمومی واحد عملیاتی و حمایوی مالی و اداری ریاست جمهوری
                            ا.ا
                        </p>
                        <p>
                            با تقدیم سلام و احترام!
                        </p>
                        <p>
                            {{$fece9->department->name_dr}} به اجناس ذیل نیاز دارد:
                        </p>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
           
                @foreach ($fece9->itemDetails as $index => $item)
                @php $total = getTotal($item->id); @endphp
                <tr>
                    <td></td>
                    <td></td>
                    <td>
                    
                        {{$item->name_dr}}
                        
                     
                            <span class="m-widget1__number m--font-info pl-4 lead font-weight-bold">
                                @php
                                    $contract_item = App\ContractItems::where('item_details_id',$item->id)->first();
                                @endphp
                                موجود در قرارداد : {{$contract_item->available_amount}} {{$item->unit->name_dr}}
                            </span>
                        
                    </td>
                    <td>
                      <ul class="list-group list-group-flush item-count">
                          <li class="list-group-item  {{$item->pivot->status == 0 ? 'border border-dark rounded-circle' : ''}} " id="list-group-item-{{$item->id}}">{{$item->pivot->quantity}}</li>
                      </ul>

                    </td>
                    <td>{{$item->unit->name_dr}}</td>
                    <td></td>
                </tr>

            @endforeach

                
                <tr>
                    <td></td>
                    <td></td>
                    <td>
                      
                        <p>
                            امید است در مورد تهیه و تدارک آن بالای شعبه مربوطه هدایت فرموده ممنون سازید
                        </p>
                        <p class="text-center">
                            {{$fece9->description}}
                        </p>
                        <p>
                            تایید است:
                        </p>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>

            </tbody>
            <tfoot>
                <tr>
                    <td colspan="5">
                        11. اقلام که در آن خط گرفته شده در تحویلخانه موجود نیست خریداری آن درخواست گردد.
                        <div class="row p-3">
                            <div class="col-md-2">
                                <hr class="pb-2">
                                آمر تحویلخانه
                            </div>
                            <div class="col-md-2">
                                <hr class="pb-2">
                                تاریخ
                            </div>
                        </div>
                    </td>
                    <td>
                        12. نمبر تکت توزیع
                    </td>
                </tr>

            </tfoot>

        </table>
        <tr>
            <div class="row">
                <div class="col-md-8">
                    <p>لطفا اجناس ذیل خدمات را فراهم نماید.</p>
                    <p>در یک اصل و دو کاپی ترتیب میشود.</p>
                    <p>الف:‌اصل آن به تحویلخانه حفظ میگردد.</p>
                </div>
                <div class="col-md-4">
                    4. اسم نمایندگی ________________________
                </div>
            </div>
        </tr>

        @if($fece9->processed_fece9_file)
        <div class="row no-print">
            <div class="col-md-3 mb-2 pl-0">
                <div class="update-image-wrapper">
                    <img class="w-100" src="{{asset($fece9->processed_fece9_file)}}" alt="" />

                    <div class="overlay">
                        <div class="text">
                            <a href="{{asset($fece9->processed_fece9_file)}}" data-fancybox
                                data-caption="{{$fece9->description}}">
                                نمایش
                            </a>



                            <form action="{{route('fece9s.replace_attached_file')}}" id="image_form" method="POST"
                                enctype="multipart/form-data">
                                @csrf
                                <input type="file" class="custom-file-input" id="image" multiple name="image"
                                    onchange="$('#image_form').submit();" ref="file">
                                <input type="hidden" id="fece9_id" name="id" value="{{$fece9->id}}">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
    <div class="modal fade" id="reject_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">رد ف س ۹</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form>
                    @csrf
                    <div class="modal-body">
                        <div class="form-group m-form__group">
                            <label for="exampleTextarea">دلیل رد</label>
                            <textarea class="form-control m-input" v-model="reject_remark" id="exampleTextarea" rows="3" name="reject_remark"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer float-right">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>
                        <button type="button" class="btn btn-primary" @click="saveReject()">ثبت</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" v-if="!edit" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">تصحیح اجناس</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card-body">
                        <div class="row">
                            <h5 class="col-lg-6">
                                نام جنس
                            </h5>
                            {{-- <h5 class="col-lg-3">
                                تعداد درخواست
                            </h5> --}}
                            <h5 class="col-lg-6">
                                تعداد تجدید 
                            </h5>
                        </div>
                        
                        <div class="form-group row" v-for="(item, index) in fece9_item_details  ">
                            <label class="col-lg-6 col-form-label ">@{{item.name}}</label>
                            {{-- <label class="col-lg-3 col-form-label ">@{{item.quantity}}</label> --}}
                            <div class="col-lg-6">
                                {{-- <input type="number" class="form-control" placeholder="" @mouseout="changeItem(item.pivot.id,$event.target.value)" /> --}}
                                <input type="number" class="form-control" v-model="item.quantity" v-on:keyup='validate($event,item.item_details_id)' placeholder="" :id="item.id" />
                            </div>
                        </div>
                      
                   
                       
                
            
                    </div>
                  <span v-if="error" class="text-danger">تعداد تجدید بیشتر از تعداد درخواست شده نمی تواند</span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">بستن</button>
                    <button type="button" class="btn btn-primary font-weight-bold" @click="submitChangesItem()" :disabled="error">ذخیره تغیرات</button>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@push('custom-css')
<style>
    .danger-input{
        border: 1px solid red;
    }
    thead th {
        border: 1px solid #777 !important;
        text-align: center;
    }

    tbody td {
        border-left: 1px solid #777;
        border-right: 1px solid #777;
        border-bottom: 0 !important;
        border-top: 0 !important;
    }


    tfoot td {
        border: 1px solid #777 !important;
    }

    hr {
        border-color: #777;
    }

    .list-group-item {
        border: none;
    }

    .item-count li {
        padding: 10px 15px;
    }

    .list-group.list-group-flush li {
        margin: 2px;
    }

    .item-description {
        height: 15cm;
        ;
    }

    @media print {
        table {
            /* height: 90vh; */
        }
    }

    input[name=image] {
        display: none;
    }

    /* image slide hover styles */

    .update-image-wrapper {
        position: relative;
        /* width: 50%; */
    }

    .image {
        display: block;
        width: 100%;
        height: auto;
    }

    .overlay {
        position: absolute;
        bottom: 0;
        left: 0;
        right: 0;
        background-color: #008cba5e;
        overflow: hidden;
        width: 100%;
        height: 0;
        transition: .5s ease;
    }

    .update-image-wrapper:hover .overlay {
        height: 100%;
    }

    .text {
        color: white;
        font-size: 20px;
        position: absolute;
        top: 50%;
        left: 50%;
        -webkit-transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        transform: translate(-50%, -50%);
        text-align: center;
    }

    .text a {
        background: #fff;
        padding: 5px 10px;
        margin: 3px;
        font-size: .7em;
    }

    .text a:hover {
        text-decoration: none;
    }
</style>
@endpush
@push('custom-js')
<script>
    item_app = new Vue({
            el: '#inspection_show',
            data: {

                reject_remark:'',
                id:'{{$fece9->id}}',
                item_details: {!!$fece9->itemDetails!!},
                item_details_main: {!!$fece9->itemDetails!!},
                fece9_item_details:[],
                edit: false,
                error: false,

            },
            methods: {

                approve(){

                    axios.post("{{url('fece9s/insepection_approved')}}", {
                        "fece9_id": {{$fece9->id}},
                        'fece9_item_details' : this.fece9_item_details
                    })
                    .then(res => {
                        window.location.href = "{!!route('fece9s.inspection_approval')!!}";

                    })
                    .catch(err => {
                        console.log(err);
                    });
                },
                edit(){
                    $('#exampleModal').modal('show');

                    // axios.post("{{url('fece9s/insepection_approved')}}", {
                    //     "fece9_id": {{$fece9->id}},
                    // })
                    // .then(res => {
                    //     window.location.href = "{{route('fece9s.inspection_approval')}}";
                    //     // $('.modal-content').html(res.data);
                    //     // $('#exampleModal').modal('show');
                    // })
                    // .catch(err => {
                    //     console.log(err);
                    // });

                },
                reject(){
                    //open reject model
                    $('#reject_model').modal('show');

                },
                saveReject(){
                    //save reject remark 
                        axios.post("{{route('fece9s.inspenction_reject_fece9')}}", {
                             "reject_remark": this.reject_remark,
                             "id": this.id,

                        })
                        .then(res => {
                            window.location.href = "{{route('fece9s.inspection_approval')}}";

                        })
                        .catch(err => {
                            console.log(err);

                        })

                

                 },
               
                 submitChangesItem(){

                    axios.post("{{route('fece9s.inspection_update')}}",
                                { 
                                    'fece9_item_details' : this.fece9_item_details
                                })
                    .then(res=> {
                        // window.location.reload();
                        $('#exampleModal').modal('toggle');
                        this.edit = true
                    })
                    .catch(err => {
                            console.log(err);

                        })


                 },
                 completeUpdate(){
                     axios.post("{{route('fece9s.inspection_update')}}",
                        {
                            'inspection_type': 2,
                            'fece9_id' : this.id
                        })
                     .then(res => {
                        window.location.href = "{{route('fece9s.inspection_approval')}}";
                     })
                     .catch(err => {
                         console.log(err);
                     })
                 },
                 validate(event,id){
                     var main_item = this.item_details_main.find(item => item.id == id);
                     var item_quantity = main_item['pivot']['quantity'];
                     var id = event.target.id;
                     var value = event.target.value;

                     if(value > item_quantity){

                         $('#'+id).addClass('danger-input');
                        this.error = true;

                     }else{

                        this.error = false;
                        $('#'+id).removeClass('danger-input');
                        
                     }
                 }
           
        },
        mounted() {

            for(i=0;i<this.item_details.length;i++){
                var object = {};
                object['id'] = this.item_details[i]['pivot']['id'];
                object['name'] = this.item_details[i]['pivot']['name'];
                object['quantity'] = this.item_details[i]['pivot']['quantity'];
                object['unit'] = this.item_details[i]['unit']['name_dr'];
                object['item_details_id'] = this.item_details[i]['id'];
                this.fece9_item_details.push(object);
            }

            
        },
        });


</script>
@endpush
