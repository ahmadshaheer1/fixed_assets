@extends('layouts.master')
@section('title','تاییدی هیئت معاینه')
@section('content')
<span id="inspection">
<ul class="nav nav-tabs" role="tablist">
    <li class="nav-item" @click="type = null">
        <a class="nav-link active" data-toggle="tab" href="#m_tabs_1_1">
            <i class="la la-angle-double-down"></i> دریافت شده
        </a>
    </li>
    <li class="nav-item"  @click="type = '1'">
        <a class="nav-link" data-toggle="tab" href="#m_tabs_1_2">
            <i class="la la-check"></i> تایید شده
        </a>
    </li>
    <li class="nav-item"  @click="type = '2'">
        <a class="nav-link" data-toggle="tab" href="#m_tabs_1_3">
            <i class="la la-edit"></i> تصحیح شده
        </a>
    </li>
    <li class="nav-item"  @click="type = '3'">
        <a class="nav-link" data-toggle="tab" href="#m_tabs_1_4">
            <i class="la la-close"></i> مسترد شده
        </a>
    </li>
</ul>
<div class="tab-content" id="inspection">
    <div class="tab-pane active" id="m_tabs_1_1" role="tabpanel">
        <table id="example" class="table table-striped table-bordered jquery-datatable" style="width:100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>شماره</th>
                    <th>تاریخ</th>
                    <th>ریاست</th>
                    <th>نوعیت ف س ۹</th>
                    <th>توضیحات</th>
                    <th>عملیات</th>
                </tr>
            </thead>
            <tbody>
                {{-- @foreach ($fece9s->where('inspection_approval','=',null) as $item)
                @php
                $i=1;
                @endphp --}}
                <tr v-for="(item, index) in fece9s" v-if="item.inspection_approval == type">
                    <td>@{{++index}}</td>
                    <td>@{{item.number}}</td>
                    <td>@{{item.date}}</td>
                    <td>@{{item.department.name_dr}}</td>
                    <td>
                        @{{item.fece9_type === 1 ? 'ذخیروی' : 'غیر ذخیروی'}}
                        
                    </td>

                    <td>@{{item.description}}</td>
                    <td>
                        <a :href="`{{url('fece9s/insepection_show/')}}/`+item.id" title="نمایش ف س ۹"
                            class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air">
                            <i class="la la-eye"></i>
                        </a>
              



                    </td>
                </tr>
                {{-- @endforeach --}}
        </table>
    </div>
  
</div>
</span>

@endsection
@push('custom-js')
<script>
    item_app = new Vue({
            el: '#inspection',
            data: {

                fece9s: {!!$fece9s!!},
                type: null
               

            },
            methods: {

           
        },
        mounted() {


            
        },
        });


</script>
@endpush
