@extends('layouts.master')
@section('title', trans('global.edit_fece9'))
@section('content')
<form class="form" action="{{route('fece9s.update', $fece9->id)}}" method="POST">
    @csrf
    @method('PATCH')
    <div class="form-group m-form__group row ">
      <div class="form-group col-md-6 {{$errors->has('number') ? 'has-danger' : ''}}">
        <label for="fece9_number">{{trans('global.fece9_number')}}</label>
        <input type="text" name="number" value="{{$fece9->number}}" class="form-control m-input m-input--square" id="number" placeholder="نمبر ف س ۹ را وارید نماید.">
        @if($errors->has('number'))
            <div class="form-control-feedback">{{$errors->first('number')}}</div>
        @endif
      </div>

      <div class="form-group col-md-3 {{$errors->has('date') ? 'has-danger' : ''}}">
        <label for="inputPassword4">{{trans('global.fece9_date')}}</label>
         <div class="input-group date">
            <input type="text" name="date" value="{{$fece9->date}}" class="form-control persian_date" placeholder="تاریخ انتخاب کنید.">
            <div class="input-group-append">
              <span class="input-group-text">
                <i class="la la-calendar"></i>
              </span>
            </div>
        </div>
        @if($errors->has('date'))
          <div class="form-control-feedback">{{$errors->first('date')}}</div>
        @endif
      </div>
      <div class="form-group col-md-3 {{$errors->has('fece9_type') ? 'has-danger' : ''}}">
        <label for="inputPassword4">{{trans('global.fece9_type')}}</label>
            <select class="form-control" name="fece9_type">
                {{-- <option value="">{{trans('global.fece9_type')}}</option> --}}
                <option value="0" {{$fece9->fece9_type =='0' ? 'selected' :''}}>یومیه</option>
                <option value="1" {{$fece9->fece9_type =='1' ? 'selected' :''}}>ذخیروی</option>
                <option value="2" {{$fece9->fece9_type =='2' ? 'selected' :''}}>غیر ذخیروی</option>
                <option value="3" {{$fece9->fece9_type =='3' ? 'selected' :''}}>غیر مصرفی</option>
            </select>
        @if($errors->has('fece9_type'))
          <div class="form-control-feedback">{{$errors->first('fece9_type')}}</div>
        @endif
      </div>

    </div>
    <div class="form-group m-form__group row">
            <div class=" col-md-6 items_list {{$errors->has('list') ? 'has-danger' : ''}}">

                <div class="form-group">
                    <label for="inputState">{{trans('global.sub_category')}}</label>
                    <select id="sub_category_id" onchange="submitItemSpecifications()" name="sub_category_id" ref="item_details_id" class="form-control">
                        <option value="none" selected>یگ گزینه را انتخاب نماید .</option>
                        @foreach ($sub_category as $value)
                            <option value="{{$value->id}}"  {{old('sub_category_id') == $value->id ? 'selected' : ''}}>{{$value->name_dr}}</option>
                        @endforeach
                    </select>
                </div>


                </div>
        <div class="form-group col-md-6 {{$errors->has('department_id') ? 'has-danger' : ''}}">
            <label for="department">{{trans('global.requesting_department')}}</label>
            <select class="form-control select2" name="department_id" id="department">
                @foreach ($departments as $item)
                <option value="{{$item->id}}" {{$fece9->department_id == $item->id ? 'selected' : ''}}>{{$item->name_dr}}</option>
                @endforeach
            </select>
            @if($errors->has('department_id'))
                <div class="form-control-feedback">{{$errors->first('department_id')}}</div>
            @endif
        </div>
      </div>
      <div class="col-md-6" id="app">
            <div class="row items_list {{$errors->has('list') ? 'has-danger' : ''}}">
                <div class="form-group col-md-4">
                    <label for="item_details">{{trans('global.item')}}</label>
                    <select id="item_details" name="item" ref="item_details_id" class="form-control">
                        <option value="none" selected>یگ گزینه را انتخاب نماید.</option>
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label for="inputState">{{trans('global.quantity')}}</label>
                    <input type="number" ref="quantity" name="quantity" class="form-control" min="1" placeholder="{{trans('global.quantity')}}">
                </div>
                <div class="form-group col-md-4">
                        <button type="button" v-on:click="addToList()" id="add_to_list" class="btn btn-outline-brand btn-square add-btn position-absolute" style="bottom:0;">+ اضافه نمودن</button>
                </div>

                @if($errors->has('list'))
                    <div class="form-control-feedback">{{$errors->first('list')}}</div>
                @endif
            </div>
            <div class="row">
                <table class="table items_table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>جنس</th>
                            <th>مقدار</th>
                            <th>عملیات</th>
                        </tr>
                    </thead>
                    <tbody>
                        {{-- @foreach ($items_list as $index => $item)
                            <tr id="item_{{++$index}}">
                                <td>{{$index}}</td>
                                <td>{{$item['name']}}</td>
                                <td>{{$item['quantity']}}</td>
                                <td>
                                    <a href="javascript:void(0)" onclick="deleteItem({{$index}})" class="text-danger"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>

                        @endforeach --}}
                        <tr v-for="(item, index) in list">
                            <td>@{{index+1}}</td>
                            <td>@{{item.name}}</td>
                            <td>@{{item.quantity}}</td>
                            <td>
                                <a href="javascript:void(0)" v-on:click="removeFromList(index)" class="text-danger"><i class="fa fa-trash"></i></a>
                            </td>

                        </tr>
                    </tbody>
                </table>
                <input type="hidden" name="list" v-model="JSON.stringify(list)"  multiple>

            </div>
        </div>
      {{-- <input type="hidden" name="list" value='' multiple> --}}
      <input type="hidden" name="updated_by" value='{{auth()->user()->id}}' >
    <div class="form-group m-form__group row {{$errors->has('description') ? 'has-danger' : ''}}">
      <label for="inputAddress2">توضیحات</label>
      <textarea class="form-control" name="description" id="" rows="7">{{$fece9->description}}</textarea>
        @if($errors->has('description'))
            <div class="form-control-feedback">{{$errors->first('description')}}</div>
        @endif
    </div>

    <button type="submit" class="btn btn-primary">
        ذخیره
    </button>

    <a type="button" href="{{url('fece9/index')}}" class="btn btn-default">
        برگشت
    </a>

  </form>
@endsection
@push('custom-js')
    <script>
     
        // list = {!! $items_list ? json_encode($items_list, true) : '[]'  !!}
        // $(function() {
        //     $('input[name=list]').val(JSON.stringify(list));
        // })
        // console.log(list);
        // $('#add_to_list').click(function() {
        //     item = $('select[name=item] option:selected').val();
        //     name = $('select[name=item] option:selected').text();
        //     quantity = $('input[name=quantity]').val();
        //     if((item != 'none' && quantity != "")) {
        //         if(!(list.some(test => test.item_details_id == item))) {
        //             list.push({'item_details_id': item, 'name': name, 'quantity': quantity});
        //             $('.items_table > tbody:last-child').append('<tr id="item_'+list.length+'"><td>'+list.length+'</td><td>'+name+'</td><td>'+quantity+'</td><td><a href="javascript:void(0)" onclick="deleteItem('+list.length+')" class="text-danger"><i class="fa fa-trash"></i></a></td></tr>')
        //             $('select[name=item]').val('none').trigger('change');
        //             $('input[name=quantity]').val('');

        //             $('input[name=list]').val(JSON.stringify(list));

        //             $('select[name=item]').val('none').trigger('change');

        //             $('.items_list').removeClass('has-danger');
        //             $('.item_error').removeClass('d-block').addClass('d-none');
        //             $('.duplicate_item_error').removeClass('d-block').addClass('d-none');
        //         }
        //         else {
        //             $('.items_list').addClass('has-danger');
        //             $('.duplicate_item_error').removeClass('d-none').addClass('d-block');

        //         }
        //     }
        //     else {
        //         $('.items_list').addClass('has-danger');
        //         $('.item_error').removeClass('d-none').addClass('d-block');

        //     }
        //     // if(list.some(item => item.item_details_id != item)) {
        //     //     list.push({'item_details_id': item, 'quantity': quantity});
        //     //     console.log('hey');
        //     // }
        //     // $('.items_table > tbody:last-child').append('<tr id="item_'+list.length+'"><td>'+list.length+'</td><td>'+name+'</td><td>'+quantity+'</td><td><a href="javascript:void(0)" onclick="deleteItem('+list.length+')" class="text-danger"><i class="fa fa-trash"></i></a></td></tr>')

        //     // $('select[name=item]').val('none').trigger('change');
        //     // $('input[name=quantity]').val('');

        //     // $('input[name=list]').val(JSON.stringify(list));
        //     // console.log($('input[name=list]').val());

        // });

        // function deleteItem(id) {
        //     $('tr#item_'+id).remove();
        //     list.splice(id-1, 1);
        //     $('input[name=list]').val(JSON.stringify(list));
        //     console.log($('input[name=list]').val());

        // }
        var app = new Vue({
            el: '#app',
            data: {
                message: 'Hello Vue!',
                list: {!! $items_list ? json_encode($items_list, 1) : '[]'  !!}
            },
            methods: {
                addToList: function(event) {

                        id = this.$refs.item_details_id.value

                        quantity = this.$refs.quantity.value

                        name = this.$refs.item_details_id.options[this.$refs.item_details_id.options.selectedIndex].text

                        let itemInList = this.list.filter(item => item.item_details_id===id);
                        let isItemInList = itemInList.length > 0;

                        if (isItemInList === false && id != 'none' && quantity != "") {
                            this.list.push({item_details_id: id, name: name, quantity: quantity })
                        }
                },

                removeFromList: function(index) {
                    this.list.splice(index, 1);
                }
            },
        })

        $('#sub_category_id').change(function() {
            sub_category_id = $(this).val();
            getSubCategoryKeys(sub_category_id);
        })



        function getSubCategoryKeys(sub_category_id, data) {
            console.log(data);
            $.ajax({
                method:'GET',
                url: '{{url("get_sub_categories_select_options")}}/'+sub_category_id,
                data: {data},
                success: function(resp) {
                    $('#keys_response').html(resp);
                }
            })
        }

        function submitItemSpecifications() {
            col1 = $('#col1').val();
            col2 = $('#col2').val();
            col3 = $('#col3').val();
            col4 = $('#col4').val();
            col5 = $('#col5').val();
            col6 = $('#col6').val();
            col7 = $('#col7').val();
            col8 = $('#col8').val();
            col9 = $('#col9').val();
            col10 = $('#col10').val();
            col11 = $('#col11').val();
            col12 = $('#col12').val();
            sub_category_id = {!! old('sub_category_id') ? old('sub_category_id') : "$('#sub_category_id').val();" !!}

            $.ajax({
                method: 'get',
                url: "{{url('get_item_details_by_specifications')}}/"+sub_category_id,
                data: {'col1': col1, 'col2': col2, 'col3': col3, 'col4': col4,'col5': col5,'col6': col6,'col7': col7,'col8': col8
                ,'col9': col9,'col10': col10,'col11': col11 ,'col12': col12},
                success: function(resp) {
                    console.log(resp);
                    $('#item_details').html('');
                    $.each(resp, populateOptions)
                }
            })
            $('#modal_sub_category_id').val(sub_category_id);
            console.log(sub_category_id+" - "+col1+" - "+col2+" - "+col3+" - "+col4);
        }

        function populateOptions(index, item) {
            $('#item_details').append('<option value="'+item.id+'">'+item.name_dr+'</option>');
        }


        $(function() {
           {!! old('sub_category_id') ? "submitItemSpecifications(); getSubCategoryKeys(".old('sub_category_id').");" : '' !!}
        })
    </script>

@endpush
