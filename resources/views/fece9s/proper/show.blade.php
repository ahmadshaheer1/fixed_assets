@extends('layouts.master')
@section('title','نمایش ف س ۹')
@section('content')
<div class="m-portlet__body" id="app">

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                <p class="font-weight-bold">1. قرار شماره: {{$fece9->number}}</p>
                <p class="font-weight-bold">2. تاریخ: {{$fece9->date}}</p>
            </div>
            <div class="col-md-6">
                <h3 class="text-center">درخواست تحویلخانه</h3>
            </div>
            <div class="col-md-3">
                <p class="font-weight-bold">فورمه ف س ۹</p>
            <p class="font-weight-bold" style="white-space: nowrap;">3. اسم شعبه درخواست دهنده:‌{{$fece9->department->name_dr}}</p>
            </div>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th>شماره اقلام	</th>
                    <th>شماره ذخیره	</th>
                    <th>تفصیلات جنسی خدمات	</th>
                    <th>مقدار	</th>
                    <th>واحد	</th>
                    <th>معامله شد بحساب</th>
                </tr>
                <tr>
                    <th>5</th>
                    <th>6</th>
                    <th>7</th>
                    <th>8</th>
                    <th>9</th>
                    <th>10</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td></td>
                    <td></td>
                    <td>
                        <p class="font-weight-bold">
                            به معاونیت محترم خدماتی و حمایوی ریاست عمومی واحد عملیاتی و حمایوی مالی و اداری ریاست جمهوری ا.ا
                        </p>
                        <p>
                            با تقدیم سلام و احترام!
                        </p>
                        <p>
                            {{$fece9->department->name_dr}} به اجناس ذیل نیاز دارد:
                        </p>

                        <div class="form-group row " v-if="fece9.fece9_type == 0">
                            <div class="form-group col-md-6 m-form__group">
                                <label for="fece9_mahal_taslimi">انتخاب معتمد</label>
                                <select class="form-control employee"   @change="availableItem($event.target.value)" id="motamed_id">
                                    {{-- @foreach($available_items as $item) --}}
                                        <option selected="selected" disabled>انتخاب معتمد</option>
                                        <option  v-for="item in mounteds" :value="item.receiver_employee_id" >@{{item.employee.name_dr}}</option>
                                    {{-- @endforeach --}}
                                </select>
                            </div>
                        </div>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                
                @foreach ($fece9->itemDetails as $index => $item)
                    @php $total = getTotal($item->id); @endphp 
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <span v-modal="index={{$index}}"></span>

                            @if ($item->itemDetailsSpecifications->count() > 0)
                                <label class=" m-checkbox m-checkbox--state-success no-print  {{$item->pivot->status == 1 ? 'd-none' :'inline-block'}}">
                                    <input type="checkbox" {{$item->pivot->status == 1 ? 'checked' : ''}}  id="check-{{$item->id}}" {{$item->pivot->quantity > $total['remaining'] ? 'disabled' : ''}} value="" onclick="changeItemStatus({{$item->id}})" class="no-print">
                                    <span ></span>
                                </label>
                                   
                            @elseif(empty($fece9->processed_fece9_file) || str_replace(url('/'), '', url()->previous()) =='/meem7')


                            @elseif($fece9->fece9_type == 0)

                                <label class=" m-checkbox m-checkbox--state-success no-print {{$item->pivot->status == 1 ? 'd-none' :'inline-block'}}"  v-if="available_item.length > 0" >
                                    <input type="checkbox"   id="check-{{$item->id}}"  value="" :disabled="available_item[index].total_alloted >= fece9_item_details[index].pivot.quantity ? false : true" onclick="changeItemStatus({{$item->id}})" class="no-print">
                                    <span></span>
                                </label>
                            @else
                                <a class="pr-3" v-if="fece9.fece9_type !=0" href="{{route('item_details.show', $item->id)}}" title="اضافه نمودن اجناس" v-if="fece9.fece9_type !=1">
                                    <i class="fa fa-plus"></i>
                                </a>
                            @endif

                            {{-- checkbox for yomya --}}
                            {{$item->name_dr}}
                            
                            @if($fece9->fece9_type == 3)
                                <span class="m-widget1__number m--font-info pl-4 lead font-weight-bold" >
                                        {{-- <i class="fa fa-calculator"></i> --}}
                                        تعداد موجود :  {{$total['remaining']}}
                                </span>
                            @elseif($fece9->fece9_type == 0)
                                <span class="m-widget1__number m--font-info pl-4 lead font-weight-bold" >
                                        <span v-if="available_item.length > 0" >
                                        مقدار موجود :   @{{available_item[index].total_alloted}}

                                        </span>
                                </span>
                            @else
                                <span class="m-widget1__number m--font-info pl-4 lead font-weight-bold" v-else>
                                    @php
                                        $contract_item = App\ContractItems::where('item_details_id',$item->id)->first();
                                    @endphp
                                    موجود در قرارداد : {{$contract_item->available_amount}}
                                </span>
                            @endif
                            
                        </td>
                        <td>
                          <ul class="list-group list-group-flush item-count">
                              <li class="list-group-item  {{$item->pivot->status == 0 ? 'border border-dark rounded-circle' : ''}} " id="list-group-item-{{$item->id}}">{{$item->pivot->quantity}}</li>
                          </ul>

                        </td>
                        <td>{{$item->unit->name_dr}}</td>
                        <td></td>
                    </tr>


                @endforeach
                <tr>
                    <td></td>
                    <td></td>
                    <td>
                        <p>
                            امید است در مورد تهیه و تدارک آن بالای شعبه مربوطه هدایت فرموده ممنون سازید
                        </p>
                        <p class="text-center">
                            {{$fece9->description}}
                        </p>
                        <p>
                            تایید است:
                        </p>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>

            </tbody>
            <tfoot>
                <tr>
                    <td colspan="5">
                        11. اقلام که در آن خط گرفته شده در تحویلخانه موجود نیست خریداری آن درخواست گردد.
                        <div class="row p-3">
                            <div class="col-md-2">
                                <hr class="pb-2">
                                آمر تحویلخانه
                            </div>
                            <div class="col-md-2">
                                <hr class="pb-2">
                                تاریخ
                            </div>
                        </div>
                    </td>
                    <td>
                        12. نمبر تکت توزیع
                    </td>
                </tr>

            </tfoot>

        </table>
        <tr>
            <div class="row">
                <div class="col-md-8">
                    <p>لطفا اجناس ذیل خدمات را فراهم نماید.</p>
                    <p>در یک اصل و دو کاپی ترتیب میشود.</p>
                    <p>الف:‌اصل آن به تحویلخانه حفظ میگردد.</p>
                </div>
                <div class="col-md-4">
                    4. اسم نمایندگی ________________________
                </div>
            </div>
        </tr>

        @if($fece9->processed_fece9_file)
            <div class="row no-print">
                <div class="col-md-3 mb-2 pl-0">
                    <div class="update-image-wrapper">
                        <img class="w-100px" src="{{asset('public/images/pdf-2.png')}}"  alt="" />

                        <div class="overlay">
                            <div class="text">
                                <a href="{{asset($fece9->processed_fece9_file)}}" data-fancybox data-caption="{{$fece9->description}}">
                                    نمایش
                                </a>
                                <a href="javascript:void(0)" onclick="$('input[name=image]').click();">
                                        تصحیح
                                </a>


                                <form action="{{route('fece9s.replace_attached_file')}}" id="image_form" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <input type="file" class="custom-file-input" id="image" multiple name="image" onchange="$('#image_form').submit();" ref="file">
                                    <input type="hidden" id="fece9_id" name="id" value="{{$fece9->id}}">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
    <div class="row no-print"> 
       <form method="POST" action="{{route('fece9s.update',$fece9->id)}}"> 
            @csrf
            @method('patch')
            <input type="hidden" name="change_status" value="1">
            <div class="col-lg-6 d-inline">
                <button type="submit" class="btn btn-success m-btn--wide">ثبت</button>
                <button type="button" class="btn btn-brand" onclick="printFece9()">چاپ</button>
                <input type="hidden" id="fece9_id_for_status" name="id" value="{{$fece9->id}}">

                {{--
                <button type="button" class="btn btn-brand">ارسال</button> --}}
                <a href="{{route('fece9s.index')}}" class="btn btn-secondary">لغو</a>
            </div>
       </form>
    </div>
</div>

@endsection
@push('custom-css')
    <style>

        thead th {
            border: 1px solid #777 !important;
            text-align: center;
        }

        tbody td {
            border-left: 1px solid #777;
            border-right: 1px solid #777;
            border-bottom: 0 !important;
            border-top: 0 !important;
        }


        tfoot td {
            border: 1px solid #777 !important;
        }
        hr {
            border-color: #777;
        }
        .list-group-item {
            border: none;
        }

        .item-count li {
            padding: 10px 15px;
        }

        .list-group.list-group-flush li {
            margin: 2px;
        }

        .item-description {
            height: 15cm;
            ;
        }

        @media print {
            table {
                /* height: 90vh; */
            }
        }

        input[name=image] {
            display: none;
        }
        /* image slide hover styles */

        .update-image-wrapper {
            position: relative;
            /* width: 50%; */
        }

        .image {
            display: block;
            width: 100%;
            height: auto;
        }

        .overlay {
            position: absolute;
            bottom: 0;
            left: 0;
            right: 0;
            background-color: #008cba5e;
            overflow: hidden;
            width: 100%;
            height: 0;
            transition: .5s ease;
        }

        .update-image-wrapper:hover .overlay {
            height: 100%;
        }

        .text {
            color: white;
            font-size: 20px;
            position: absolute;
            top: 50%;
            left: 50%;
            -webkit-transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
            text-align: center;
        }

        .text a {
            background: #fff;
            padding: 5px 10px;
            margin: 3px;
            font-size: .7em;
        }

        .text a:hover {
            text-decoration: none;
        }

        .w-100px{
            width: 150px;
        }
    </style>
@endpush
@push('custom-js')
  <script>


var app = new Vue({
            el: '#app',
            data: {
                fece9: {!!$fece9!!},
                mounteds: {!!$available_items!!},
                available_item:[],
                index:'',
                fece9_item_details : {!!$fece9->itemDetails!!},
                motamed_id:''
            },
            methods: {
                availableItem(receiver_employee_id){
                  this.available_item = this.mounteds.filter(item => item.receiver_employee_id == receiver_employee_id)
                },
       
            },
        })


    function changeItemStatus(id){

      var value=  $('#check-'+id).is(":checked") ? 1 : 0;
      if(value == 0){
         $('#list-group-item-'+id).addClass('border border-dark rounded-circle');
      }else{
        $('#list-group-item-'+id).removeClass('border border-dark rounded-circle');
      }
      var fece9_id_for_status = $('#fece9_id_for_status').val();
      var motamed_id = $('#motamed_id').val();
      $.ajax({
             type: "POST",
             url: '{{route('fece9s.item_details_status')}}',
             data: {
                 'id': id,
                 'status': value,
                 'fece9_id':fece9_id_for_status,
                 'motamed_id':motamed_id, 
             },
             success: function(data) {
                 alert('موفقانه تغیر ایجاد گردید.');

             }

      });

    }

    //redirect after pring m-page

      function printFece9(){

        window.print()

      }
      window.onafterprint = function(){
          window.location.href="{{route('fece9s.index')}}";
          // window.close();
      }

  </script>
@endpush
