@extends('layouts.master')
@section('title', trans('global.create_fece9'))
@section('content')

<form class="form" action="{{route('fece9s.store')}}" method="POST" id="app">

    @csrf
    <div class="form-group m-form__group row ">
      <div class="form-group col-md-6 {{$errors->has('number') ? 'has-danger' : ''}}">
        <label for="fece9_number">{{trans('global.fece9_number')}}</label>
        <input type="number" name="number" value="{{old('number')}}" class="form-control m-input m-input--square" id="number" placeholder="نمبر ف س ۹ را وارید نماید.">
        @if($errors->has('number'))
            <div class="form-control-feedback">{{$errors->first('number')}}</div>
        @endif
      </div>

      <div class="form-group col-md-3 {{$errors->has('date') ? 'has-danger' : ''}}">
        <label for="inputPassword4">{{trans('global.fece9_date')}}</label>
         <div class="input-group date">
            <input type="text" name="date" value="{{old('date')}}" class="form-control persian_date" placeholder="تاریخ انتخاب کنید.">
            <div class="input-group-append">
              <span class="input-group-text">
                <i class="la la-calendar"></i>
              </span>
            </div>
        </div>
        @if($errors->has('date'))
          <div class="form-control-feedback">{{$errors->first('date')}}</div>
        @endif
      </div>
      <div class="form-group col-md-3 {{$errors->has('fece9_type') ? 'has-danger' : ''}}">
        <label for="inputPassword4">{{trans('global.fece9_type')}}</label>
            <select class="form-control" @change="changeFece9Type()" name="fece9_type" v-model="fece9_type">
                <option value="">{{trans('global.fece9_type')}}</option>
                <option value="0"  {{old('fece9_type') == 0 ? 'selected' : ''}}>یومیه</option>
                <option value="1"  {{old('fece9_type') == 1 ? 'selected' : ''}}>ذخیروی</option>
                <option value="2"  {{old('fece9_type') == 2 ? 'selected' : ''}}>غیر ذخیروی</option>
                <option value="3"  {{old('fece9_type') == 3 ? 'selected' : ''}}>غیر مصرفی</option>
            </select>
        @if($errors->has('fece9_type'))
          <div class="form-control-feedback">{{$errors->first('fece9_type')}}</div>
        @endif
      </div>

    </div>
    <div class="form-group m-form__group row" >
        <div class="col-md-6">
                <div class="row items_list {{$errors->has('list') ? 'has-danger' : ''}}">

                    <div class="form-group col-md-12">
                        <label for="inputState">{{trans('global.sub_category')}}</label>
                        <select id="sub_category_id" onchange="submitItemSpecifications()" name="sub_category_id" ref="item_details_id" class="form-control select2">
                            <option value="none" selected>یگ گزینه را انتخاب نماید  </option>
                            {{-- @foreach ($item_details as $value)
                                <option value="{{$value->id}}"  {{old('sub_category_id') == $value->id ? 'selected' : ''}}>{{$value->name_dr}}</option>
                                @endforeach --}}
                                <option v-for="item in sub_category" :value="item.id" 
                                 {{-- {{old('sub_category_id') == $value->id ? 'selected' : ''}} --}}
                                 >@{{item.name_dr}}</option>
                        </select>
                    </div>


                </div>
                <div class="row mr-0" id="keys_response">
                </div>


                <div class="row items_list {{$errors->has('list') ? 'has-danger' : ''}}">
                    <div class="form-group col-md-3">
                        <label for="inputState">{{trans('global.item')}}</label>
                        <select id="item_details" name="item" ref="item_details_id" class="form-control select2">
                            <option value="none" selected>یگ گزینه را انتخاب نماید.</option>
                                @foreach ($item_details as $value)
                                <option value="{{$value->id}}"  {{old('item') == $value->id ? 'selected' : ''}} >{{$value->name_dr}}</option>
                                @endforeach
                        </select>
                        <div class="form-control-feedback item_error d-none">انتخاب جنس و مقدار آن ضروری میباشد</div>
                        <div class="form-control-feedback duplicate_item_error d-none">جنس قبلا اضافه گردیده است.</div>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="inputState">{{trans('global.quantity')}}</label>
                        <input type="number" ref="quantity" name="quantity" class="form-control" min="1" placeholder="{{trans('global.quantity')}}">
                    </div>
                    <div class="form-group col-md-3">
                        <button type="button" v-on:click="addToList()" id="add_to_list" class="btn btn-outline-brand btn-square add-btn position-absolute" style="bottom:0;">+ اضافه نمودن</button>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="inputState">&nbsp;</label>
                        <div class=" m-portlet__foot--fit float-left" style="display: contents;">
                            <div class="m-form__actions">
                                <button type="button" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air"  data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">درخواست</button>
                            </div>
                        </div>
                    </div>

                    @if($errors->has('list'))
                        <div class="form-control-feedback">{{$errors->first('list')}}</div>
                    @endif
                </div>
                <div class="row">
                    
                    <table class="table items_table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>جنس</th>
                                <th>مقدار</th>
                                <th>عملیات</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="(item, index) in list">
                                <td>@{{index+1}}</td>
                                <td>@{{item.name}}</td>
                                <td>@{{item.quantity}}</td>
                                <td>
                                    <a href="javascript:void(0)" v-on:click="editFromList(item.item_details_id,index)" class="text-primary pl-2"><i class="fa fa-edit"></i></a>
                                    <a href="javascript:void(0)" v-on:click="removeFromList(index)" class="text-danger"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <input type="hidden" name="list" v-model="JSON.stringify(list)"  multiple>
                </div>
            </div>
            
            <div class="form-group col-md-6 {{$errors->has('department_id') ? 'has-danger' : ''}}">
                <label for="department">{{trans('global.requesting_department')}}</label>
                <select class="form-control select2" name="department_id" id="department">
                    @foreach ($departments as $item)
                        <option value="{{$item['id']}}" {{old('department_id') == $item['id'] ? 'selected' : ''}}>{{$item['name_dr']}}</option>
                    @endforeach
                </select>
                @if($errors->has('department_id'))
                    <div class="form-control-feedback">{{$errors->first('department_id')}}</div>
                @endif

                <div class="row" v-if="editItem.length">
                    <div class="col-md-12 pt-lg-12" v-for="(item, index) in editItem">
                        <table class="table items_table" width="100%">
                            <tr>
                                <td>جنس</td>
                                <td>مقدار</td>
                                <td>ذخیره</td>
                            </tr>
                            <tr>
                                <td><input type="text" class="form-control" readonly :value="item.name"></td>
                                <td><input type="number" class="form-control" id="edit_quantity" :value="item.quantity"></td>
                                <td>
                                    <a href="javascript:void(0)" v-on:click="updateFromList(item.item_details_id)" class=" btn btn-primary btn-sm"><i class="fa fa-save"></i></a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>



            <div class="form-group col-md-4 {{$errors->has('independent_unit') ? 'has-danger' : ''}}" v-if="fece9_type == 0">
                <label for="department">اداره</label>
                <select class="form-control " name="independent_unit" id="department">
                    @foreach ($independent_unit as $item)
                        <option value="{{$item['id']}}" {{old('independent_unit') == $item['id'] ? 'selected' : ''}}>{{$item['name_dr']}}</option>
                    @endforeach
                </select>
                @if($errors->has('independent_unit'))
                    <div class="form-control-feedback">{{$errors->first('independent_unit')}}</div>
                @endif
            </div>
    </div>
      <input type="hidden" name="created_by" value='{{auth()->user()->id}}' >
    <div class="form-group m-form__group row {{$errors->has('description') ? 'has-danger' : ''}}">
        <div class="form-group col-md-6">
            <label for="inputAddress2">متن ف س ۹</label>
            <textarea class="form-control" name="text" id="" rows="7">{{old('text')}}</textarea>
                @if($errors->has('text'))
                    <div class="form-control-feedback">{{$errors->first('text')}}</div>
                @endif
        </div>
        <div class="form-group col-md-6">
            <label for="inputAddress2">ملاحظات</label>
            <textarea class="form-control" name="description" id="" rows="7">{{old('description')}}</textarea>
                @if($errors->has('description'))
                    <div class="form-control-feedback">{{$errors->first('description')}}</div>
                @endif
        </div>


    </div>

    <button type="submit" class="btn btn-primary">ذخیره</button>
    <a type="button" href="{{url('fece9/index')}}" class="btn btn-default">برگشت</a>
    <button type="reset" class="btn btn-danger">حذف کردن</button>
  </form>
  {{-- Model for request to add item --}}
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form  action="{{route('requested_items.store')}}" method="POST">
                <input type="hidden" id="modal_sub_category_id" name="sub_category_id">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">درخواست برای اضافه نمودن اجناس</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body pt-0">
                    @csrf
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">لیست اجناس</label>
                        <textarea class="form-control text-editor" name="requested_items" id="message-text" rows="5" cols="10"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">ثبت</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
@push('custom-css')
    <style>
        #keys_response .col-md-3, #keys_response .col-md-2, #keys_response .col-md-1 {
            padding-left: 5px;
            padding-right: 5px;
        }
    </style>
@endpush
@push('custom-js')
    <script>
        var app = new Vue({
            el: '#app',
            data: {
                message: 'Hello Vue!',
                list: {!! old('list') ? old('list') : '[]'  !!},
                editItem:[],
                editItemIndex:0,
                sub_category:{!!$item_details!!},
                consumable_sub_categories:{!!$consumable_sub_categories!!},
                non_consumable_sub_categories:{!!$non_consumable_sub_categories!!},
                fece9_type: {{ old('fece9_type') ?  old('fece9_type') : '0' }},
            },
            methods: {
                addToList: function(event) {

                    id = this.$refs.item_details_id.value

                    quantity = this.$refs.quantity.value

                    name = this.$refs.item_details_id.options[this.$refs.item_details_id.options.selectedIndex].text

                    let itemInList = this.list.filter(item => item.item_details_id===id);
                    let isItemInList = itemInList.length > 0;

                    if (isItemInList === false && id != 'none' && quantity != "") {
                        this.list.push({item_details_id: id, name: name, quantity: quantity });
                        this.$refs.quantity.value = '';
                    }
                },
                editFromList: function(id,index){
                    this.editItem = this.list.filter(item => item.item_details_id===id);
                    this.editItemIndex=index;
                },
                updateFromList: function(id){
                        edit_quantity = $('#edit_quantity').val();
                        this.list.splice(this.editItemIndex, 1);
                        this.list.push({item_details_id: id, name: this.editItem[0]['name'], quantity:edit_quantity});
                        this.editItem = [];
                        this.editItemIndex = 0;

                    
                },

                removeFromList: function(index) {
                    this.list.splice(index, 1);
                },

                changeFece9Type: function() {
                    if(this.fece9_type === "0" || this.fece9_type === "1" || this.fece9_type === "2") {
                        this.sub_category = this.consumable_sub_categories;
                    }
                    else {
                        this.sub_category = this.non_consumable_sub_categories;
                    }
                },

            },
        })


        $('#sub_category_id').change(function() {
            sub_category_id = $(this).val();
            getSubCategoryKeys(sub_category_id);
        })



        function getSubCategoryKeys(sub_category_id, data) {
            console.log(data);
            $.ajax({
                method:'GET',
                url: '{{url("get_sub_categories_select_options")}}/'+sub_category_id,
                data: {data},
                success: function(resp) {
                    $('#keys_response').html(resp);
                }
            })
        }

        function submitItemSpecifications() {
            col1 = $('#col1').val();
            col2 = $('#col2').val();
            col3 = $('#col3').val();
            col4 = $('#col4').val();
            col5 = $('#col5').val();
            col6 = $('#col6').val();
            col7 = $('#col7').val();
            col8 = $('#col8').val();
            col9 = $('#col9').val();
            col10 = $('#col10').val();
            col11 = $('#col11').val();
            col12 = $('#col12').val();
            sub_category_id = {!! old('sub_category_id') ? old('sub_category_id') : "$('#sub_category_id').val();" !!}

            $.ajax({
                method: 'get',
                url: "{{url('get_item_details_by_specifications')}}/"+sub_category_id,
                data: {'col1': col1, 'col2': col2, 'col3': col3, 'col4': col4,'col5': col5,'col6': col6,'col7': col7,'col8': col8
                ,'col9': col9,'col10': col10,'col11': col11 ,'col12': col12
            },
                success: function(resp) {
                    console.log(resp);
                    $('#item_details').html('');
                    $.each(resp, populateOptions)
                }
            })
            $('#modal_sub_category_id').val(sub_category_id);
            console.log(sub_category_id+" - "+col1+" - "+col2+" - "+col3+" - "+col4);
        }

        function populateOptions(index, item) {
            $('#item_details').append('<option value="'+item.id+'">'+item.name_dr+'</option>');
        }


        $(function() {
           {!! old('sub_category_id') ? "submitItemSpecifications(); getSubCategoryKeys(".old('sub_category_id').");" : '' !!}
        })



    </script>

@endpush
