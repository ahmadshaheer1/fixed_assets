@extends('layouts.master')
@section('title','ترتیب ف س ۵')
@section('content')
<div id="show_fece9">
    <legend class="mt-2 text-dark font-weight-bold">مشخصات ف س ۹</legend>
    <div class="row m-row--no-padding m-row--col-separator-xl">
            <div class="col-md-6 meem7_descriptions">
                <div class="m-widget24__item">
                    <h4 class="m-widget24__title">
                        {{trans('global.fece9_number')}}
                    </h4>
                    <span class="m-widget24__stats m--font-brand pull-left">
                            @{{fece9.number}}
                    </span>

                </div>
            </div>
            <div class="col-md-6 meem7_descriptions">
                <div class="m-widget24__item">
                    <h4 class="m-widget24__title">
                        {{trans('global.fece9_date')}}
                    </h4>
                    <span class="m-widget24__stats m--font-brand pull-left">
                            @{{fece9.date}}
                    </span>

                </div>
            </div>
            <div class="col-md-6 meem7_descriptions">
                <div class="m-widget24__item">
                    <h4 class="m-widget24__title">
                        {{trans('global.requesting_department')}}
                    </h4>
                    <span  v-if="fece9.department" class="m-widget24__stats m--font-brand pull-left">
                        @{{fece9.department.name_dr}}
                    </span>

                </div>
            </div>
            <div class="col-md-6 meem7_descriptions">
                <div class="m-widget24__item">
                    <h4 class="m-widget24__title">
                            نمایش فایل
                    </h4>
                    <span class="m-widget24__stats m--font-brand pull-left">
                        <a :href="'{{asset('/')}}'+fece9.processed_fece9_file"  data-fancybox><i class="flaticon-attachment"></i></a>
                    </span>
                </div>
            </div>
        </div>
    <form class="m-form m-form--fit m-form--label-align-right" action="{{route('fece5s.store')}}" enctype="multipart/form-data" method="POST" v-if="fece9.fece9_type == 3">
      @csrf
        <div class="row">
            <div class="col-md-12">
                <div id="accordion" class="mt-2">
                    <legend v-if="fece5s.length" class="mt-2 text-dark font-weight-bold"> ف س ۵ های ترتیب شده</legend>
                    <div v-for="(item, index) in fece5s" class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button type="button" class="btn btn-link" data-toggle="collapse" :data-target="'#ac' + index" aria-expanded="true" aria-controls="collapseOne">
                                        <span class="mr-2">شماره ف س ۵</span><span class="text-dark pr-1">(@{{item.number}})</span>  --  <span class="mr-2">تاریخ ف س ۵:</span><span class="text-dark pr-1">(@{{item.date}})</span> -- <span class="mr-2">توزیع کننده:</span> (@{{item.distributer_employee.name_dr}})<span class="text-dark pr-1">
                                            {{-- (@{{item.distributerEmployee.name_dr}}) --}}
                                        </span>
                                </button>
                            </h5>
                        </div>

                        <div :id="'ac' + index" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>شماره</th>
                                                <th>اسم جنس</th>
                                                {{-- <th>تعداد درخواست شده</th> --}}
                                                <th>تسلیمی</th>
                                                <th>عملیات</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(item_details, index) in item.allotments">
                                                <td>@{{++index}}</td>
                                                <td>@{{item_details.item_details_specifications.item_details.name_dr}}</td>
                                                {{-- <td>@{{item_details.requested_quantity}}</td> --}}
                                                <td>
                                                    <span class="m--font-primary" v-if="item_details.item_details_specifications.sub_category_keys.col1">
                                                        @{{item_details.item_details_specifications.sub_category_keys.col1}} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        @{{item_details.item_details_specifications.col1}} ||
                                                    </span>
                                                    <span class="m--font-primary" v-if="item_details.item_details_specifications.sub_category_keys.col2">
                                                            @{{item_details.item_details_specifications.sub_category_keys.col2}} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        @{{item_details.item_details_specifications.col2}} ||
                                                    </span>
                                                    <span class="m--font-primary" v-if="item_details.item_details_specifications.sub_category_keys.col3">
                                                            @{{item_details.item_details_specifications.sub_category_keys.col3}} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        @{{item_details.item_details_specifications.col3}} ||
                                                    </span>
                                                    <span class="m--font-primary" v-if="item_details.item_details_specifications.sub_category_keys.col4">
                                                            @{{item_details.item_details_specifications.sub_category_keys.col4}} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        @{{item_details.item_details_specifications.col4}}
                                                    </span>
                                                    <span class="m--font-primary" v-if="item_details.item_details_specifications.sub_category_keys.col5">
                                                            @{{item_details.item_details_specifications.sub_category_keys.col5}} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        @{{item_details.item_details_specifications.col5}}
                                                    </span>
                                                    <span class="m--font-primary" v-if="item_details.item_details_specifications.sub_category_keys.col6">
                                                            @{{item_details.item_details_specifications.sub_category_keys.col6}} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        @{{item_details.item_details_specifications.col6}}
                                                    </span>
                                                    <span class="m--font-primary" v-if="item_details.item_details_specifications.sub_category_keys.col7">
                                                            @{{item_details.item_details_specifications.sub_category_keys.col7}} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        @{{item_details.item_details_specifications.col7}}
                                                    </span>
                                                    <span class="m--font-primary" v-if="item_details.item_details_specifications.sub_category_keys.col8">
                                                            @{{item_details.item_details_specifications.sub_category_keys.col8}} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        @{{item_details.item_details_specifications.col8}}
                                                    </span>
                                                    <span class="m--font-primary" v-if="item_details.item_details_specifications.sub_category_keys.col9">
                                                            @{{item_details.item_details_specifications.sub_category_keys.col9}} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        @{{item_details.item_details_specifications.col9}}
                                                    </span>
                                                    <span class="m--font-primary" v-if="item_details.item_details_specifications.sub_category_keys.col10">
                                                            @{{item_details.item_details_specifications.sub_category_keys.col10}} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        @{{item_details.item_details_specifications.col10}}
                                                    </span>
                                                    <span class="m--font-primary" v-if="item_details.item_details_specifications.sub_category_keys.col11">
                                                            @{{item_details.item_details_specifications.sub_category_keys.col11}} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        @{{item_details.item_details_specifications.col11}}
                                                    </span>
                                                    <span class="m--font-primary" v-if="item_details.item_details_specifications.sub_category_keys.col12">
                                                            @{{item_details.item_details_specifications.sub_category_keys.col12}} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        @{{item_details.item_details_specifications.col12}}
                                                    </span>
                                                    <span class="m--font-primary">
                                                        <i class="fa fa-arrow-right text-success"></i> <i class="fa fa-arrow-left text-success"></i> @{{item_details.employee.name_dr}}
                                                    </span>
                                                </td>
                                                <td>
                                                    <a href="#"  data-toggle="modal" data-target="#allotment_history" title="تاریخچه جمع و قید جنس" @click="showItemAllotmentHistory(item_details.item_details_specifications.id)">
                                                        <i class="la la-list-alt"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </form>
    <div v-else>
        <table class="table">
            <thead>
                <tr>
                    <th>شماره</th>
                    <th>اسم جنس</th>
                    <th>تعداد درخواست شده</th>
                    {{-- <th>تسلیمی</th>
                    <th>عملیات</th> --}}
                </tr>
            </thead>
            <tbody>
                <tr v-for="(item, index) in fece9_item_details[0].item_details">
                    <td>@{{++index}} </td>
                    <td>@{{item.name_dr}} </td>
                    <td>
                        <span class="m--font-primary">
                        مقدار :  @{{item.pivot.quantity}}  @{{item.unit.name_dr}}
                        </span>
                        <span class="m--font-info">
                        
                        </span>
                        
                    </td>
                    
                </tr>
            </tbody>
        </table>
    </div>


    <div class="modal fade" id="allotment_history" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">تاریخچه جمع و قید جنس</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div  class="m-scrollable" data-scrollable="true" data-height="380" data-mobile-height="300">

                        <!--Begin::Timeline 2 -->
                        <div  v-if="allotment_history.length > 0" class="m-timeline-2">
                            <div class="m-timeline-2__items  m--padding-top-25 m--padding-bottom-30">
                                    <div v-for="(item, index) in allotment_history" class="m-timeline-2__item">
                                        <span class="m-timeline-2__item-time m--font-primary">@{{item.allotment_date}}</span>
                                        <div class="m-timeline-2__item-cricle">
                                            <i :class="['fa fa-genderless', item.status == 0 ? 'm--font-warning' : 'm--font-success']"></i>
                                        </div>
                                        <div class="m-timeline-2__item-text  m--padding-top-5">
                                            <div class="font-weight-bold">
                                                @{{item.employee.name_dr}}
                                            </div>
                                            <div class="">
                                                @{{item.employee.current_position_dr}}
                                            </div>
                                            <div v-if="item.condition == 0" class="m--font-success">
                                                جدید
                                            </div>
                                            <div v-else-if="item.condition == 1" class="m--font-warning">
                                                مستعمل
                                            </div>
                                            <div v-else-if="item.condition == 2" class="m--font-danger">
                                                داغمه
                                            </div>
                                            <div v-else-if="item.condition == 3" class="m--font-danger">
                                                غیر فعال
                                            </div>
                                            <div v-if="item.status == 0" class="font-weight-bold m--font-info">
                                                تاریخ اعاده:‌@{{item.return_date}}
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection
@push('custom-js')
    <script>
        item_app = new Vue({
            el: '#show_fece9',
            data: {
                //fece9 details
                fece9: '',

                // fece5s
                fece5s: [],


                // item details specifications for allotment
                item_details_specifications: [ ],
                item_details_specifications_edit: { },
                extra_specification_keys:'',

                // allotment

                // selected allotment items based on their item details id
                allotment_list: {},
                allotment_history: [],
                //maximum available allotments based on requested items
                max_allotments: 0,

                // currently alloted alloted < = max_allotments
                alloted: 0,
                edit: false,

                // current active item details > specs of which has to be selected
                current_item_details_id: 0,

                // to check whether allotment of specs of an item details is completed
                completed_item_details: {},

                // all employees
                employees: [],

                // show fece5 information form after adding allotment items finishes
                finishAdding: false,

                //Fece5
                fece5_number:'',
                fece5_date:'',
                distributer_employee_id:'',
                fece5_description:'',
                fece5_item_details:'',
                // main form validation
                error: false,
                success: false,
                errors: [],
                fece9_item_details:{!!$fece9_item_details!!}

            },
            methods: {
                getFece9Details(id) {
                    axios.get("{{url('get_fece9_details')}}/"+id)
                        .then(res => {
                            this.fece9 = res.data;
                            this.fece5s = res.data.fece5s;
                            // this.max_allotments = this.fece9.item_details[].pivot.quantity;


                        })
                        .catch(err => {

                        });
                },
                getAllEmployees() {
                    axios.get("{{url('get_all_employees')}}")
                        .then(res => {
                            this.employees = res.data;
                        })
                        .catch(err => {
                            console.log(err);

                        })

                },
                getItemDetailsSpecifications(index, item_details_id) {
                    this.edit = false;
                    axios.get("{{url('get_item_details_specifications')}}/"+item_details_id)
                        .then(res => {
                            $('input.checkbox').prop('checked', false);
                            this.alloted = 0;
                            this.item_details_specifications = res.data.items;
                            this.extra_specification_keys = res.data.extra_specifications_keys;
                            this.max_allotments = this.fece9.item_details[index].pivot.quantity;
                            this.current_item_details_id = item_details_id;
                            this.allotment_list[this.current_item_details_id] = [];


                        })
                        .catch(err => {

                        })
                },
                itemAllotableToUser(index, id, event) {
                    console.log(this.item_details_specifications.filter(item => item.id == id));
                    if(event.target.checked) {
                        if(this.alloted < this.max_allotments) {
                            ++this.alloted;
                            this.item_details_specifications.filter(item => item.id == id)[0].receiver_employee_id = 0;
                        }
                    }
                    else {
                        console.log(this.allotment_list);
                        if(this.alloted <= this.max_allotments) {
                            this.item_details_specifications.filter(item => item.id == id)[0].receiver_employee_id = null;
                            this.allotment_list[this.current_item_details_id] = this.allotment_list[this.current_item_details_id].filter(item => item.id != id);
                            --this.alloted;
                        }
                    }
                    if(this.alloted == this.max_allotments) {
                        $('input.checkbox:not(input.checkbox:checked)').prop('disabled', true);
                    }
                    else {
                        $('input.checkbox:not(input.checkbox:checked)').prop('disabled', false);
                    }
                },
                addItemDetailsSpecificationsToAllotmentList(index) {

                    if(!this.allotment_list[this.current_item_details_id]) {
                        this.allotment_list[this.current_item_details_id] = []
                    }
                    if(this.allotment_list[this.current_item_details_id].filter(item => item.id == this.item_details_specifications[index].id).length == 0) {
                        this.item_details_specifications[index]['fece9_id'] = "{{$fece9_id}}";
                        // this.item_details_specifications[index]['sub_category_keys'] = null;
                        this.allotment_list[this.current_item_details_id].push(this.item_details_specifications[index]);
                    }
                    console.log(this.allotment_list[this.current_item_details_id]);


                },
                finishAddingToAllotedList() {
                    if(this.alloted == this.max_allotments && this.item_details_specifications.filter(item => item.receiver_employee_id == 0).length == 0) {
                        this.item_details_specifications_edit[this.current_item_details_id] = this.item_details_specifications;
                        this.item_details_specifications = [];
                        this.completed_item_details[this.current_item_details_id] = true;
                        this.alloted = 0;

                        // show fece5 form and load datepicker for the date field
                        this.finishAdding = true;
                        // $(".persian_date").persianDatepicker();

                    }
                },
                selectAllItems(event) {
                    max = this.max_allotments;
                    if(event.target.checked) {
                        this.alloted = this.max_allotments;
                        for(i=0;i<max;i++){
                            $('#'+i).attr('checked', 'checked');
                            this.item_details_specifications[i].receiver_employee_id = 0;
                        }
                        $('input.checkbox:not(input.checkbox:checked)').prop('disabled', true);
                    }
                    else {

                        for(i=0;i<max;i++)
                            this.item_details_specifications[i].receiver_employee_id = null;

                        this.alloted=0;
                        $('input.checkbox:not(input.checkbox:checked)').prop('disabled', false);
                        $('table tbody input[type=checkbox]').removeAttr('checked');
                    }
                },
                editItemDetailsSpecificationsFromAllotmentList(index,item_details_id){
                    // this.getItemDetailsSpecifications(index-1, item_details_id);
                    this.item_details_specifications = this.item_details_specifications_edit[item_details_id];
                    this.alloted = 0;
                    len = this.item_details_specifications.length;
                    this.edit = true;
                    for(i=0;i<len;i++){

                            if(this.item_details_specifications[i].receiver_employee_id > 0){

                                this.alloted +=1;
                            }

                    }
                    this.max_allotments = this.alloted;

                },
                showItemAllotmentHistory(item_details_specifications_id) {
                    axios.get("{{url('allotments/get_item_details_specifications_allotment_history')}}/"+item_details_specifications_id)
                        .then(res => {
                            this.allotment_history = res.data;
                            console.log(this.allotment_history);
                        })
                        .catch(err => {
                            console.log(err);
                        });

                },




            },
            mounted() {
                this.getFece9Details("{{$fece9_id}}");
                this.getAllEmployees();
            },
        });



    </script>

@endpush
