@extends('layouts.master')
@section('title',' صفحه اصلی')
@section('content')
  <div class="m-portlet  m-portlet--unair">
      <div class="m-portlet__body  m-portlet__body--no-padding">
          <div class="row m-row--no-padding m-row--col-separator-xl">
              <div class="col-md-12 col-lg-6 col-xl-3">

                  <!--begin::Total Profit-->
                  <div class="m-widget24">
                      <div class="m-widget24__item">
                          <h4 class="m-widget24__title">
                            مجموع اجناس جمع و قید شده
                            </h4><br>
                          <span class="m-widget24__desc">
                          </span>
                          <span class="m-widget24__stats m--font-brand">
                              {{$item_details_specifications}}
                          </span>
                          <div class="m--space-10"></div>
                          <div class="progress m-progress--sm">
                              <div class="progress-bar m--bg-brand" role="progressbar" style="width: 78%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                          </div>
                          <span class="m-widget24__change">
                              Change
                          </span>
                          <span class="m-widget24__number">
                              78%
                          </span>
                      </div>
                  </div>

                  <!--end::Total Profit-->
              </div>

              <div class="col-md-12 col-lg-6 col-xl-3">

                  <!--begin::New Orders-->
                  <div class="m-widget24">
                      <div class="m-widget24__item">
                          <h4 class="m-widget24__title">
                            مجموع اجناس ثبت شده 

                          </h4><br>
                          <span class="m-widget24__desc">
                          </span>
                          <span class="m-widget24__stats m--font-danger">
                              {{$item_details_specifications_count}}
                          </span>
                          <div class="m--space-10"></div>
                          <div class="progress m-progress--sm">
                              <div class="progress-bar m--bg-danger" role="progressbar" style="width: 69%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                          </div>
                          <span class="m-widget24__change">
                              Change
                          </span>
                          <span class="m-widget24__number">
                              69%
                          </span>
                      </div>
                  </div>

                  <!--end::New Orders-->
              </div>
              <div class="col-md-12 col-lg-6 col-xl-3">

                <!--begin::New Feedbacks-->
                <div class="m-widget24">
                    <div class="m-widget24__item">
                        <h4 class="m-widget24__title">
                            مجموع اشخاص ثبت شده
                        </h4><br>
                        <span class="m-widget24__desc">
                        </span>
                        <span class="m-widget24__stats m--font-info">
                            {{$allotments}}
                        </span>
                        <div class="m--space-10"></div>
                        <div class="progress m-progress--sm">
                            <div class="progress-bar m--bg-info" role="progressbar" style="width: 84%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <span class="m-widget24__change">
                            Change
                        </span>
                        <span class="m-widget24__number">
                            84%
                        </span>
                    </div>
                </div>

                <!--end::New Feedbacks-->
            </div>
              <div class="col-md-12 col-lg-6 col-xl-3">

                  <!--begin::New Users-->
                  <div class="m-widget24">
                      <div class="m-widget24__item">
                          <h4 class="m-widget24__title">
                            مجموع ف س ۵ ها
                          </h4><br>
                          <span class="m-widget24__desc">
                             
                          </span>
                          <span class="m-widget24__stats m--font-success">
                              {{$fece5}}
                          </span>
                          <div class="m--space-10"></div>
                          <div class="progress m-progress--sm">
                              <div class="progress-bar m--bg-success" role="progressbar" style="width: 90%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                          </div>
                          <span class="m-widget24__change">
                              Change
                          </span>
                          <span class="m-widget24__number">
                              90%
                          </span>
                      </div>
                  </div>

                  <!--end::New Users-->
              </div>
          </div>
      </div>
  </div>
  <div class="container">
    <div class="row my-3">
        <div class="col">
            <h4>چارت کارکرد استفاده کننده گان</h4>
            <h5> 
                <i class="m-nav-grid__icon  flaticon-add-label-button" style="color: #36a3f7;"></i>
                <span>اجناس ذخیره شده</span><br>
                <i class="m-nav-grid__icon  flaticon-add-label-button" style="color: #716aca;"></i>
                <span>اجناس توزیع شده</span>

            </h5>
        </div>
    </div>
    <div class="row my-2">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <canvas id="chBar" height="100"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('custom-js')
    <script>
      /* chart.js chart examples */

// chart colors
var colors = ['#36a3f7','#716aca','#444444','#c3e6cb','#dc3545','#6c757d'];

var chBar = document.getElementById("chBar");
var chartData = {
  labels: [
      @php
        foreach($users as $item){
            echo  '"'.$item->employee->name_dr.'",';
        }
      @endphp
      ],
  datasets: [{
    data: {{$all_item_created}},
    backgroundColor: colors[0]
  },
  {
    data: {{$alloted_item_created}},
    backgroundColor: colors[1]
  }]
};
if (chBar) {
  new Chart(chBar, {
  type: 'bar',
  data: chartData,
  options: {
    scales: {
      xAxes: [{
        barPercentage: 0.4,
        categoryPercentage: 0.5
      }],
      yAxes: [{
        ticks: {
          beginAtZero: false
        }
      }]
    },
    legend: {
      display: false
    }
  }});
}
 
    </script>    
@endpush
@push('custom-css')
<style>
.line-color{

}
</style>
@endpush