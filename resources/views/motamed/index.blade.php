@extends('layouts.master')
@section('title',trans('global.motamed_allotments'))
@section('content')
{{-- jquery-datatable --}}

<div class="row m--margin-bottom-20">
    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
      <label>{{trans('global.name')}}:</label>
      <input type="text"  id="name" class="form-control m-input" placeholder="{{trans('global.name')}}" data-col-index="0">
    </div>
    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
      <label>{{trans('global.father_name')}}:</label>
      <input type="text" id="father_name" class="form-control m-input" placeholder="{{trans('global.father_name')}}" data-col-index="1">
    </div>
  
    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
      <label>{{trans('global.designation')}}:</label>
      <input type="text" id="designation" class="form-control m-input" placeholder="{{trans('global.designation')}}" data-col-index="4">
    </div>
    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
      <label>{{trans('global.department')}}:</label>
      <input type="text" id="department" class="form-control m-input" placeholder="{{trans('global.department')}}" data-col-index="4">
    </div>
    
  </div>
    <div class="text-right">
      <button onclick="serach()" class=" text-center btn btn-info btn-sm">{{trans('global.search')}}<i class="la la-search"></i></button>
    </div>

  <div class="m-separator m-separator--md m-separator--dashed"></div>
  
<div id="data_table" class="table-responsive">
  
  @include('motamed.motamed_list')
</div>
@endsection
@push('custom-js')
<script>
  $.ajaxSetup({
  headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
  });

  $(document).ready(function(){
    
    $(document).on('click','.pagination a',function(event){
      event.preventDefault();
      var page = $(this).attr('href').split('page=')[1];
      get_data(page);

    });
    function get_data(page){
      $.ajax({
          url:'{{url("")}}/allotments/get_motamed_allotments_ajax?page='+page,
          beforeSend: function() {
        // setting a timeout
          $("#data_table").prepend('<div class="m-blockui "><span>لطفا منتظر باشید </span><span><div class="m-loader  m-loader--brand m-loader--lg"></div></span></div>');
          },
          success:function(data){

              $("#data_table").html(data);
          }
      });
    }   
  });
  function serach(){
      var name = $('#name').val();
      var father_name = $('#father_name').val();
      var designation = $('#designation').val();
      var department = $('#department').val();
      $.ajax({
          type:"post",
          url: "{{ route('allotments.get_employees_allotments_search', ['type' => 'motamed']) }}",
          data:{
            'name' : name,
            'father_name' : father_name,
            'designation' : designation,
            'department' : department
          },
          dataType: "text",
          beforeSend: function() {
        // setting a timeout
          $("#data_table").prepend('<center><div class="m-blockui justify-content-center"><span>لطفا منتظر باشید </span><span><div class="m-loader  m-loader--brand m-loader--lg"></div></span></div></center>');
          },
          success:function(data){
             console.log(data);
            $("#data_table").html(data);
          }

      });
    }
</script>

@endpush
@push('custom-css')
    <style>
      #data_table .m-blockui {
        color: #5867dd;
        text-align: center !important;
        position: fixed;
        top: 30%;
        right: 50%;
        display: inline-table;
      }
    </style>  
@endpush
