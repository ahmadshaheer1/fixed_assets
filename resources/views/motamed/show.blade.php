@extends('layouts.master')
@section('title', trans('global.motamed_allotments'))
@section('content')
    <div class="wrapper" id="motamed_allotments">
        <div class="m-invoice-2">
            <div class="m-invoice__wrapper">
                <div class="m-invoice__head" style="">
                    <div class="m-invoice__container">
                        <div class="m-invoice__logo pb-2">
                            <div class="d-flex justify-content-between align-items-start">
                                <div>
                                    <img class="w-25"
                                        src="https://b-buildingbusiness.com/wp-content/uploads/2018/08/unknown_person-1024x1024-1.png">
                                </div>
                            </div>
                        </div>
                        <div class="m-invoice__items">
                            <div class="m-invoice__item">
                                <span class="m-invoice__subtitle">اسم</span>
                                <span class="m-invoice__text">{{ $employee->name_dr }} ({{ $employee->last_name }})</span>
                            </div>
                            <div class="m-invoice__item">
                                <span class="m-invoice__subtitle">نام پدر</span>
                                <span class="m-invoice__text">{{ $employee->father_name_dr }}</span>
                            </div>
                            <div class="m-invoice__item">
                                <span class="m-invoice__subtitle">عنوان وظیفه</span>
                                <span class="m-invoice__text">{{ $employee->current_position_dr }}</span>
                            </div>
                            <div class="m-invoice__item">
                                <span class="m-invoice__subtitle">اداره</span>
                                <span
                                    class="m-invoice__text">{{ isset($employee->department->name_dr) ? $employee->department->name_dr : '' }}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="m-invoice__body">
                    <div class="table-responsive">
                        <div class="search">
                            <input type="text" class="search_box" placeholder="جستجو..." data-search />
                            <input type="text" class="search_box" placeholder="جستجو به اساس تکت توزیع / م۷"
                                data-search_details />
                        </div>
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th class="un">
                                        <label class="m-checkbox" style="display:inline;margin:1.5em;">
                                            <input type="checkbox" class="checkbox"
                                                @change="checkAll($event,{{ $employee->id }})">
                                            <span></span>
                                        </label>
                                        {{-- انتخاب همه --}}
                                    </th>
                                    <th>شماره</th>
                                    <th>اسم جنس</th>
                                    <th>تعداد جنس</th>
                                    <th>توضیحات</th>
                                    <th>تاریخ تسلیمی</th>
                                    <th>تکت توزیع / م۷ </th>
                                    <th>تاریخ اعاده</th>
                                    <th>موجودیت</th>
                                    <th>حالت</th>
                                    <th class="un">عملیات</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($status == null || $status == 1 )
                                    @foreach ($employee->allotments->where('status' ,1) as $index => $item)
                                        @include('motamed.show_motamed_allotment', ['item' => $item]) 
                                    @endforeach
                                @else 
                                    @foreach ($employee->allotments->where('status' , 0) as $index => $item)
                                        @include('motamed.show_motamed_allotment', ['item' => $item])
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>








                    <div class="row no-print">
                        <a href="#" data-toggle="modal" v-if="item_details_specifications.length>0"
                            @click="clearModel()" data-target="#allotment_history" title="توزیع جنس به اساس کتاب"
                            class="btn m-btn--square  btn-success ull-left"> <i class="la la-rotate-left"></i> توزیع جنس
                            به اساس کتاب</a>
                        @if($status == null || $status == 1 )
                        <div class="col-lg-2">
                            <button title="اعاده جنس" @click="printAllotments()" class="btn m-btn--square  btn-info"> <i
                                    class="la la-print"></i> چاپ لست</button>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade bd-example-modal-lg" id="allotment_history" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">تاریخچه جمع و قید جنس</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {{-- Start of Alloted to new Employees --}}
                        <span class="p-5 text-danger" v-if="error">لطفآ تسلیم گیرنده گان را انتخاب نمائید</span>
                        <div v-if="allotment_history.length > 0" class="m-scrollable" data-scrollable="true"
                            data-height="380" data-mobile-height="300">

                            <!--Begin::Timeline 2 -->
                            <div class="m-timeline-2">
                                <div class="m-timeline-2__items  m--padding-top-25 m--padding-bottom-30">
                                    <div v-for="(item, index) in allotment_history" class="m-timeline-2__item">
                                        <span class="m-timeline-2__item-time m--font-primary">@{{ item.allotment_date }}</span>
                                        <div class="m-timeline-2__item-cricle">
                                            <i
                                                :class="[item.status == 0 ? 'fa fa-genderless m--font-warning' : item.status ==
                                                    2 ? 'fa fa-home m--font-danger icon-sm' :
                                                    'fa fa-genderless m--font-success'
                                                ]"></i>
                                        </div>
                                        <div class="m-timeline-2__item-text  m--padding-top-5">
                                            <div class="font-weight-bold">
                                                <span v-if="item.motamed != null">
                                                    @{{ item.motamed.name_dr }} @{{ item.motamed.last_name }} -- ولد --
                                                    @{{ item.motamed.father_name_dr }}
                                                </span>
                                                <span v-else>
                                                    <span v-if="item.employee_type == '0'">@{{ item.employee.name_dr }}
                                                        @{{ item.employee.last_name }} -- ولد -- @{{ item.employee.father_name_dr }}</span>
                                                    <span v-if="item.employee_type =='1'">@{{ item.external_employee.name_dr }}
                                                        @{{ item.external_employee.last_name }} -- ولد -- @{{ item.external_employee.father_name_dr }}</span>
                                                </span>
                                            </div>
                                            <div class="">
                                                <span v-if="item.motamed != null"> @{{ item.motamed.current_position_dr }}</span>
                                                <span v-else>
                                                    <span v-if="item.employee_type == '0'">@{{ item.employee.current_position_dr }}</span>
                                                    <span v-if="item.employee_type =='1'">@{{ item.external_employee.current_position_dr }}</span>
                                                </span>
                                            </div>
                                            <div v-if="item.condition == 0" class="m--font-success">
                                                جدید
                                            </div>
                                            <div v-else-if="item.condition == 1" class="m--font-warning">
                                                مستعمل
                                            </div>
                                            <div v-else-if="item.condition == 2" class="m--font-danger">
                                                داغمه
                                            </div>
                                            <div v-else-if="item.condition == 3" class="m--font-danger">
                                                غیر فعال
                                            </div>
                                            <div v-if="item.status == 0" class="font-weight-bold m--font-info">
                                                تاریخ اعاده:‌@{{ item.return_date }}
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        {{-- End of Alloted to new Employees --}}

                        <div v-else-if="active_allotment.length > 0">
                            <div>
                                <table class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>شماره</th>
                                            <th>جنس</th>
                                            <th>توضیحات</th>
                                            <th>حالت جنس </th>
                                            <th>{{ trans('global.fece9_date') }}</th>
                                            <th>شخص تسلیم گیرنده</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for=" (item,index) in active_allotment">
                                            <td>@{{ ++index }}</td>
                                            <td>@{{ item.item_details_specifications.item_details.name_dr }}</td>
                                            <td>
                                                <div class="ditails" style="display:none">
                                                    <span class="m--font-primary"
                                                        v-if="item.item_details_specifications.sub_category_keys.col1">
                                                        @{{ item.item_details_specifications.sub_category_keys.col1 }} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        @{{ item.item_details_specifications.col1 }} ||
                                                    </span>
                                                    <span class="m--font-primary"
                                                        v-if="item.item_details_specifications.sub_category_keys.col2">
                                                        @{{ item.item_details_specifications.sub_category_keys.col2 }} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        @{{ item.item_details_specifications.col2 }} ||
                                                    </span>
                                                    <span class="m--font-primary"
                                                        v-if="item.item_details_specifications.sub_category_keys.col3">
                                                        @{{ item.item_details_specifications.sub_category_keys.col3 }} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        @{{ item.item_details_specifications.col3 }} ||
                                                    </span>
                                                    <span class="m--font-primary"
                                                        v-if="item.item_details_specifications.sub_category_keys.col4">
                                                        @{{ item.item_details_specifications.sub_category_keys.col4 }} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        @{{ item.item_details_specifications.col4 }}
                                                    </span>
                                                    <span class="m--font-primary"
                                                        v-if="item.item_details_specifications.sub_category_keys.col5">
                                                        @{{ item.item_details_specifications.sub_category_keys.col5 }} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        @{{ item.item_details_specifications.col5 }}
                                                    </span>
                                                    <span class="m--font-primary"
                                                        v-if="item.item_details_specifications.sub_category_keys.col6">
                                                        @{{ item.item_details_specifications.sub_category_keys.col6 }} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        @{{ item.item_details_specifications.col6 }}
                                                    </span>
                                                    <span class="m--font-primary"
                                                        v-if="item.item_details_specifications.sub_category_keys.col7">
                                                        @{{ item.item_details_specifications.sub_category_keys.col7 }} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        @{{ item.item_details_specifications.col7 }}
                                                    </span>
                                                    <span class="m--font-primary"
                                                        v-if="item.item_details_specifications.sub_category_keys.col8">
                                                        @{{ item.item_details_specifications.sub_category_keys.col8 }} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        @{{ item.item_details_specifications.col8 }}
                                                    </span>
                                                    <span class="m--font-primary"
                                                        v-if="item.item_details_specifications.sub_category_keys.col9">
                                                        @{{ item.item_details_specifications.sub_category_keys.col9 }} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        @{{ item.item_details_specifications.col9 }}
                                                    </span>
                                                    <span class="m--font-primary"
                                                        v-if="item.item_details_specifications.sub_category_keys.col10">
                                                        @{{ item.item_details_specifications.sub_category_keys.col10 }} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        @{{ item.item_details_specifications.col10 }}
                                                    </span>
                                                    <span class="m--font-primary"
                                                        v-if="item.item_details_specifications.sub_category_keys.col11">
                                                        @{{ item.item_details_specifications.sub_category_keys.col11 }} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        @{{ item.item_details_specifications.col11 }}
                                                    </span>
                                                    <span class="m--font-primary"
                                                        v-if="item.item_details_specifications.sub_category_keys.col12">
                                                        @{{ item.item_details_specifications.sub_category_keys.col12 }} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        @{{ item.item_details_specifications.col12 }}
                                                    </span>
                                                </div>
                                                <a href="javascript:;" class="btn btn-xs green toggle_description">
                                                    نمایش <i class="fa fa-plus"></i>
                                                </a>
                                            </td>
                                            <td>@{{ item.allotment_date }}</td>

                                            <td width="150">

                                                <span v-if="item.condition==1">جدید</span>
                                                <span v-else-if="item.condition==0">داغمه</span>
                                                <span v-else-if="item.condition==2">مستعمل</span>
                                                <span v-else-if="item.condition==3">غیر فعال</span>
                                            </td>

                                            <td>
                                                <span v-if="item.employee_type == 0 "> @{{ item.employee.name_dr }}
                                                    @{{ item.employee.last_name }} -- ولد -- @{{ item.employee.father_name_dr }}</span>
                                                <span v-else> @{{ item.external_employee.name_dr }} @{{ item.external_employee.last_name }} -- ولد --
                                                    @{{ item.external_employee.father_name_dr }}</span>
                                            </td>

                                        </tr>
                                    </tbody>
                                </table>
                                <div class="form-group row ">
                                    <div class="form-group col-md-6 m-form__group p-4 ">
                                        <button class="btn btn-success save-button"
                                            @click="roleBackAllotement(active_allotment[0].id,active_allotment[0].item_details_specifications.id)">
                                            انتقال به دیپو <i class="fa fa-archive"></i></button>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div v-else-if="item_details_specifications.length>0" class="m-portlet__body">
                            <div>
                                <table class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>شماره</th>
                                            <th>جنس</th>
                                            <th>توضیحات</th>
                                            <th>
                                                تعداد
                                            </th>
                                            <th>حالت جنس
                                                <label
                                                    class="m-checkbox m-checkbox--single m-checkbox--state m-checkbox--state-primary">
                                                    <input type="checkbox" @change="selectStates($event.target.checked)">
                                                    <span></span>
                                                </label>
                                            </th>
                                            <th>تکت توزیع / م۷</th>
                                            <th>{{ trans('global.fece9_date') }}
                                                <label
                                                    class="m-checkbox m-checkbox--single m-checkbox--state m-checkbox--state-primary">
                                                    <input type="checkbox" @change="selectDates($event.target.checked)">
                                                    <span></span>
                                                </label>
                                            </th>

                                            <th>
                                                شخص تسلیم گیرنده

                                                <label
                                                    class="m-checkbox m-checkbox--single m-checkbox--state m-checkbox--state-primary">
                                                    <input type="checkbox" @change="selectDates($event.target.checked)">
                                                    <span></span>
                                                </label>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for=" (item,index) in item_details_specifications">
                                            <td>@{{ ++index }}</td>
                                            <td>@{{ item.item_details.name_dr }}</td>
                                            <td>
                                                <div class="ditails" style="display:none">
                                                    <span class="m--font-primary" v-if="item.sub_category_keys.col1">
                                                        @{{ item.sub_category_keys.col1 }} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        @{{ item.col1 }} ||
                                                    </span>
                                                    <span class="m--font-primary" v-if="item.sub_category_keys.col2">
                                                        @{{ item.sub_category_keys.col2 }} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        @{{ item.col2 }} ||
                                                    </span>
                                                    <span class="m--font-primary" v-if="item.sub_category_keys.col3">
                                                        @{{ item.sub_category_keys.col3 }} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        @{{ item.col3 }} ||
                                                    </span>
                                                    <span class="m--font-primary" v-if="item.sub_category_keys.col4">
                                                        @{{ item.sub_category_keys.col4 }} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        @{{ item.col4 }}
                                                    </span>
                                                    <span class="m--font-primary" v-if="item.sub_category_keys.col5">
                                                        @{{ item.sub_category_keys.col5 }} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        @{{ item.col5 }}
                                                    </span>
                                                    <span class="m--font-primary" v-if="item.sub_category_keys.col6">
                                                        @{{ item.sub_category_keys.col6 }} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        @{{ item.col6 }}
                                                    </span>
                                                    <span class="m--font-primary" v-if="item.sub_category_keys.col7">
                                                        @{{ item.sub_category_keys.col7 }} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        @{{ item.col7 }}
                                                    </span>
                                                    <span class="m--font-primary" v-if="item.sub_category_keys.col8">
                                                        @{{ item.sub_category_keys.col8 }} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        @{{ item.col8 }}
                                                    </span>
                                                    <span class="m--font-primary" v-if="item.sub_category_keys.col9">
                                                        @{{ item.sub_category_keys.col9 }} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        @{{ item.col9 }}
                                                    </span>
                                                    <span class="m--font-primary" v-if="item.sub_category_keys.col10">
                                                        @{{ item.sub_category_keys.col10 }} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        @{{ item.col10 }}
                                                    </span>
                                                    <span class="m--font-primary" v-if="item.sub_category_keys.col11">
                                                        @{{ item.sub_category_keys.col11 }} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        @{{ item.col11 }}
                                                    </span>
                                                    <span class="m--font-primary" v-if="item.sub_category_keys.col12">
                                                        @{{ item.sub_category_keys.col12 }} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        @{{ item.col12 }}
                                                    </span>
                                                    <span class="m--font-primary"
                                                        v-if="item.sub_category_keys.distribution_type=='1'">
                                                        تعداد موجود ||
                                                    </span>
                                                    <span class="m--font-info">
                                                        @{{ item.item_amount }}
                                                    </span>

                                                </div>
                                                <a href="javascript:;" class="btn btn-xs green toggle_description">
                                                    نمایش <i class="fa fa-plus"></i>
                                                </a>
                                            </td>
                                            <td width="" v-if="item.sub_category_keys.distribution_type=='1'">
                                                <div class="input-group date">
                                                    <input type="number" v-model="select_all_amount"
                                                        v-on:keyup="validate($event.target.value,item.item_amount)"
                                                        name="select_all_amount" :id="'select_all_amount' + (index - 1)"
                                                        :ref="'select_all_amount' + (index - 1)"
                                                        value="{{ old('select_all_amount') }}" min="1"
                                                        :max="item.item_amount"
                                                        :class="['form-control', allotment_error == true ? 'is-invalid' : '']"
                                                        placeholder="تعداد توزیع">
                                                </div>
                                            </td>
                                            <td width="" else>
                                                <div class="input-group date">
                                                    <input type="number" disabled v-model="select_all_amount"
                                                        v-on:keyup="validate($event.target.value,item.item_amount)"
                                                        name="select_all_amount" :id="'select_all_amount' + (index - 1)"
                                                        :ref="'select_all_amount' + (index - 1)"
                                                        value="{{ old('select_all_amount') }}" min="1"
                                                        :max="item.item_amount"
                                                        :class="['form-control', allotment_error == true ? 'is-invalid' : '']"
                                                        placeholder="تعداد توزیع">
                                                </div>
                                            </td>
                                            <td width="100">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <select class="form-control" :id="'condition' + (index - 1)"
                                                            :ref="'condition' + (index - 1)">

                                                            <option value="0" :selected="select_all_state == 0">جدید
                                                            </option>
                                                            <option value="1" :selected="select_all_state == 1">مستعمل
                                                            </option>
                                                            <option value="2" :selected="select_all_state == 2">داغمه
                                                            </option>
                                                            <option value="3" :selected="select_all_state == 3">غیر
                                                                فعال</option>
                                                        </select>
                                                        <!---->
                                                        {{-- @{{ item.condition == 0 ? "selected" : "" }}
                                                    @{{ item.condition == 1 ? "selected" : "" }}
                                                    @{{ item.condition == 2 ? "selected" : "" }} --}}
                                                    </div>
                                                </div>
                                            </td>
                                            <td width="">
                                                <div class="input-group ">
                                                    <input type="text" :id="'details' + (index - 1)"
                                                        :ref="'details' + (index - 1)" value="{{ old('details') }}"
                                                        class="form-control " placeholder="توضیحات">
                                                </div>

                                            </td>

                                            <td width="">
                                                <div class="input-group date">
                                                    {{-- <input type="text"  v-model="select_all_date" name="date" :id="'date'+(index-1)"  onfocus='$(".persian_date").persianDatepicker();' :ref="'date'+(index-1)" value="{{old('date')}}" class="form-control persian_date" placeholder="تاریخ انتخاب کنید."> --}}
                                                </div>
                                                <date-picker locale="fa" format="jYYYY-jMM-jDD" :id="'date' + (index - 1)"
                                                    :ref="'date' + (index - 1)" required></date-picker>

                                            </td>

                                            <td width="500">
                                                <div class="input-group mb-3">
                                                    <input type="text" :ref="'name' + (index - 1)"
                                                        class="form-control" placeholder="نام">
                                                    <input type="text" :ref="'father_name' + (index - 1)"
                                                        class="form-control" placeholder="نام پدر">
                                                    <div class="input-group-append">
                                                        <button class="btn btn-success " :id="index - 1"
                                                            @click="searchEmp(index -1)">
                                                            <i class="fa fa-search"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="employee_type">نوعیت کارمند</label>
                                                    <select class="form-control" id="employee_type"
                                                        :ref="'employee_type' + (index - 1)">
                                                        <option value="0">داخلی</option>
                                                        <option value="1">خارجی</option>
                                                    </select>
                                                </div>
                                                <div class="col-*" :ref="'employeeDetails' + (index - 1)"
                                                    :id="'employeeDetails' + (index - 1)">
                                                </div>
                                                <table>
                                                    <tr v-for="(emp, empIndex) in employeeByIndex(index -1 )"
                                                        :key="emp.id">
                                                        <td>
                                                            @{{ emp.name_dr }}
                                                            ولد
                                                            @{{ emp.father_name_dr }}
                                                            وظیفه
                                                            @{{ emp.current_position_dr }}
                                                        </td>
                                                        <td>
                                                            <button class="btn btn-success save-button"
                                                                @click="addItemDetailsSpecificationsToReturnList(index-1, item.id, emp.id,emp)">
                                                                تائید <i class="la la-check"></i></button>
                                                        </td>
                                                    </tr>
                                                </table>

                                            </td>

                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="form-group row ">
                                <div class="form-group col-md-6 m-form__group ">
                                    <button class="btn btn-success save-button" :disabled="allotment_error"
                                        @click="submitAllotements()"> {{ trans('global.submit') }} <i
                                            class="la la-rotate-left"></i></button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('custom-css')
    <style>
        label.m-checkbox {
            display: inline;
            margin: 1.5em;
        }

        .m-invoice-2 .m-invoice__wrapper .m-invoice__body table tbody tr:first-child td {
            padding-top: 10px;
            padding-bottom: 10px;
        }

        .m-timeline-2:before {
            right: 5.89rem;
        }

        .m-timeline-2 .m-timeline-2__items .m-timeline-2__item .m-timeline-2__item-cricle {
            right: 5.1rem;
        }

        @media (min-width: 992px) {
            .modal-lg {
                max-width: 70%;
            }
        }


        .m-invoice-2 .m-invoice__wrapper .m-invoice__body table thead tr th:not(:first-child) {
            text-align: right;
        }

        .m-invoice__logo.pb-2 {
            padding-top: 0 !important;
        }

        .table_border td {
            border: 1px solid #000;
        }

        .table_header th {
            border: 1px solid #000 !important;
            color: #000 !important;
        }

        @media (min-width: 992px) {

            .modal-lg {
                max-width: 90%;
            }
        }

        tr.hidden {
            display: none;
        }

        .search_box {
            padding: 8px;
            width: 280px;
            border: 2px solid #ccc;
            border-radius: 15px;
        }

        input.search_box :focus {
            padding: 8px;
            width: 280px;
            border: 2px solid #ccc !important;
            border-radius: 15px;
        }

        input:focus {
            outline-offset: -22px;
        }

        :focus {
            outline: -webkit-focus-ring-color auto 1px;
            outline-color: -webkit-focus-ring-color;
            outline-style: auto;
            outline-width: 1px;
            border-radius: 15px;
        }

        .icon-sm {
            font-size: 1.2rem !important;
            right: -0.7rem !important;
        }
    </style>
@endpush
@push('custom-js')
    <script src="{{ asset('public/js/moment.js') }}"></script>
    <script src="{{ asset('public/js/moment-jalali.js') }}"></script>
    <script src="{{ asset('public/js/vue-persian-datetime-picker-browser.js') }}"></script>
    <script>
        Vue.component('v-select', VueSelect.VueSelect)
        motamed_allotments = new Vue({
            el: '#motamed_allotments',
            data: {
                allotment_history: [],
                item_details_specifications: [],
                employees: [
                    // {
                    // 'id':0,
                    // 'name_dr': 'اعاده به دیپو'
                    // }
                ],
                // dipo:{'id':'0','name_dr':'اعاده به دیپو'},
                current_employee_id: '{{ $employee->id }}',
                current_item_details_id: 0,
                return_list: [],
                return_list_string: [],
                number: '',
                date: '',
                description: '',
                success: false,
                error: false,
                errors: '',
                receiver_employee_error: false,
                employee_list: '',
                show_form: false,
                receiver_id: null,
                return_type: '',
                model_title: '',
                alloted_item_amount: 1,
                return_error: false,
                active_allotment: [],
                specifications: {},
                allotments: {},
                extra_specifications: '',
                select_all_state: '',
                select_all_amount: 1,
                allotment_error: false,
                allotment_list: [],
            },
            components: {
                DatePicker: VuePersianDatetimePicker
            },
            methods: {

                validate(alloted_item, item_amount) {
                    if (alloted_item > item_amount) {
                        this.return_error = true;
                    } else {
                        this.return_error = false;
                    }
                },
                showItemAllotmentHistory(item_details_specifications_id, item_balances_id = null) {
                    this.model_title = 'تاریخچه جمع و قید جنس';
                    axios.get("{{ url('allotments/get_item_details_specifications_allotment_history') }}/" +
                            item_details_specifications_id + '/' + item_balances_id)
                        .then(res => {
                            this.allotment_history = res.data;
                        })
                        .catch(err => {
                            console.log(err);
                        });

                },
                addItemDetailsSpecificationsToReturnList(index, item_details_specifications_id, receiver_id, emp) {
                    this.$refs['employeeDetails' + index][0].innerHTML = emp.name_dr + ' ولد ' + emp
                        .father_name_dr + 'وظیفه ' + emp.current_position_dr;
                    var employee_type = 'employee_type' + index
                    date = 'date' + index;
                    details = 'details' + index;
                    condition = 'condition' + index;
                    select_all_amount = 'select_all_amount' + index;
                    var object = {};
                    object['item_details_specifications_id'] = item_details_specifications_id;
                    // object['receiver_employee_id'] = this.receiver_id;
                    object['receiver_employee_id'] = receiver_id;
                    object['status'] = 1;
                    object['employee_type'] = this.$refs[employee_type][0].value
                    object['condition'] = this.$refs[condition][0].value;
                    object['allotment_date'] = this.$refs[date][0].value;
                    object['details'] = this.$refs[details][0].value;
                    object['total_alloted'] = this.$refs[select_all_amount][0].value;
                    this.allotment_list[index] = object;
                    this.allotment_list_string = JSON.stringify(this.allotment_list[index]);
                    this.employees = []
                },
                submitAllotements() {

                    if (this.$refs[details][0].value != '' && this.receiver_id != '') {


                        if (this.item_details_specifications.length == this.allotment_list.length) {
                            axios.post("{{ route('allotments') }}", {
                                    'allotment_list': this.allotment_list
                                })
                                .then(res => {
                                    location.reload();
                                })
                                .catch(err => {
                                    console.log(err);
                                });
                        } else {
                            this.error = true;
                        }
                    }

                    $('.save-button').disable();
                },
                getItemDetailsSpecifications(evt, index, item_details_specifications_id) {
                    if (evt) {
                        axios.get("{{ url('allotments/get_item_details_specifications') }}/" +
                                item_details_specifications_id)
                            .then(res => {
                                this.allotment_history = [];
                                this.item_details_specifications.push(res.data);
                            })
                            .catch(err => {
                                console.log(err);
                            });
                    } else {

                        var rem = this.item_details_specifications.filter(item => {
                            return item.id == item_details_specifications_id
                        })[0];

                        remove_id = this.item_details_specifications.indexOf(rem);
                        this.item_details_specifications.splice(remove_id, 1);
                    }

                },
                // checkType(index,item_details_specifications_id){
                //     if (this.return_list.return_type !='')
                //         this.show_form = true;
                //     if(this.return_type !=2){
                //         this.item_details_specifications.map((item,index) => {
                //             this.addItemDetailsSpecificationsToReturnList(index,item.id);
                //         })
                //     }
                // },
                getAllEmployees() {
                    axios.get("{{ url('get_all_employees') }}")
                        .then(res => {
                            this.employees = res.data;
                        })
                        .catch(err => {
                            console.log(err);

                        })

                },
                validate(alloted_item, item_amount) {
                    if (alloted_item > item_amount) {
                        this.allotment_error = true;
                    } else {
                        this.allotment_error = false;
                    }
                },
                selectStates(event) {

                    if (event) {
                        this.select_all_state = this.$refs.condition0[0].value;
                    } else {
                        this.select_all_state = '';
                    }
                },
                clearModel() {
                    this.active_allotment = [];
                    this.allotment_history = [];
                },
                searchEmp(index) {
                    var name = 'name' + index
                    var father_name = 'father_name' + index
                    var employee_type = 'employee_type' + index
                    axios.get("{{ url('get_employees_by_name_father') }}/" + this.$refs[employee_type][0].value +
                            "/" + this.$refs[name][0].value + "/" + this.$refs[father_name][0].value + "/")
                        .then(res => {
                            this.$set(this.employees, index, res.data);
                        })
                        .catch(err => {
                            console.log(err);

                        })
                },
                submitForm: function() {


                    if (this.validateReturn()) {
                        axios.post("{{ route('item_return_requests.store') }}", {
                                'number': this.number,
                                'date': this.$refs.date["value"],
                                'description': this.description,
                                'return_list': this.return_list,
                                'status': '0',
                                'employee_id': this.current_employee_id,
                                'item_amount': this.alloted_item_amount,
                            })
                            .then(res => {
                                this.success = true;
                                window.location.href = "{{ url('item_return_requests') }}" + '/' + res
                                .data;
                            })
                            .catch(err => {
                                this.error = true;
                                this.errors = err.response.data.errors;
                            })
                    } else {
                        this.receiver_employee_error = true;

                    }
                },

                validateReturn() {


                    if (this.return_type == 2) {
                        if (this.return_list.length == this.item_details_specifications.length) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                    return true;


                },
                printAllotments: function() {
                    $('.details').css('display', 'block');
                    $('.m-invoice-2 .m-invoice__wrapper .m-invoice__body table tbody tr td').css('color',
                        '#000');
                    $('.m-portlet .m-portlet__body').css('color', '#000');
                    $('table').removeClass('table-bordered');
                    $('table').addClass('table_border');
                    $('table').addClass('table_header');
                    $('.un').css('display', 'none');
                    $('.search_box').css('display', 'none');
                    $('.m-invoice-2 .m-invoice__wrapper .m-invoice__body table tbody tr td').css('text-align',
                        'center !important');
                    window.print();
                    $('.un').css('display', 'block');
                    $('.search_box').css('display', 'block');
                    $('.details').css('display', 'none');
                    $('.m-invoice-2 .m-invoice__wrapper .m-invoice__body table tbody tr td').css('color',
                        '#6f727d');
                    $('table').removeClass('table_border');
                    $('table').removeClass('table_header');
                    $('table').addClass('table-bordered');
                    $('.m-portlet .m-portlet__body').css('color', '#575962');


                },
                getSelect2: function() {
                    // $(".select2").select2();
                },


                roleBackAllotement(allotment_id, item_details_specifications_id) {
                    axios.get("{{ url('allotments/role_back_allotment') }}/" + allotment_id + "/" +
                            item_details_specifications_id)
                        .then(res => {
                            location.reload();
                        })
                        .catch(err => {
                            console.log(err);
                        });

                },
                checkAll: function(event, employee_id) {
                    this.model_title = 'اعاده جنس';
                    if (event.target.checked) {
                        $('input.checkbox:not(input.checkbox:disabled)').prop('checked', true);

                        axios.get("{{ url('allotments/get_all_item_details_specifications') }}/" + employee_id)
                            .then(res => {
                                for ($i = 0; $i < res.data.length; $i++) {
                                    this.item_details_specifications.push(res.data[$i][
                                        'item_details_specifications'
                                    ]);
                                }
                            })
                            .catch(err => {
                                console.log(err);
                            });
                    } else {
                        this.item_details_specifications = [];
                        $('input.checkbox:not(input.checkbox:disabled)').prop('checked', false);
                    }
                    var len = $('input[type="checkbox"]:checked').length - 1;


                },
                onSearch(search, loading) {
                    loading(true);
                    this.search(loading, search, this);
                },
                search: _.debounce((loading, search, vm) => {

                    axios.post(`{{ route('get_employee_axios') }}`, {
                            'name': search
                        })
                        .then(res => {
                            console.log(res.data.length);
                            if (res.data.length != 0)
                                vm.employees = res.data.items;
                            // else{
                            //     vm.employees=[];
                            //     vm.employees.push(vm.dipo);    
                            // }
                            loading(false);
                        })
                        .catch(err => {
                            console.log(err);
                        });

                }, 350)

            },

            mounted() {
                // this.getAllEmployees();
            },
            computed: {

                employeeByIndex() {
                    return (index) => {
                        return this.employees[index] || [];
                    };
                },
                getMotamedName() {
                    return (id) => {
                        const motamed_name = this.motameds.find(motamed => motamed.id == id)
                        return motamed_name ? motamed_name.name_dr : ''
                    }
                },
            },
        });

        $('#allotment_history').on('hidden.bs.modal', function() {
            motamed_allotments.allotment_history = '';
        });

        $('[data-search]').on('keyup', function() {
            var searchVal = $(this).val();
            var filterItems = $('[data-filter-item]');

            if (searchVal != '') {
                filterItems.addClass('hidden');
                $('[data-filter-item][data-filter-name*="' + searchVal.toLowerCase() + '"]').removeClass('hidden');
            } else {
                filterItems.removeClass('hidden');
            }
        });
        $('[data-search_details]').on('keyup', function() {
            var searchVal = $(this).val();
            var filterItems = $('[ data-filter-item-details]');

            if (searchVal != '') {
                filterItems.addClass('hidden');
                $('[data-filter-item][data-filter-details*="' + searchVal.toLowerCase() + '"]').removeClass(
                    'hidden');
            } else {
                filterItems.removeClass('hidden');
            }
        });
    </script>
@endpush
