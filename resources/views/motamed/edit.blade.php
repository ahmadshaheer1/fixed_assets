@extends('layouts.master')
@section('title', trans('global.edit_motamed'))
@section('content')

<form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{route('motamed.update', encrypt($motamed->id))}}">
    @csrf
    @method('PUT')
    <div class="m-portlet__body">
        <div class="form-group m-form__group {{$errors->has('employee_id') ? 'has-danger' : ''}}">
            <label for="employee_id">کارمند</label>
            <select class="form-control select2 m-input m-input--square" name="employee_id">
                <option value="">--انتخاب--</option>
                @foreach ($employees as $value)
                    <option {{$motamed->employee_id == $value->id ? 'selected' : ''}} value="{{$value->id}}">{{$value->name_dr}} &nbsp; ({{$value->last_name}}) &nbsp; ولد &nbsp;{{$value->father_name_dr}}</option>
                @endforeach
            </select>
            @if($errors->has('employee_id'))
                <div class="form-control-feedback">{{$errors->first('employee_id')}}</div>
            @endif
        </div>
        <div class="form-group m-form__group {{$errors->has('motamed_type_id') ? 'has-danger' : ''}}">
            <label for="motamed_type_id">{{trans('global.motamed_type')}}</label>
            <select class="form-control select2 m-input m-input--square" name="motamed_type_id">
                <option value="">--انتخاب--</option>
                @foreach ($motamed_types as $value)
                    <option {{$motamed->motamed_type_id == $value->id ? 'selected' : ''}} value="{{$value->id}}">{{$value->name}}</option>
                @endforeach
            </select>
            @if($errors->has('motamed_type_id'))
                <div class="form-control-feedback">{{$errors->first('motamed_type_id')}}</div>
            @endif
        </div>
    </div>
    <div class="m-portlet__foot m-portlet__foot--fit">
        <div class="m-form__actions">
            <button type="submit" class="btn btn-primary">{{trans('global.submit')}}</button>
            <button type="reset" class="btn btn-danger">{{trans('global.cancel')}}</button>
            <a  href="{{route('allotments.motamed_allotments')}}" class="btn btn-info">برگشت</a>
        </div>
    </div>
</form>
@endsection
