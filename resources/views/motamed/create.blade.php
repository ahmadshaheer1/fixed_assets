@extends('layouts.master')
@section('title',' ایجاد معتمد')
@section('content')
  {!! Form::open(array('route' => 'motamed.store','method'=>'POST')) !!}
    <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">

                    <div class="form-group m-form__group {{$errors->has('employee_id') ? 'has-danger' : ''}}">
                    <label class="form-control-label" for="employee_id">کارمند</label>
                    <select class="form-control select2" name="employee_id">
                        <option value="">--انتخاب--</option>
                        @foreach ($employees as $value)
                        <option {{old('employee_id') == $value->id ? 'selected' : ''}} value="{{$value->id}}">{{$value->name_dr}} ولد {{$value->father_name_dr}}</option>
                        @endforeach
                    </select>
                    <div class="form-control-feedback">{{$errors->first('employee_id')}}</div>
                    </div>
                
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">

                    <div class="form-group m-form__group {{$errors->has('motamed_type_id') ? 'has-danger' : ''}}">
                    <label class="form-control-label" for="motamed_type_id">{{trans('global.motamed_type')}}</label>
                    <select class="form-control select2" name="motamed_type_id">
                        <option value="">--انتخاب--</option>
                        @foreach ($motamed_types as $value)
                        <option {{old('motamed_type_id') == $value->id ? 'selected' : ''}} value="{{$value->id}}">{{$value->name}} </option>
                        @endforeach
                    </select>
                    <div class="form-control-feedback">{{$errors->first('motamed_type_id')}}</div>
                    </div>
                
            </div>
            <div class="row">
                <div class="col-2">
                </div>
                <div class="col-12">
                    <button  type="submit" class="btn btn-primary">ثبت</button>
                    <button  type="reset" class="btn btn-secondary">تنظیم مجدد</button>
                    <a  href="{{route('users.index')}}" class="btn btn-danger">برگشت</a>
                </div>
            </div>
    </div>
  {!! Form::close() !!}
@endsection
