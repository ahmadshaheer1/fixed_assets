
<div id="create_vendor">

    <div v-if="success" class="m-alert m-alert--icon m-alert--outline alert alert-success alert-dismissible fade show" role="alert">
            <div class="m-alert__icon">
                <i class="la la-check"></i>
            </div>
            <div class="m-alert__text">
                @{{message}}
            </div>
            <div class="m-alert__close">
                <button style="line-height: 3;" type="button" class="close" data-dismiss="alert" aria-label="Close">
                </button>
            </div>
        </div>

<form class="m-form m-form--fit m-form--label-align-right"  @submit.prevent="createVendor">
    @csrf
    <div class="m-portlet__body">
        <div :class="['form-group m-form__group', error_messages.{{$name}} ? 'has-danger': '']">
            <label for="vendor_name">{{trans('global.name')}}</label>
            <input name="{{$name}}" v-model="{{$name}}" type="text" class="form-control m-input m-input--square" id="vendor_name" aria-describedby="emailHelp" placeholder="{{trans('global.name')}}">

            <div class="form-control-feedback">@{{error_messages.{!! $name !!}}}</div>
        </div>
        <div class="form-group m-form__group">
            <label for="vendor_description">{{trans('global.description')}}</label>
            <textarea name="description" v-model="description" class="form-control" id="vendor_description"placeholder="{{trans('global.description')}}">{{old('description')}}</textarea>
        </div>
    </div>
    <div class="m-portlet__foot m-portlet__foot--fit">
        <div class="m-form__actions">
            <button type="submit" class="btn btn-primary">{{trans('global.submit')}}</button>
            <button type="reset" class="btn btn-danger">{{trans('global.cancel')}}</button>
        </div>
    </div>
</form>
</div>
<script>
      axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        vendors_app = new Vue({
            el: '#create_vendor',
            data: {
                // main_category_id: {!! json_encode($data['main_category_id'], 1) !!},
                {{$name}}: '',
                description: '',

                error: false,

                error_messages: [

                ],

                success: false,
            },
            methods: {
                createVendor(event) {
                    // if(this.error == false) {
                        axios.post("{{route('vendors.store')}}", {
                            "description": this.description,
                            "{{$name}}": this.{{$name}},
                        })
                        .then(res => {
                            console.log('okkk');
                            this.success =  true;
                            this.message =  res.data.success;
                            $('#exampleModal').modal('hide')
                        })
                        .catch(err => {
                            this.error = true;
                            this.error_messages = err.response.data.errors;
                        })

                    // }
                }
            },
            beforeMount() {
                // if(typeof this.main_category_id != 'number'){
                //     this.error = true;
                //     this.error_messages.main_category_message = 'کتگوری اصلی باید از فورم قبلی انتخاب گردد!!!';
                // }
            }
        });
        </script>

