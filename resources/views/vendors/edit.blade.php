<div id="edit_vendor">
        <div v-if="success" class="m-alert m-alert--icon m-alert--outline alert alert-success alert-dismissible fade show" role="alert">
            <div class="m-alert__icon">
                <i class="la la-check"></i>
            </div>
            <div class="m-alert__text">
                @{{message}}
            </div>
            <div class="m-alert__close">
                <button style="line-height: 3;" type="button" class="close" data-dismiss="alert" aria-label="Close">
                </button>
            </div>
        </div>
    <form class="m-form m-form--fit m-form--label-align-right" method="POST" @submit.prevent="editVendor">
        @csrf
        @method('PUT')
        <div class="m-portlet__body">
            <div :class="['form-group m-form__group', error_messages.name_dr ? 'has-danger': '']">
                <label for="vendor_name">{{trans('global.name')}}</label>
                <input name="{{$name}}" v-model="vendor_name" type="text" class="form-control m-input m-input--square" id="vendor_name" aria-describedby="emailHelp" placeholder="{{trans('global.name')}}">
                <div class="form-control-feedback">@{{error_messages.name_dr}}</div>
            </div>
            <div class="form-group m-form__group">
                <label for="vendor_description">{{trans('global.description')}}</label>
                <textarea name="description" class="form-control" id="vendor_description"placeholder="{{trans('global.description')}}" v-model="description"></textarea>
            </div>
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">
            <div class="m-form__actions">
                <button type="submit" class="btn btn-primary">{{trans('global.submit')}}</button>
                <button type="reset" class="btn btn-danger">{{trans('global.cancel')}}</button>
            </div>
        </div>
    </form>
</div>

<script>
        edit_vendor=new Vue({
             el: "#edit_vendor",
             data: {
               vendor_id: {{$vendor->id}},
               vendor_name: '{{$vendor->$name}}',
               description: '{{$vendor->description}}',
               error: false,
               error_messages: [],
               success: false,

             },
             methods: {
                editVendor(event){

                 axios.put("{{url('vendors')}}/"+this.vendor_id, {
                   "{{$name}}": this.vendor_name,
                   'description': this.description,
                 })
                 .then(res => {
                   this.success = true;
                   this.message = res.data.success;
                   $('#exampleModal').modal('hide')


                 })
                 .catch(err => {
                   console.log(err.data);
                   this.error = true;
                   this.error_messages = err.response.data.errors;
                 })
               }


             }
           });
 </script>
