@extends('layouts.master')
@section('title', trans('global.end_categories'))
@section('content')
<table class="table table-striped- table-bordered table-hover table-checkable jquery-datatable" id="m_table_1">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">{{trans('global.name')}}</th>
        <th scope="col">{{trans('global.nature')}}</th>
        <th scope="col">{{trans('global.description')}}</th>
        <th scope="col">{{trans('global.actions')}}</th>
      </tr>
    </thead>
    <tbody>

        @foreach ($data as $key => $item)
            <tr id="{{$item->id}}_tr">
                <th scope="row">{{++$key}}</th>
                <td>{{$item->$name}}</td>
                <td>{{$item->nature =='0' ? 'غیر صرفی' :'مصرفی'}}</td>
                <td>{{str_limit(strip_tags($item->description), 150, ' ...')}}</td>
                <td>
                    {{-- <a class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" href="{{ route('end_categories.show',$item->id) }}"><i class="la la-eye"></i></a> --}}
                    <a class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" href="{{ route('main_categories.edit',$item->id) }}"><i class="la la-edit"></i></a>
                    <a href="javascript:void(0)" onclick="deleteRecord('{{route('main_categories.destroy', $item->id)}}','{{$item->id}}_tr');" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill ">
                        <i class="fa fa-trash"></i>
                    </a>
                </td>
            </tr>
        @endforeach
    </tbody>
  </table>
@endsection
{{--
@push('create-button')
<a href="{{ route('main_categories.create') }}" class="btn btn-success m-btn m-btn--custom ">
    <span>
    <span>اضافه نمودن</span>
    <i class="la la-plus"></i>
    </span>
</a>
@endpush --}}
