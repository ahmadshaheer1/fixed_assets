<div id="edit_main_category">
<div v-if="success" class="m-alert m-alert--icon m-alert--outline alert alert-success alert-dismissible fade show" role="alert">
        <div class="m-alert__icon">
            <i class="la la-check"></i>
        </div>
        <div class="m-alert__text">
            @{{message}}
        </div>
        <div class="m-alert__close">
            <button style="line-height: 3;" type="button" class="close" data-dismiss="alert" aria-label="Close">
            </button>
        </div>
</div>
<form class="m-form m-form--fit m-form--label-align-right" method="POST"   @submit.prevent="editMainCategory" >
    @csrf
    @method('PUT')
    <div class="m-portlet__body">
        <div :class="['form-group m-form__group', error_messages.{{$name}} ? 'has-danger': '']">
            <label for="main_category_name">{{trans('global.name')}}</label>
            <input name="{{$name}}" value="" v-model="category_name" type="text" class="form-control m-input m-input--square" id="main_category_name" aria-describedby="emailHelp" placeholder="{{trans('global.name')}}">

                <div class="form-control-feedback">@{{error_messages.{!! $name !!}}}</div>

        </div>
        <div :class="['form-group m-form__group', error_messages.nature ? 'has-danger': '']">
            <label for="exampleSelect1">{{trans('global.nature')}}</label>
            <select class="form-control" name="nature" v-model="nature">
                <option value="">{{trans('global.nature')}}</option>
                <option value="0">غیر مصرفی</option>
                <option value="1">مصرفی</option>
            </select>

                <div class="form-control-feedback">@{{error_messages.nature}}</div>

        </div>
        <div class="form-group m-form__group">
            <label for="main_category_description">{{trans('global.description')}}</label>
        <textarea name="description" class="form-control" id="main_category_description"placeholder="{{trans('global.description')}}" v-model="description">@{{description}}</textarea>
        </div>
    </div>
    <div class="m-portlet__foot m-portlet__foot--fit">
        <div class="m-form__actions">
            <button type="submit" class="btn btn-primary">{{trans('global.submit')}}</button>
            <button type="reset" class="btn btn-danger">{{trans('global.cancel')}}</button>
        </div>
    </div>
</form>
</div>
<script>
 edit_main_category=new Vue({
      el: "#edit_main_category",
      data: {
        category_id: {{$main_category->id}},
        category_name: '{{$main_category->$name}}',
        nature: '{{$main_category->nature}}',
        description: '{{$main_category->description}}',
        error: false,
        error_messages: [],
        success: false,

      },
      methods: {
        editMainCategory(event){

          axios.put("{{url('main_categories')}}/"+this.category_id, {
            "{{$name}}": this.category_name,
            'nature': this.nature,
            'description': this.description,

          })
          .then(res => {
            this.success = true;
            this.message = res.data.success;
            $('#exampleModal').modal('hide')


          })
          .catch(err => {
            console.log(err.data);
            this.error = true;
            this.error_messages = err.response.data.errors;
          })
        }


      }
    });
</script>
