<div id="create_main_category">
  <div v-if="success" class="m-alert m-alert--icon m-alert--outline alert alert-success alert-dismissible fade show" role="alert">
      <div class="m-alert__icon">
          <i class="la la-check"></i>
      </div>
      <div class="m-alert__text">
          @{{message}}
      </div>
      <div class="m-alert__close">
          <button style="line-height: 3;" type="button" class="close" data-dismiss="alert" aria-label="Close">
          </button>
      </div>
  </div>
  <form class="m-form m-form--fit m-form--label-align-right main_category_form" @submit.prevent="createMainCategory" >
    <div class="modal-body">
            @csrf
            <div class="m-portlet__body">

                <div :class="['form-group m-form__group', error_messages.{{$name}} ? 'has-danger': '']" id="name_dr">
                    <label for="main_category_name">{{trans('global.name')}}</label>
                    <input name="{{$name}}"  v-model="{{$name}}" type="text" class="form-control m-input m-input--square" id="main_category_name" aria-describedby="emailHelp" placeholder="{{trans('global.name')}}">
                    <div class="form-control-feedback">@{{error_messages.{!! $name !!}}}</div>

                </div>
                <div :class="['form-group m-form__group', error_messages.nature ? 'has-danger': '']" id="nature">
                    <label for="exampleSelect1">{{trans('global.nature')}}</label>
                    <select class="form-control" name="nature" v-model="nature">
                        <option value="">{{trans('global.nature')}}</option>
                        <option value="1">مصرفی</option>
                        <option value="0">غیر مصرفی</option>
                    </select>
                    <div class="form-control-feedback">@{{error_messages.nature}}</div>
                </div>
                <div class="form-group m-form__group">
                    <label for="main_category_description">{{trans('global.description')}}</label>
                    <textarea name="description" class="form-control" v-model='description' id="main_category_description"placeholder="{{trans('global.description')}}"></textarea>

                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div class="m-portlet__foot m-portlet__foot--fit">
                <div class="m-form__actions">
                    <button type="submit" class="btn btn-primary">{{trans('global.submit')}}</button>
                    <button type="reset" class="btn btn-danger">{{trans('global.cancel')}}</button>
                </div>
            </div>
        </div>
    </form>
  </div>
<script>

    main_category=new Vue({
      el: "#create_main_category",
      data: {
        {{$name}}: '',
        nature: '',
        description: '',
        error: false,
        error_messages: [],
        success: false,

      },
      methods: {
        createMainCategory(event){
          if(this.error == false){
          axios.post("{{route('main_categories.store')}}", {
            "{{$name}}": this.{{$name}},
            'nature': this.nature,
            'description': this.description,

          })
          .then(res => {
            this.success = true;
            this.message = res.data.success;
            $('#exampleModal').modal('hide')


          })
          .catch(err => {
            this.error = true;
            this.error_messages = err.response.data.errors;
          })
        }

        }
      }
    });



    // $('.main_category_form').submit(function(e) {
    //
    //     e.preventDefault();
    //     $.ajax({
    //         method: "POST",
    //         data: $('.main_category_form').serialize(),
    //         url: "{{route('main_categories.store')}}",
    //         success: function(resp) {
    //           $('#exampleModal').modal('hide');
    //           $("#ajax-success").show();
    //           $("#ajax-success").delay(3000).fadeOut();
    //
    //         },
    //         error: function(data) {
    //             $.each(data.responseJSON.errors, function(key, value) {
    //
    //                 $('#'+key).addClass('has-danger');
    //                 $('#'+key+" .form-control-feedback").html(value);
    //             });
    //
    //
    //                 // $('#'+key).addClass('has-danger');
    //                 // $('#'+key+" .form-control-feedback").html(value);
    //             console.log(data.responseJSON.errors);
    //
    //         },
    //     })
    //
    // });
</script>
