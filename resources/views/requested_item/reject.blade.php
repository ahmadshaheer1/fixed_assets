@extends('layouts.master')
@section('title', trans('global.item_details_reject'))
@section('content')
<table class="table table-striped- table-bordered table-hover jquery-datatable" id="m_table_1">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">{{trans('global.user')}}</th>
        <th scope="col">{{trans('global.sub_category')}}</th>
        <th scope="col">{{trans('global.description')}}</th>
        <th scope="col">{{trans('global.reject_reason_show')}}</th>
        <th scope="col">{{trans('global.status')}}</th>
      </tr>
    </thead>
    <tbody>

        @foreach ($requested_items as $key => $item)
            <tr id="{{$item->id}}_tr">
                <td>{{++$key}}</td>
                <td>{{$item->user->employee->name_dr}}</td>
                <td>{{$item->subCategory->name_dr}}</td>
                <td>{!!str_limit($item->requested_items, 150, ' ...')!!}</td>
                <td>{{$item->reject_reason}}</td>
                <td><i class="la la-close"></i></td>
            </tr>
        @endforeach
    </tbody>
  </table>
@endsection

