
<table class="table table-striped- table-bordered table-hover table-checkable " id="m_table_1">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">{{trans('global.name')}}</th>
            <th scope="col">{{trans('global.father_name')}}</th>
            <th scope="col">{{trans('global.designation')}}</th>
            <th scope="col">{{trans('global.department')}}</th>
            <th scope="col">{{trans('global.actions')}}</th>
          </tr>
        </thead>
        <tbody>
            @php
                if(count($employee) ==0){
                    echo '<tr class="odd"><th valign="top" colspan="6" class="dataTables_empty text-center">معلومات مورد نظر دریافت نشد</th></tr>';
                }
            @endphp
    
            @foreach ($employee as $index => $item)
                <tr id="{{$item->id}}_tr">
                    <th scope="row">{{$item->id}}</th>
                    <td>{{$item->name_dr}} ({{$item->last_name}})</td>
                    <td>{{$item->father_name_dr}}</td>
                    <td>{{$item->current_position_dr}}</td>
                    <td>
                      {{$item->department_id != 0 ? $item->department->name_dr ??  ''  :''}}
                    </td>
                    <td>
                        <a class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" href="{{ route('allotments.get_employee_allotments',[encrypt($item->id),'type'=>'0']) }}"><i class="la la-eye"></i></a>
                       
                     </td>
                </tr>
            @endforeach
        </tbody>
</table>
    
 {!! $employee->links()!!}
