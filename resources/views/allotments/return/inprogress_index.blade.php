@extends('layouts.master')
@section('title',trans('global.return_request'))
@section('content')
<div class="m-wizard m-wizard--1 m-wizard--info" id="m_wizard">

        <!--begin: Message container -->
        <div class="m-portlet__padding-x">

            <!-- Here you can put a message or alert -->
        </div>

        <!--end: Message container -->

        <!--begin: Form Wizard Head -->
        <div class="m-wizard__head m-portlet__padding-x">

            <!--begin: Form Wizard Progress -->
            @if(!$item_return_request->isEmpty())
            <div class="m-wizard__progress">
                <div class="progress">
                <div class="progress-bar" role="progressbar" style="width: calc({{$item_return_request->first()->status == 0 ? '66.6%' : '100%'}} + 26px);" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
           

            <!--end: Form Wizard Progress -->

            <!--begin: Form Wizard Nav -->
            <div class="m-wizard__nav">
                <div class="m-wizard__steps">
                    <div class="m-wizard__step m-wizard__step--current" m-wizard-target="m_wizard_form_step_1">
                        <div class="m-wizard__step-info">
                            <a href="#" class="m-wizard__step-number">
                                <span><i class="fa fa-check text-light"></i></span>
                            </a>
                            <div class="m-wizard__step-line">
                                <span></span>
                            </div>
                            <div class="m-wizard__step-label">
                               درخواست اعاده جنس
                            </div>
                        </div>
                    </div>
                    <div class="m-wizard__step  m-wizard__step--current" m-wizard-target="m_wizard_form_step_2">
                        <div class="m-wizard__step-info">
                            <div class="m-wizard__step-number">
                                <span class="text-light"><i class="fa fa-check"></i></span>
                            </div>
                            <div class="m-wizard__step-line">
                                <span></span>
                            </div>
                            <div class="m-wizard__step-label">
                                چاپ نمودن و طی مراحل اسناد
                            </div>
                        </div>
                    </div>
                    <div class="m-wizard__step m-wizard__step--current" m-wizard-target="m_wizard_form_step_3">
                        <div class="m-wizard__step-info">
                            <div class="m-wizard__step-number">
                                @if($item_return_request->first()->status == 0)
                                    <span class="bg-info"><span>3</span></span>
                                @else
                                    <span class="text-light"><i class="fa fa-check"></i></span>
                                @endif
                            </div>
                            <div class="m-wizard__step-line">
                                <span></span>
                            </div>
                            <div class="m-wizard__step-label">
                              اپلود نمودن درخواست اعاده اجناس طی مراحل شده
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <!--end: Form Wizard Nav -->
        </div>
</div>
<table class="table table-striped- table-bordered table-hover table-checkable jquery-datatable" id="m_table_1">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">{{trans('global.name')}}</th>
        <th scope="col">{{trans('global.designation')}}</th>
        <th scope="col">{{trans('global.department')}}</th>
        <th scope="col">{{trans('global.actions')}}</th>
        <th scope="col">{{trans('global.actions')}}</th>
      </tr>
    </thead>
    <tbody>

        @foreach ($item_return_request as $index => $item)
            <tr id="{{$item->id}}_tr">
                <th scope="row">{{++$index}}</th>
                <td>{{$item->employee->name_dr}}</td>
                <td>{{$item->employee->current_position_dr}}</td>
                <td>{{$item->date}}</td>
                @if ($item->status == 0)
                <td>
                    <a href="javascript:void(0)" data-toggle="modal" data-target="#upload_modal" onclick="$('input[name=id]').val({{$item->id}})" class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air">
                        <i class="fa fa-upload"></i>
                    </a>
                </td>
                @else
                <td>
                </td>        
                @endif
                <td>
                    {{-- <a class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" href="{{ route('departments.show',$item->id) }}"><i class="la la-eye"></i></a> --}}
                    <a class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" href="{{ route('item_return_requests.show',$item->id) }}"><i class="la la-eye"></i></a>
                    {{-- <a href="javascript:void(0)" onclick="deleteRecord('{{route('departments.destroy', $item->id)}}','{{$item->id}}_tr');" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill ">
                        <i class="fa fa-trash"></i>
                    </a> --}}
                </td>
                
            </tr>
        @endforeach
    </tbody>
  </table>

   {{-- Start of the Modal  --}}
    <!-- Modal -->
    <div class="modal fade" id="upload_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">اپلود نمودن درخواست اعاده اجناس طی مراحل شده</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('attach_file_requested_item')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="form-group m-form__group col-lg-12">
                        <div></div>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="image" multiple name="image" ref="file">
                            <label class="custom-file-label" for="customFile">انتخاب فایل</label>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="id" value="">
                <div class="modal-footer">
                    <div class="row">

                        <div class="col-md-6">
                            <div class="btn-group m-btn-group" role="group" aria-label="...">
                                <button type="submit" class="btn btn-brand">ارسال <i class="la la-paperclip"></i></button>
                                <button type="button" class="btn btn btn-info" data-dismiss="modal">لغو</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
        {{-- End of the Modal  --}}
@endsection
@push('create-button')

@endpush
