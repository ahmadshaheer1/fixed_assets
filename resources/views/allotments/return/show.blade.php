@extends('layouts.master')
@section('title', 'صفحه اصلی')
@section('content')
    <div id="return_item_details_specifications">
        <div class="m-wizard m-wizard--1 m-wizard--info" id="m_wizard">

            <!--begin: Message container -->
            <div class="m-portlet__padding-x">

                <!-- Here you can put a message or alert -->
            </div>

            <!--end: Message container -->

            <!--begin: Form Wizard Head -->
            <div class="m-wizard__head m-portlet__padding-x">

                <!--begin: Form Wizard Progress -->
                <div class="m-wizard__progress">
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width: calc(33.3% + 26px);" aria-valuenow="50"
                            aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>

                <!--end: Form Wizard Progress -->

                <!--begin: Form Wizard Nav -->
                <div class="m-wizard__nav">
                    <div class="m-wizard__steps">
                        <div class="m-wizard__step m-wizard__step--current" m-wizard-target="m_wizard_form_step_1">
                            <div class="m-wizard__step-info">
                                <a href="#" class="m-wizard__step-number">
                                    <span><i class="fa fa-check text-light"></i></span>
                                </a>
                                <div class="m-wizard__step-line">
                                    <span></span>
                                </div>
                                <div class="m-wizard__step-label">
                                    درخواست اعاده جنس
                                </div>
                            </div>
                        </div>
                        <div class="m-wizard__step  m-wizard__step--current" m-wizard-target="m_wizard_form_step_2">
                            <div class="m-wizard__step-info">
                                <div class="m-wizard__step-number">
                                    <span class="bg-info"><span>2</span></span>
                                </div>
                                <div class="m-wizard__step-line">
                                    <span></span>
                                </div>
                                <div class="m-wizard__step-label">
                                    چاپ نمودن و طی مراحل اسناد
                                </div>
                            </div>
                        </div>
                        <div class="m-wizard__step" m-wizard-target="m_wizard_form_step_3">
                            <div class="m-wizard__step-info">
                                <div class="m-wizard__step-number">
                                    <span><span>3</span></span>
                                </div>
                                <div class="m-wizard__step-line">
                                    <span></span>
                                </div>
                                <div class="m-wizard__step-label">
                                    اپلود نمودن درخواست اعاده اجناس طی مراحل شده
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--end: Form Wizard Nav -->
            </div>
        </div>
        <div id="printableArea">

            <div class="row">
                <div class="col-md-12 text-center text-dark">
                    <h4>
                        @switch($item_return_request->itemDetailsSpecifications[0]->pivot->return_type)
                            @case(1)
                                فورم اعاده
                            @break

                            @case(2)
                                مکتوب
                            @break
                        @endswitch
                    </h4><br>
                    <h5>درخواست اعاده جنس</h5>
                </div>
            </div>
            <br>
            <div class="row align-items-center">
                <div class="col-10">
                    <p class="mb-0">
                        اینجانب ({{ $item_return_request->employee->name_dr }})
                        {{ $item_return_request->employee->current_position_dr }} میخواهم اجناس ذیل را تسلیم دهم
                    </p>
                </div>
                @if ($item_return_request->status == 1 && $item_return_request->itemDetailsSpecifications[0]->pivot->return_type == 1)
                    <div class="col-2 mb-2">
                        <label for="item_status">حالت جنس: </label>
                        <select class="form-control" id="item_status" v-model="item_status">
                            <option disabled>حالت جنس</option>
                            <option value="0">جدید</option>
                            <option value="1">مستعمل</option>
                            <option value="2">داغمه</option>
                            <option value="3">ترمیم طلب</option>
                            <option value="4">مفقود</option>
                        </select>
                    </div>
                @endif
            </div>
            <div class="row mb-3">

                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>شماره</th>
                            <th>جنس</th>
                            <th>توضیحات</th>
                            @if ($item_return_request->itemDetailsSpecifications[0]->pivot->return_type == 2)
                                <th style="width:120px">نرخ جدید</th>
                            @endif
                            <th>نوعیت تسلیمی</th>
                            <th v-if="item_status != '4'">شخص تسلیم گیرنده</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($item_return_request->itemDetailsSpecifications as $index => $item)
                            <tr>
                                <td>{{ $index + 1 }}</td>
                                <td>{{ $item->itemDetails->name_dr }}</td>
                                <td>
                                    <span class="m--font-primary">
                                        {{ $item->subCategoryKeys->col1 }} :
                                    </span>
                                    <span class="m--font-info">
                                        {{ $item->col1 }} ||
                                    </span>
                                    <span class="m--font-primary">
                                        {{ $item->subCategoryKeys->col2 }} :
                                    </span>
                                    <span class="m--font-info">
                                        {{ $item->col2 }} ||
                                    </span>
                                    <span class="m--font-primary">
                                        {{ $item->subCategoryKeys->col3 }} :
                                    </span>
                                    <span class="m--font-info">
                                        {{ $item->col3 }} ||
                                    </span>
                                    <span class="m--font-primary">
                                        {{ $item->subCategoryKeys->col4 }} :
                                    </span>
                                    <span class="m--font-info">
                                        {{ $item->col4 }} ||
                                    </span>
                                    <span class="m--font-primary">
                                        {{ $item->subCategoryKeys->col5 }} :
                                    </span>
                                    <span class="m--font-info">
                                        {{ $item->col5 }} ||
                                    </span>
                                    <span class="m--font-primary">
                                        {{ $item->subCategoryKeys->col6 }} :
                                    </span>
                                    <span class="m--font-info">
                                        {{ $item->col6 }} ||
                                    </span>
                                    <span class="m--font-primary">
                                        {{ $item->subCategoryKeys->col7 }} :
                                    </span>
                                    <span class="m--font-info">
                                        {{ $item->col7 }} ||
                                    </span>
                                    <span class="m--font-primary">
                                        {{ $item->subCategoryKeys->col8 }} :
                                    </span>
                                    <span class="m--font-info">
                                        {{ $item->col8 }} ||
                                    </span>
                                    <span class="m--font-primary">
                                        {{ $item->subCategoryKeys->col9 }} :
                                    </span>
                                    <span class="m--font-info">
                                        {{ $item->col9 }} ||
                                    </span>
                                    <span class="m--font-primary">
                                        {{ $item->subCategoryKeys->col10 }} :
                                    </span>
                                    <span class="m--font-info">
                                        {{ $item->col10 }} ||
                                    </span>
                                    <span class="m--font-primary">
                                        {{ $item->subCategoryKeys->col11 }} :
                                    </span>
                                    <span class="m--font-info">
                                        {{ $item->col11 }} ||
                                    </span>
                                    <span class="m--font-primary">
                                        {{ $item->subCategoryKeys->col12 }} :
                                    </span>
                                    <span class="m--font-info">
                                        {{ $item->col12 }} ||
                                    </span>
                                    <span class="m--font-primary">
                                        قیمت فی واحد :
                                    </span>
                                    <span class="m--font-info">
                                        {{ $item->price }} ||
                                    </span>
                                    <span class="m--font-primary">
                                        تعداد تسلیمی :
                                    </span>
                                    <span class="m--font-info">
                                        {{ $item_return_request->item_amount }}
                                    </span>
                                </td>
                                @if ($item_return_request->itemDetailsSpecifications[0]->pivot->return_type == 2)
                                    <td>
                                        @if ($item_return_request->status == 1 && $item->subCategoryKeys->item_type != 1)
                                            <input type="number" class="form-control" id="price_{{ $loop->iteration }}"
                                                ref="price_{{ $loop->iteration }}" value="{{ $item->price }}">
                                        @endif
                                    </td>
                                @endif
                                <td>
                                    @switch($item->pivot->return_type)
                                        @case(0)
                                            داغمه
                                        @break

                                        @case(1)
                                            فورم اعاده
                                        @break

                                        @case(2)
                                            مکتوب
                                        @break

                                        @case(3)
                                            مفقود
                                        @case(4)
                                            املاک
                                        @break
                                    @endswitch

                                </td>
                                <td v-if="item_status != '4'">

                                    @if (
                                        $item_return_request->status == 1 &&
                                            $item->pivot->receiver_employee_id == null &&
                                            $item->subCategoryKeys->item_type != 1)
                                        <input type="number" hidden id="receiver{{ $loop->iteration }}"
                                            ref="receiver{{ $loop->iteration }}" />
                                        <v-select label="name_dr" :options="employees" item-text="" dir="rtl"
                                            v-model="receiver_id" :reduce="employees => employees.employee_id"
                                            @search="onSearch" @input="addReceiver({{ $loop->iteration }})"
                                            {{-- @input="allotToStock( {{ $item_return_request->id }} , {{ $item->pivot->item_details_specifications_id }}, {{ $loop->iteration }}, {{ $item->pivot->return_type }})" --}} {{-- @input="this.$refs['receiver' +{{ $loop->iteration }}].value = this.receiver_id" --}}>
                                            <template slot="no-options">
                                                لطفآ اسم را بنویسید
                                            </template>
                                            <template slot="option" slot-scope="option">
                                                @{{ option.name_dr }}

                                                (@{{ option.last_name }})
                                                ولد --
                                                @{{ option.father_name_dr }}

                                                وظیفه --
                                                @{{ option.current_position_dr }}


                                            </template>
                                        </v-select>
                                    @elseif($item->pivot->receiver_employee_id > 0)
                                        {{ getEmployee($item->pivot->receiver_employee_id)->name_dr }}
                                    @endif
                                </td>

                            </tr>
                        @endforeach
                    </tbody>
                </table>

                </ul>
            </div>
            @if ($item_return_request->status == 1)
                <div class="row no-print">
                    <div class="col-md-3 mb-2 pl-0">
                        <div class="update-image-wrapper">
                            <img class="w-100px" src="{{ asset('public/images/pdf-2.png') }}" alt="" />

                            <div class="overlay">
                                <div class="text">
                                    <a href="{{ asset($item_return_request->request_file) }}" data-fancybox
                                        data-caption="{{ $item_return_request->description }}">
                                        نمایش
                                    </a>
                                    <a href="javascript:void(0)" onclick="$('input[name=image]').click();">
                                        تصحیح
                                    </a>


                                    <form action="{{ route('replace_return_requests_file') }}" id="image_form"
                                        method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <input type="file" class="custom-file-input" id="image" multiple
                                            name="image" onchange="$('#image_form').submit();" ref="file">
                                        <input type="hidden" id="fece9_id" name="id"
                                            value="{{ $item_return_request->id }}">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            <div class="row no-print">
                <div class="col-lg-1">
                    <button type="button" class="btn btn-brand" onclick="printItemRequest()">چاپ <i
                            class="la la-print"></i></button>

                </div>
                @if ($item_return_request->status == 1)
                    <div class="col-lg-3">
                        <button
                            @click="confirmAllotmentList({{ $item_return_request->itemDetailsSpecifications->count() }}, {{ $item_return_request->itemDetailsSpecifications }})"
                            {{-- href="{{ route('item_return_requests.conformation', ['id' => $item_return_request->id, 'status' => '2']) }}" --}} class="btn m-btn--square btn-success">تایید <i class="la la-check"></i>
                        </button>
                    </div>
                @endif

            </div>

        </div>
    </div>


@endsection
@push('custom-css')
    <style>
        thead th {
            border: 1px solid #777 !important;
            text-align: center;
        }

        tbody td {
            border-left: 1px solid #777;
            border-right: 1px solid #777;
            border-bottom: 0 !important;
            border-top: 0 !important;
        }


        tfoot td {
            border: 1px solid #777 !important;
        }

        hr {
            border-color: #777;
        }

        .list-group-item {
            border: none;
        }

        .item-count li {
            padding: 10px 15px;
        }

        .list-group.list-group-flush li {
            margin: 2px;
        }

        .item-description {
            height: 15cm;
            ;
        }

        @media print {
            table {
                /* height: 90vh; */
            }
        }

        input[name=image] {
            display: none;
        }

        /* image slide hover styles */

        .update-image-wrapper {
            position: relative;
            /* width: 50%; */
        }

        .image {
            display: block;
            width: 100%;
            height: auto;
        }

        .overlay {
            position: absolute;
            bottom: 0;
            left: 0;
            right: 0;
            background-color: #008cba5e;
            overflow: hidden;
            width: 100%;
            height: 0;
            transition: .5s ease;
        }

        .update-image-wrapper:hover .overlay {
            height: 100%;
        }

        .text {
            color: white;
            font-size: 20px;
            position: absolute;
            top: 50%;
            left: 50%;
            -webkit-transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
            text-align: center;
        }

        .text a {
            background: #fff;
            padding: 5px 10px;
            margin: 3px;
            font-size: .7em;
        }

        .text a:hover {
            text-decoration: none;
        }




        table.table tr th,
        table.table tr td {
            border: 1px solid #777;
        }

        table .with_background {
            background: gainsboro !important;

        }

        table .with_background th {
            font-weight: bold;
            white-space: nowrap;
        }

        table tbody tr td {
            border-bottom: 0px !important;
            border-top: 0px !important;
        }

        .w-100px {
            width: 150px;
        }
    </style>
@endpush
@push('custom-js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-paginate/3.6.0/vue-paginate.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-paginate/3.6.0/vue-paginate.min.js"></script>
    <script>
        Vue.component('v-select', VueSelect.VueSelect);
        item_app = new Vue({
            el: '#return_item_details_specifications',
            data: {
                //variable for editing
                edit: false,
                specifications: [],
                employees: [],
                receiver_id: '',
                allotment_list: {},
                return_list: [],
                allotment_list_string: '',
                item_status: ''

            },
            methods: {
                confirmAllotmentList(total, return_item_details_specification) {
                    var error = false;
                    if (return_item_details_specification[0].pivot.return_type == 2 || this.item_status) {
                        for (let i = 0; i < total; i++) {
                            let object = {};

                            var receiver = 'receiver' + (i + 1);
                            if (return_item_details_specification[0].pivot.return_type == 1 && this.item_status !=
                                4 && this.$refs[receiver] && this.$refs[receiver].value == '') {
                                alert("لطفاشخص تسلیم ګیرنده جنس شماره " + (i + 1) + " را انتخاب کنید!")
                                error = true;
                            }
                            object['receiver_employee_id'] = (return_item_details_specification[0].pivot
                                    .return_type == 1 && this.item_status != 4) ? this.$refs[receiver].value :
                                return_item_details_specification[0].pivot.receiver_employee_id;
                            price = 'price_' + (i + 1);
                            object['item_details_specifications_id'] = return_item_details_specification[i].id;
                            object['new_price'] = return_item_details_specification[0].pivot.return_type == 1 ? return_item_details_specification[0].price :this.$refs[price].value;
                            this.return_list[i] = object;
                        }
                        if (!error) {
                            this.allotment_list['item_status'] = this.item_status ||
                                return_item_details_specification[0].condition;
                            this.allotment_list['return_list'] = this.return_list;

                            axios.post(
                                    "{{ route('item_return_requests.conformation', ['id' => $item_return_request->id]) }}", {
                                        ...this.allotment_list
                                    })
                                .then(res => {
                                    window.location.href = res.data
                                })
                                .catch(err => {
                                    console.log(err);
                                })
                        }
                    } else {
                        alert("لطفا حالت جنس را انتخاب کنید!");
                    }
                },
                addReceiver(index) {
                    var receiver = 'receiver' + index;
                    this.$refs[receiver].value = this.receiver_id
                },
                onSearch(search, loading) {
                    loading(true);
                    this.search(loading, search, this, this.department_id);
                },
                search: _.debounce((loading, search, vm, department_id) => {

                    axios.post(`{{ route('get_employee_axios') }}`, {
                            'name': search,
                            'department_id': department_id
                        })
                        .then(res => {
                            // console.log(res.data);   
                            // res.json().then(json => (vm.employees = json.items));  
                            vm.employees = res.data.items;
                            // this.employees = res.data.items;                           
                            loading(false);
                        })
                        .catch(err => {
                            console.log(err);
                        });

                }, 350)
            },
            mounted() {},
        });

        //print fece5
        function printItemRequest() {
            document.getElementById('m_wizard').innerHTML = '';
            window.print();

        }
        window.onafterprint = function() {
            window.location.href = "{{ route('item_return_requests.index') }}";
            // window.close();
        }
    </script>
@endpush
