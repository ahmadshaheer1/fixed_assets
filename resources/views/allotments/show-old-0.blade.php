@extends('layouts.master')
@section('title',trans('global.employee_allotments'))
@section('content')
<div class="wrapper" id="employee_allotments">

    <div class="m-invoice-2" >
        <div class="m-invoice__wrapper">
            <div class="m-invoice__head" style="">
                <div class="m-invoice__container">
                    <div class="m-invoice__logo pb-2">
                        <img class="w-25" src="https://b-buildingbusiness.com/wp-content/uploads/2018/08/unknown_person-1024x1024-1.png">

                    </div>
                    <div class="m-invoice__items">
                        <div class="m-invoice__item">
                            <span class="m-invoice__subtitle">اسم</span>
                            <span class="m-invoice__text">{{$employee->name_dr}}</span>
                        </div>
                        <div class="m-invoice__item">
                            <span class="m-invoice__subtitle">نام پدر</span>
                            <span class="m-invoice__text">{{$employee->father_name_dr}}</span>
                        </div>
                        <div class="m-invoice__item">
                            <span class="m-invoice__subtitle">عنوان وظیفه</span>
                            <span class="m-invoice__text">{{$employee->current_position_dr}}</span>
                        </div>
                        <div class="m-invoice__item">
                            <span class="m-invoice__subtitle">اداره</span>
                            <span class="m-invoice__text">{{ isset($employee->department->name_dr) ? $employee->department->name_dr : ''}}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="m-invoice__body">
                <div class="table-responsive">
                    <div class="search">
                        <input type="text" class="search_box" placeholder="جستجو..." data-search />
                    </div>
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th class="un">
                                    <label class="m-checkbox" style="display:inline;margin:1.5em;">
                                        <input type="checkbox" class="checkbox" @change="checkAll($event,{{$employee->id}})">
                                        <span></span>
                                    </label>
                                    {{-- انتخاب همه --}}
                                </th>
                                <th>شماره</th>
                                <th>اسم جنس</th>
                                <th>تعداد جنس</th>
                                <th>توضیحات</th>
                                <th>تاریخ تسلیمی</th>
                                <th>تاریخ اعاده</th>
                                <th>موجودیت</th>
                                <th>حالت</th>
                                <th class="un">عملیات</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($employee->allotments as $index => $item)
                                <tr  data-filter-item data-filter-name="{{$item->itemDetailsSpecifications->itemDetails['name_dr']}}" >
                                    <td class="un">
                                        <div class="m-checkbox-list">
                                            <label class="m-checkbox" style="display:inline;margin:1.5em;" >
                                            @php

                                               if($item->itemBalance){
                                                   $item_amount = $item->itemBalance->item_amount;
                                               }else {
                                                   $item_amount = 1;
                                               }

                                            @endphp
                                                {{-- {{$item->itemDetailsSpecifications->subCategoryKeys->distribution_type ? $item->itemBalance->item_amount : 1}})"> --}}

                                            <input type="checkbox" {{$item->itemDetailsSpecifications->condition =='2' ? 'disabled' : ''}}
                                             {{ $item->status !=1 ? 'disabled' : '' }} id="{{$index}}"  class="checkbox"
                                              @change="getItemDetailsSpecifications(
                                                            $event.target.checked,
                                                            {{$index}},
                                                            {{$item->item_details_specifications_id}},
                                                            {{$item->itemDetailsSpecifications->subCategoryKeys->distribution_type ? $item_amount : 1}})">
                                                <span></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td class="text-center">{{++$index}}</td>
                                    <td>{{$item->itemDetailsSpecifications->itemDetails['name_dr']}}</td>
                                    <td>
                                        @if ($item->itemDetailsSpecifications->subCategoryKeys->distribution_type)
                                            {{$item_amount}}
                                        @else
                                            1
                                        @endif
                                    </td>
                                    <td>
                                        <div class="details">
                                            @if ($item->itemDetailsSpecifications->subCategoryKeys->col1)
                                            <span class="m--font-primary">
                                                    {{$item->itemDetailsSpecifications->subCategoryKeys->col1}} :
                                            </span>
                                            <span class="m--font-info">
                                                {{$item->itemDetailsSpecifications->col1}} ||
                                            </span>
                                        @endif
                                        @if ($item->itemDetailsSpecifications->subCategoryKeys->col2)
                                            <span class="m--font-primary">
                                                    {{$item->itemDetailsSpecifications->subCategoryKeys->col2}} :
                                            </span>
                                            <span class="m--font-info">
                                                {{$item->itemDetailsSpecifications->col2}} ||
                                            </span>
                                        @endif
                                        
                                        @if ($item->itemDetailsSpecifications->subCategoryKeys->col3)
                                            <span class="m--font-primary">
                                                    {{$item->itemDetailsSpecifications->subCategoryKeys->col3}} :
                                            </span>
                                            <span class="m--font-info">
                                                {{$item->itemDetailsSpecifications->col3}} ||
                                            </span>
                                        @endif
                                        <br>
                                        @if ($item->itemDetailsSpecifications->subCategoryKeys->col4)
                                            <span class="m--font-primary">
                                                    {{$item->itemDetailsSpecifications->subCategoryKeys->col4}} :
                                            </span>
                                            <span class="m--font-info">
                                                {{$item->itemDetailsSpecifications->col4}}
                                            </span>
                                        @endif
                                        ||
                                        <span class="m--font-primary">
                                                قیمت :
                                        </span>
                                        <span class="m--font-info">
                                            
                                            {{$item->itemDetailsSpecifications->price}}
                                            {{ $item->itemDetailsSpecifications->currency ==0 ? 'افغانی' : 'دالر'}}
                                        </span>
                                        ||
                                        @if ($item->itemDetailsSpecifications->subCategoryKeys->distribution_type)
                                            <span class="m--font-primary">
                                                قیمت مجموعی :
                                                {{-- {{($item->itemDetailsSpecifications->price) * ($item->itemBalance->item_amount) }} --}}
                                                {{ $item->itemDetailsSpecifications->currency ==0 ? 'افغانی' : 'دالر'}}
                                            </span>
                                        @endif
                                    </div>
                                        <a href="javascript:;" class="btn btn-xs green toggle_description">
                                        نمایش <i class="fa fa-plus"></i>
                                        </a>
                                    
                                    </td>
                                    <td>
                                        {{$item->allotment_date}}
                                    </td>
                                    <td>
                                        {{$item->return_date}}
                                    </td>
                                    <td>
                                        @if ($item->status == 1)
                                            <i class="la la-check m--font-success"></i>
                                        @elseif($item->status == 2)
                                            <i class="la la-home m--font-warning"></i>
                                        @else
                                            <i class="la la-close m--font-danger"></i>
                                        @endif
                                    </td>
                                    <td>
                                        @if ($item->itemDetailsSpecifications->condition == 0)
                                            <span class="m--font-success p-2">
                                                جدید
                                            </span>
                                        @elseif($item->itemDetailsSpecifications->condition == 1)
                                            <span class="m--font-warning p-2">
                                                مستعمل
                                            </span>
                                        @else
                                            <span class="m--font-danger p-2">
                                                داغمه
                                            </span>

                                        @endif
                                    </td>
                                    <td class="un">
                                        {{-- <a href="#" data-toggle="modal"  data-target="#allotment_history" title="" >
                                            <i class="la la-rotate-left"></i>
                                        </a> --}}

                                        @php
                                            

                                            $item_balance_type = $item->itemDetailsSpecifications->ItemBalance->where('type','0')->count();
                                             if($item_balance_type){
                                                $item_balance_type = $item_balance_type->first()->id;
                                             }else {
                                                $item_balance_type = 'null';
                                             }

                                        @endphp
                                        <a href="#"  data-toggle="modal" data-target="#allotment_history" title="تاریخچه جمع و قید جنس"
                                         @click="showItemAllotmentHistory(
                                             {{$item->item_details_specifications_id}},
                                             {{$item->itemDetailsSpecifications->subCategoryKeys->distribution_type ? $item_balance_type : 'null'}}
                                            {{-- this is old code  --}}
                                             {{-- $item->itemDetailsSpecifications->subCategoryKeys->distribution_type ? $item->itemDetailsSpecifications->ItemBalance->where('type','0')->first()->id : 'null' --}}
                                             )
                                             ">
                                             
                                             
                                           
                                            <i class="la la-list-alt"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="row no-print">
                        <div class="col-lg-10">
                            <a  v-if="item_details_specifications.length>0" href="#" data-toggle="modal" @click="" data-target="#allotment_history" title="اعاده جنس" class="btn m-btn--square  btn-success"> <i class="la la-rotate-left"></i> اعاده جنس</a>
                        </div>
                        <div class="col-lg-2">
                            <button title="اعاده جنس" @click="printAllotments()" class="btn m-btn--square  btn-info"> <i class="la la-print"></i> چاپ لست</button>
                        </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="allotment_history" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">  @{{model_title}} ({{$employee->name_dr}})</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div  class="m-scrollable" data-scrollable="true" data-height="380" data-mobile-height="300">

                        <!--Begin::Timeline 2 -->
                        <div  v-if="allotment_history.length > 0" class="m-timeline-2">
                            <div class="m-timeline-2__items  m--padding-top-25 m--padding-bottom-30">
                                    <div v-for="(item, index) in allotment_history" class="m-timeline-2__item">
                                        <span class="m-timeline-2__item-time m--font-primary">@{{item.allotment_date}}</span>
                                        <div class="m-timeline-2__item-cricle">
                                            <i :class="[ item.status == 0 ? 'fa fa-genderless m--font-warning' : item.status == 2 ? 'fa fa-home m--font-danger icon-sm' :'fa fa-genderless m--font-success']"></i>
                                        </div>
                                        <div class="m-timeline-2__item-text  m--padding-top-5">
                                            <div class="font-weight-bold">
                                                @{{item.employee.name_dr}}
                                            </div>
                                            <div class="">
                                                @{{item.employee.current_position_dr}}
                                            </div>
                                            <div v-if="item.condition == 0" class="m--font-success">
                                                جدید
                                            </div>
                                            <div v-else-if="item.condition == 1" class="m--font-warning">
                                                مستعمل
                                            </div>
                                            <div v-else-if="item.condition == 2" class="m--font-danger">
                                                داغمه
                                            </div>
                                            <div v-if="item.status == 0" class="font-weight-bold m--font-info">
                                                تاریخ اعاده:‌@{{item.return_date}}
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>

                        <!--End::Timeline 2 -->
                        <div v-if="allotment_history.length == 0" class="">
                            <div class="m-accordion m-accordion--bordered" id="m_accordion_2" role="tablist">

                                <div class="m-wizard m-wizard--1 m-wizard--info" id="m_wizard">

                                        <!--begin: Message container -->
                                        <div class="m-portlet__padding-x">

                                            <!-- Here you can put a message or alert -->
                                        </div>

                                        <!--end: Message container -->

                                        <!--begin: Form Wizard Head -->
                                        <div class="m-wizard__head m-portlet__padding-x">

                                            <!--begin: Form Wizard Progress -->
                                            <div class="m-wizard__progress">
                                                <div class="progress">
                                                    <div class="progress-bar" role="progressbar" style="width: calc(0% + 26px);" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>

                                            <!--end: Form Wizard Progress -->

                                            <!--begin: Form Wizard Nav -->
                                            <div class="m-wizard__nav">
                                                <div class="m-wizard__steps">
                                                    <div class="m-wizard__step m-wizard__step--current" m-wizard-target="m_wizard_form_step_1">
                                                        <div class="m-wizard__step-info">
                                                            <a href="#" class="m-wizard__step-number">
                                                                <span class="bg-info"><span>1</span></span>
                                                            </a>
                                                            <div class="m-wizard__step-line">
                                                                <span></span>
                                                            </div>
                                                            <div class="m-wizard__step-label">
                                                                درخواست اعاده جنس
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="m-wizard__step" m-wizard-target="m_wizard_form_step_2">
                                                        <div class="m-wizard__step-info">
                                                            <div class="m-wizard__step-number">
                                                                <span><span>2</span></span>
                                                            </div>
                                                            <div class="m-wizard__step-line">
                                                                <span></span>
                                                            </div>
                                                            <div class="m-wizard__step-label">
                                                                چاپ نمودن و طی مراحل اسناد
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="m-wizard__step" m-wizard-target="m_wizard_form_step_3">
                                                        <div class="m-wizard__step-info">
                                                            <div class="m-wizard__step-number">
                                                                <span><span>3</span></span>
                                                            </div>
                                                            <div class="m-wizard__step-line">
                                                                <span></span>
                                                            </div>
                                                            <div class="m-wizard__step-label">
                                                                اپلود نمودن درخواست اعاده اجناس طی مراحل شده
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <!--end: Form Wizard Nav -->
                                        </div>
                                </div>
                                <div class="">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th style="width: 50px;">شماره</th>
                                                <th>جنس</th>
                                                <th>توضیحات</th>
                                                <th>تعداد تسلیمی</th>
                                                <th style="width:120px">نوعیت تسلیمی</th>
                                                <th v-if="return_type ==2">شخص تسلیم گیرنده</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for=" (item,index) in item_details_specifications">
                                                <td>@{{++index}}</td>
                                                <td>@{{item.item_details.name_dr}}</td>
                                                <td>
                                                <div class="details">
                                                    <span class="m--font-primary" v-if="item.sub_category_keys.col1">
                                                        @{{item.sub_category_keys.col1}} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        @{{item.col1}} ||
                                                    </span>
                                                    <span class="m--font-primary" v-if="item.sub_category_keys.col2">
                                                        @{{item.sub_category_keys.col2}} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        @{{item.col2}} ||
                                                    </span>
                                                    <span class="m--font-primary" v-if="item.sub_category_keys.col3">
                                                            @{{item.sub_category_keys.col3}} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        @{{item.col3}} ||
                                                    </span>
                                                    <span class="m--font-primary" v-if="item.sub_category_keys.col4">
                                                        @{{item.sub_category_keys.col4}} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        @{{item.col4}} ||
                                                    </span>
                                                    <span class="m--font-primary" v-if="item.sub_category_keys.distribution_type">
                                                      تعداد :
                                                    </span>
                                                    <span class="m--font-info">
                                                        {{-- deleted_by = item_amount_alloted_to_user --}}
                                                        @{{item.deleted_by}}
                                                    </span>
                                                </div>
                                                <a href="javascript:;" class="btn btn-xs green toggle_description">
                                                    نمایش <i class="fa fa-plus"></i>
                                                </a>
                                                </td>
                                                <td>
                                                    <span v-if="item.deleted_by">
                                                       <input type="number" min="1" :max="item.deleted_by" :class="['form-control', return_error == true ? 'is-invalid' : '']" placeholder="تعداد تسلیمی"  v-model="alloted_item_amount" v-on:keyup="validate($event.target.value,item.deleted_by)">
                                                    </span>
                                                    <span v-else>
                                                        1
                                                    </span>
                                                </td>
                                                <td>
                                                    <select class="form-control" v-model="return_type" @change="checkType(index-1,item.id)">
                                                        <option selected disabled>نوعیت تسلیمی</option>
                                                        <template v-if="item.sub_category_keys.item_type == 1">
                                                            <option value="4" :disabled="!item.sub_category_keys.item_type">املاک</option>
                                                        </template>
                                                        <template v-else>
                                                            <option value="0">داغمه</option>
                                                            <option value="1">فورم اعاده</option>
                                                            <option value="2">درخواستی</option>
                                                            <option value="3">مفقود</option>
                                                        </template>
                                                    </select>   
                                                </td>
                                                <td v-if="return_type ==2 ">
                                                    {{-- <select class="form-control select2" :ref="index-1" @change="addItemDetailsSpecificationsToReturnList(index-1,item.id,$event.target.value)">
                                                            <option selected disabled>{{trans('global.receiver_employee')}}</option>
                                                            <option  value="0" >دیپو</option>
                                                            <option  v-for="item in employees" :value="item.id" >@{{item.name_dr}}</option>
                                                    </select> --}}
                                                    <v-select
                                                        label="name_dr" 
                                                        :options="employees"
                                                        item-text="name_dr"
                                                        dir="rtl"
                                                        v-model="receiver_id"
                                                        :reduce="employees=> employees.id"
                                                        @search="onSearch"
                                                        @input="addItemDetailsSpecificationsToReturnList(index-1,item.id)"
                                                                                                
                                                    >
                                                    <template slot="no-options">
                                                            لطفآ اسم را بنویسید        
                                                        </template>
                                                        <template slot="option" slot-scope="option" v-if="option.father_name_dr">
                                                                @{{  option.name_dr }}
                                                                ولد:
                                                                @{{  option.father_name_dr }}--
                                                                وظیفه:
                                                                @{{option.current_position_dr}}
                                                        </template>
                                                        <template slot="option" slot-scope="option" v-else>
                                                                @{{  option.name_dr }}
                                                             
                                                        </template>
                                                    </v-select>
                                                </td>

                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <!--begin::Item-->
                                <div class="m-accordion__item" >
                                    <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_2_item_1_head" data-toggle="collapse" href="#m_accordion_2_item_1_body" aria-expanded="    false">
                                        <span class="m-accordion__item-icon"><i class="fa flaticon-user-ok"></i></span>
                                        <span class="m-accordion__item-title">
                                            
                                            <span v-if="return_type === 0">داغمه</span>
                                            <span v-if="return_type == 1">فورم اعاده</span>
                                            <span v-if="return_type == 2">درخواستی</span>
                                            <span v-if="return_type == 3">مفقود</span>
                                            <span v-if="return_type == 4">املاک</span>
                                            <span v-else></span>
                                        </span>
                                        <span class="m-accordion__item-mode"></span>
                                    </div>
                                    <div class="m-accordion__item-body collapse" id="m_accordion_2_item_1_body" class=" " role="tabpanel" aria-labelledby="m_accordion_2_item_1_head" data-parent="#m_accordion_2">
                                        <div class="m-accordion__item-content" v-if="show_form == true">
                                            <form  class="m-form m-form--fit m-form--label-align-right" method="POST" @submit.prevent="submitForm"  enctype="multipart/form-data" action="{{route('item_return_requests.store')}}">
                                                @csrf

                                                <input type="hidden" name="employee_id" :value="current_employee_id">
                                                <input type="hidden" name="status" value="0">
                                                <input type="hidden" v-model="return_list_string" >
                                                <div class="m-portlet__body">
                                                <div class="form-group row ">
                                                    <div class="has-danger col-md-12" v-if="receiver_employee_error==true">
                                                            <div class="form-control-feedback">شخص تسلیم گیرنده را انتخاب نمائید</div>
                                                    </div>
                                                    <div :class="['form-group col-md-6 m-form__group',errors.number ? 'has-danger' : '' ]">
                                                    <label for="fece5_number">نمبر</label>
                                                        <input type="number" name="number" value="{{old('number')}}" v-model="number" class="form-control m-input m-input--square" id="number" placeholder="نمبر">

                                                            <div class="form-control-feedback" v-if="error">@{{errors.number}}</div>

                                                    </div>
                                                    <div :class="['form-group col-md-6 m-form__group pt-0',errors.date ? 'has-danger' : '' ]">
                                                    <label for="inputPassword4">تاریخ</label>
                                                    <div class="input-group date">
                                                        <input type="text" name="date" value="{{old('date')}}"  ref="date" class="form-control persian_date" onfocus='$(".persian_date").persianDatepicker();' placeholder="{{trans('global.meem7_date')}}">
                                                            <div class="input-group-append">
                                                            <span class="input-group-text">
                                                                <i class="la la-calendar"></i>
                                                            </span>
                                                            </div>
                                                    </div>
                                                    <div class="form-control-feedback" v-if="error">@{{errors.date}}</div>
                                                    </div>
                                                </div>

                                                <div class="form-group row ">

                                                        <div  :class="['form-group m-form__group col-md-12 pt-0',errors.description ? 'has-danger' : '' ]">
                                                            <label for="department_description">{{trans('global.description')}}</label>
                                                            <textarea name="description" v-model="description" class="form-control" id="department_description"placeholder="{{trans('global.description')}}">{{old('description')}}</textarea>
                                                            <div class="form-control-feedback" v-if="error">@{{errors.description}}</div>
                                                        </div>

                                                    </div>

                                                    <div class="form-group row ">
                                                        <div class="form-group col-md-6 m-form__group">
                                                            <button class="btn btn-success">ارسال <i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                                                        </div>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>

                                <!--end::Item-->

                                <!--begin::Item-->
                                {{-- <div class="m-accordion__item">
                                    <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_2_item_2_head" data-toggle="collapse" href="#m_accordion_2_item_2_body" aria-expanded="    false">
                                        <span class="m-accordion__item-icon"><i class="fa  flaticon-placeholder"></i></span>
                                        <span class="m-accordion__item-title">اعاده به دیپو</span>
                                        <span class="m-accordion__item-mode"></span>
                                    </div>
                                    <div class="m-accordion__item-body collapse" id="m_accordion_2_item_2_body" class=" " role="tabpanel" aria-labelledby="m_accordion_2_item_2_head" data-parent="#m_accordion_2">
                                        <div class="m-accordion__item-content">
                                            <p>
                                                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into
                                                electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown
                                                printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with
                                                the release of Letraset sheets containing
                                            </p>
                                        </div>
                                    </div>
                                </div> --}}

                                <!--end::Item-->
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div> --}}
            </div>
        </div>
    </div>
</div>
@endsection
@push('custom-css')
<style>
    label.m-checkbox {
        display: inline;
        margin: 1.5em;
    }
    .m-invoice-2 .m-invoice__wrapper .m-invoice__body table tbody tr:first-child td {
        padding-top: 10px;
        padding-bottom: 10px;
    }
    .m-timeline-2:before {
        right: 5.89rem;
    }
    .m-timeline-2 .m-timeline-2__items .m-timeline-2__item .m-timeline-2__item-cricle {
        right: 5.1rem;
    }
    @media (min-width: 992px){
        .modal-lg {
            max-width: 70%;
        }
    }


.m-invoice-2 .m-invoice__wrapper .m-invoice__body table thead tr th:not(:first-child) {
text-align: right;
}
.m-invoice__logo.pb-2 {
    padding-top: 0 !important;
}

.table_border td{
    border:1px solid #000;
}
.table_header th{
    border: 1px solid #000 !important;
    color: #000 !important;
}

@media (min-width: 992px){

    .modal-lg {
        max-width: 90%;
    }
}

tr.hidden {
    display: none;
}
.search_box{
    padding: 8px;
    width: 280px;
    border: 2px solid #ccc;
    border-radius: 15px;
}

input.search_box :focus{
    padding: 8px;
    width: 280px;
    border: 2px solid #ccc !important;
    border-radius: 15px;
}

input:focus {
    outline-offset: -22px;
}

:focus {
    outline: -webkit-focus-ring-color auto 1px;
    outline-color: -webkit-focus-ring-color;
    outline-style: auto;
    outline-width: 1px;
    border-radius: 15px;
}

.icon-sm{
    font-size: 1.2rem !important;
    right: -0.7rem !important;
}

</style>

@endpush
@push('custom-js')
    <script>
        Vue.component('v-select', VueSelect.VueSelect)
        employee_allotments = new Vue({
            el: '#employee_allotments',
            data: {
                allotment_history: [],
                item_details_specifications:[],
                employees:[
                    // {
                    // 'id':0,
                    // 'name_dr': 'اعاده به دیپو'
                    // }
                ],
                // dipo:{'id':'0','name_dr':'اعاده به دیپو'},
                current_employee_id:'{{$employee->id}}',
                current_item_details_id: 0,
                return_list: [],
                return_list_string:[],
                number:'',
                date:'',
                description:'',
                success:false,
                error:false,
                errors:'',
                receiver_employee_error:false,
                employee_list:'',
                show_form:false,
                receiver_id:null,
                return_type:'',
                model_title:'',
                alloted_item_amount:1,
                return_error: false,
            },
            methods: {
                showItemAllotmentHistory(item_details_specifications_id,item_balances_id=null) {

                    this.model_title = 'تاریخچه جمع و قید جنس';
                    axios.get("{{url('allotments/get_item_details_specifications_allotment_history')}}/"+item_details_specifications_id+'/'+item_balances_id)
                        .then(res => {
                            this.allotment_history = res.data;
                            console.log(this.allotment_history);
                        })
                        .catch(err => {
                            console.log(err);
                        });

                },
                validate(alloted_item,item_amount){
                    if(alloted_item > item_amount){
                        this.return_error = true;
                    }
                    else{
                        this.return_error = false;
                    }
                },
                getItemDetailsSpecifications(evt,index,item_details_specifications_id,item_amount) {
                    this.model_title = 'اعاده جنس';
                    //if check
                    if(evt){
                    axios.get("{{url('allotments/get_item_details_specifications')}}/"+item_details_specifications_id)
                        .then(res => {
                            res.data.deleted_by = item_amount;
                            this.item_details_specifications.push(res.data);
                        })
                        .catch(err => {
                            console.log(err);
                        });
                    }else{
                        var rem = this.item_details_specifications.filter(item=>{return item.id==item_details_specifications_id})[0];

                        remove_id = this.item_details_specifications.indexOf(rem);
                        this.item_details_specifications.splice(remove_id,1);
                    }

                },
                addItemDetailsSpecificationsToReturnList(index,item_details_specifications_id) {
                    var object = {};
                    object['item_details_specifications_id'] = item_details_specifications_id;
                    object['receiver_employee_id'] = this.receiver_id;
                    object['current_employee_id'] = this.current_employee_id;
                    //if allot to stock status will be 1 else status well be 0 and allote to employee
                    object['status'] = this.receiver_id == 0 ? 1 : 0;
                    object['return_type'] = this.return_type;
                    this.return_list[index] = object;

                    
                        
                    this.return_list_string=JSON.stringify(this.return_list[index] );
                },
                checkType(index,item_details_specifications_id){
                    if (this.return_list.return_type !='')
                        this.show_form = true;
                    if(this.return_type !=2){
                        this.addItemDetailsSpecificationsToReturnList(index,item_details_specifications_id);
                    }
                },
                getAllEmployees() {
                    axios.get("{{url('get_all_employees')}}")
                        .then(res => {
                            this.employees = res.data;
                        })
                        .catch(err => {
                            console.log(err);

                        })

                },
                submitForm: function() {
                    if(this.return_list.length == this.item_details_specifications.length){
                        axios.post("{{route('item_return_requests.store')}}", {
                            'number': this.number,
                            'date': this.$refs.date["value"],
                            'description': this.description,
                            'return_list': this.return_list,
                            'status': '0',
                            'employee_id': this.current_employee_id,
                            'item_amount':this.alloted_item_amount,
                        })
                        .then(res => {
                            this.success =  true;
                            window.location.href ="{{url('item_return_requests')}}" +'/'+res.data;
                        })
                        .catch(err => {
                            this.error = true;
                            this.errors = err.response.data.errors;
                        })
                    }else{
                        this.receiver_employee_error = true;
                        
                    }
                },
                printAllotments: function (){
                    $('.details').css('display', 'block');
                    $('.m-invoice-2 .m-invoice__wrapper .m-invoice__body table tbody tr td').css('color', '#000');
                    $('.m-portlet .m-portlet__body').css('color', '#000');
                    $('table').removeClass('table-bordered');
                    $('table').addClass('table_border');
                    $('table').addClass('table_header');
                    $('.un').css('display', 'none');
                    $('.search_box').css('display', 'none');
                    $('.m-invoice-2 .m-invoice__wrapper .m-invoice__body table tbody tr td').css('text-align', 'center !important');
                    window.print();
                    $('.un').css('display', 'block');
                    $('.search_box').css('display', 'block');
                    $('.details').css('display', 'none');
                    $('.m-invoice-2 .m-invoice__wrapper .m-invoice__body table tbody tr td').css('color', '#6f727d');
                    $('table').removeClass('table_border');
                    $('table').removeClass('table_header');
                    $('table').addClass('table-bordered');
                    $('.m-portlet .m-portlet__body').css('color', '#575962');

                    
                },
                getSelect2: function(){
                    // $(".select2").select2();
                },
                checkAll: function(event,employee_id){
                    this.model_title = 'اعاده جنس';
                    if(event.target.checked) {
                        $('input.checkbox:not(input.checkbox:disabled)').prop('checked', true);

                        axios.get("{{url('allotments/get_all_item_details_specifications')}}/"+employee_id)
                        .then(res => {
                            for($i=0;$i<res.data.length;$i++){
                                this.item_details_specifications.push(res.data[$i]['item_details_specifications']);
                             }
                        })
                        .catch(err => {
                            console.log(err);
                        });
                    }else{
                        this.item_details_specifications = [];
                        $('input.checkbox:not(input.checkbox:disabled)').prop('checked', false);
                    }
                    var len = $('input[type="checkbox"]:checked').length-1;


                },
                onSearch(search, loading) {
                    loading(true);
                    this.search(loading, search, this);
                    },
                search: _.debounce((loading, search, vm) => {
                    
                    axios.post(`{{route("get_employee_axios")}}`,{
                                'name' : search
                                })           
                        .then(res => {
                            console.log(res.data.length);
                            if(res.data.length !=0) 
                                vm.employees = res.data.items;
                            // else{
                            //     vm.employees=[];
                            //     vm.employees.push(vm.dipo);    
                            // }
                            loading(false);
                        })
                        .catch(err => {
                            console.log(err);
                        });
                        
                }, 350)
                
            },

            mounted() {
                // this.getAllEmployees();
            },


        });

        $('#allotment_history').on('hidden.bs.modal', function () {
            employee_allotments.allotment_history = '';
        });

        $('[data-search]').on('keyup', function() {
            var searchVal = $(this).val();
            var filterItems = $('[data-filter-item]');

            if ( searchVal != '' ) {
                filterItems.addClass('hidden');
                $('[data-filter-item][data-filter-name*="' + searchVal.toLowerCase() + '"]').removeClass('hidden');
            } else {
                filterItems.removeClass('hidden');
            }
        });
    </script>

@endpush
