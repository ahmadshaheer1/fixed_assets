@extends('layouts.master')
@section('title','اعاده اجناس')
@section('content')

<div class="wrapper" id="create_item_details_specifications">

    <div class="form-group m-form__group row">
        <div class="col-md-6" id="app">
            <div class="row items_list">

                <div class="form-group col-md-12">
                    <label for="inputState">{{trans('global.item_details')}}</label>
                    <select id="sub_category_id"  name="item_details" ref="item_details" @change="getAvailableItems($event.target.value,$event.target.textcontent)" class="form-control">
                        <option value="none" selected disabled>یگ گزینه را انتخاب نماید .</option>
                        <option v-for="item in item_details" :value="item.id">@{{item.name_dr}}</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-6" v-if="alloted>0">
            <span v-for="(item, index) in allotment_list[current_item_details_id]" class="d-block">
                <span v-if="item.col1">
                        @{{extra_specifications.col1}}:
                        <span class="text-success"> @{{item.col1}} |</span>
                </span>
                <span v-if="item.col2">
                        @{{extra_specifications.col2}}:
                        <span class="text-success"> @{{item.col2}} |</span>
                </span>
                <span v-if="item.col3">
                        @{{extra_specifications.col3}}:
                        <span class="text-success"> @{{item.col3}} |</span>
                </span>
                <span v-if="item.col4">
                        @{{extra_specifications.col4}}:
                        <span class="text-success"> @{{item.col4}} |</span>
                </span>
                <span v-if="item.col5">
                        @{{extra_specifications.col5}}:
                        <span class="text-success"> @{{item.col5}} |</span>
                </span>
                <span v-if="item.col6">
                        @{{extra_specifications.col6}}:
                        <span class="text-success"> @{{item.col6}} |</span>
                </span>
                <span v-if="item.col7">
                        @{{extra_specifications.col7}}:
                        <span class="text-success"> @{{item.col7}} |</span>
                </span>
                <span v-if="item.col8">
                        @{{extra_specifications.col8}}:
                        <span class="text-success"> @{{item.col8}} |</span>
                </span>
                <span v-if="item.col9">
                        @{{extra_specifications.col9}}:
                        <span class="text-success"> @{{item.col9}} |</span>
                </span>
                <span v-if="item.col10">
                        @{{extra_specifications.col10}}:
                        <span class="text-success"> @{{item.col10}} |</span>
                </span>
                <span v-if="item.col11">
                        @{{extra_specifications.col11}}:
                        <span class="text-success"> @{{item.col11}} |</span>
                </span>
                <span v-if="item.col12">
                        @{{extra_specifications.col12}}:
                        <span class="text-success"> @{{item.col12}} |</span>
                </span>
                <span v-if="item.price">
                        قیمت:
                        <span class="text-success"> @{{item.price}} | </span>
                </span>
                <span>
                        واحد پولی:
                        <span v-if="item.currency == 0" class="text-success">افغانی</span>
                        <span v-else-if="item.currency == 1" class="text-success">دالر</span>
                </span>

                    <i class="fa fa-arrow-right text-success"></i> <i class="fa fa-arrow-left text-success"></i> @{{employees.filter(employee => employee.id == item.receiver_employee_id)[0].name_dr}}
            </span>
        </div>    
        </div>

        <div class="row" v-if="items.length>0">
            <div class="col-md-12">
                <table class="table table-bordered m-2">
                    <thead>
                        <tr>
                            <th width="10">انتخاب</th>
                            <th>شماره</th>
                            <th>قیمت</th>
                            <th>واحد پولی</th>
                            <th>حالت جنس</th>
                            <th v-if="extra_specifications.col1" >@{{extra_specifications.col1}}</th>
                            <th v-if="extra_specifications.col2" >@{{extra_specifications.col2}}</th>
                            <th v-if="extra_specifications.col3" >@{{extra_specifications.col3}}</th>
                            <th v-if="extra_specifications.col4" >@{{extra_specifications.col4}}</th>
                            <th v-if="extra_specifications.col5" >@{{extra_specifications.col5}}</th>
                            <th v-if="extra_specifications.col6" >@{{extra_specifications.col6}}</th>
                            <th v-if="extra_specifications.col7" >@{{extra_specifications.col7}}</th>
                            <th v-if="extra_specifications.col8" >@{{extra_specifications.col8}}</th>
                            <th v-if="extra_specifications.col9" >@{{extra_specifications.col9}}</th>
                            <th v-if="extra_specifications.col10" >@{{extra_specifications.col10}}</th>
                            <th v-if="extra_specifications.col11" >@{{extra_specifications.col11}}</th>
                            <th v-if="extra_specifications.col12" >@{{extra_specifications.col12}}</th>
                            <th>اضافه شده از طریق</th>
                            <th>لست کارمندان</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(item, index) in items" v-if="item.status != 1">
                            <td>
                                <div class="m-checkbox-list" v-if="item.status != 1">
                                    <label class="m-checkbox" style="display:inline;margin:1.5em;" >
                                    <input type="checkbox"  :id="index"  class="checkbox"  @change="itemAllotableToUser(index, item.id, $event)">
                                        <span></span>
                                    </label>
                                </div>
                            </td>
                            <td>@{{++index}}</td>
                            {{-- <td>@{{item.serial_number}}</td> --}}
                            <td>@{{item.price}}</td>
                            <td>
                                <span v-if="item.currency == 0">افغانی</span>
                                <span v-else-if="item.currency == 1">دالر</span>
                            <td>
                                <span v-if="item.condition == 0">جدید</span>
                                <span v-else-if="item.condition == 1">مستعمل</span>
                                <span v-else-if="item.condition == 2">داغمه</span>
                            </td>
                            <td v-if="extra_specifications.col1">@{{item.col1}}</td>
                            <td v-if="extra_specifications.col2">@{{item.col2}}</td>
                            <td v-if="extra_specifications.col3">@{{item.col3}}</td>
                            <td v-if="extra_specifications.col4">@{{item.col4}}</td>
                            <td v-if="extra_specifications.col5">@{{item.col5}}</td>
                            <td v-if="extra_specifications.col6">@{{item.col6}}</td>
                            <td v-if="extra_specifications.col7">@{{item.col7}}</td>
                            <td v-if="extra_specifications.col8">@{{item.col8}}</td>
                            <td v-if="extra_specifications.col9">@{{item.col9}}</td>
                            <td v-if="extra_specifications.col10">@{{item.col10}}</td>
                            <td v-if="extra_specifications.col11">@{{item.col11}}</td>
                            <td v-if="extra_specifications.col12">@{{item.col12}}</td>
                            <td>
                                <span v-if="item.meem7_id == null">دیپو</span>
                                <span v-else>م۷</span>
                            </td>
                            <td>
                                <div :class="['form-group m-0', item.receiver_employee_id == 0 ? 'has-danger' : '']">
                                    <select v-if="item.receiver_employee_id != null" v-model="item.receiver_employee_id" @change="addItemDetailsSpecificationsToAllotmentList(index-1)" class="form-control form-control-sm m-input">
                                        <option v-for="item in employees" :value="item.id" >@{{item.name_dr}}</option>
                                    </select>
                                </div>
                            </td>    
                        </tr> 
                    </tbody>
                </table>
                <button @click="finishAddingToAllotedList()" class="btn btn-success">اضافه نمودن</button>
            </div>
        </div>
</div>    
    




@endsection

@push('custom-js')
<script>
    item_app = new Vue({
        el: '#create_item_details_specifications',
        data: {
            // available items
            items: [],

            item_details: [],

            //variable for editing
            edit:false,
            edit_specifications:{},

            // validation
            error: false,
            success: false,
            

            // bulk assignment values
            master: {
                price: '',
                currency: '',
                condition: '',
                col1: '',
                col2: '',
                col3: '',
                col4: '',
                col5: '',
                col6: '',
                col7: '',
                col8: '',
                col9: '',
                col10: '',
                col11: '',
                col12: '',
            },

            // new items specifications
            specifications: [

            ],

            // variables for extra specifications rendering
            extra_specifications: [],

            // form
            total_forms: 1,

            // allotment_history
            allotment_history: [],

            item_details_specifications:[],
            employees:[],
            
            // fece5s
            fece5s: [],


            // item details specifications for allotment
            item_details_specifications: [ ],
            item_details_specifications_edit: { },
            extra_specification_keys:'',

            // allotment

            // selected allotment items based on their item details id
            allotment_list: {},

            //maximum available allotments based on requested items
            max_allotments: 0,

            // currently alloted alloted < = max_allotments
            
            alloted:0,
            edit: false,

            // current active item details > specs of which has to be selected
            current_item_details_id: 0,

            // to check whether allotment of specs of an item details is completed
            completed_item_details: {},

            // show fece5 information form after adding allotment items finishes
            finishAdding: false,

            //Fece5
            fece5_number:'',
            fece5_date:'',
            distributer_employee_id:'',
            fece5_description:'',
            fece5_item_details:'',
            // main form validation
            
            errors: [],
            allotment_header:[],
        },
        methods: {
           
            getAvailableItems(item_details_id,name) {
                alert(name);
                axios.get("{{url('item_details_specifications')}}/"+item_details_id)
                    .then(res => {
                        this.items = res.data;
                        this.getSpecificationsKeys(item_details_id);
                        this.current_item_details_id = item_details_id;
                        this.allotment_list[this.current_item_details_id] = [];
                    })
                    .catch(err => {

                    })
            },
            addItemDetailsSpecificationsToAllotmentList(index) {

                if(!this.allotment_list[this.current_item_details_id]) {
                    this.allotment_list[this.current_item_details_id] = []
                }
                if(this.allotment_list[this.current_item_details_id].filter(item => item.id == this.items[index].id).length == 0) {
                    // this.items[index]['fece9_id'] = "11";
                    this.allotment_list[this.current_item_details_id].push(this.items[index]);
                }
            },
            itemAllotableToUser(index,id, event) {
                    if(event.target.checked) {
                            this.items.filter(item =>item.id == id)[0].receiver_employee_id=0;                            
                            this.items.filter(item =>item.id == id)[0].col4=this.items[0].col4+' ';                            
                       }
                    else {
                            this.items.filter(item => item.id == id)[0].receiver_employee_id = null;
                            this.items.filter(item =>item.id == id)[0].col4=this.items[0].col4; 
                            this.allotment_list[this.current_item_details_id] = this.allotment_list[this.current_item_details_id].filter(item => item.id != id);                           
                    } 
            },
            getSpecificationsKeys(sub_category_id) {

                axios.get("{{url('get_sub_categories_keys')}}/"+sub_category_id)

                .then(res => {
                    this.extra_specifications = res.data;
                })
                .catch(err => {
                    console.log(err);

                })
            },

            getItemDetailsSpecifications(evt,index,item_details_specifications_id) {
                //if check
                if(evt){
                axios.get("{{url('allotments/get_item_details_specifications')}}/"+item_details_specifications_id)
                    .then(res => {
                        this.item_details_specifications.push(res.data);
                    })
                    .catch(err => {
                        console.log(err);
                    });
                }else{
                    var rem = this.item_details_specifications.filter(item=>{return item.id==item_details_specifications_id})[0];

                    remove_id = this.item_details_specifications.indexOf(rem);
                    this.item_details_specifications.splice(remove_id,1);
                }

            },
            getItemDetails() {
                axios.get("{{url('get_item_details')}}/")
                .then(res => {
                    this.item_details = res.data;
                })
                .catch(err => {
                    console.log(err);

                })
            },
            getAllEmployees() {
                axios.get("{{url('get_all_employees')}}")
                    .then(res => {
                        this.employees = res.data;
                    })
                    .catch(err => {
                        console.log(err);

                    })

            },
            finishAddingToAllotedList(){
                var obj = {};
                obj["id"] = this.current_item_details_id ;
                obj["name"] = nieto.value;
                 this.allotment_header.push(obj); 
            }
            
        },
        mounted() {
            this.getItemDetails();
            
            this.getAllEmployees();
            // this.getSpecificationsKeys();
        },
    });



</script>

@endpush

