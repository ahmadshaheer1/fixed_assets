@extends('layouts.master')
@section('title', trans('global.employee_allotments'))
@section('content')
    <div class="wrapper" id="employee_allotments">

        <div class="m-invoice-2">
            <div class="m-invoice__wrapper">
                <div class="m-invoice__head" style="">
                    <div class="m-invoice__container">
                        <div class="m-invoice__logo pb-2">
                            <div class="d-flex justify-content-between align-items-start">
                                <div>
                                    <img class="w-25"
                                        src="https://b-buildingbusiness.com/wp-content/uploads/2018/08/unknown_person-1024x1024-1.png">
                                </div>
                                <div>
                                    @if ($employee_attachment->pluck('attachment')->isEmpty())
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#upload_modal"
                                            onclick="$('input[name=id]').val({{ $employee->id }})"
                                            class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air">
                                            <i class="fa fa-upload"></i>
                                        </a>
                                    @else
                                        @php
                                            $attachmentPath = asset($employee_attachment->pluck('attachment')->first());
                                        @endphp
                                        <div class="btn-group" role="group">
                                            <a href="{{ $attachmentPath }}" target="_blank" class="btn btn-outline-info"
                                                style="border: none;">
                                                <i class="fas fa-paperclip fa-lg"></i>
                                            </a>
                                            <a href="javascript:void(0)" class="btn btn-outline-info" style="border: none;"
                                                onclick="openEditModal({{ $employee->id }}, 'تصحیح فایل');">
                                                <i class="fas fa-redo-alt"></i>
                                            </a>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="m-invoice__items">
                            <div class="m-invoice__item">
                                <span class="m-invoice__subtitle">اسم</span>
                                <span class="m-invoice__text">{{ $employee->name_dr }} ({{ $employee->last_name }})</span>
                            </div>
                            <div class="m-invoice__item">
                                <span class="m-invoice__subtitle">نام پدر</span>
                                <span class="m-invoice__text">{{ $employee->father_name_dr }}</span>
                            </div>
                            <div class="m-invoice__item">
                                <span class="m-invoice__subtitle">عنوان وظیفه</span>
                                <span class="m-invoice__text">{{ $employee->current_position_dr }}</span>
                            </div>
                            <div class="m-invoice__item">
                                <span class="m-invoice__subtitle">اداره</span>
                                <span
                                    class="m-invoice__text">{{ isset($employee->department->name_dr) ? $employee->department->name_dr : '' }}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="m-invoice__body">
                    <div class="table-responsive">
                        <div class="search">
                            <input type="text" class="search_box" placeholder="جستجو..." data-search />
                            <input type="text" class="search_box" placeholder="جستجو به اساس تکت توزیع / م۷"
                                data-search_details />
                        </div>
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th class="un">
                                        <label class="m-checkbox" style="display:inline;margin:1.5em;">
                                            <input type="checkbox" class="checkbox"
                                                @change="checkAll($event,{{ $employee->id }})">
                                            <span></span>
                                        </label>
                                        {{-- انتخاب همه --}}
                                    </th>
                                    <th>شماره</th>
                                    <th>اسم جنس</th>
                                    <th>واحد</th>
                                    <th>تعداد جنس</th>
                                    <th>توضیحات</th>
                                    <th>تاریخ تسلیمی</th>
                                    <th>تکت توزیع / م۷ </th>
                                    <th>تاریخ اعاده</th>
                                    <th>موجودیت</th>
                                    <th>حالت</th>
                                    <th class="un">عملیات</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($status == null || $status == 1)
                                    @foreach ($employee->allotments->where('status' , 1) as $index => $item)
                                        @include('allotments.show_allotments', ['item' => $item])
                                    @endforeach
                                @else 
                                    @foreach ($employee->allotments->where('status' , 0) as $index => $item)
                                        @include('allotments.show_allotments', ['item' => $item])
                                    @endforeach
                                @endif
                                </tbody>
                        </table>
                    </div>
                    <div class="row no-print">
                        <div class="col-lg-10">
                            <a v-if="item_details_specifications.length>0" href="#" data-toggle="modal"
                                @click="" data-target="#allotment_history" title="اعاده جنس"
                                class="btn m-btn--square  btn-success"> <i class="la la-rotate-left"></i> دوره تسلیمی</a>
                            <button v-if="show_merge_items_button" @click="mergeItems()" class="btn btn-primary"> <i
                                    class="la la-plus"></i>
                                یکجا کردن</button>
                        </div>
                        @if($status == null || $status == 1)
                        <div class="col-lg-2">
                            <button title="اعاده جنس" @click="printAllotments()" class="btn m-btn--square  btn-info"> <i
                                    class="la la-print"></i> چاپ لست</button>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        {{-- Allotment History --}}
        <div class="modal fade" id="allotment_history" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle"> @{{ model_title }}
                            {{ $employee->name_dr }} ({{ $employee->last_name }})</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <div class="m-scrollable" data-scrollable="true" data-height="380" data-mobile-height="300">

                            <!--Begin::Timeline 2 -->
                            <div v-if="allotment_history.length > 0" class="m-timeline-2">
                                <div class="m-timeline-2__items  m--padding-top-25 m--padding-bottom-30">
                                    <div v-for="(item, index) in allotment_history" class="m-timeline-2__item">
                                        <span class="m-timeline-2__item-time m--font-primary">@{{ item.allotment_date }}</span>
                                        <div class="m-timeline-2__item-cricle">
                                            <i
                                                :class="[item.status == 0 ? 'fa fa-genderless m--font-warning' : item.status ==
                                                    2 ? 'fa fa-home m--font-danger icon-sm' :
                                                    'fa fa-genderless m--font-success'
                                                ]"></i>
                                        </div>
                                        <div class="m-timeline-2__item-text  m--padding-top-5">
                                            <div class="font-weight-bold">
                                                <span v-if="item.employee_type == 0 "> @{{ item.employee.name_dr }}
                                                    @{{ item.employee.last_name }} -- ولد -- @{{ item.employee.father_name_dr }}</span>
                                                <span v-else> @{{ item.external_employee.name_dr }} @{{ item.external_employee.last_name }} -- ولد --
                                                    @{{ item.external_employee.father_name_dr }}</span>
                                            </div>
                                            <div class="">

                                                <span v-if="item.employee_type == 0 "> @{{ item.employee.current_position_dr }}</span>
                                                <span v-else> @{{ item.external_employee.current_position_dr }} </span>
                                            </div>
                                            <div v-if="item.condition == 0" class="m--font-success">
                                                جدید
                                            </div>
                                            <div v-else-if="item.condition == 1" class="m--font-warning">
                                                مستعمل
                                            </div>
                                            <div v-else-if="item.condition == 2" class="m--font-danger">
                                                داغمه
                                            </div>
                                            <div v-else-if="item.condition == 3" class="m--font-danger">
                                                غیر فعال
                                            </div>
                                            <div v-if="item.status == 0" class="font-weight-bold m--font-info">
                                                تاریخ اعاده:‌@{{ item.return_date }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--End::Timeline 2 -->
                            <div v-if="allotment_history.length == 0" class="">
                                <div class="m-accordion m-accordion--bordered" id="m_accordion_2" role="tablist">

                                    <div class="m-wizard m-wizard--1 m-wizard--info" id="m_wizard">

                                        <!--begin: Message container -->
                                        <div class="m-portlet__padding-x">

                                            <!-- Here you can put a message or alert -->
                                        </div>

                                        <!--end: Message container -->

                                        <!--begin: Form Wizard Head -->
                                        <div class="m-wizard__head m-portlet__padding-x">

                                            <!--begin: Form Wizard Progress -->
                                            <div class="m-wizard__progress">
                                                <div class="progress">
                                                    <div class="progress-bar" role="progressbar"
                                                        style="width: calc(0% + 26px);" aria-valuenow="50"
                                                        aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>

                                            <!--end: Form Wizard Progress -->

                                            <!--begin: Form Wizard Nav -->
                                            <div class="m-wizard__nav">
                                                <div class="m-wizard__steps">
                                                    <div class="m-wizard__step m-wizard__step--current"
                                                        m-wizard-target="m_wizard_form_step_1">
                                                        <div class="m-wizard__step-info">
                                                            <a href="#" class="m-wizard__step-number">
                                                                <span class="bg-info"><span>1</span></span>
                                                            </a>
                                                            <div class="m-wizard__step-line">
                                                                <span></span>
                                                            </div>
                                                            <div class="m-wizard__step-label">
                                                                درخواست اعاده جنس
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="m-wizard__step" m-wizard-target="m_wizard_form_step_2">
                                                        <div class="m-wizard__step-info">
                                                            <div class="m-wizard__step-number">
                                                                <span><span>2</span></span>
                                                            </div>
                                                            <div class="m-wizard__step-line">
                                                                <span></span>
                                                            </div>
                                                            <div class="m-wizard__step-label">
                                                                چاپ نمودن و طی مراحل اسناد
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="m-wizard__step" m-wizard-target="m_wizard_form_step_3">
                                                        <div class="m-wizard__step-info">
                                                            <div class="m-wizard__step-number">
                                                                <span><span>3</span></span>
                                                            </div>
                                                            <div class="m-wizard__step-line">
                                                                <span></span>
                                                            </div>
                                                            <div class="m-wizard__step-label">
                                                                اپلود نمودن درخواست اعاده اجناس طی مراحل شده
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <!--end: Form Wizard Nav -->
                                        </div>
                                    </div>
                                    <div class="">
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th style="width: 50px;">شماره</th>
                                                    <th>جنس</th>
                                                    <th>توضیحات</th>
                                                    <th>تعداد تسلیمی</th>
                                                    <th style="width:120px">نوعیت تسلیمی</th>
                                                    <th v-if="return_type ==2">شخص تسلیم گیرنده</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for=" (item, outerIndex) in item_details_specifications"
                                                    :key="outerIndex">
                                                    <td>@{{ ++outerIndex }}</td>
                                                    <td>@{{ item.item_details.name_dr }}</td>
                                                    <td>
                                                        <div class="details">
                                                            <span class="m--font-primary"
                                                                v-if="item.sub_category_keys.col1">
                                                                @{{ item.sub_category_keys.col1 }} :
                                                            </span>
                                                            <span class="m--font-info">
                                                                @{{ item.col1 }} ||
                                                            </span>
                                                            <span class="m--font-primary"
                                                                v-if="item.sub_category_keys.col2">
                                                                @{{ item.sub_category_keys.col2 }} :
                                                            </span>
                                                            <span class="m--font-info">
                                                                @{{ item.col2 }} ||
                                                            </span>
                                                            <span class="m--font-primary"
                                                                v-if="item.sub_category_keys.col3">
                                                                @{{ item.sub_category_keys.col3 }} :
                                                            </span>
                                                            <span class="m--font-info">
                                                                @{{ item.col3 }} ||
                                                            </span>
                                                            <span class="m--font-primary"
                                                                v-if="item.sub_category_keys.col4">
                                                                @{{ item.sub_category_keys.col4 }} :
                                                            </span>
                                                            <span class="m--font-info">
                                                                @{{ item.col4 }} ||
                                                            </span>
                                                            <span class="m--font-primary"
                                                                v-if="item.sub_category_keys.col5">
                                                                @{{ item.sub_category_keys.col5 }} :
                                                            </span>
                                                            <span class="m--font-info">
                                                                @{{ item.col5 }} ||
                                                            </span>
                                                            <span class="m--font-primary"
                                                                v-if="item.sub_category_keys.col6">
                                                                @{{ item.sub_category_keys.col6 }} :
                                                            </span>
                                                            <span class="m--font-info">
                                                                @{{ item.col6 }} ||
                                                            </span>
                                                            <span class="m--font-primary"
                                                                v-if="item.sub_category_keys.col7">
                                                                @{{ item.sub_category_keys.col7 }} :
                                                            </span>
                                                            <span class="m--font-info">
                                                                @{{ item.col7 }} ||
                                                            </span>
                                                            <span class="m--font-primary"
                                                                v-if="item.sub_category_keys.col8">
                                                                @{{ item.sub_category_keys.col8 }} :
                                                            </span>
                                                            <span class="m--font-info">
                                                                @{{ item.col8 }} ||
                                                            </span>
                                                            <span class="m--font-primary"
                                                                v-if="item.sub_category_keys.col9">
                                                                @{{ item.sub_category_keys.col9 }} :
                                                            </span>
                                                            <span class="m--font-info">
                                                                @{{ item.col9 }} ||
                                                            </span>
                                                            <span class="m--font-primary"
                                                                v-if="item.sub_category_keys.col10">
                                                                @{{ item.sub_category_keys.col10 }} :
                                                            </span>
                                                            <span class="m--font-info">
                                                                @{{ item.col10 }} ||
                                                            </span>
                                                            <span class="m--font-primary"
                                                                v-if="item.sub_category_keys.col11">
                                                                @{{ item.sub_category_keys.col11 }} :
                                                            </span>
                                                            <span class="m--font-info">
                                                                @{{ item.col11 }} ||
                                                            </span>
                                                            <span class="m--font-primary"
                                                                v-if="item.sub_category_keys.col12">
                                                                @{{ item.sub_category_keys.col12 }} :
                                                            </span>
                                                            <span class="m--font-info">
                                                                @{{ item.col12 }} ||
                                                            </span>
                                                            <span class="m--font-primary"
                                                                v-if="item.sub_category_keys.distribution_type">
                                                                تعداد :
                                                            </span>
                                                            <span class="m--font-info">
                                                                {{-- deleted_by = item_amount_alloted_to_user --}}
                                                                @{{ item.deleted_by }}
                                                            </span>
                                                        </div>
                                                        <a href="javascript:;"
                                                            class="btn btn-xs green toggle_description">
                                                            نمایش <i class="fa fa-plus"></i>
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <span v-if="item.deleted_by">
                                                            <input type="number" min="1" :max="item.deleted_by"
                                                                :class="['form-control', return_error == true ? 'is-invalid' :
                                                                    ''
                                                                ]"
                                                                placeholder="تعداد تسلیمی" v-model="alloted_item_amount"
                                                                v-on:keyup="validate($event.target.value,item.deleted_by)">
                                                        </span>
                                                        <span v-else>
                                                            1
                                                        </span>
                                                    </td>
                                                    <td>
                                                        <select class="form-control" v-model="return_type"
                                                            @change="checkType(outerIndex-1,item.id)">
                                                            <option selected disabled>نوعیت تسلیمی</option>
                                                            <template v-if="item.sub_category_keys.item_type == 1">
                                                                <option value="4"
                                                                    :disabled="!item.sub_category_keys.item_type">املاک
                                                                </option>
                                                            </template>
                                                            <template v-else>
                                                                <option value="1">فورم اعاده</option>
                                                                <option value="2">مکتوب</option>
                                                            </template>
                                                        </select>
                                                    </td>
                                                    <td v-if="return_type == 2">
                                                        {{-- new employee search by name and father name --}}
                                                        <div class="input-group mb-3">
                                                            <input type="text" :ref="'name' + (outerIndex - 1)"
                                                                class="form-control" placeholder="نام">
                                                            <input type="text" :ref="'father_name' + (outerIndex - 1)"
                                                                class="form-control" placeholder="نام پدر">

                                                            <div class="input-group-append">
                                                                <button class="btn btn-success " :id="outerIndex - 1"
                                                                    @click="searchEmp(outerIndex - 1)">
                                                                    <i class="fa fa-search"></i>
                                                                </button>
                                                            </div>

                                                        </div>
                                                        <div class="row">
                                                            <div class="col-4">
                                                                <div class="form-check">
                                                                    <input class="form-check-input" type="radio"
                                                                        :name="'employee_type' + (outerIndex - 1)"
                                                                        id="flexRadioDefault1" value="0"
                                                                        :ref="'employee_type' + (outerIndex - 1)">
                                                                    داخلی
                                                                    </label>
                                                                </div>
                                                                <div class="form-check">
                                                                    <input class="form-check-input" type="radio"
                                                                        :name="'employee_type' + (outerIndex - 1)"
                                                                        id="flexRadioDefault2" value="1"
                                                                        :ref="'employee_type' + (outerIndex - 1)">
                                                                    خارجی
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-*"
                                                                :ref="'selectedEmpDetails' + (outerIndex - 1)"
                                                                :id="'selectedEmpDetails' + (outerIndex - 1)">

                                                            </div>
                                                        </div>
                                                        <table>
                                                            <tr v-for="(emp, empIndex) in getEmployeesByIndex(outerIndex -1 )"
                                                                :key="emp.id">
                                                                <td>
                                                                    @{{ emp.name_dr }}
                                                                    ولد
                                                                    @{{ emp.father_name_dr }}
                                                                    وظیفه
                                                                    @{{ emp.current_position_dr }}
                                                                </td>
                                                                <td>
                                                                    <button class="btn btn-success save-button"
                                                                        :id="outerIndex - 1"
                                                                        @click="addSelectedEmployeeToList(outerIndex - 1, emp, item.id)">
                                                                        تائید <i class="la la-check"></i>
                                                                    </button>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>

                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <!--begin::Item-->
                                    <div class="m-accordion__item">
                                        <div class="m-accordion__item-head collapsed" role="tab"
                                            id="m_accordion_2_item_1_head" data-toggle="collapse"
                                            href="#m_accordion_2_item_1_body" aria-expanded="    false">
                                            <span class="m-accordion__item-icon"><i
                                                    class="fa flaticon-user-ok"></i></span>
                                            <span class="m-accordion__item-title">

                                                <span v-if="return_type === 0">داغمه</span>
                                                <span v-if="return_type == 1">فورم اعاده</span>
                                                <span v-if="return_type == 2">مکتوب</span>
                                                {{-- <span v-if="return_type == 2">درخواستی</span> --}}
                                                <span v-if="return_type == 3">مفقود</span>
                                                <span v-if="return_type == 4">املاک</span>
                                                <span v-else></span>
                                            </span>
                                            <span class="m-accordion__item-mode"></span>
                                        </div>
                                        <div class="m-accordion__item-body collapse" id="m_accordion_2_item_1_body"
                                            class=" " role="tabpanel" aria-labelledby="m_accordion_2_item_1_head"
                                            data-parent="#m_accordion_2">
                                            <div class="m-accordion__item-content" v-if="show_form == true">
                                                <form class="m-form m-form--fit m-form--label-align-right" method="POST"
                                                    @submit.prevent="submitForm" enctype="multipart/form-data"
                                                    action="{{ route('item_return_requests.store') }}">
                                                    @csrf

                                                    <input type="hidden" name="employee_id"
                                                        :value="current_employee_id">
                                                    <input type="hidden" name="status" value="0">
                                                    <input type="hidden" v-model="return_list_string">
                                                    <div class="m-portlet__body">
                                                        <div class="form-group row ">
                                                            <div class="has-danger col-md-12"
                                                                v-if="receiver_employee_error==true">
                                                                <div class="form-control-feedback">شخص تسلیم گیرنده را
                                                                    انتخاب نمائید</div>
                                                            </div>
                                                            <div
                                                                :class="['form-group col-md-6 m-form__group', errors.number ?
                                                                    'has-danger' : ''
                                                                ]">
                                                                <label for="fece5_number">نمبر</label>
                                                                <input type="number" name="number"
                                                                    value="{{ old('number') }}" v-model="number"
                                                                    class="form-control m-input m-input--square"
                                                                    id="number" placeholder="نمبر">

                                                                <div class="form-control-feedback" v-if="error">
                                                                    @{{ errors.number }}</div>

                                                            </div>
                                                            <div
                                                                :class="['form-group col-md-6 m-form__group pt-0', errors.date ?
                                                                    'has-danger' : ''
                                                                ]">
                                                                <label for="inputPassword4">تاریخ</label>
                                                                <div class="input-group date">
                                                                    <input type="text" name="date"
                                                                        value="{{ old('date') }}" ref="date"
                                                                        class="form-control persian_date"
                                                                        onfocus='$(".persian_date").persianDatepicker();'
                                                                        placeholder="{{ trans('global.meem7_date') }}">
                                                                    <div class="input-group-append">
                                                                        <span class="input-group-text">
                                                                            <i class="la la-calendar"></i>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                                <div class="form-control-feedback" v-if="error">
                                                                    @{{ errors.date }}</div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row ">

                                                            <div
                                                                :class="['form-group m-form__group col-md-12 pt-0', errors
                                                                    .description ? 'has-danger' : ''
                                                                ]">
                                                                <label
                                                                    for="department_description">{{ trans('global.description') }}</label>
                                                                <textarea name="description" v-model="description" class="form-control"
                                                                    id="department_description"placeholder="{{ trans('global.description') }}">{{ old('description') }}</textarea>
                                                                <div class="form-control-feedback" v-if="error">
                                                                    @{{ errors.description }}</div>
                                                            </div>

                                                        </div>

                                                        <div class="form-group row ">
                                                            <div class="form-group col-md-6 m-form__group">
                                                                <button class="btn btn-success">ارسال <i
                                                                        class="fa fa-paper-plane"
                                                                        aria-hidden="true"></i></button>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                    <!--end::Item-->

                                    <!--begin::Item-->
                                    {{-- <div class="m-accordion__item">
                                    <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_2_item_2_head" data-toggle="collapse" href="#m_accordion_2_item_2_body" aria-expanded="    false">
                                        <span class="m-accordion__item-icon"><i class="fa  flaticon-placeholder"></i></span>
                                        <span class="m-accordion__item-title">اعاده به دیپو</span>
                                        <span class="m-accordion__item-mode"></span>
                                    </div>
                                    <div class="m-accordion__item-body collapse" id="m_accordion_2_item_2_body" class=" " role="tabpanel" aria-labelledby="m_accordion_2_item_2_head" data-parent="#m_accordion_2">
                                        <div class="m-accordion__item-content">
                                            <p>
                                                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into
                                                electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown
                                                printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with
                                                the release of Letraset sheets containing
                                            </p>
                                        </div>
                                    </div>
                                </div> --}}

                                    <!--end::Item-->
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div> --}}
                </div>
            </div>
        </div>

        <div class="modal fade bd-example-modal-lg" id="editModal" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">تصحیح جنس</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <div class="table-responsive-lg">
                            <table class="table">
                                <tr>

                                    <th class="px-1">
                                        <div class="input-group">
                                            <input type="number" v-model="specifications.price"
                                                class="form-control big-input"
                                                placeholder=" {{ trans('global.price') }}">

                                        </div>
                        </div>
                        </th>
                        <th class="px-1" width="150">
                            <div class="form-group">
                                <div class="input-group">
                                    <select v-model="specifications.currency" class="form-control big-input">
                                        <option value="0" selected>افغانی</option>
                                        <option value="1">دالر</option>
                                    </select>

                                </div>
                            </div>
                    </div>
                    </th>

                    <th class="px-1" width="150">
                        <div class="form-group">
                            <div class="input-group">
                                <select v-model="specifications.condition" class="form-control big-input">
                                    <option value="">{{ trans('global.condition') }}</option>
                                    <option value="0">جدید</option>
                                    <option value="1">مستعمل</option>
                                    <option value="2">داغمه</option>
                                    <option value="3">غیر فعال</option>
                                </select>

                            </div>
                        </div>
                </div>
                </th>
                <th class="px-1">
                    <div class="input-group">
                        <input type="text" v-model="specifications.details" class="form-control big-input"
                            placeholder=" {{ trans('global.details') }}">

                    </div>
            </div>
            </th>
            <th class="px-1">
                <div v-if="extra_specifications.col1" class="form-group ">
                    <div class="input-group">
                        <input type="text" v-model="specifications.col1" class="form-control big-input"
                            :placeholder="extra_specifications.col1">

                    </div>
                </div>
        </div>
        </th>
        <th class="px-1">
            <div v-if="extra_specifications.col2" class="form-group">
                <div class="input-group">
                    <input type="text" v-model="specifications.col2" class="form-control big-input"
                        :placeholder="extra_specifications.col2">

                </div>
            </div>
    </div>
    </th>
    <th class="px-1">
        <div v-if="extra_specifications.col3" class="form-group">
            <div class="input-group">
                <input type="text" v-model="specifications.col3" class="form-control big-input"
                    :placeholder="extra_specifications.col3">

            </div>
        </div>
        </div>
    </th>
    <th class="px-1">
        <div v-if="extra_specifications.col4" class="form-group">
            <div class="input-group">
                <input type="text" v-model="specifications.col4" class="form-control big-input"
                    :placeholder="extra_specifications.col4">

            </div>
        </div>
        </div>
    </th>
    <th class="px-1">
        <div v-if="extra_specifications.col5" class="form-group">
            <div class="input-group">
                <input type="text" v-model="specifications.col5" class="form-control big-input"
                    :placeholder="extra_specifications.col5">

            </div>
        </div>
        </div>
    </th>
    <th class="px-1">
        <div v-if="extra_specifications.col6" class="form-group">
            <div class="input-group">
                <input type="text" v-model="specifications.col6" class="form-control big-input"
                    :placeholder="extra_specifications.col6">

            </div>
        </div>
        </div>
    </th>
    <th class="px-1">
        <div v-if="extra_specifications.col7" class="form-group">
            <div class="input-group">
                <input type="text" v-model="specifications.col7" class="form-control big-input"
                    :placeholder="extra_specifications.col7">

            </div>
        </div>
        </div>
    </th>
    <th class="px-1">
        <div v-if="extra_specifications.col8" class="form-group">
            <div class="input-group">
                <input type="text" v-model="specifications.col8" class="form-control big-input"
                    :placeholder="extra_specifications.col8">

            </div>
        </div>
        </div>
    </th>
    <th class="px-1">
        <div v-if="extra_specifications.col9" class="form-group">
            <div class="input-group">
                <input type="text" v-model="specifications.col9" class="form-control big-input"
                    :placeholder="extra_specifications.col9">

            </div>
        </div>
        </div>
    </th>
    <th class="px-1">
        <div v-if="extra_specifications.col10" class="form-group">
            <div class="input-group">
                <input type="text" v-model="specifications.col10" class="form-control big-input"
                    :placeholder="extra_specifications.col10">

            </div>
        </div>
        </div>
    </th>
    <th class="px-1">
        <div v-if="extra_specifications.col11" class="form-group">
            <div class="input-group">
                <input type="text" v-model="specifications.col11" class="form-control big-input"
                    :placeholder="extra_specifications.col11">

            </div>
        </div>
        </div>
    </th>
    <th class="px-1">
        <div v-if="extra_specifications.col12" class="form-group">
            <div class="input-group">
                <input type="text" v-model="specifications.col12" class="form-control big-input"
                    :placeholder="extra_specifications.col12">

            </div>
        </div>
        </div>
    </th>
    <th class="px-1">
        <div v-if="extra_specifications.distribution_type =='1'" class="form-group">
            <div class="input-group">
                <input type="text" v-model="specifications.item_amount" class="form-control" placeholder="تعداد">

            </div>
        </div>
        </div>
    </th>
    <th class="" style="line-height:3" width="75">

        <div>
            <button @click="updateItemDetailsSpecifications()"
                class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill"
                title="Edit">
                <i class="la la-edit"></i>
            </button>
        </div>
    </th>
    </tr>
    </table>
    </div>
    </div>
    </div>
    </div>
    </div>

    <div class="modal fade bd-example-modal-lg" id="return_allotment" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">تاریخچه جمع و قید جنس</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">


                    {{-- Start of Edit Allotment --}}
                    <div>
                        <div>
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>شماره</th>
                                        <th>جنس</th>
                                        <th>توضیحات</th>
                                        <th>حالت جنس </th>
                                        <th>{{ trans('global.fece9_date') }}</th>
                                        <th>شخص تسلیم گیرنده</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for=" (item,index) in active_allotment">
                                        <td>@{{ ++index }}</td>
                                        <td>@{{ item.item_details_specifications.item_details.name_dr }}</td>
                                        <td>
                                            <div class="ditails" style="display:none">
                                                <span class="m--font-primary"
                                                    v-if="item.item_details_specifications.sub_category_keys.col1">
                                                    @{{ item.item_details_specifications.sub_category_keys.col1 }} :
                                                </span>
                                                <span class="m--font-info">
                                                    @{{ item.item_details_specifications.col1 }} ||
                                                </span>
                                                <span class="m--font-primary"
                                                    v-if="item.item_details_specifications.sub_category_keys.col2">
                                                    @{{ item.item_details_specifications.sub_category_keys.col2 }} :
                                                </span>
                                                <span class="m--font-info">
                                                    @{{ item.item_details_specifications.col2 }} ||
                                                </span>
                                                <span class="m--font-primary"
                                                    v-if="item.item_details_specifications.sub_category_keys.col3">
                                                    @{{ item.item_details_specifications.sub_category_keys.col3 }} :
                                                </span>
                                                <span class="m--font-info">
                                                    @{{ item.item_details_specifications.col3 }} ||
                                                </span>
                                                <span class="m--font-primary"
                                                    v-if="item.item_details_specifications.sub_category_keys.col4">
                                                    @{{ item.item_details_specifications.sub_category_keys.col4 }} :
                                                </span>
                                                <span class="m--font-info">
                                                    @{{ item.item_details_specifications.col4 }}
                                                </span>
                                                <span class="m--font-primary"
                                                    v-if="item.item_details_specifications.sub_category_keys.col5">
                                                    @{{ item.item_details_specifications.sub_category_keys.col5 }} :
                                                </span>
                                                <span class="m--font-info">
                                                    @{{ item.item_details_specifications.col5 }}
                                                </span>
                                                <span class="m--font-primary"
                                                    v-if="item.item_details_specifications.sub_category_keys.col6">
                                                    @{{ item.item_details_specifications.sub_category_keys.col6 }} :
                                                </span>
                                                <span class="m--font-info">
                                                    @{{ item.item_details_specifications.col6 }}
                                                </span>
                                                <span class="m--font-primary"
                                                    v-if="item.item_details_specifications.sub_category_keys.col7">
                                                    @{{ item.item_details_specifications.sub_category_keys.col7 }} :
                                                </span>
                                                <span class="m--font-info">
                                                    @{{ item.item_details_specifications.col7 }}
                                                </span>
                                                <span class="m--font-primary"
                                                    v-if="item.item_details_specifications.sub_category_keys.col8">
                                                    @{{ item.item_details_specifications.sub_category_keys.col8 }} :
                                                </span>
                                                <span class="m--font-info">
                                                    @{{ item.item_details_specifications.col8 }}
                                                </span>
                                                <span class="m--font-primary"
                                                    v-if="item.item_details_specifications.sub_category_keys.col9">
                                                    @{{ item.item_details_specifications.sub_category_keys.col9 }} :
                                                </span>
                                                <span class="m--font-info">
                                                    @{{ item.item_details_specifications.col9 }}
                                                </span>
                                                <span class="m--font-primary"
                                                    v-if="item.item_details_specifications.sub_category_keys.col10">
                                                    @{{ item.item_details_specifications.sub_category_keys.col10 }} :
                                                </span>
                                                <span class="m--font-info">
                                                    @{{ item.item_details_specifications.col10 }}
                                                </span>
                                                <span class="m--font-primary"
                                                    v-if="item.item_details_specifications.sub_category_keys.col11">
                                                    @{{ item.item_details_specifications.sub_category_keys.col11 }} :
                                                </span>
                                                <span class="m--font-info">
                                                    @{{ item.item_details_specifications.col11 }}
                                                </span>
                                                <span class="m--font-primary"
                                                    v-if="item.item_details_specifications.sub_category_keys.col12">
                                                    @{{ item.item_details_specifications.sub_category_keys.col12 }} :
                                                </span>
                                                <span class="m--font-info">
                                                    @{{ item.item_details_specifications.col12 }}
                                                </span>
                                            </div>
                                            <a href="javascript:;" class="btn btn-xs green toggle_description">
                                                نمایش <i class="fa fa-plus"></i>
                                            </a>
                                        </td>
                                        <td>@{{ item.allotment_date }}</td>

                                        <td width="150">

                                            <span v-if="item.condition==0">جدید</span>
                                            <span v-else-if="item.condition==1">مستعمل</span>
                                            <span v-else-if="item.condition==2">داغمه</span>
                                            <span v-else-if="item.condition==3"> غیر فعال </span>
                                        </td>

                                        <td>
                                            <span v-if="item.employee_type == 0 "> @{{ item.employee.name_dr }}
                                                @{{ item.employee.last_name }} -- ولد -- @{{ item.employee.father_name_dr }}</span>
                                            <span v-else> @{{ item.external_employee.name_dr }} @{{ item.external_employee.last_name }} -- ولد --
                                                @{{ item.external_employee.father_name_dr }}</span>
                                        </td>

                                    </tr>
                                </tbody>
                            </table>
                            <div class="form-group row ">
                                <div class="form-group col-md-6 m-form__group p-4 ">
                                    <button class="btn btn-success save-button"
                                        @click="roleBackAllotement(active_allotment[0].id,active_allotment[0].item_details_specifications.id)">
                                        انتقال به دیپو <i class="fa fa-archive"></i></button>
                                </div>
                                {{-- <div class="form-group col-md-6 m-form__group p-4 ">
                                    <button class="btn btn-primary save-button" @click="submitAllotements()"> توزیع به شخص دیگر  <i class="fa fa-share-alt"></i></button>
                                </div> --}}
                            </div>

                        </div>

                    </div>

                    {{-- Start of Edit Allotment --}}




                </div>
                {{-- <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div> --}}
            </div>
        </div>
    </div>
    {{-- Start Item Edit --}}
    <div class="modal fade bd-example-modal-lg" id="editItemModal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">تصحیح جمع و قید</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="table-responsive-lg">
                        <table class="table">
                            <tr>

                                تکت توزیع / م۷
                                <th class="px-1">
                                    <div class="input-group">

                                        <input type="text" v-model="allotments.details" class="form-control big-input"
                                            placeholder=" تکت توزیع / م۷">

                                    </div>
                                </th>
                                <label style="margin-right: 35rem;">تاریخ</label>
                                <th class="px-1">
                                    <div>
                                        <date-picker v-model="allotments.allotment_date" locale="fa"
                                            format="jYYYY-jMM-jDD" required></date-picker>

                                    </div>
                                </th>

                                <th class="" style="line-height:3" width="75">

                                    <div>
                                        <button @click="updateItem()"
                                            class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill"
                                            title="Edit">
                                            <i class="la la-edit"></i>
                                        </button>
                                    </div>
                                </th>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- Edit Item Edit --}}
    </div>







    {{-- Start of the Modal  --}}
    <!-- Modal -->
    <div class="modal fade" id="upload_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">اپلود نمودن فایل</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('employee_attachment.attach_file_and_send') }}" method="POST"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group m-form__group col-lg-12">
                            <div></div>
                            <div class="custom-file">
                                <input type="hidden" name='employee_type' value="{{ $type }}">
                                <input type="file" class="custom-file-input" id="image" multiple name="image"
                                    ref="file">
                                <label class="custom-file-label" for="customFile">انتخاب فایل</label>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="id" value="">
                    <div class="modal-footer">
                        <div class="row">

                            <div class="col-md-6">
                                <div class="btn-group m-btn-group" role="group" aria-label="...">
                                    <button type="submit" class="btn btn-brand" id="upload_modal_submit_button">ثبت <i
                                            class="fas fa-plus"></i></button>
                                    <button type="button" class="btn btn btn-info" data-dismiss="modal">لغو</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- End of the Modal  --}}
@endsection
@push('custom-css')
    <style>
        label.m-checkbox {
            display: inline;
            margin: 1.5em;
        }

        .m-invoice-2 .m-invoice__wrapper .m-invoice__body table tbody tr:first-child td {
            padding-top: 10px;
            padding-bottom: 10px;
        }

        .m-timeline-2:before {
            right: 5.89rem;
        }

        .m-timeline-2 .m-timeline-2__items .m-timeline-2__item .m-timeline-2__item-cricle {
            right: 5.1rem;
        }

        @media (min-width: 992px) {
            .modal-lg {
                max-width: 70%;
            }
        }


        .m-invoice-2 .m-invoice__wrapper .m-invoice__body table thead tr th:not(:first-child) {
            text-align: right;
        }

        .m-invoice__logo.pb-2 {
            padding-top: 0 !important;
        }

        .table_border td {
            border: 1px solid #000;
        }

        .table_header th {
            border: 1px solid #000 !important;
            color: #000 !important;
        }

        @media (min-width: 992px) {

            .modal-lg {
                max-width: 90%;
            }
        }

        tr.hidden {
            display: none;
        }

        .search_box {
            padding: 8px;
            width: 280px;
            border: 2px solid #ccc;
            border-radius: 15px;
        }

        input.search_box :focus {
            padding: 8px;
            width: 280px;
            border: 2px solid #ccc !important;
            border-radius: 15px;
        }

        input:focus {
            outline-offset: -22px;
        }

        :focus {
            outline: -webkit-focus-ring-color auto 1px;
            outline-color: -webkit-focus-ring-color;
            outline-style: auto;
            outline-width: 1px;
            border-radius: 15px;
        }

        .icon-sm {
            font-size: 1.2rem !important;
            right: -0.7rem !important;
        }
    </style>
@endpush
@push('custom-js')
    <script src="{{ asset('public/js/moment.js') }}"></script>
    <script src="{{ asset('public/js/moment-jalali.js') }}"></script>
    <script src="{{ asset('public/js/vue-persian-datetime-picker-browser.js') }}"></script>
    <script>
        Vue.component('v-select', VueSelect.VueSelect)
        employee_allotments = new Vue({
            el: '#employee_allotments',
            data: {
                allotment_history: [],
                item_details_specifications: [],
                employees: [],
                // dipo:{'id':'0','name_dr':'اعاده به دیپو'},
                current_employee_id: '{{ $employee->id }}',
                current_item_details_id: 0,
                return_list: [],
                return_list_string: [],
                number: '',
                date: '',
                description: '',
                success: false,
                error: false,
                errors: '',
                receiver_employee_error: false,
                employee_list: '',
                show_form: false,
                receiver_id: null,
                return_type: '',
                model_title: '',
                alloted_item_amount: 1,
                return_error: false,
                active_allotment: [],
                specifications: {},
                allotments: {},
                extra_specifications: '',
                merge_items: {},
                show_merge_items_button: false
            },
            components: {
                DatePicker: VuePersianDatetimePicker
            },
            computed: {
                getEmployeesByIndex() {
                    return (outerIndex) => {
                        return this.employees[outerIndex] || [];
                    };
                },
            },

            methods: {
                //Add/Remove Item to/From merge_items list
                addItemsToMergeItemsList(checkbox_status, item_id, item_details_specifications_id) {
                    
                    if (checkbox_status) {
                        // Add item to merge_items
                        this.merge_items[item_details_specifications_id] ?
                            this.merge_items[item_details_specifications_id].push(item_id) :
                            this.$set(this.merge_items, item_details_specifications_id, [item_id]);
                    } else {
                        // Remove item from merge_items
                        if (this.merge_items[item_details_specifications_id]) {
                            const index = this.merge_items[item_details_specifications_id].indexOf(item_id);
                            if (index > -1) {
                                this.merge_items[item_details_specifications_id].splice(index, 1);
                                if (this.merge_items[item_details_specifications_id].length === 0) {
                                    this.$delete(this.merge_items, item_details_specifications_id);
                                }
                            }
                        }
                    }
                    // Check if there are multiple items with the same item_details_specifications_id to show the merge button or not
                    this.show_merge_items_button = (this.merge_items[item_details_specifications_id] &&
                            this.merge_items[item_details_specifications_id].length > 1) ?
                        true : false;
                },
                //send merge request to server
                mergeItems() {
                    // push employee_id and employee_type to the merge_items

                    //send merge_items to server
                    axios.post(
                            "{{ route('merge_duplicate_items', ['employee_id' => $employee->id, 'employee_type' => count($employee->allotments) > 0 ? $employee->allotments[0]->employee_type : null]) }}", {
                                ...this.merge_items
                            })
                        .then(res => {

                            this.success = true;
                            window.reload();
                        })
                        .catch(err => {
                            console.log(err);
                            this.error = true;
                            this.errors = err.response.data.errors;
                        })
                },
                searchEmp(index) {
                    var name = 'name' + index;
                    var father_name = 'father_name' + index;
                    var emp_name = this.$refs[name][0].value;
                    var emp_father_name = this.$refs[father_name][0].value;
                    var employee_type = this.$refs['employee_type' + index].find(input => input.checked)
                        .value;


                    axios.get("{{ url('get_employees_by_name_father') }}/" + employee_type + "/" +
                            emp_name +
                            "/" + emp_father_name)
                        .then(res => {
                            this.$set(this.employees, index, res.data);
                        })
                        .catch(err => {
                            console.log(err);

                        })
                },
                addSelectedEmployeeToList(index, receiver, item_details_specifications_id) {

                    this.$refs['selectedEmpDetails' + index][0].innerHTML = receiver.name_dr + ' ولد ' +
                        receiver
                        .father_name_dr + ' وظیفه ' + receiver.current_position_dr;

                    var object = {};
                    object['item_details_specifications_id'] = item_details_specifications_id;
                    object['receiver_employee_id'] = receiver.id;
                    object['current_employee_id'] = this.current_employee_id;
                    //if allot to stock status will be 1 else status well be 0 and allote to employee
                    object['status'] = this.receiver_id == 0 ? 1 : 0;
                    object['return_type'] = this.return_type;
                    this.return_list[index] = object;



                    this.return_list_string = JSON.stringify(this.return_list[index]);

                    this.$set(this.employees, index, []);
                },
                showItemAllotmentHistory(item_details_specifications_id, item_balances_id = null) {

                    this.model_title = 'تاریخچه جمع و قید جنس';
                    axios.get(
                            "{{ url('allotments/get_item_details_specifications_allotment_history') }}/" +
                            item_details_specifications_id + '/' + item_balances_id)
                        .then(res => {
                            this.allotment_history = res.data;
                        })
                        .catch(err => {
                            console.log(err);
                        });

                },
                validate(alloted_item, item_amount) {
                    if (alloted_item > item_amount) {
                        this.return_error = true;
                    } else {
                        this.return_error = false;
                    }
                },
                getItemDetailsSpecifications(evt, index, item_details_specifications_id, item_amount) {
                    this.model_title = 'اعاده جنس';
                    //if check
                    if (evt) {
                        axios.get("{{ url('allotments/get_item_details_specifications') }}/" +
                                item_details_specifications_id)
                            .then(res => {
                                res.data.deleted_by = item_amount;
                                this.item_details_specifications.push(res.data);
                            })
                            .catch(err => {
                                console.log(err);
                            });
                    } else {
                        var rem = this.item_details_specifications.filter(item => {
                            return item.id == item_details_specifications_id
                        })[0];

                        remove_id = this.item_details_specifications.indexOf(rem);
                        this.item_details_specifications.splice(remove_id, 1);
                    }

                },
                addItemDetailsSpecificationsToReturnList(index, item_details_specifications_id) {
                    var object = {};
                    object['item_details_specifications_id'] = item_details_specifications_id;
                    object['receiver_employee_id'] = this.receiver_id;
                    object['current_employee_id'] = this.current_employee_id;
                    //if allot to stock status will be 1 else status well be 0 and allote to employee
                    object['status'] = this.receiver_id == 0 ? 1 : 0;
                    object['return_type'] = this.return_type;
                    this.return_list[index] = object;



                    this.return_list_string = JSON.stringify(this.return_list[index]);
                },
                checkType(index, item_details_specifications_id) {
                    if (this.return_list.return_type != '')
                        this.show_form = true;
                    if (this.return_type != 2) {
                        this.item_details_specifications.map((item, index) => {
                            this.addItemDetailsSpecificationsToReturnList(index, item.id);
                        })
                    }
                },
                // getAllEmployees() {
                //     axios.get("{{ url('get_all_employees') }}")
                //         .then(res => {
                //             this.employees = res.data;
                //         })
                //         .catch(err => {
                //             console.log(err);

                //         })

                // },
                submitForm: function() {


                    if (this.validateReturn()) {
                        axios.post("{{ route('item_return_requests.store') }}", {
                                'number': this.number,
                                'date': this.$refs.date["value"],
                                'description': this.description,
                                'return_list': this.return_list,
                                'status': '0',
                                'employee_id': this.current_employee_id,
                                'item_amount': this.alloted_item_amount,
                            })
                            .then(res => {
                                this.success = true;
                                window.location.href = "{{ url('item_return_requests') }}" + '/' + res
                                    .data;
                            })
                            .catch(err => {
                                this.error = true;
                                this.errors = err.response.data.errors;
                            })
                    } else {
                        this.receiver_employee_error = true;

                    }
                },

                validateReturn() {


                    if (this.return_type == 2) {
                        if (this.return_list.length == this.item_details_specifications.length) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                    return true;


                },
                printAllotments: function() {
                    $('.details').css('display', 'block');
                    $('.m-invoice-2 .m-invoice__wrapper .m-invoice__body table tbody tr td').css('color',
                        '#000');
                    $('.m-portlet .m-portlet__body').css('color', '#000');
                    $('table').removeClass('table-bordered');
                    $('table').addClass('table_border');
                    $('table').addClass('table_header');
                    $('.un').css('display', 'none');
                    $('.search_box').css('display', 'none');
                    $('.m-invoice-2 .m-invoice__wrapper .m-invoice__body table tbody tr td').css(
                        'text-align',
                        'center !important');
                    window.print();
                    $('.un').css('display', 'block');
                    $('.search_box').css('display', 'block');
                    $('.details').css('display', 'none');
                    $('.m-invoice-2 .m-invoice__wrapper .m-invoice__body table tbody tr td').css('color',
                        '#6f727d');
                    $('table').removeClass('table_border');
                    $('table').removeClass('table_header');
                    $('table').addClass('table-bordered');
                    $('.m-portlet .m-portlet__body').css('color', '#575962');


                },
                getSelect2: function() {
                    // $(".select2").select2();
                },

                editItemAllotment(item_details_specifications_id, item_balances_id = null) {
                    axios.get("{{ url('allotments/get_item_details_specifications_allotment_active') }}/" +
                            item_details_specifications_id + '/' + item_balances_id)
                        .then(res => {
                            this.active_allotment = res.data;
                            this.allotment_history = [];

                        })
                        .catch(err => {
                            console.log(err);
                        });

                },
                editItemDetailsSpecifications(id) {

                    axios.get("{{ url('edit_item_details_specifications') }}/" + id)
                        .then(res => {

                            this.specifications = res.data.item_details_specification;
                            this.extra_specifications = res.data.sub_category_keys;

                        })
                        .catch(err => {

                        });
                },
                editItem(id) {
                    axios.get("{{ url('edit_item') }}/" + id)
                        .then(res => {
                            this.allotments = res.data.allotments;

                        })
                        .catch(err => {

                        });
                },
                updateItem() {
                    id = this.allotments.id;
                    axios.put("{{ url('update_item') }}/" + id, {
                            'allotments': this.allotments
                        })
                        .then(res => {
                            location.reload();

                        })
                        .catch(err => {
                            console.log(err);
                        })
                },

                roleBackAllotement(allotment_id, item_details_specifications_id) {
                    axios.get("{{ url('allotments/role_back_allotment') }}/" + allotment_id + "/" +
                            item_details_specifications_id)
                        .then(res => {
                            location.reload();
                        })
                        .catch(err => {
                            console.log(err);
                        });

                },
                checkAll: function(event, employee_id) {
                    this.model_title = 'اعاده جنس';
                    if (event.target.checked) {
                        $('input.checkbox:not(input.checkbox:disabled)').prop('checked', true);

                        axios.get("{{ url('allotments/get_all_item_details_specifications') }}/" +
                                employee_id)
                            .then(res => {
                                for ($i = 0; $i < res.data.length; $i++) {
                                    this.item_details_specifications.push(res.data[$i][
                                        'item_details_specifications'
                                    ]);
                                }
                            })
                            .catch(err => {
                                console.log(err);
                            });
                    } else {
                        this.item_details_specifications = [];
                        $('input.checkbox:not(input.checkbox:disabled)').prop('checked', false);
                    }
                    var len = $('input[type="checkbox"]:checked').length - 1;


                },
                // onSearch(search, loading) {
                //     loading(true);
                //     this.search(loading, search, this);
                // },
                // search: _.debounce((loading, search, vm) => {

                //     axios.post(`{{ route('get_employee_axios') }}`, {
                //             'name': search
                //         })
                //         .then(res => {
                //             console.log(res.data.length);
                //             if (res.data.length != 0)
                //                 vm.employees = res.data.items;
                //             // else{
                //             //     vm.employees=[];
                //             //     vm.employees.push(vm.dipo);    
                //             // }
                //             loading(false);
                //         })
                //         .catch(err => {
                //             console.log(err);
                //         });

                // }, 350)

            },

            mounted() {
                // this.getAllEmployees();
            },


        });

        $('#allotment_history').on('hidden.bs.modal', function() {
            employee_allotments.allotment_history = '';
        });

        $('[data-search]').on('keyup', function() {
            var searchVal = $(this).val();
            var filterItems = $('[data-filter-item]');

            if (searchVal != '') {
                filterItems.addClass('hidden');
                $('[data-filter-item][data-filter-name*="' + searchVal.toLowerCase() + '"]').removeClass(
                    'hidden');
            } else {
                filterItems.removeClass('hidden');
            }
        });
        $('[data-search_details]').on('keyup', function() {
            var searchVal = $(this).val();
            var filterItems = $('[ data-filter-item-details]');

            if (searchVal != '') {
                filterItems.addClass('hidden');
                $('[data-filter-item][data-filter-details*="' + searchVal.toLowerCase() + '"]').removeClass(
                    'hidden');
            } else {
                filterItems.removeClass('hidden');
            }
        });

        function openEditModal(itemId, title) {
            $('input[name=id]').val(itemId);
            $('#upload_modal .modal-title').text(title);
            $('#upload_modal_submit_button').text('تصحیح');
            $('#upload_modal').modal('show');
        }
    </script>
@endpush
