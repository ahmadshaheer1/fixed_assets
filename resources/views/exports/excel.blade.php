<table id="printable" class="table table-striped table-bordered jquery-datatable" style="width:100%" dir="rtl" border="1">
    <thead>
        <tr style="background:#ccc; font-size:18px; font-weight:bold;">
            <th>#</th>
            <th>استفاده کننده</th>
            <th>ولد</th>
            <th>مشخصات جنس</th>
            <th>کمپنی</th>
            <th>سریال نمبر</th>
            <th>تعداد</th>
            <th>واحد</th>
            <th>فیات</th>
            <th>اداره</th>
            <th>جمله قیمت</th>
            <th>تاریخ توزیع</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($reports as $item)
            <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$item->itemDetailsSpecifications->motamed->employee['name_dr']}}</td>
                <td>{{$item->itemDetailsSpecifications->motamed->employee['father_name_dr']}}</td>
                <td>{{$item->itemDetailsSpecifications->itemDetails['name_dr']}}</td>
                <td>{{$item->itemDetailsSpecifications->itemDetails->vendor['name_dr']}}</td>
                <td>{{$item->itemDetailsSpecifications['col1']}} || {{$item->itemDetailsSpecifications['col2']}} || {{$item->itemDetailsSpecifications['col3']}} || {{$item->itemDetailsSpecifications['col4']}} ||
                    {{$item->itemDetailsSpecifications['col5']}} || {{$item->itemDetailsSpecifications['col6']}} || {{$item->itemDetailsSpecifications['col7']}} || {{$item->itemDetailsSpecifications['col8']}} ||
                    {{$item->itemDetailsSpecifications['col9']}} || {{$item->itemDetailsSpecifications['col10']}} || {{$item->itemDetailsSpecifications['col11']}} || {{$item->itemDetailsSpecifications['col12']}}
                </td>
                <td>1</td>
                <td>{{$item->itemDetailsSpecifications['itemDetails']['unit']['name_dr']}}</td>
                <td>{{$item->itemDetailsSpecifications['price']}} || {{$item->itemDetailsSpecifications['currency']==0 ? 'افغانی' : 'دالر'}}</td>
                <td>{{$item->itemDetailsSpecifications->motamed->employee['current_position_dr']}}</td>
                <td>{{$item->itemDetailsSpecifications['price']}} || {{$item->itemDetailsSpecifications['currency']==0 ? 'افغانی' : 'دالر'}}</td>
                <td>{{$item->itemDetailsSpecifications->motamed->allotment_date}}</td>
            </tr>
        @endforeach
    
</table>