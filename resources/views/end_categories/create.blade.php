  <div  id="create_end_categories" >
    <div v-if="success" class="m-alert m-alert--icon m-alert--outline alert alert-success alert-dismissible fade show" role="alert">
        <div class="m-alert__icon">
            <i class="la la-check"></i>
        </div>
        <div class="m-alert__text">
            @{{message}}
        </div>
        <div class="m-alert__close">
            <button style="line-height: 3;" type="button" class="close" data-dismiss="alert" aria-label="Close">
            </button>
        </div>
    </div>
    <form class="m-form m-form--fit m-form--label-align-right end_category_form" @submit.prevent="createEndCategory">
        @csrf
        <div class="m-portlet__body">
          <div v-if="error" class="form-group m-form__group has-danger">
                <div  class="form-control-feedback ">@{{error_messages.sub_category_message}}</div>
        </div>
          <div class="form-group m-form__group has-danger" id="sub_category_id">
            <div class="form-control-feedback has-danger"></div>
          </div>
            <div :class="['form-group m-form__group',error_messages.{{$name}} ? 'has-danger' : '']" id="name_dr">
                <label for="main_category_name">{{trans('global.name')}}</label>
                <input name="{{$name}}"type="text" v-model="{{$name}}" class="form-control m-input m-input--square" id="main_category_name" aria-describedby="emailHelp" placeholder="{{trans('global.name')}}">
                    <div class="form-control-feedback">@{{error_messages.{!!$name!!}}}</div>
            </div>
            <div class="form-group m-form__group">
                <label for="main_category_description">{{trans('global.description')}}</label>
                <textarea name="description" v-model="description" class="form-control" id="main_category_description"placeholder="{{trans('global.description')}}">{{old('description')}}</textarea>
            </div>
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">
            <div class="m-form__actions">
                <button type="submit" class="btn btn-primary">{{trans('global.submit')}}</button>
                <button type="reset" class="btn btn-danger">{{trans('global.cancel')}}</button>
            </div>
        </div>
    </form>
  </div>
<script>

        end_category_app = new Vue({
            el: '#create_end_categories',
            data: {
              sub_category_id: {!! json_encode($data['sub_category_id'],1)!!},
              {{$name}}: '',
              description: '',
              error: false,
              error_messages: [],
              success: false,
            },
            methods :{
              createEndCategory(event){
                if(this.error == false){
                  axios.post("{{route('end_categories.store')}}", {
                    'sub_category_id': this.sub_category_id,
                    'description': this.description,
                    "{{$name}}": this.{{$name}},
                  })
                  .then(res => {
                    this.success = true;
                    this.message = res.data.success;
                    $('#exampleModal').modal('hide')
                  })
                  .catch(err => {
                    this.error = true;
                    this.error_messages = err.response.data.errors;
                  })
                }

              }

            },
            beforeMount(){
                if(typeof this.sub_category_id !='number'){
                  this.error=true;
                  this.error_messages.sub_category_message='کتگوری فرعی باید از فورم قبلی انتخاب گردد!!!';
                }
            }
        });
</script>
