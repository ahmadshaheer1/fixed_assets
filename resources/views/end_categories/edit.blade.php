<div id="edit_‌‌end_category">
    <div v-if="success" class="m-alert m-alert--icon m-alert--outline alert alert-success alert-dismissible fade show" role="alert">
        <div class="m-alert__icon">
            <i class="la la-check"></i>
        </div>
        <div class="m-alert__text">
            @{{message}}
        </div>
        <div class="m-alert__close">
            <button style="line-height: 3;" type="button" class="close" data-dismiss="alert" aria-label="Close">
            </button>
        </div>
    </div>
    <form class="m-form m-form--fit m-form--label-align-right" method="POST"  @submit.prevent="editEndCategory">
        @csrf
        @method('PUT')
        <div class="m-portlet__body">
            <div :class="['form-group m-form__group', error_messages.name_dr ? 'has-danger': '']">
                <label for="main_category_name">{{trans('global.name')}}</label>
                <input name="{{$name}}" value="" v-model="end_category" type="text" class="form-control m-input m-input--square" id="main_category_name" aria-describedby="emailHelp" placeholder="{{trans('global.name')}}">
                <div class="form-control-feedback">@{{error_messages.name_dr}}</div>
            </div>
            <div class="form-group m-form__group">
                <label for="main_category_description">{{trans('global.description')}}</label>
                <textarea name="description" class="form-control" id="main_category_description"placeholder="{{trans('global.description')}}" v-model="description"></textarea>
            </div>
            <div :class="['form-group m-form__group', error_messages.sub_category_id ? 'has-danger': '']">
                <label for="exampleSelect1">{{trans('global.sub_category')}}</label>
                <select class="form-control m-input" name="sub_category_id" id="sub_category_id" v-model="sub_category_id">
                    <option value="">انتخاب کتگوری فرعی</option>

                        <option v-for=" sub_category in sub_categories" :value="sub_category.id">@{{sub_category.name_dr}}</option>

                </select>
                <div class="form-control-feedback">@{{error_messages.sub_category_id}}</div>
            </div>
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">
            <div class="m-form__actions">
                <button type="submit" class="btn btn-primary">{{trans('global.submit')}}</button>
                <button type="reset" class="btn btn-danger">{{trans('global.cancel')}}</button>
            </div>
        </div>
    </form>
</div>
<script>
        edit_‌‌end_category=new Vue({
             el: "#edit_‌‌end_category",
             data: {
               category_id: {{$endCategory->id}},
               sub_category_id: '{{$endCategory->sub_category_id}}',
               sub_categories: JSON.parse('{!!json_encode($sub_categories, 1)!!}'),
               end_category: '{{$endCategory->$name}}',
               description: '{{$endCategory->description}}',
               error: false,
               error_messages: [],
               success: false,

             },
             methods: {
              editEndCategory(event){

                 axios.put("{{url('end_categories')}}/"+this.category_id, {
                   "{{$name}}": this.end_category,
                   'sub_category_id': this.sub_category_id,
                   'description': this.description,

                 })
                 .then(res => {
                   this.success = true;
                   this.message = res.data.success;
                   $('#exampleModal').modal('hide')
                 })
                 .catch(err => {
                   console.log(err.data);
                   this.error = true;
                   this.error_messages = err.response.data.errors;
                 })
               }


             }
           });
</script>
