@extends('layouts.master')
@section('title', trans('global.edit_external_employee'))
@section('content')

<form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{route('external_employee.update', encrypt($external_employee->id))}}">
    @csrf
    @method('PUT')
    <div class="m-portlet__body">
        <div class="form-group m-form__group {{$errors->has('name_dr') ? 'has-danger' : ''}}">
            <label for="department_name">{{trans('global.name')}}</label>
            <input name="name_dr" value="{{ $external_employee->name_dr }}" type="text" class="form-control m-input m-input--square" id="department_name" aria-describedby="emailHelp" placeholder="{{trans('global.name')}}">
            @if($errors->has('name_dr'))
                <div class="form-control-feedback">{{$errors->first('name_dr')}}</div>
            @endif
        </div>
        <div class="form-group m-form__group {{$errors->has('father_name_dr') ? 'has-danger' : ''}}">
            <label for="department_name">{{trans('global.father_name')}}</label>
            <input name="father_name_dr" value="{{ $external_employee->father_name_dr }}"  type="text" class="form-control m-input m-input--square" id="department_name" aria-describedby="emailHelp" placeholder="{{trans('global.father_name')}}">
            @if($errors->has('father_name_dr'))
                <div class="form-control-feedback">{{$errors->first('father_name_dr')}}</div>
            @endif
        </div>
        <div class="form-group m-form__group {{$errors->has('current_position_dr') ? 'has-danger' : ''}}">
            <label for="department_name">{{trans('global.designation')}}</label>
            <input name="current_position_dr" value="{{ $external_employee->current_position_dr }}"  type="text" class="form-control m-input m-input--square" id="department_name" aria-describedby="emailHelp" placeholder="{{trans('global.designation')}}">
            @if($errors->has('current_position_dr'))
                <div class="form-control-feedback">{{$errors->first('current_position_dr')}}</div>
            @endif
        </div>
    </div>
    <div class="form-group m-form__group {{$errors->has('external_department_id') ? 'has-danger' : ''}}">
        <label for="employee_id">اداره بیرونی</label>
        <select class="form-control select2 m-input m-input--square" name="external_department_id">
            <option value="">--انتخاب--</option>
            @foreach ($external_departments as $value)
                <option {{$external_employee->external_department_id == $value->id ? 'selected' : ''}} value="{{$value->id}}">نام دری&nbsp;&nbsp;({{$value->name_dr}})&nbsp;&nbsp;&nbsp;&nbsp; نام پشتو&nbsp;&nbsp; ({{$value->name_pa}}) </option>
            @endforeach
        </select>
        @if($errors->has('external_department_id'))
            <div class="form-control-feedback">{{$errors->first('external_department_id')}}</div>
        @endif
    </div>
</div>
    <div class="m-portlet__foot m-portlet__foot--fit">
        <div class="m-form__actions">
            <button type="submit" class="btn btn-primary">{{trans('global.submit')}}</button>
            <button type="reset" class="btn btn-danger">{{trans('global.cancel')}}</button>
        </div>
    </div>
</form>
@endsection
