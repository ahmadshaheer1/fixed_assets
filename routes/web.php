<?php

use Illuminate\Validation\Factory;
use App\ItemDetails;
use App\User;
use App\Employee;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ItemDetailsSpecifications;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['auth']], function () {

    Route::get('/', 'HomeController@index')->name('home');

    // Route::group(['prefix' => 'fece9', 'as' => 'fece9s.'], function() {
    //     Route::get('/', 'Fece9Controller@index')->name('index');
    //
    // });
    Route::group(['prefix' => 'fece9s', 'as' => 'fece9s.'], function () {
        Route::post('attach_file_and_send', 'Fece9Controller@attachFileAndSend')->name('attach_file_and_send');

        Route::get('received_create', 'Fece9Controller@receivedIndex')->name('received_create');

        Route::get('received_show/{id}', 'Fece9Controller@receivedShow')->name('received_show');


        Route::get('in_progress', 'Fece9Controller@inProgress')->name('in_progress');
        Route::get('inspection_approval', 'Fece9Controller@inspectionApproval')->name('inspection_approval');

        Route::post('replace_attached_file', 'Fece9Controller@replaceAttachedFile')->name('replace_attached_file');
        
        Route::get('insepection_show/{id}', 'Fece9Controller@insepectionShow')->name('insepection_show');
        Route::post('insepection_approved', 'Fece9Controller@insepectionApproved')->name('insepection_approved');
        Route::post('inspenction_reject_fece9', 'Fece9Controller@inspectionRejectFece9')->name('inspenction_reject_fece9');
        Route::post('inspection_update', 'Fece9Controller@inspectionUpdate')->name('inspection_update');

        Route::get('completed', 'Fece9Controller@completedFece9s')->name('completed');
        Route::get('completed_show/{id}', 'Fece9Controller@completedShow')->name('completed_show');




        Route::post('item_details_status', 'Fece9Controller@Fece9ItemDetailsStatus')->name('item_details_status');
    });
    Route::get('fece9_print/{id}','Fece9Controller@printFece9')->name('fece9_print');
    Route::resource('fece9s', 'Fece9Controller');
    Route::resource('fece2s', 'Fece2Controller');
    Route::get('fece2_conformation/{id?}/{status?}', 'Fece2Controller@fece2Confirmation')->name('fece2_conformation');
    Route::post('fece2_attach_file_and_send', 'Fece2Controller@attachFileAndSend')->name('fece2_attach_file_and_send');
    Route::post('reject_fece2/{id?}', 'Fece2Controller@rejectFece2')->name('reject_fece2');
    Route::post('fece2_replace_attached_file', 'Fece2Controller@replaceAttachedFile')->name('fece2_replace_attached_file');
    Route::get('wait_meem7', 'Fece2Controller@WaitMeem7')->name('fece2s.wait_meem7');
    Route::get('confirmed', 'Fece2Controller@Fece2Confirmed')->name('fece2s.confirmed');
    Route::post('fece2_meem7_attach_file', 'Fece2Controller@attachFileMeem7')->name('fece2_meem7_attach_file');
    Route::get('get_fece9_details/{id}', 'Fece9Controller@getFece9Details')->name('fece9s.get_fece9');

    Route::get('meem7/get_meem7_item_details_specifications/{meem7_id}', 'Meem7Controller@getMeem7ItemDetailsSpecifications')->name('meem7.get_meem7_item_details_specifications');
    Route::get('meem7/get_meem7_extra_specifications/{meem7_id}', 'Meem7Controller@getMeem7ExtraSpecifications')->name('meem7.get_meem7_item_details_specifications');
    Route::get('create_meem7/{fece9_id}', 'Meem7Controller@createMeem7')->name('meem7.create_meem7');
    Route::get('create_meem7_ghair_zakhirawi/{department_id}/{month}/{fece9_type}', 'Meem7Controller@createMeem7GhairZakhirawi')->name('meem7.create_meem7_ghair_zakhirawi');
    Route::get('meem7_completed', 'Meem7Controller@completed')->name('meem7.completed');
    // Route::get('meem7_show/meem7_id', 'Meem7Controller@meem7Show')->name('meem7_show');
    Route::resource('meem7', 'Meem7Controller');


    Route::resource('fece5s', 'Fece5Controller');
    Route::resource('motamed', 'MotamedController');
    Route::resource('motamed_type', 'MotamedTypeController');
    Route::get('get_motamed_type_allotments_ajax', 'MotamedTypeController@getMotamedTypeAjax')->name('get_motamed_type_allotments_ajax');
    Route::get('get_motameds', 'MotamedController@getMotameds')->name('get_motameds');
    Route::post('get_motamed_type_search', 'MotamedTypeController@getMotamedTypeSearch')->name('get_motamed_type_search');
    

    Route::post('allotments', 'AllotmentController@store')->name('allotments');
    Route::get('allotments/get_employees_allotments', 'AllotmentController@getEmployeesAllotments')->name('allotments.get_employees_allotments');
    Route::get('allotments/motamed_allotments', 'AllotmentController@getMotamedAllotments')->name('allotments.motamed_allotments');
    Route::get('allotments/get_employees_allotments_ajax', 'AllotmentController@getEmployeesAllotmentsAjax')->name('allotments.get_employees_allotments_ajax');
    Route::get('allotments/get_motamed_allotments_ajax', 'AllotmentController@getMotamedAllotmentsAjax')->name('allotments.get_motamed_allotments_ajax');
    Route::post('allotments/get_employees_allotments_search/{type?}', 'AllotmentController@getEmployeesAllotmentsSearch')->name('allotments.get_employees_allotments_search');
    Route::get('allotments/get_employee_allotments/{id}/{type}/{status?}', 'AllotmentController@getEmployeeAllotments')->name('allotments.get_employee_allotments');
    Route::get('allotments/get_motamed_allotments/{id}/{status?}', 'MotamedController@getMotamedAllotments')->name('allotments.get_motamed_allotments');
    Route::get('allotments/get_item_details_specifications_allotment_history/{item_details_specifications_id}/{item_balances_id?}', 'AllotmentController@getItemDetailsSpecificationsAllotmentHistory')->name('allotments.get_item_details_specifications_allotment_history');
    Route::get('allotments/get_item_details_specifications_allotment_active/{item_details_specifications_id}/{item_balances_id?}', 'AllotmentController@getItemDetailsSpecificationsAllotmentActive')->name('allotments.get_item_details_specifications_allotment_active');
    Route::get('allotments/role_back_allotment/{allotment_id}/{item_details_specifications_id}', 'AllotmentController@roleBackAllotement')->name('allotments.role_back_allotment');
    Route::get('allotments/get_item_details_specifications/{item_details_specifications_id}', 'AllotmentController@getItemDetailsSpecifications')->name('allotments.get_item_details_specifications');
    Route::get('allotments/get_all_item_details_specifications/{employee_id}', 'AllotmentController@getAllItemDetailsSpecifications')->name('allotments.get_all_item_details_specifications');
    Route::post('allotment/merge_duplicate_items/{employee_id}/{employee_type}','AllotmentController@mergeDuplicateItems')->name('merge_duplicate_items');
    // Route::resource('external_employee', 'ExternalEmployeeController');
    Route::prefix('allotments')->group(function () {
        Route::resource('external_employee', 'ExternalEmployeeController');
    });
    Route::get('get_external_employee_ajax', 'ExternalEmployeeController@getExternalEmployeeAjax')->name('external_employee.get_external_employee_ajax');
    Route::post('get_external_employees_search', 'ExternalEmployeeController@getExternalEmployeesSearch')->name('external_employee.get_external_employees_search');
    Route::get('get_external_employee/{id}', 'ExternalEmployeeController@getExternalEmployee')->name('get_external_employee');
    Route::post('get_motamed', 'AllotmentController@getMotamed')->name('get_motamed');

    Route::resource('item_return_requests', 'ItemReturnRequesdController');
    Route::get('item_return_requests_completed','ItemReturnRequesdController@completed')->name('item_return_requests.completed');
    Route::get('item_return_requests_inprogress','ItemReturnRequesdController@inprogress')->name('item_return_requests.inprogress');
    Route::post('item_return_requests/conformation/{id}', 'ItemReturnRequesdController@itemReturnConformation')->name('item_return_requests.conformation');
    Route::post('attach_file_requested_item', 'ItemReturnRequesdController@attachFileAndSend')->name('attach_file_requested_item');
    Route::post('replace_return_requests_file', 'ItemReturnRequesdController@replaceAttachedFile')->name('replace_return_requests_file');


    Route::group(['prefix' => 'fece5s', 'as' => 'fece5s.'], function () {
        Route::post('attach_file_and_send', 'Fece5Controller@attachFileAndSend')->name('attach_file_and_send');
        Route::post('replace_attached_file', 'Fece5Controller@replaceAttachedFile')->name('replace_attached_file');
        Route::get('conformation/{id?}/{status?}', 'Fece5Controller@fece5Conformation')->name('conformation');
        Route::post('reject_fece5/{id?}', 'Fece5Controller@rejectFece5')->name('reject_fece5');
    });

    Route::post('export_excel','ReportController@exportExcel')->name('export_excel');

    // Route::group(['prefix' => 'items', 'as' => 'items.'], function() {
    //     Route::get('/', 'ItemDetailsController@index')->name('index');
    //
    // });
    // Route::resource('end_items_details', 'ItemDetailsController');

    // Settings

    // Unit
    Route::resource('units', 'UnitController');
    Route::resource('contractor_company', 'ContractorCompanyController');
    Route::resource('contracts', 'ContractController');
    Route::get('contracts_show_item/{id?}', 'ContractController@contractsShowItem')->name('contracts.show_item');
    Route::resource('contract_type', 'ContractTypeController');
    Route::resource('contract_items', 'ContractItemsController');

    // Department
    Route::resource('departments', 'DepartmentController');
    Route::get('get_departments', 'DepartmentController@getDepartment')->name('get_departments');
    Route::resource('external_departments', 'ExternalDepartmentController');
    

    // Sub Categories Keys
    Route::get('sub_categories_keys/create/{sub_categories_id?}', 'SubCategoriesKeyController@create')->name('sub_categories_keys.create');
    Route::resource('sub_categories_keys', 'SubCategoriesKeyController')->except('create');
    Route::get('get_sub_categories_keys/{sub_category_id?}/{data?}', 'SubCategoriesKeyController@getSubCategoriesKeys')->name('sub_categories_keys.get_sub_categories_keys');
    Route::get('get_sub_categories_select_options/{sub_category_id?}/{data?}', 'SubCategoriesKeyController@getSubCategoriesSelectOptions')->name('sub_categories_keys.get_sub_categories_select_options');
    Route::get('get_item_details_by_specifications/{sub_category_id?}', 'SubCategoriesKeyController@getItemDetailsBySpecifications')->name('sub_categories_keys.get_item_details_by_specifications');

    // Vendor
    Route::resource('vendors', 'VendorController');

    // Main Category
    Route::resource('main_categories', 'MainCategoryController');
    Route::post('get_sub_categories', 'MainCategoryController@getSubCategories')->name('get_sub_categories');
    Route::post('get_end_categories', 'MainCategoryController@getEndCategories')->name('get_end_categories');
    Route::resource('sub_categories', 'SubCategoryController');
    Route::resource('end_categories', 'EndCategoryController');

    Route::post('get_setting_modal_view', 'ItemDetailsController@getSettingModalView')->name('get_setting_modal_view');

    // testing the new items format
    // Route::get('item_details/create_new_items', 'ItemDetailsController@createNewItems')->name('item_details.create_new_items');
    Route::get('get_categories', 'ItemDetailsController@getCategories')->name('get_categories');
    Route::post('get_related_categories', 'ItemDetailsController@getRelatedCategories')->name('get_related_categories');
    Route::get('get_all_employees', 'EmployeeController@getAllEmployees')->name('get_all_employees');
    Route::get('get_employees_by_dep/{department_id}', 'EmployeeController@getEmployeesByDep')->name('get_all_employees');
    Route::get('get_employees_by_father/{father_name}', 'EmployeeController@getEmployeesByFather')->name('get_employees_by_father');
    Route::get('get_employees_by_name_father/{employee_type?}/{name?}/{father_name?}', 'EmployeeController@getEmployeesByNameFather')->name('get_employees_by_name_father');

    Route::resource('item_details', 'ItemDetailsController');
    Route::get('get_item_details', 'ItemDetailsController@getItemDetails')->name('get_item_details');
    Route::get('get_item_details_ajax', 'ItemDetailsController@getItemDetailsAjax')->name('get_item_details_ajax');
    Route::post('item_details_search', 'ItemDetailsController@itemDetailsSearch')->name('item_details_search');
    Route::get('get_item_details_consumable', 'ItemDetailsController@getItemDetailsConsumable')->name('get_item_details_consumable');

    // check if all requested items alloted based on item_details_id
    Route::get('fece9_all_items_alloted/{fece9_id}/{item_details_id}', 'ItemDetailsController@fece9AllItemsAlloted')->name('item_details.fece9_all_items_alloted');

    Route::resource('item_details_specifications', 'ItemDetailsSpecificationsController');
    Route::get('edit_item_details_specifications/{id}/', 'ItemDetailsSpecificationsController@editItemDetailsSpecifications')->name('item_details_specifications.edit_item_details_specifications');
    Route::get('get_item_details_specifications/{item_details_id}/{fece9_id?}', 'ItemDetailsSpecificationsController@getItemDetailsSpecifications')->name('item_details_specifications.get_item_details_specifications');
    Route::get('get_item_details_specifications_axios/{item_details_id}', 'ItemDetailsSpecificationsController@getItemDetailsSpecificationsAxios')->name('item_details_specifications.get_item_details_specifications_axios');
    Route::post('item_details_attach_file_and_send', 'ItemDetailsSpecificationsController@attachFileAndSend')->name('item_details_attach_file_and_send');
    Route::get('edit_item/{id}/', 'AllotmentController@editItem')->name('edit_item');
    Route::put('update_item/{id}/', 'AllotmentController@update')->name('update_item');
    Route::resource('requested_items', 'RequestedItemController');


    Route::get('requested_item_details/{id}', 'RequestedItemController@create')->name('requested_item_details');
    Route::get('requested_items_accept', 'RequestedItemController@accept')->name('requested_items_accept');
    Route::get('requested_items_reject', 'RequestedItemController@reject')->name('requested_items_reject');
    // Route::get('requested_item_details_index', 'ItemDetailsController@indexRequestedItem')->name('requested_item_details_index');

    // Items Informations
    Route::get('items_basic_informations', 'MainCategoryController@itemsBasicInformations')->name('items_basic_informations');

    //employee
    Route::post('employee/{name?}', 'EmployeeController@ajaxSearch')->name('get_employee');

    Route::post('employee_axios/{name?}', 'EmployeeController@axiosSearch')->name('get_employee_axios');
    Route::post('get_employee_father_axios/{father_name?}', 'EmployeeController@getEmployeeFatherAxios')->name('get_employee_father_axios');

    // Route::group(['prefix' => 'items_basic_informations', 'as' => 'items_basic_informations.'], function() {
    //     Route::get('/', 'MainCategoryController@index')->name('index');
    //
    // });
    Route::post('check_password','UserController@checkPassword')->name('check_password');
    Route::resource('roles', 'RoleController');
    Route::resource('users', 'UserController');
    Route::resource('permissions', 'PermissionsController');
    Route::resource('item_report', 'ReportController');
    Route::get('item_report_details', 'ReportController@itemDetailsReport')->name('item_report_details');
    //ajax serach for item details
    Route::post('select2_search_ajax/{name?}', 'ReportController@ajaxSearch')->name('select2_search_ajax');
    Route::post('item_axios/{name?}', 'ReportController@axiosSearch')->name('item_axios');
    Route::post('available_to_aloot', 'ItemDetailsSpecificationsController@availableToAloot')->name('available_to_aloot');
    Route::post('attach_file_and_send', 'EmployeeAttachmentController@attachFileAndSend')->name('employee_attachment.attach_file_and_send');
    
});
// Route::get('purchased_items', 'HomeController@purchasedItems')->name('items.purchased');



// Route::get('fece9/index', 'HomeController@fess9Index');
// Route::get('fece9/create', 'HomeController@fece9Create')->name('fece9.create');
// Route::get('fece9/show', 'HomeController@fece9Show')->name('fece9.show');
// Route::get('purchased_items', 'HomeController@purchasedItemsIndex')->name('purchased_items.index');
// Route::get('purchased_items/create', 'HomeController@purchasedItemsCreate')->name('purchased_items.create');
// //Allotment Item
// Route::get('allotment', 'HomeController@allotmentIndex')->name('allotments.index');
// Route::get('allotment/create', 'HomeController@allotmentCreate')->name('allotments.create');

// // Items Alloted to User
// Route::get('allotment/individual/alloted_to_user', 'HomeController@allotedToUser')->name('allotments.alloted_to_user');
// Route::get('allotment/individual/request_return/{type}/{inputs}', 'HomeController@requestReturn')->name('allotments.request_return');
// Route::get('allotment/individual/show_alloted_to_user', 'HomeController@showAllotedToUser')->name('allotments.show_alloted_to_user');

// // Return Items Request
// Route::get('request_return_alloted_items', 'HomeController@returnAllotedItemsIndex')->name('request_return_alloted_items.index');
// Route::get('request_return_alloted_items/{id}/show', 'HomeController@returnAllotedItemsShow')->name('request_return_alloted_items.show');

// // Return Item Form
// Route::get('return_items_form', 'HomeController@returnItemsFormIndex')->name('return_items_form.index');
// Route::get('return_items_form/create', 'HomeController@returnItemsFormCreate')->name('return_items_form.create');
// Route::get('return_items_form/{id}/show', 'HomeController@returnItemsFormShow')->name('return_items_form.show');


// // Route::get('allotment/show_return_items_request/{id}', 'HomeController@showReturnItemsRequest')->name('allotments.show_return_items_request');


// //SHOW FECE9
// Route::get('received_fece9', 'HomeController@receivedFece9')->name('received_fece9');
// Route::get('completed_fece9', 'HomeController@completedFece9')->name('completed_fece9');
// Route::get('completed_fece9_details', 'HomeController@completedFece9Details')->name('completed_fece9_details');
// Route::get('rejected_fece9', 'HomeController@rejectedFece9')->name('rejected_fece9');
// Route::get('purchase_items_list', 'HomeController@purchaseItemsList')->name('purchase_items_list');
// Route::get('create_purchase_form', 'HomeController@createPurchaseForm')->name('create_purchase_form');
// Route::get('view_recieved', 'HomeController@viewRecieved')->name('view_recieved');
// // Route::resource('documents', 'DocumentController');
// // Fees5 Route
// Route::get('fees5','HomeController@fees5Create')->name('fees5.create');
// // Distribution Ticket
// Route::get('list_distribution_ticket','HomeController@distributionTicketIndex')->name('distribution_ticket.index');
// Route::get('view_distribution_ticket','HomeController@distributionTicketView')->name('distribution_ticket.view');
// Route::get('print_distribution_ticket','HomeController@printDistributionTicket')->name('distribution_ticket.print');
// Route::get('view_print_distribution_ticket','HomeController@viewPrintDistributionTicket')->name('distribution_ticket.view.print');

// Route::get('distribution_ticket/list','HomeController@distributionTicketIndex')->name('distribution_ticket.index');
// Route::get('distribution_ticket/view','HomeController@distributionTicketView')->name('distribution_ticket.view');
// Route::get('distribution_ticket/list/printed','HomeController@listPrintedDistributionTicket')->name('list.printed.distribution_ticket');
// Route::get('distribution_ticket/view/printed','HomeController@viewPrintedDistributionTicket')->name('view.printed.distribution_ticket');
// // allotted Items Rotue
// Route::get('allotted/items','HomeController@allottedItemsIndex')->name('allotted_items.index');
// // Form Tabakhi
// Route::get('form_tabakhi/show','HomeController@formTabkhiShow')->name('form_tabakhi.show');
// // Stock Route
// Route::get('stock','HomeController@stockIndex')->name('stock.index');

Auth::routes(['register'=>false]);

Route::get('/home', 'HomeController@index')->name('home');
// Route::get('test', function () {
//     return view('welcome');
// });
