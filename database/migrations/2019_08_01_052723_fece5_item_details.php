<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Fece5ItemDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('fece5_item_details', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('fece5_id')->nullable()->default(null);
            $table->integer('fece9_id')->nullable()->default(null);
            $table->integer('receiver_employee_id')->nullable()->default(null);
            $table->integer('item_details_id')->nullable()->default(null);
            $table->integer('item_details_specifications_id')->nullable()->default(null);

            $table->integer('condition')->default(0)->comment('0 -> new, 1 -> used, 2-> daghma');
            $table->integer('status')->default(0)->comment('0->progress,1->approve,2->reject');

            $table->integer('created_by')->nullable()->default(null);
            $table->integer('updated_by')->nullable()->default(null);
            $table->integer('deleted_by')->nullable()->default(null);

            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
