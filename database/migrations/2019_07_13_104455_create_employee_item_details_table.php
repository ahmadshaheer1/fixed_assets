<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeItemDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_item_details', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('employee_id')->nullable()->default(null);
            $table->integer('end_item_details_id')->nullable()->default(null);
            $table->string('allotment_date', 32)->nullable()->default(null);
            $table->string('return_date', 32)->nullable()->default(null);
            $table->integer('status')->nullable()->default(null)->comment('0 -> not alloted to user, 1 -> alloted to user');
            $table->integer('total_alloted')->nullable()->default(null);
            $table->integer('fece5_id')->nullable()->default(null);
            $table->integer('condition')->nullable()->default(null)->comment('0 -> daghma, 1->jadid, 2 -> mustamal, 3 -> mafqood');
            $table->integer('created_by')->nullable()->default(null);
            $table->integer('updated_by')->nullable()->default(null);
            $table->integer('deleted_by')->nullable()->default(null);
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_end_item_details');
    }
}
