<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemDetailsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'item_details';

    /**
     * Run the migrations.
     * @table end_item_details
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name_dr')->nullable()->default(null);
            $table->string('name_pa')->nullable()->default(null);
            $table->string('name_en')->nullable()->default(null);
            // $table->integer('serial_number')->nullable()->default(null);
            $table->integer('main_category_id')->nullable()->default(null);
            $table->integer('sub_category_id')->nullable()->default(null);
            $table->integer('end_category_id')->nullable()->default(null);
            $table->integer('vendor_id')->nullable()->default(null);
            $table->integer('unit_id')->nullable()->default(null);
            $table->string('description')->nullable()->default(null);
            // $table->integer('price')->nullable()->default(null);

            // $table->integer('currency')->nullable()->default(null);
            // $table->string('department_id')->nullable()->default(null);
            // $table->integer('alloted')->nullable()->default(null)->comment('0 -> not alloted, 1 -> alloted');
            $table->integer('requested_item_id')->nullable()->default(null);
            $table->integer('created_by')->nullable()->default(null);
            $table->integer('updated_by')->nullable()->default(null);
            $table->integer('deleted_by')->nullable()->default(null);
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
