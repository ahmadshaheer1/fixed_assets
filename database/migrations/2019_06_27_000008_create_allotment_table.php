<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllotmentTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'allotments';

    /**
     * Run the migrations.
     * @table allotment
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('receiver_employee_id')->nullable()->default(null);
            $table->integer('item_details_specifications_id')->nullable()->default(null);
            $table->string('allotment_date', 32)->nullable()->default(null);
            $table->string('return_date', 32)->nullable()->default(null);
            $table->integer('status')->nullable()->default(null)->comment('0 -> not alloted to user, 1 -> alloted to user');
            $table->integer('total_alloted')->nullable()->default(null);
            $table->integer('fece5_id')->nullable()->default(null);
            $table->integer('condition')->nullable()->default(null)->comment('0 -> daghma, 1->jadid, 2 -> mustamal, 3 -> mafqood');
            $table->integer('created_by')->nullable()->default(null);
            $table->integer('updated_by')->nullable()->default(null);
            $table->integer('deleted_by')->nullable()->default(null);
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
