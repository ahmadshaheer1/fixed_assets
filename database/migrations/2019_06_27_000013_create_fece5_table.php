<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFece5Table extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'fece5s';

    /**
     * Run the migrations.
     * @table fece5
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('number')->nullable()->default(null);
            $table->string('date')->nullable()->default(null);
            $table->string('description')->nullable()->default(null);
            $table->string('reject_remark')->nullable()->default(null);
            $table->integer('status')->default(0)->comment('0->progress,1->approve,2->reject');
            $table->integer('department_id')->nullable()->default(null);
            $table->integer('distributer_employee_id')->nullable()->default(null);
            $table->integer('receiver_employee_id')->nullable()->default(null);
            $table->integer('fece9_id')->nullable()->default(null);
            $table->string('processed_fece5_file')->nullable()->default(null);
            $table->integer('created_by')->nullable()->default(null);
            $table->integer('updated_by')->nullable()->default(null);
            $table->integer('deleted_by')->nullable()->default(null);
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
