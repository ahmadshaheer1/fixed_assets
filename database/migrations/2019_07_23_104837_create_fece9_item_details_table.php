<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFece9ItemDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fece9_item_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('fece9_id');
            $table->integer('item_details_id');
            $table->string('name');
            $table->integer('quantity');
            $table->integer('status')->nullable()->comment('0-> item is not exist, 1-> item exist');
            $table->integer('distributed')->comment('0-> not distributed, 1-> distributed')->default(0);
            $table->integer('created_by')->nullable()->default(null);
            $table->integer('updated_by')->nullable()->default(null);
            $table->integer('deleted_by')->nullable()->default(null);
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fece9_item_details');
    }
}
