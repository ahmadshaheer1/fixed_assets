<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFece9Table extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'fece9s';

    /**
     * Run the migrations.
     * @table fece9
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('number')->nullable()->default(null);
            $table->string('date')->nullable()->default(null);
            $table->longText('description')->nullable()->default(null);
            $table->string('department_id')->nullable()->default(null);
            $table->integer('status')->default(0)->comment('0 -> newly created, 1 -> formally_processed, 2 -> completed, 3 -> in_progress');
            $table->string('processed_fece9_file')->nullable();
            $table->integer('created_by')->nullable()->default(null);
            $table->integer('updated_by')->nullable()->default(null);
            $table->integer('deleted_by')->nullable()->default(null);
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
