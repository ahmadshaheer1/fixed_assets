<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ItemReturnRequests extends Migration
{
    public $tableName = 'item_return_request';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('item_return_request', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('number');
            $table->string('date')->nullable();
            $table->text('description')->nullable();
            $table->integer('employee_id')->nullable();
            $table->string('request_file')->nullable();
            $table->integer('price');
            $table->integer('status');
            $table->integer('created_by')->nullable()->default(null);
            $table->integer('updated_by')->nullable()->default(null);
            $table->integer('deleted_by')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists($this->tableName);
    }
}
