<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemDetailsSpecificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_details_specifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('item_details_id');
            $table->integer('sub_categories_key_id')->nullable()->default(null);
            $table->integer('meem7_id')->nullable()->default(null);
            $table->integer('price');
            $table->integer('currency');
            $table->string('col1')->nullable()->default(null);
            $table->string('col2')->nullable()->default(null);
            $table->string('col3')->nullable()->default(null);
            $table->string('col4')->nullable()->default(null);
            $table->integer('status')->comment('0 -> available, 1 -> alloted');
            $table->integer('condition')->comment('0 -> new, 1 -> used, 2-> daghma');
            $table->integer('created_by')->nullable()->default(null);
            $table->integer('updated_by')->nullable()->default(null);
            $table->integer('deleted_by')->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_details_specifications');
    }
}
