<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeem7Table extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'meem7s';

    /**
     * Run the migrations.
     * @table meem7
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('number')->nullable()->default(null);
            $table->string('date')->nullable()->default(null);
            $table->string('description')->nullable()->default(null);
            $table->string('department_id')->nullable()->default(null);
            // $table->integer('status')->nullable()->default(null);
            $table->string('mahal_taslimi')->nullable()->default(null);
            $table->integer('akhz_az_wajh_employee_id')->nullable()->default(null);

            $table->integer('maktoob_number')->nullable()->default(null);
            $table->string('maktoob_date')->nullable()->default(null);
            $table->string('meem3_file')->nullable()->default(null);
            $table->string('fece9_id');

            $table->string('status')->comment('0 -> incomplete i.e. items not entered to app, 1 -> complete i.e. items entered to app');

            $table->integer('created_by')->nullable()->default(null);
            $table->integer('updated_by')->nullable()->default(null);
            $table->integer('deleted_by')->nullable()->default(null);
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
