<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserTableSeeder::class,
            // MainCategoriesTableSeeder::class,
            // SubCategoriesTableSeeder::class,
            // EndCategoriesTableSeeder::class,
            // ItemDetailsTableSeeder::class,
            // VendorsTableSeeder::class,
            DepartmentsTableSeeder::class,
            UnitsTableSeeder::class,
            EmployeeTableSeeder::class,
        ]);
    }
}
