<?php

use Illuminate\Database\Seeder;
use App\Unit;

class UnitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Unit::truncate();
        Unit::create(
            [
                'name_dr' => 'دانه',
            ],
            [
                'name_dr' => 'پایه',
            ]
        );
    }
}
