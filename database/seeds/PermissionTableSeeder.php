<?php


use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;


class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $permissions = [
            [   'name' => 'sub_categories_key_edit',
                'name_dr' => 'تصحیح نوعیت مشخصات',
                'guard_name' => 'web'
            ],
            [   'name' => 'category_delete',
                'name_dr' => 'حذف کتگوری',
                'guard_name' => 'web'
            ],
            [   'name' => 'category_edit',
                'name_dr' => 'تصحیح کتگوری',
                'guard_name' => 'web'
            ],
            [   'name' => 'sub_categories_key_delete',
                'name_dr' => 'حذف معلومات اساسی اجناس',
                'guard_name' => 'web'
            ],
            [   'name' => 'item_details_edit',
                'name_dr' => 'تصحیح اجناس',
                'guard_name' => 'web'
            ],
            [   'name' => 'item_details_delete',
                'name_dr' => 'حذف اجناس',
                'guard_name' => 'web'
            ],
            [   'name' => 'vendor_delete',
                'name_dr' => 'حذف کمپنی',
                'guard_name' => 'web'
            ],
            [   'name' => 'vendor_edit',
                'name_dr' => 'تصحیح کمپنی',
                'guard_name' => 'web'
            ],
            [   'name' => 'unit_edit',
                'name_dr' => 'تصحیح واحد',
                'guard_name' => 'web'
            ],
            [   'name' => 'unit_delete',
                'name_dr' => 'حذف واحد',
                'guard_name' => 'web'
            ],
        ];


        foreach ($permissions as $permission) {
             Permission::create($permission);
        }
    }
}
