<?php

use Illuminate\Database\Seeder;
use App\ItemDetails;

class ItemDetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ItemDetails::truncate();
        $faker = Faker\Factory::create('fa_IR');
        for($i = 0; $i < 15 ; $i++) {
            ItemDetails::create(
                [
                    'name_dr' => $faker->name,
                    'description' => $faker->realText($maxNbChars = 200, $indexSize = 2),
                    'main_category_id' => $faker->numberBetween($min = 1, $max = 20),
                    'sub_category_id' => $faker->numberBetween($min = 1, $max = 20),
                    'end_category_id' => $faker->numberBetween($min = 1, $max = 20),
                    'vendor_id' => $faker->numberBetween($min = 1, $max = 20),
                    'unit_id' => $faker->numberBetween($min = 1, $max = 20),
                ]
                );
        }


    }
}
