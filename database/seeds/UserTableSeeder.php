<?php

use Illuminate\Database\Seeder;
use App\User;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user=['email'=>'admin@test.com','password'=>Hash::make('admin@123'), 'employee_id' => 1];

        User::create($user);

    }
}
