<?php

namespace App\Exceptions;

use Exception;

class Fece5AlreadyExists extends Exception
{
    /**
     * Report or log an exception.
     *
     * @return void
     */
    public function report()
    {
        \Log::debug('Fece5 Already Exists');
    }
}
