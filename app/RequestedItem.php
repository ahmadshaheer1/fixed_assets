<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class RequestedItem extends Model
{
  protected $fillable = ['sub_category_id', 'requested_items','reject_reason','status', 'created_by', 'updated_by', 'deleted_by'];

    public function user(){
      return $this->belongsTo('App\User','created_by','id');
    }

    public function subCategory() {
        return $this->belongsTo('App\SubCategory');
    }

    public function itemDetails() {
        return $this->hasMany('App\ItemDetails');
    }


    //User privileges
    public function scopeUser($query, $value) {
      if($value != null ) {
          $query->where('col1', 'LIKE', $value);
      }
    }

    public static function boot() {
      parent::boot();

      // create a event to happen on updating
      static::updating(function($table)  {
        $table->updated_by = Auth::user()->id ;
      });

      // create a event to happen on deleting
      static::deleting(function($table)  {
        $table->deleted_by = Auth::user()->id ;
      });

      // create a event to happen on saving
      static::saving(function($table)  {
        $table->created_by = Auth::user()->id ;
      });
    }
}
