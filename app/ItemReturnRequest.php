<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Fico7489\Laravel\Pivot\Traits\PivotEventTrait;

class ItemReturnRequest extends Model
{
    //
    use LogsActivity;
    use SoftDeletes;
    use PivotEventTrait;

    protected $fillable = ['number', 'date',  'description', 'employee_id', 'request_file', 'item_amount', 'status', 'created_by', 'updated_by', 'deleted_by'];

    protected static $logAttributes = ['number', 'date',  'description', 'employee_id', 'request_file', 'item_amount', 'status', 'created_by', 'updated_by', 'deleted_by'];

    public function itemDetailsSpecifications()
    {
        return $this->belongsToMany('App\ItemDetailsSpecifications')
            ->withPivot(
                'id',
                'item_return_request_id',
                'item_details_specifications_id',
                'current_employee_id',
                'receiver_employee_id',
                'return_type',
                'new_price',
                'status',
                'created_by',
                'updated_by',
                'deleted_by'
            )
            ->withTimestamps();
    }

    public function employee()
    {
        return $this->belongsTo('App\Employee', 'employee_id', 'id');
    }



    public static function boot()
    {
        parent::boot();
        static::pivotAttaching(function ($model, $relationName, $pivotIds, $pivotIdsAttributes) {
            if ($relationName == 'itemDetailsSpecifications') {
                activity()
                    ->performedOn($model)
                    ->causedBy(auth()->user())
                    ->withProperties($pivotIdsAttributes)
                    ->log($relationName);
            }
        });
        // create a event to happen on updating
        static::updating(function ($table) {
            $table->updated_by = Auth::user()->id;
        });

        // create a event to happen on deleting
        static::deleting(function ($table) {
            $table->deleted_by = Auth::user()->id;
        });

        // create a event to happen on saving
        static::saving(function ($table) {
            $table->created_by = Auth::user()->id;
        });
    }
}
