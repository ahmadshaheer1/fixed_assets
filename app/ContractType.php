<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class ContractType extends Model
{

    use LogsActivity;
    use SoftDeletes;
    protected $fillable = ['id', 'name_dr', 'name_pa', 'name_en', 'description','created_by', 'updated_by', 'deleted_by', 'deleted_at', 'created_at', 'updated_at'];
    protected static $logAttributes = ['id', 'name_dr', 'name_pa', 'name_en', 'description', 'created_by', 'updated_by', 'deleted_by', 'deleted_at', 'created_at', 'updated_at'];
   
   
    public static function boot() {
        parent::boot();

        // create a event to happen on updating
        static::updating(function($table)  {
            $table->updated_by = Auth::user()->id ;
        });

        // create a event to happen on deleting
        static::deleting(function($table)  {
            $table->deleted_by = Auth::user()->id ;
        });

        // create a event to happen on saving
        static::saving(function($table)  {
            $table->created_by = Auth::user()->id ;
        });
    }
}
