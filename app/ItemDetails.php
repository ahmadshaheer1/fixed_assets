<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemDetails extends Model
{
    use LogsActivity;
    use SoftDeletes;

    protected $fillable = ['name_dr', 'name_pa', 'name_en', 'serial_number','price','currency','main_category_id','sub_category_id','end_category_id','vendor_id','unit_id', 'attachment' ,'description', 'requested_item_id', 'sub_categories_key_id', 'col1', 'col2', 'col3', 'col4', 'created_by', 'updated_by', 'deleted_by'];

    protected static $logAttributes = ['name_dr', 'name_pa', 'name_en', 'serial_number','price','currency','main_category_id','sub_category_id','end_category_id','vendor_id','unit_id','attachment', 'description', 'requested_item_id' ,'sub_categories_key_id', 'col1', 'col2', 'col3', 'col4', 'created_by', 'updated_by', 'deleted_by'];

    public function MainCategory() {
        return $this->belongsTo('App\MainCategory');
    }

    public function subCategory() {
        return $this->belongsTo('App\SubCategory');
    }
    public function endCategory() {
        return $this->belongsTo('App\EndCategory');
    }

    public function itemDetails() {
        return $this->belongsTo('App\ItemDetails');
    }

    public function vendor() {
        return $this->belongsTo('App\Vendor');
    }

    public function unit() {
        return $this->belongsTo('App\Unit');
    }

    public function department() {
        return $this->belongsTo('App\Department');
    }

    public function employee() {
        return $this->belongsToMany('App\Employee', 'employee_end_item_details')->withPivot('allotment_date', 'return_date', 'status', 'total_alloted', 'fece5_id', 'condition')->withTimestamps();
    }

    public function fece9s() {
        return $this->belongsToMany('App\Fece9')->withPivot('name', 'quantity', 'created_by', 'updated_by', 'deleted_by')->withTimestamps();
    }

    public function fece5s() {
        return $this->belongsToMany('App\Fece5')->withPivot('name', 'quantity', 'created_by', 'updated_by', 'deleted_by')->withTimestamps();
    }

    public function itemDetailsSpecifications() {
        return $this->hasMany('App\ItemDetailsSpecifications');
    }

    //get data user based

    public function scopeuserBasedItemdetails($query){

        if(auth()->user()->hasRole('entry_user')){

            $query->where('created_by',auth()->user()->id);
        }
        return $query;

    }

    public function scopeCol1($query, $value) {
        if($value != null && $value != 'none') {
            $query->where('col1', 'LIKE', $value);
        }
    }

    public function scopeCol2($query, $value) {
        if($value != null && $value != 'none') {
            $query->where('col2', 'LIKE', $value);
        }
    }

    public function scopeCol3($query, $value) {
        if($value != null && $value != 'none') {
            $query->where('col3', 'LIKE', $value);
        }
    }

    public function scopeCol4($query, $value) {
        if($value != null && $value != 'none') {
            $query->where('col4', 'LIKE', $value);
        }
    }
   
    public function scopeOfEmployee($query, $value) {
        if($value) {
          $query->where('employee_id', $value);
        }
        return $query;
      }
    public function scopeOfMainCategory($query, $value) {
        if($value) {
          $query->where('main_category_id', $value);
        }
        return $query;
      }
    public function scopeOfSubCategory($query, $value) {
        if($value) {
          $query->where('sub_category_id', $value);
        }
        return $query;
      }
    public function scopeOfEndCategory($query, $value) {
        if($value) {
          $query->where('end_category_id', $value);
        }
        return $query;
      }
    public function scopeOfItemDetails($query, $value) {
        if($value) {
          $query->where('id', $value);
        }
        return $query;
      }

    public static function boot() {
        parent::boot();

        // create a event to happen on updating
        static::updating(function($table)  {
            $table->updated_by = Auth::user()->id ;
        });

        // create a event to happen on deleting
        static::deleting(function($table)  {
            $table->deleted_by = Auth::user()->id ;
        });

        // create a event to happen on saving
        static::saving(function($table)  {
            $table->created_by = Auth::user()->id ;
        });
    }
}
