<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Traits\LogsActivity;

class Meem7Fece9 extends Model
{
    use LogsActivity;
    use SoftDeletes;
    //
    protected $fillable = ['meem7_id', 'fece9_id'];

    protected static $logAttributes = ['meem7_id', 'fece9_id'];


    public function fece9() {
        return $this->belongsTo('App\Fece9');
    }
    public static function boot() {
        parent::boot();

        // create a event to happen on updating
        static::updating(function($table)  {
            $table->updated_by = Auth::user()->id ;
        });

        // create a event to happen on deleting
        static::deleting(function($table)  {
            $table->deleted_by = Auth::user()->id ;
        });

        // create a event to happen on saving
        static::saving(function($table)  {
            $table->created_by = Auth::user()->id ;
        });
    }
}
