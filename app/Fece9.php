<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Fico7489\Laravel\Pivot\Traits\PivotEventTrait;

class Fece9 extends Model
{
    use LogsActivity;
    use SoftDeletes;
    use PivotEventTrait;


    protected $fillable = ['number', 'date',  'description','text','reject_remark', 'department_id', 'processed_fece9_file', 'fece9_type', 'inspection_approval','independent_unit', 'status', 'created_by', 'updated_by', 'deleted_by'];

    protected static $logAttributes = ['number', 'date',  'description','text','reject_remark', 'department_id', 'processed_fece9_file', 'fece9_type', 'inspection_approval','independent_unit', 'status', 'created_by', 'updated_by', 'deleted_by'];

    public function department()
    {
        return $this->belongsTo('App\Department');
    }


    public function fece5s()
    {
        return $this->hasMany('App\Fece5');
    }

    public function meem7()
    {
        return $this->hasOne('App\Meem7', 'fece9_id', 'id');
    }

    public function itemDetails()
    {
        return $this->belongsToMany('App\ItemDetails')->with('unit')->withPivot('id', 'name', 'quantity', 'status', 'distributed', 'created_by', 'updated_by', 'deleted_by')->withTimestamps();
    }

    public function fece5ItemDetails()
    {
        return $this->hasMany('App\ItemDetailsSpecifications', 'fece9_id', 'id');
    }
    
    public function fece9ItemDetails()
    {
        return $this->hasMany('App\Fece9ItemDetails', 'fece9_id', 'id');
    }



    public static function boot()
    {
        parent::boot();

        // static::attach(function ($model, $relationName, $pivotIds, $pivotIdsAttributes) {
        //     activity()
        //     ->performedOn($model)
        //     ->causedBy(auth()->user())
        //     ->withProperties($pivotIdsAttributes)
        //     ->log('Look, I logged something');
        // });
        static::pivotAttaching(function ($model, $relationName, $pivotIds, $pivotIdsAttributes) {
            if ($relationName == 'itemDetails') {
                activity()
                    ->performedOn($model)
                    ->causedBy(auth()->user())
                    ->withProperties($pivotIdsAttributes)
                    ->log($relationName);
            }
        });
        // create a event to happen on updating
        static::updating(function ($table) {
            $table->updated_by = Auth::user()->id ;
        });

        // create a event to happen on deleting
        static::deleting(function ($table) {
            $table->deleted_by = Auth::user()->id ;
        });

        // create a event to happen on saving
        static::saving(function ($table) {
            $table->created_by = Auth::user()->id ;
        });
    }
}
