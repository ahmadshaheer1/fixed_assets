<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Traits\LogsActivity;

class ContractItemBalance extends Model
{
    //

    use LogsActivity;
    use SoftDeletes;
    protected $table = 'contract_item_balances';
    protected $fillable = ['item_details_id','fece9_id','meem7_id','contract_id','quantity','created_at','updated_at','created_by','updated_by','deleted_at','deleted_by'];
    protected static $logAttributes = ['item_details_id','fece9_id','meem7_id','contract_id','quantity','created_at','updated_at','created_by','updated_by','deleted_at','deleted_by'];

    public function itemDetails(){
        return $this->belongsTo('App\ItemDetails');
    }
    public function fece9(){
        return $this->belongsTo('App\Fece9');
    }
    public function meem7(){
        return $this->belongsTo('App\Meem7');
    }
    public static function boot() {
        parent::boot();

        // create a event to happen on updating
        static::updating(function($table)  {
            $table->updated_by = Auth::user()->id ;
        });

        // create a event to happen on deleting
        static::deleting(function($table)  {
            $table->deleted_by = Auth::user()->id ;
        });

        // create a event to happen on saving
        static::saving(function($table)  {
            $table->created_by = Auth::user()->id ;
        });
    }
}
