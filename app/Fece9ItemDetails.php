<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Traits\LogsActivity;

class Fece9ItemDetails extends Model
{


    use LogsActivity;
    use SoftDeletes;

    
    protected $fillable = ['fece9_id','item_details_id','name','quantity','status','motamed_id','distributed','created_by','updated_by','deleted_by','deleted_at','created_at','updated_at'];
    protected static $logAttributes = ['fece9_id', 'item_details_id', 'name', 'quantity', 'status','motamed_id', 'distributed', 'created_by', 'updated_by', 'deleted_by', 'deleted_at', 'created_at', 'updated_at'];

    public static function boot()
    {
        parent::boot();

     
        // create a event to happen on updating
        static::updating(function ($table) {
            $table->updated_by = Auth::user()->id ;
        });

        // create a event to happen on deleting
        static::deleting(function ($table) {
            $table->deleted_by = Auth::user()->id ;
        });

        // create a event to happen on saving
        static::saving(function ($table) {
            $table->created_by = Auth::user()->id ;
        });
    }
}
