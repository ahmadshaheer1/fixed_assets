<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Fico7489\Laravel\Pivot\Traits\PivotEventTrait;
class Fece5 extends Model
{
    use LogsActivity;
    use SoftDeletes;
    use PivotEventTrait;

    protected $fillable = ['number', 'date',  'description','status','reject_remark','department_id', 'fece9_id','processed_fece5_file', 'receiver_employee_id', 'distributer_employee_id','created_by', 'updated_by', 'deleted_by'];

    protected static $logAttributes = ['number', 'date',  'description','status','reject_remark','department_id', 'fece9_id','processed_fece5_file','receiver_employee_id', 'distributer_employee_id', 'created_by', 'updated_by', 'deleted_by'];


    public function department() {
        return $this->belongsTo('App\Department');
    }

    public function allotments() {
        return $this->hasMany('App\Allotment');
    }

    public function distributerEmployee() {
        return $this->belongsTo('App\Employee', 'distributer_employee_id', 'id');
    }


    public function fece9() {
        return $this->belongsTo('App\Fece9');
    }

    public function itemDetails() {
        return $this->belongsToMany('App\ItemDetails')->withPivot('id', 'fece9_id', 'receiver_employee_id','item_details_id', 'item_details_specifications_id' , 'condition', 'status', 'created_by', 'updated_by', 'deleted_by')->withTimestamps();
    }


    public function receiver() {
        return $this->belongsToMany('App\ItemDetails')
                    ->withTimestamps();
    }

    public static function boot() {
        parent::boot();
        static::pivotAttaching(function ($model, $relationName, $pivotIds, $pivotIdsAttributes) {
            if($relationName == 'itemDetails') {
                activity()
                ->performedOn($model)
                ->causedBy(auth()->user())
                ->withProperties($pivotIdsAttributes)
                ->log($relationName);}
        });
        // create a event to happen on updating
        static::updating(function($table)  {
            $table->updated_by = Auth::user()->id ;
        });

        // create a event to happen on deleting
        static::deleting(function($table) {
            $table->deleted_by = Auth::user()->id ;
        });

        // create a event to happen on saving
        static::saving(function($table)  {
            $table->created_by = Auth::user()->id ;
        });
    }
}
