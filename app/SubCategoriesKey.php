<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class SubCategoriesKey extends Model
{
    //

    use LogsActivity;
    use SoftDeletes;

    protected $fillable = ['sub_category_id','col1', 'col2', 'col3','col4'  ,'col5','col6','col7','col6','col8','col7','col9','col8','col10','col11','col12','distribution_type','item_type','created_by', 'updated_by', 'deleted_by'];

    protected static $logAttributes = ['sub_category_id','col1', 'col2', 'col3','col4'  ,'col5','col6','col7','col6','col8','col7','col9','col8','col10','col11','col12','distribution_type','item_type','created_by', 'updated_by', 'deleted_by'];

    public function subCategory() {

        return $this->belongsTo('App\SubCategory');
    }

    public function itemDetailsSpecifications() {
        return $this->hasMany('App\ItemDetailsSpecifications');
    }

    public static function boot() {
        parent::boot();

        // create a event to happen on updating
        static::updating(function($table)  {
            $table->updated_by = Auth::user()->id ;
        });

        // create a event to happen on deleting
        static::deleting(function($table)  {
            $table->deleted_by = Auth::user()->id ;
        });

        // create a event to happen on saving
        static::saving(function($table)  {
            $table->created_by = Auth::user()->id ;
        });
    }
}
