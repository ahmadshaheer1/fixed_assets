<?php

namespace App\Exports;

use App\Allotment;
use App\User;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;


class ReportExport implements FromView
{


    public function __construct($request)
    {
        $this->main_category_id = $request->main_category_id;
        $this->sub_category_id = $request->sub_category_id;
        $this->end_category_id = $request->end_category_id;
        $this->item_details_id = $request->item_details_id;
        $this->department_id = $request->department_id;
        $this->employee_id = $request->employee_id;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
           

        return $result;

    }
    public function view(): View
    {
        $main_category_id = $this->main_category_id;
        $sub_category_id = $this->sub_category_id;
        $end_category_id = $this->end_category_id;
        $item_details_id = $this->item_details_id;
        $department_id = $this->department_id;
        $employee_id = $this->employee_id;

        $reports = Allotment::
        ofEmployee($employee_id)
        ->whereHas('itemDetailsSpecifications.itemDetails',
            function ($query) use ($main_category_id,$sub_category_id,$end_category_id,$item_details_id){
                return  $query
                            ->ofMainCategory($main_category_id)
                            ->ofSubCategory($sub_category_id)
                            ->ofEndCategory($end_category_id)
                            ->ofItemDetails($item_details_id)
                            ;
            }
        )
        ->whereHas('receiverEmployee.department', function ($q) use($department_id){
            return $q->ofDepartment($department_id);
        })
        ->with('itemDetailsSpecifications',
                'itemDetailsSpecifications.motamed',
                'itemDetailsSpecifications.motamed.employee',
                'itemDetailsSpecifications.itemDetails',
                'itemDetailsSpecifications.itemDetails.vendor',
                'itemDetailsSpecifications.itemDetails.unit'
        )
        ->get();
        return view('exports.excel', compact('reports'));
    }
}
