<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeAttachment extends Model
{
    use LogsActivity;
    use SoftDeletes;
    protected $table = 'employee_attachments';
    protected $fillable = ['attachment','employee_id','created_at','updated_at','employee_type','created_by','updated_by','deleted_at','deleted_by'];
    protected static $logAttributes = ['attachment','employee_id','created_at','updated_at','employee_type','created_by','updated_by','deleted_at','deleted_by'];
    public function employee()
{
    return $this->belongsTo('App\Employee', 'employee_id', 'id');
}
    protected static function boot()
    {
    parent::boot();

        // create a event to happen on updating
        static::updating(function($table)  {
            $table->updated_by = Auth::user()->id ;
        });

        // create a event to happen on deleting
        static::deleting(function($table) {
            $table->deleted_by = Auth::user()->id ;
        });

        // create a event to happen on saving
        static::saving(function($table)  {
            $table->created_by = Auth::user()->id ;
        });
    }
    public function external_department() {
        return $this->belongsTo('App\ExternalDepartment');
    }
    
}
