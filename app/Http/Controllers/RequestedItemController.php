<?php

namespace App\Http\Controllers;

use App\EndCategory;
use App\MainCategory;
use App\RequestedItem;
use App\SubCategory;
use App\Unit;
use App\Vendor;
use Illuminate\Http\Request;

class RequestedItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $requested_items=RequestedItem::where('status',0)->get();
                //return view('item_details.index_requested');

        return view('requested_item.index',compact('requested_items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $main_categories=MainCategory::all();
        $sub_categories=SubCategory::all();
        $end_categories=EndCategory::all();
        $vendors=Vendor::all();
        $units=Unit::all();
        $requested_item=RequestedItem::find($id);
          return view('requested_item.create',compact('main_categories','sub_categories','end_categories','vendors','units','requested_item'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       $request_item=RequestedItem::create($request->all());
       return redirect()->route('fece9s.create')->with('success', trans('global.create_successful_requested_items'));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RequestedItem  $requestedItem
     * @return \Illuminate\Http\Response
     */
    public function show(RequestedItem $requestedItem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RequestedItem  $requestedItem
     * @return \Illuminate\Http\Response
     */
    public function edit(RequestedItem $requestedItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RequestedItem  $requestedItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RequestedItem $requestedItem)
    {
        $requestedItem->update($request->all());
        return redirect()->route('requested_items.index')->with('success', trans('global.reject_successful'));

    }


    //Accepted Requested Item 

    public function accept()
    {
        $requested_items=RequestedItem::where('status',1)->get();
        return view('requested_item.accept',compact('requested_items'));
    }

    //Reject Requested Item 

    public function reject()
    {
        $requested_items=RequestedItem::where('status',2)->get();
        return view('requested_item.reject',compact('requested_items'));
    }





    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RequestedItem  $requestedItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(RequestedItem $requestedItem)
    {
        //
    }
}
