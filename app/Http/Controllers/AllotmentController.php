<?php

namespace App\Http\Controllers;

use App\Allotment;
use App\Employee;
use App\ExternalEmployee;
use App\ItemBalance;
use App\Motamed;
use App\ItemDetailsSpecifications;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AllotmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        foreach ($request->allotment_list as $item) {
            // return $item['total_alloted'];
            $item_details_specifications = ItemDetailsSpecifications::find($item['item_details_specifications_id']);
            $available_item = $item_details_specifications->item_amount - $item['total_alloted'];
            // if item be jam jam
            if ($item_details_specifications->subCategoryKeys->distribution_type > 0) {
                $item_details_specifications_update = ItemDetailsSpecifications::where('id', $item['item_details_specifications_id']);

                if ($available_item < 1) {
                    $item_details_specifications_update->update(['status' => 1, 'item_amount' => $available_item]);
                } else {
                    $item_details_specifications_update->update(['item_amount' => $available_item]);
                }


                $item_balance = [
                    'item_details_id'   => $item_details_specifications->item_details_id,
                    'item_details_specifications_id' => $item_details_specifications->id,
                    'item_amount' => $item['total_alloted'],
                    'type'  => '0',
                ];
                $inserted_itme_balance = ItemBalance::create($item_balance);

                $item['item_balances_id']  = $inserted_itme_balance->id;
            } else {
                // Check If Employee Type Motamed 
                // Tawze items ba jam motamed
                if ($item['employee_type'] == 2) {
                    ItemDetailsSpecifications::where('id', $item['item_details_specifications_id'])->update(['status' => 0, 'motamed_id' => $item['receiver_employee_id']]);
                    $employee_id = Motamed::find($item['receiver_employee_id'])->employee->id;
                    $item['receiver_employee_id'] = $employee_id;
                } else {
                    // Tawze items az jam Motamed ba Jam karmandan degar
                    $jam_motamed = Allotment::where('item_details_specifications_id', $item['item_details_specifications_id'])->where('status', 1)->first();
                    if ($jam_motamed) {
                        $jam_motamed->update(['status' => 0]);
                    }
                    //
                    ItemDetailsSpecifications::where('id', $item['item_details_specifications_id'])->update(['status' => 1]);
                }
            }

            Allotment::create($item);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Allotment  $allotment
     * @return \Illuminate\Http\Response
     */
    public function show(Allotment $allotment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Allotment  $allotment
     * @return \Illuminate\Http\Response
     */
    public function edit(Allotment $allotment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Allotment  $allotment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, string $id)
    {
        $allotment = Allotment::find($id);
        $allotment->update($request->allotments);
        return response()->json([
            'status'  => 'success',
            'message' => trans('global.update_successful')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Allotment  $allotment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Allotment $allotment)
    {
        //
    }

    public function oldAllotments()
    {
        $item_details = ItemDetails::all();
        return view('allotments.book.old_allotments', compact('item_details'));
    }

    public function getEmployeesAllotments()
    {
        $authEmployee = auth()->user();
        if ($authEmployee->user_type == 1) {
            $authUserDepartmentId = getAuthEmployeeDepartment();
            $employee = Employee::where('department_id', $authUserDepartmentId)->select('id', 'name_dr', 'father_name_dr', 'current_position_dr', 'department_id')->paginate(10);
        } else {
            $employee = Employee::select('id', 'name_dr', 'father_name_dr', 'current_position_dr', 'department_id')->paginate(10);
        }

        return view('allotments.index', compact('employee'));
    }
    public function getMotamedAllotments()
    {
        $motamed = Motamed::paginate(10);
        return view('motamed.index', compact('motamed'));
    }
    public function getEmployeesAllotmentsAjax(Request $request)
    {
        if ($request->ajax()) {

            $authEmployee = auth()->user();
            if ($authEmployee->user_type == 1) {
                $authUserDepartmentId = getAuthEmployeeDepartment();
                $employee = Employee::where('department_id', $authUserDepartmentId)->select('id', 'name_dr', 'father_name_dr', 'current_position_dr', 'department_id', 'last_name')->paginate(10);
            } else {
                $employee = Employee::select('id', 'name_dr', 'father_name_dr', 'current_position_dr', 'department_id')->paginate(10);
            }

            return view('allotments.employee_list', compact('employee'))->render();
        }
    }
    public function getMotamedAllotmentsAjax(Request $request)
    {
        if ($request->ajax()) {
            $motamed = Motamed::select('id', 'name_dr', 'father_name_dr', 'current_position_dr', 'department_id', 'last_name')->paginate(10);
            return view('motamed.motamed_list', compact('motamed'))->render();
        }
    }
    public function getEmployeesAllotmentsSearch(Request $request, $type = null)
    {
        $name = $request->name;
        $father_name = $request->father_name;
        $designation = $request->designation;
        $department = $request->department;
        // return $name;
        if ($request->ajax()) {
            if ($type == 'motamed') {
                $motamed = Motamed::search($name, $father_name, $designation, $department)->paginate(100);
                return view('motamed.motamed_list', compact('motamed'))->render();
            }
            $employee = Employee::search($name, $father_name, $designation, $department)->paginate(100);
            return view('allotments.employee_list', compact('employee'))->render();
        }
    }
    public function getEmployeeAllotments($employee_id, $type, $status = null)
    {
        $employee_id = decrypt($employee_id);
        $employee_attachment = null;
        if ($type == 0) {
            $employee = Employee::find($employee_id);
            // $allotment_history = Allotment::where('item_details_specifications_id', 1)->where('employee_type',0)->orderBy('allotment_date', 'asc')->get();
            $items =  $employee->allotments;
            $employee_attachment =  $employee->employee_attachments->where('employee_type', 0);
            // return $employee_attachment;
            // return $employee_attachment[0]['id'];
        } else {
            $employee = ExternalEmployee::find($employee_id);
            // $allotment_history = Allotment::where('item_details_specifications_id', 1)->where('employee_type',0)->orderBy('allotment_date', 'asc')->get();
            $items =  $employee->allotments;
            $employee_attachment =  $employee->employee_attachments->where('employee_type', 1);
            // return $employee_attachment;
        }
        return view('allotments.show', compact('employee', 'employee_attachment', 'type', 'status'));
    }
    public function getItemDetailsSpecificationsAllotmentHistory($item_details_specifications_id, $item_balances_id)
    {
        if ($item_balances_id != 'null') {
            $allotment_history = Allotment::with('employee')->where('item_details_specifications_id', $item_details_specifications_id)->where('item_balances_id', $item_balances_id)->orderBy('allotment_date', 'asc')->get();
        } else {
            $allotment = Allotment::select('employee_type')->where('item_details_specifications_id', $item_details_specifications_id)->first();
            if ($allotment->employee_type == '0') {
                $allotment_history = Allotment::with('employee')->where('item_details_specifications_id', $item_details_specifications_id)->orderBy('allotment_date', 'asc')->get();
            } else if ($allotment->employee_type == '1') {
                $allotment_history = Allotment::with('externalEmployee')->where('item_details_specifications_id', $item_details_specifications_id)->orderBy('allotment_date', 'asc')->get();
            } else {
                $allotment_history = Allotment::with('motamed')->with('employee')->with('externalEmployee')->where('item_details_specifications_id', $item_details_specifications_id)->orderBy('allotment_date', 'asc')->get();
                return $allotment_history;
            }
        }
        return response()->json($allotment_history);
    }
    public function getItemDetailsSpecificationsAllotmentActive($item_details_specifications_id, $item_balances_id)
    {
        if ($item_balances_id != 'null') {
            $alloted_item = Allotment::with('employee')->with('itemDetailsSpecifications.itemDetails')->where('item_details_specifications_id', $item_details_specifications_id)->where('item_balances_id', $item_balances_id)->with('itemDetailsSpecifications.subCategoryKeys')->where('status', '1')->get();
        } else {
            $allotment = Allotment::select('employee_type')->where('item_details_specifications_id', $item_details_specifications_id)->first();
            // return $allotment->employee_type;

            if ($allotment->employee_type == '0') {

                $alloted_item = Allotment::with('employee')->where('item_details_specifications_id', $item_details_specifications_id)->with('itemDetailsSpecifications.itemDetails')->with('itemDetailsSpecifications.subCategoryKeys')->orderBy('allotment_date', 'asc')->get();
            }
        }


        return response()->json($alloted_item);
    }

    //Item back to Stock
    public function roleBackAllotement($allotment_id, $item_details_specifications_id)
    {


        $allotment = Allotment::find($allotment_id);
        $item_details_specifications = ItemDetailsSpecifications::find($item_details_specifications_id);

        // jam-e jam
        if ($allotment->item_balances_id != null) {
            $item_balance = ItemBalance::find($allotment->item_balances_id);
            $item_amount = $item_balance->item_amount;
            $total_item_amount = $item_details_specifications->item_amount + $item_amount;
            $item_details_specifications->update(['item_amount' => $total_item_amount, 'status' => '0']);
            $item_balance->delete();
        } else {
            $item_details_specifications->update(['status' => '0']);
        }
        $allotment->delete();
        return 'success';
    }
    public function getItemDetailsSpecifications($item_details_specifications_id)
    {
        $item_details = ItemDetailsSpecifications::with('itemDetails')->with('subCategoryKeys')->where('id', $item_details_specifications_id)->first();
        return response()->json($item_details);
    }
    public function getAllItemDetailsSpecifications($employee_id)
    {

        $item_details_specifications = Allotment::with('itemDetailsSpecifications.itemDetails')->with('itemDetailsSpecifications.subCategoryKeys')->where('receiver_employee_id', $employee_id)->where('status', '1')->get();
        return response()->json($item_details_specifications);
    }

    public function getMotamed(Request $request)
    {
        return $allotment = Allotment::whereIn('item_details_specifications_id', $request->item_details_specifications_id)->get();
        return $allotment->first()->employee;
        return $request->item_details_specifications_id;
    }

    public function editItem($id)
    {
        $allotments = Allotment::find($id);
        return [
            'allotments' => $allotments,
        ];
    }

    /** 
     * Merge Duplicate Items
     * first: merge is done in allotment table with specific receiver_id, item_detail_specification_id
     * 
     */
    public function mergeDuplicateItems($employee_id, $employee_type, Request $request)
    {
        // get all itemDetailSpecifications
        $itemDetailSpecifications = $request->all();

        // get each item_detail_specification
        foreach ($itemDetailSpecifications as $key => $itemDetailSpecification) {
            // if total of items in itemDetailSpecification are less than 2 to merge then move to next itemDetailSpecifications
            if (count($itemDetailSpecification) < 2) continue;

            // removable and lastAllotment Ids 
            $removableDuplicateAllotments = array_slice($itemDetailSpecification, 0, -1); // returns all values except the last index of $itemDetailSpecification
            $lastDuplicateAllotment = end($itemDetailSpecification); // returns the last index value of array of $itemDetailSpecification

            // removable and last Item_balance Ids
            $removableDuplicateItemBalances = Allotment::whereIn('id', $removableDuplicateAllotments)->select('item_balances_id')->pluck('item_balances_id')->toArray();
            $lastDuplicateItemBalance = Allotment::find($lastDuplicateAllotment)->item_balances_id;

            // get every allotment from itemDetailSpecification and calculate the total_alloted of items
            $totalMergedAllotedItems = Allotment::whereIn('id', $itemDetailSpecification)->select('total_alloted')->sum('total_alloted');

            // update/delete part of duplicate allotment table
            Allotment::find($lastDuplicateAllotment)->update([
                'total_alloted' => $totalMergedAllotedItems
            ]); // update the last allotment's total_alloted column by $totalMergedAllotedItems
            Allotment::destroy($removableDuplicateAllotments); // remove the additional allotment records

            // update/delete part of duplicate item_balance table
            ItemBalance::find($lastDuplicateItemBalance)->update([
                'item_amount' => $totalMergedAllotedItems
            ]);
            ItemBalance::destroy($removableDuplicateItemBalances);
        }
    }
}
