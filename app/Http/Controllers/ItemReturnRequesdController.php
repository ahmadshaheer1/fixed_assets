<?php

namespace App\Http\Controllers;

use App\Allotment;
use Illuminate\Http\Request;
use App\Http\Requests\ReturnItemRequest;
use App\ItemBalance;
use App\ItemDetailsSpecifications;
use App\ItemReturnRequest;
use App\Motamed;
use Illuminate\Support\Facades\DB;

class ItemReturnRequesdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $item_return_request = ItemReturnRequest::whereStatus('0')->orderBy('id', 'desc')->get();
        return view('allotments.return.index', compact('item_return_request'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReturnItemRequest $request)
    {
        $item_details_specifications = arrayOfObjectsToNestedArray($request->return_list);
        $item_return_request = ItemReturnRequest::create($request->all());
        $item_return_request->itemDetailsSpecifications()->attach($item_details_specifications);

        return $item_return_request->id;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item_return_request = ItemReturnRequest::find($id);
        $item_return_request->itemDetailsSpecifications;
        return view('allotments.return.show', compact('item_return_request'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //if return type be Form aada (return to stock) item allote to motamed and it will be allotable
        if ($request->return_type == '1')
            ItemDetailsSpecifications::find($request->item_details_specification_id)->update(['status' => '0']);
        // if return type be daghma return to stock and not allotable
        elseif ($request->return_type == '0') {
            ItemDetailsSpecifications::find($request->item_details_specification_id)->update(['condition' => '2', 'status' => '1']);
        }

        $item_return_request = ItemReturnRequest::find($id);
        $item_return_request->itemDetailsSpecifications()->syncWithoutDetaching([$request->item_details_specification_id => ['receiver_employee_id' => $request->receiver_employee_id, 'new_price' => $request->new_price]]);
        // ItemReturnRequest::where('id',$id)->update(['receiver_employee_id' => $request->receiver_employee_id]);
        return "success";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function attachFileAndSend(Request $request)
    {
        $path = 'storage/uploads/return_item/';
        $item_return_request = ItemReturnRequest::find($request->id);
        $image_path = singleFileStore($request, $request->id, $path);

        $item_return_request->update(['request_file' => $image_path, 'status' => 1]);
        return redirect()->back()->with('success', 'فایل پروسس شده موفقانه اضافه گردید.');
    }

    public function replaceAttachedFile(Request $request)
    {

        $item_return_request = ItemReturnRequest::find($request->id);
        // unlink($fece9->processed_fece9_file);
        $path = 'storage/uploads/return_item/';
        singleFileStore($request, $request->id, $path);
        return redirect()->back()->with('success', 'فایل پروسس شده موفقانه تصحیح گردید.');
    }
    public function itemReturnConformation($id, Request $request)
    {
        $item_return_request = ItemReturnRequest::find($id);
        $employee_id = $item_return_request->employee_id;
        $return_date = $item_return_request->date;
        $new_alloted_item_amount = $item_return_request->item_amount;
        $item_details_specifications = $item_return_request->itemDetailsSpecifications;

        $return_lists = $request->return_list;
        foreach ($return_lists as $key => $return_list) {
            // search for the item details specifiction 
            $item_details_specifications_id = $return_list['item_details_specifications_id'];
            $item_details_specifications = ItemDetailsSpecifications::find($return_list['item_details_specifications_id']);

            // search the allotment that its already alloted to old employee
            $return_allotment = Allotment::where('receiver_employee_id', $employee_id)->where('item_details_specifications_id', $item_details_specifications->id)->first();
            $item_balances_id = $return_allotment->item_balances_id;
            $total_alloted = 0;
            $new_item_balance_id = 0;
            if ($item_balances_id) {

                $total_alloted = $return_allotment->total_alloted;
            }
            // check whether item condition is mafqod(2) or dagha(4) then item_status will be 4(not available) else 1(available)
            $item_status = in_array($request->item_status, [4, 2]) ? 4 : 0;

            $item_details_specifications->update(['status' => $item_status]);

            if ($total_alloted != $new_alloted_item_amount and $item_balances_id) {

                $item_balances = [
                    'item_details_id' => $item_details_specifications->item_details_id,
                    'item_details_specifications_id' => $item_details_specifications_id,
                    'item_amount' => $new_alloted_item_amount,
                    'type' => '0',
                ];

                $item_amount_after_allotement = $total_alloted - $new_alloted_item_amount;
                $new_item_balance_id = ItemBalance::create($item_balances)->id;
                $return_allotment->update(['total_alloted' => $item_amount_after_allotement]);
                ItemBalance::find($item_balances_id)->update(['item_amount' => $item_amount_after_allotement]);
            } else {
                if ($item_balances_id) {
                    $new_item_balance_id = $item_balances_id;
                }
                $return_allotment->update(['return_date' => $return_date, 'status' => $item_status]);
            }

            if ($request->item_status != '4') {

                $item_return_request->itemDetailsSpecifications()->syncWithoutDetaching(
                    [
                        $item_details_specifications_id =>
                        [
                            'receiver_employee_id' =>
                            $return_list['receiver_employee_id'], 'new_price' => $return_list['new_price']
                        ]
                    ]
                );

                // if item transfers to another employee the item details specification change from new to use
                $item_details_specifications->update(['condition' => $request->item_status]);
                //Allot Item to user
                $allotment = [
                    'receiver_employee_id' => $return_list['receiver_employee_id'],
                    'item_details_specifications_id' => $item_details_specifications_id,
                    'allotment_date' => $return_date,
                    'status' => '1',
                    'condition' => '1',
                    'item_balances_id' => $new_item_balance_id,
                    'total_alloted' => $new_alloted_item_amount,
                ];
                $employee = Motamed::where('employee_id', $return_list['receiver_employee_id'])->first();
                $allotment['employee_type'] = $employee ? 2 : 0;

                Allotment::create($allotment);
                $item_return_request->update(['status' => '2']);
            }
        }

        return route('item_return_requests.completed');
    }

    public function inprogress()
    {
        $item_return_request = ItemReturnRequest::whereStatus('1')->orderBy('id', 'desc')->get();
        return view('allotments.return.inprogress_index', compact('item_return_request'));
    }
    public function completed()
    {
        $item_return_request = ItemReturnRequest::whereStatus('2')->orderBy('id', 'desc')->get();
        return view('allotments.return.completed_index', compact('item_return_request'));
    }
}
