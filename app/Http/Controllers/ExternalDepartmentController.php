<?php

namespace App\Http\Controllers;

use App\ExternalDepartment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoreExternalDepartmentRequest;
use App\Http\Requests\UpdateExternalDepartmentRequest;


class ExternalDepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = ExternalDepartment::orderBy('id', 'desc')->get();
        return view('external_departments.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('external_departments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreExternalDepartmentRequest $request)
    {
        ExternalDepartment::create($request->input());
        return redirect()->route('external_departments.create')->with('success', trans('global.create_successful'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $external_department=ExternalDepartment::find(decrypt($id));
        return view('external_departments.edit', compact('external_department'));    
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateExternalDepartmentRequest $request, $id)
    {
        $external_department=ExternalDepartment::find(decrypt($id));
        $external_department->update($request->input());
        return redirect()->route('external_departments.index')->with('success', trans('global.update_successful'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $external_department=ExternalDepartment::find(decrypt($id));
        $external_department->delete();
        $external_department['deleted_by']=Auth::user()->id;
        $external_department->save();
        return response()->json([
            'status'  => 'success',
            'message' => trans('global.delete_successful')
        ]);
    }
}
