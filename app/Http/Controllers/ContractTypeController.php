<?php

namespace App\Http\Controllers;

use App\ContractType;
use Illuminate\Http\Request;
use App\Http\Requests\ContractTypeRequest;



class ContractTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contract_types=ContractType::all();
        return view('contract_type.index',compact('contract_types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contract_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContractTypeRequest $request)
    {        
        ContractType::create($request->input());
        return redirect()->route('contract_type.index')->with('success', trans('global.create_successful'));  

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ContractType  $contractType
     * @return \Illuminate\Http\Response
     */
    public function show(ContractType $contractType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ContractType  $contractType
     * @return \Illuminate\Http\Response
     */
    public function edit(ContractType $contractType)
    {
        return view('contract_type.edit',compact('contractType'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ContractType  $contractType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContractType $contractType)
    {
        $contractType->update($request->all());
        return redirect()->route('contract_type.index')->with('success', trans('global.create_successful'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContractType  $contractType
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContractType $contractType)
    {
        //
    }
}
