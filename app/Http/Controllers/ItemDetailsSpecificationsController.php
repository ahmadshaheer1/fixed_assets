<?php

namespace App\Http\Controllers;

use App\Allotment;
use App\Employee;
use App\Fece9;
use App\ItemBalance;
use App\ItemDetails;
use App\ItemDetailsSpecifications;
use App\Meem7;
use App\SubCategoriesKey;
use Illuminate\Http\Request;

class ItemDetailsSpecificationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ItemDetailsSpecifications::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // check if a single index of specifications or array of specifications are to be inserted 
        $data = isset($request->specifications[0]) ? $request->specifications : $request->input();
         foreach($data as $item_detail) {
            //  return gettype($item_detail);

            $item = ItemDetailsSpecifications::create($item_detail);

            //if item be jam jam
            if(isset($item_detail['item_amount']) >0 ){
                $item_balance = [
                    'item_details_id'   => $item->item_details_id,
                    'item_details_specifications_id' => $item->id,
                    'item_amount' => $item_detail['item_amount'],
                    'type'  => '1'
                ];
               ItemBalance::create($item_balance);
            }


            //if add item from meem7
            if(array_key_exists('meem7_id',$item_detail)){
                $meem7 = Meem7::find($item_detail['meem7_id']);
                $allotment = [
                    'item_details_specifications_id' => $item->id,
                    'receiver_employee_id' => $meem7->mahal_taslimi,
                    'allotment_date' => $meem7->date,
                    'fece5_id' => '0',
                    'condition' => '0',
                    'status' => '1',
                ];
                Allotment::create($allotment);
            }
        }

        if($request->fece9_id && $item) {
            $fece9 = Fece9::find($request->fece9_id);
            // set item details status to 1 after item details inserted
            $fece9->itemDetails()->syncWithoutDetaching([$request->item_details_id => ['status' => 1]]);

            // set meem7 status to 1 i.e. completed items insertion if no item details has the status 0
            if(!$fece9->itemDetails()->where('status', 0)->exists()) {
                $fece9->meem7()->update(['status' => 1]);
            }
        }
        if($request->ajax()) {
            return response(['success' => trans('global.create_successful')], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ItemDetailsSpecifications  $itemDetailsSpecifications
     * @return \Illuminate\Http\Response
     */
    public function show($item_details_id)
    {
        $item_balance = itemBalance::with('itemDetailsSpecifications')->where('item_details_id', $item_details_id)->get();
        // return $item_balance;
        $item_details = ItemDetails::find($item_details_id)->itemDetailsSpecifications;
        return [
            'item_details' => $item_details,
            'item_balance' => $item_balance
        ];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ItemDetailsSpecifications  $itemDetailsSpecifications
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        return ItemDetailsSpecifications::find($id);
    }
    public function editItemDetailsSpecifications($id)
    {
        //
        $item_details_specification = ItemDetailsSpecifications::find($id);
        $sub_category_id = $item_details_specification->itemDetails->subCategory->id;
        $sub_category_keys = SubCategoriesKey::where('sub_category_id',$sub_category_id)->first();

         return [
            'item_details_specification' => $item_details_specification, 
            'sub_category_keys' => $sub_category_keys, 
         ];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ItemDetailsSpecifications  $itemDetailsSpecifications
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item_details_specifications = ItemDetailsSpecifications::find($id);
        $item_details_specifications->update($request->specifications);
        return response()->json([
            'status'  => 'success',
            'message' => trans('global.update_successful')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ItemDetailsSpecifications  $itemDetailsSpecifications
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item_details_specifications = ItemDetailsSpecifications::find($id);
        $item_details_specifications->delete();
        return response()->json([
            'status'  => 'success',
            'message' => trans('global.delete_successful')
        ]);
    }


    // get item details specifications by item details id
    public function getItemDetailsSpecifications($item_details_id,$fece9_id =null) {

        $fece9 = Fece9::find($fece9_id);

        if($fece9->fece9_type == 0){
            $fece9_item_details = $fece9->fece9ItemDetails()->first();
            $motamed = Employee::find($fece9_item_details->motamed_id);
            return [
                'fece9_type' => 0,
                'motamed' => $motamed,
            ];

        }else{

            $item_details_specifications =  ItemDetailsSpecifications::where('item_details_id',$item_details_id)->where('status',0)->get();
            $item_details_specifications_single = $item_details_specifications->first();
           if($item_details_specifications_single) {
               $motamed_allotment = $item_details_specifications_single->motamed;
               $motamed = Employee::find($motamed_allotment->receiver_employee_id);
           }else{
               $motamed = [];
           }
    
            
           return [
               'items' => $item_details_specifications,
               'motamed' => $motamed,
               'extra_specifications_keys' => $item_details_specifications->first()->subCategoryKeys,
           ];
        }
       
    }

    public function getItemDetailsSpecificationsAxios(Request $request, $item_details_id){
        if($request->type == 'fece2'){
            $item_details_fece2 = ItemDetailsSpecifications::with('itemDetails')->with('subCategoryKeys')->where('item_details_id',$item_details_id)->where('status',0)->where('item_process','0')->get();
            return $item_details_fece2;
            return response()->json($item_details_fece2);
        }
        $item_details = ItemDetailsSpecifications::with('itemDetails')->with('subCategoryKeys')->where('item_details_id',$item_details_id)->where('status',0)->get();
        return response()->json($item_details);
    }
    public function attachFileAndSend (Request $request) {
        $path = 'storage/uploads/item_details/';
         $item = ItemDetailsSpecifications::find($request->id);
         $image_path = singleFileStore($request, $request->id, $path);
        $item->update(['attachment' => $image_path]);
        return redirect()->back()->with('success', 'فایل   موفقانه اضافه گردید.');
    }



}
