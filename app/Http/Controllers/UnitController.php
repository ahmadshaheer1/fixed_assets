<?php

namespace App\Http\Controllers;

use App\Unit;
use Illuminate\Http\Request;
use \Spatie\Activitylog\Models\Activity;
use App\Http\Requests\UnitRequest;
use Illuminate\Support\Facades\Auth;

class UnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Unit::orderBy('id', 'desc')->get();

        return view('units.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('units.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UnitRequest $request)
    {


        Unit::create($request->input());
        
        if($request->ajax()) {
            return response(['success' => trans('global.create_successful')], 200);

        }
        else {
            return redirect()->route('units.create')->with('success', trans('global.create_successful'));
        }

        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function show(Unit $unit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function edit(Unit $unit)
    {
        return view('units.edit', compact('unit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Unit $unit)
    {

        $unit->update($request->input());
        return response(['success' => trans('global.update_successful')], 200);
        // return redirect()->route('units.index')->with('success', trans('global.update_successful'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function destroy(Unit $unit)
    {
      $unit->delete();
      $unit['deleted_by']=Auth::user()->id;
      $unit->save();
      return response()->json([
         'status'  => 'success',
         'message' => trans('global.delete_successful')
       ]);
    }
}
