<?php

namespace App\Http\Controllers;

use App\Allotment;
use App\ItemDetails;
use App\MainCategory;
use App\SubCategory;
use App\EndCategory;
use App\Exports\ReportExport;
use App\ItemDetailsSpecifications;
use App\User;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ReportController extends Controller
{
    //


    public function index(){
        $users = User::get();
        return view('reports.item_report',compact('users'));
    }

    public function itemDetailsReport(){
        $users = User::get();
        return view('reports.item_details_report',compact('users'));

    }
    public function ajaxSearch(Request $request) {
        $tableNames = $request->table_name;
        $search = $request->searchTerm;
        if($search) {
        $model = 'App\\'.$tableNames;

        
        // $items = $model::select('id', 'name_dr')->where('name_dr','LIKE',"%$search%")->orwhere('main_category_id',$request->main_category_id)->get();
        

            if($tableNames == 'Employee'){

                $items = $model::select('id', 'name_dr', 'father_name_dr')->where('name_dr','LIKE',"%$search%")->get();

            }elseif($tableNames == 'SubCategory'){

                $items = $model::select('id', 'name_dr')->where('name_dr','LIKE',"%$search%")->orwhere('main_category_id',$request->main_category_id)->get();
            
            }elseif($tableNames == 'EndCategory'){
                $items = $model::select('id', 'name_dr')->where('name_dr','LIKE',"%$search%")->orwhere('sub_category_id',$request->sub_category_id)->get();
                
            }else{
                $items = $model::select('id', 'name_dr')->where('name_dr','LIKE',"%$search%")->get();
            }


       
       
            
        $data =array();
        foreach ($items as $item){
            
            $data[] =array('id'=>$item->id,'text'=>$item->name_dr.' - '.$item->father_name_dr);
        }
        return json_encode($data);
        }
    }

    public function axiosSearch(Request $request) {
        
        $search = $request->name;    
        $tableNames =$request->model;
        $model = 'App\\'.$tableNames;
        
        if($tableNames == 'Employee'){

            $items = $model::select('id', 'name_dr', 'father_name_dr','current_position_dr','last_name')->where('name_dr','LIKE',"%$search%")->get();

        }elseif($tableNames == 'SubCategory'){

            $items = $model::select('id', 'name_dr')->where('name_dr','LIKE',"%$search%")->orwhere('main_category_id',$request->main_category_id)->get();
        
        }elseif($tableNames == 'EndCategory'){
            $items = $model::select('id', 'name_dr')->where('name_dr','LIKE',"%$search%")->orwhere('sub_category_id',$request->sub_category_id)->get();
            
        }else{
            $items = $model::select('id', 'name_dr')->where('name_dr','LIKE',"%$search%")->get();
        }
         
        if($search) {
           
            return response()->json([
                'incomplete_results' => true,
                'items' => $items,
                'total_count' => $items->count()
            ]);
        }
    }
    
    public function store(Request $request){
   
       $department_id = $request->department_id;
       $employee_id = $request->employee_id;
       $main_category_id = $request->main_category_id;
       $sub_category_id = $request->sub_category_id;
       $end_category_id = $request->end_category_id;
       $item_details_id = $request->item_details_id;
       $type = $request->type;
       $user_id = $request->user_id;

       if($type == 'allotment'){
        return $result = Allotment::
                    ofCreated($user_id)
                    ->ofEmployee($employee_id)
                    ->whereHas('itemDetailsSpecifications.itemDetails',
                        function ($query) use ($main_category_id,$sub_category_id,$end_category_id,$item_details_id){
                            return  $query
                                        ->ofMainCategory($main_category_id)
                                        ->ofSubCategory($sub_category_id)
                                        ->ofEndCategory($end_category_id)
                                        ->ofItemDetails($item_details_id)
                                        ;
                        }
                    )
                    ->whereHas('receiverEmployee.department', function ($q) use($department_id){
                        return $q->ofDepartment($department_id);
                    })
                    ->with('itemDetailsSpecifications',
                            'itemDetailsSpecifications.motamed',
                            'itemDetailsSpecifications.motamed.employee',
                            'itemDetailsSpecifications.motamed.employee.department',
                            'itemDetailsSpecifications.itemDetails',
                            'itemDetailsSpecifications.itemDetails.vendor',
                            'itemDetailsSpecifications.itemDetails.unit',
                            'itemDetailsSpecifications.createdBy.employee',
                    )
                    ->get();
       
                }else{

                    $result = ItemDetailsSpecifications::
                        ofCreated($user_id)
                        ->whereHas('itemDetails'
                    ,
                     function ($query) use ($main_category_id,$sub_category_id,$end_category_id,$item_details_id){
                        return  $query
                                    ->ofMainCategory($main_category_id)
                                    ->ofSubCategory($sub_category_id)
                                    ->ofEndCategory($end_category_id)
                                    ->ofItemDetails($item_details_id)
                                    ;
                    }
                    )->with('itemDetails','subCategoryKeys','itemDetails.vendor','itemDetails.unit','createdBy.employee')
                    ->get();
                }
                return $result;
    }

    public function exportExcel(Request $request) 
    {

        // return $request->all();
        return Excel::download(new ReportExport($request), 'report.xlsx');
    }
}
