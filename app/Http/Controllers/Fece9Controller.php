<?php

namespace App\Http\Controllers;

use App\Allotment;
use App\Fece9;
use App\Fece5;
use App\Employee;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Http\Requests\Fece9Request;
use App\ItemDetails;
use App\EndCategory;
use Illuminate\Support\Facades\Auth;
use App\Department;
use App\Fece9ItemDetails;
use App\MainCategory;
use App\SubCategory;

class Fece9Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return activity()->performedOn(new ItemDetails)->causedBy(auth()->user())->log('hey, it works');
        $fece9s = Fece9::orderBy('id', 'desc')->where('status', 0)->get();
        return view('fece9s.proper.index', compact('fece9s'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Department::all();
        $independent_unit = Department::whereType('1')->get();
        $item_details = SubCategory::all();
        $consumable_main_categories = MainCategory::where('nature', '1')->pluck('id');
        $consumable_sub_categories = SubCategory::whereIn('main_category_id', $consumable_main_categories)->get();
        $non_consumable_sub_categories = SubCategory::whereNotIn('main_category_id', $consumable_main_categories)->get();
        // return $departments;
        return view('fece9s.proper.create', compact('departments','independent_unit', 'item_details', 'consumable_sub_categories', 'non_consumable_sub_categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Fece9Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Fece9Request $request)
    {
        $items = json_decode($request->list, true);
        // return $items;

        $path = 'storage/uploads/fece9_request/';
        $fece9 = Fece9::create($request->all());
        
        $fece9->itemDetails()->attach($items);
        
        if($request->image){
            $image_path = singleFileStore($request, $fece9->id, $path);
            $fece9->update(['request_file' => $image_path]);
        }
      


        return redirect()->route('fece9s.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Fece9  $fece9
     * @return \Illuminate\Http\Response
     */
    public function show(Fece9  $fece9)
    {
        

        $item_details_ids = $fece9->itemDetails->pluck('id');
        $available_items = Allotment::with('employee')->whereIn('item_details_id',$item_details_ids)->where('status',1)->get();
        return view('fece9s.proper.show', compact('fece9','available_items'));
    }


    // get fece9 by id
    public function getFece9Details($id)
    {
        $fece9 = Fece9::with(['itemDetails', 'fece5s.allotments.itemDetailsSpecifications.subCategoryKeys', 'fece5s.allotments.employee', 'fece5s.allotments.itemDetailsSpecifications', 'fece5s.distributerEmployee', 'fece5s.allotments.itemDetailsSpecifications.itemDetails', 'department'])->find($id);
        return $fece9;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Fece9  $fece9
     * @return \Illuminate\Http\Response
     */
    public function edit(Fece9 $fece9)
    {

        // $client = new  Client();
        // $res = $client->request('POST', 'http://10.134.60.209/api/departments', [
        //     'form_params' => [
        //         'username' => 'ali.panahi',
        //         'password' => 'test@123'
        //     ],

        // ]);

        // $departments =  json_decode($res->getBody(), true);
        $departments = Department::all();

        $item_details = ItemDetails::all();
        $sub_category = SubCategory::all();


        $items_list = array();

        foreach ($fece9->itemDetails as $item) {
            $items_list[] = [
                'item_details_id' => $item->id,
                'name' => $item->pivot->name,
                'quantity' => $item->pivot->quantity,
            ];
        }
        return view('fece9s.proper.edit', compact('fece9', 'departments', 'items_list', 'item_details', 'sub_category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Fece9  $fece9
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Fece9 $fece9)
    {

        // return $request->all();
        
        if ($request->change_status) {
            // return $fece9->itemDetails()->syncWithoutDetaching([2,1], ['status' => '0']);
            $fece9_item_details = $fece9->itemDetails()->where('status', null)->get();

            $arr = [];
            foreach ($fece9_item_details as $item) {
                $arr[$item->id] = ['status' => '0'];
            }

            $fece9->itemDetails()->syncWithoutDetaching($arr);
        } else {

            $items = json_decode($request->list, true);
            // return $items;
            $fece9->update($request->all());

            $fece9->itemDetails()->detach();

            $fece9->itemDetails()->attach($items);
        }
        return redirect()->route('fece9s.index')->with('success', trans('global.update_successful'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Fece9  $fece9
     * @return \Illuminate\Http\Response
     */
    public function destroy(Fece9 $fece9)
    {
        $fece9->delete();
        $fece9->itemDetails()->detach();
        $fece9->save();
        return response()->json([
            'status'  => 'success',
            'message' => trans('global.delete_successful')
        ]);
    }

    public function attachFileAndSend(Request $request)
    {
        $path = 'storage/uploads/fece9/';
        $fece9 = Fece9::find($request->id);
        $image_path = singleFileStore($request, $request->id, $path);
        $fece9->update(['processed_fece9_file' => $image_path, 'status' => 1]);
        return redirect()->back()->with('success', 'فایل پروسس شده موفقانه اضافه گردید.');
    }


    //Fece9 Received Functions

    public function receivedIndex()
    {
        $fece9s = Fece9::with('department')->orderBy('id', 'desc')->where('status', 1)->whereIn('fece9_type',['0','3'])->get();
        return view('fece9s.received.index', compact('fece9s'));
    }

    public function receivedShow(Request $request)
    {
        return view('fece9s.received.show', ['fece9_id' => $request->id]);
    }
    public function insepectionShow($id)
    {

        $fece9 = Fece9::find($id);
        return view('fece9s.inspection.show', compact('fece9'));
    }
    public function completedShow(Request $request)
    {
        $fece9_item_details = Fece9::with('itemDetails')->where('id',$request->id)->get();
        return view('fece9s.completed.show', ['fece9_id' => $request->id],compact('fece9_item_details'));
    }

    public function replaceAttachedFile(Request $request)
    {

        $fece9 = Fece9::find($request->id);
        // unlink($fece9->processed_fece9_file);
        $path = 'storage/uploads/fece9/';
        singleFileStore($request, $request->id, $path);
        return redirect()->back()->with('success', 'فایل پروسس شده موفقانه تصحیح گردید.');
    }
    public function completedFece9s(Request $request)
    {
        $fece9s = Fece9::orderBy('id', 'desc')->where('status', 2)->get();


        return view('fece9s.completed.index', compact('fece9s'));
    }

    public function Fece9ItemDetailsStatus(Request $request)
    {
        // return $request->motamed_id;
        $fece9 = Fece9::find($request->fece9_id);
        $fece9->itemDetails()->updateExistingPivot($request->id, ['status' => $request->status,'motamed_id' => $request->motamed_id]);
    }

    public function inProgress(Request $request)
    {
        $fece9s = Fece9::orderBy('id', 'desc')->where('status', 3)->get();
        return view('fece9s.received.in_progress', compact('fece9s'));
    }

    public function inspectionApproval()
    {
        $fece9s = Fece9::with('department')->orderBy('id', 'desc')->whereIn('fece9_type', ['1', '2'])->get();
        return view('fece9s.inspection.inspection_approval', compact('fece9s'));
    }

    // public function inspectionShow($id)
    // {
    //     return 'success';
    //     $fece9 = Fece9::find($id);
    //     return view('fece9s.inspection.show', compact('fece9'));
    // }

    public function insepectionApproved(Request $request)
    {
        $fece9 = Fece9::find($request->fece9_id);
        $fece9->update(['inspection_approval' => 1]);

        foreach ($request->fece9_item_details as $item) {
            Fece9ItemDetails::find($item['id'])->update(['status'=>0]);
         }

        return 'success';
    }

    public function inspectionRejectFece9(Request $request){

        $fece9 = Fece9::find($request->id);
        $fece9->update(['reject_remark' => $request->reject_remark,'inspection_approval'=>'3']);
        return 'success';

    }

    public function inspectionUpdate(Request $request){

        if($request->inspection_type){
            $fece9 = Fece9::find($request->fece9_id);
            $fece9->update(['inspection_approval' => $request->inspection_type]);

        }else{

            foreach ($request->fece9_item_details as $item) {
               Fece9ItemDetails::find($item['id'])->update(['quantity'=>$item['quantity'],'status'=>0]);
            }
        }
        
        return 'success';
    }

    public function printFece9($id){
        $fece9 = Fece9::find($id);
        return view('fece9s.proper.print',compact('fece9'));
    }
}
