<?php

namespace App\Http\Controllers;

use App\ExternalEmployee;
use App\ExternalDepartment;
use Illuminate\Http\Request;
use App\Http\Requests\StoreExternalEmployeeRequest;
use App\Http\Requests\UpdateExternalEmployeeREquest;
use Illuminate\Support\Facades\Auth;

class ExternalEmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $external_employee=ExternalEmployee::paginate(10);
        $external_departments = ExternalDepartment::all();
        return view('external-employee.index',compact('external_employee','external_departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $external_departments = ExternalDepartment::all();
        return view('external-employee.create',compact('external_departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreExternalEmployeeRequest $request)
    {
        $external_employee = ExternalEmployee::create($request->input());
        if($external_employee){
            return redirect()->route('external_employee.create')->with('success', trans('global.create_successful'));
        }       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       
        $external_employee=ExternalEmployee::find(decrypt($id));
        $external_departments = ExternalDepartment::all();
        return view('external-employee.edit',compact('external_employee','external_departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateExternalEmployeeREquest $request, $id)
    {
       
        $external_employee=ExternalEmployee::find(decrypt($id));
        $external_employee->update($request->input());
        return redirect()->route('external_employee.index')->with('success', trans('global.update_successful'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $external_employee=ExternalEmployee::find(decrypt($id));
        $external_employee->delete();
        $external_employee['deleted_by']=Auth::user()->id;
        $external_employee->save();
        return response()->json([
            'status'  => 'success',
            'message' => trans('global.delete_successful')
        ]);
    }
    public function getExternalEmployeeAjax(Request $request){
      
        if($request->ajax()){  
            $external_employee=ExternalEmployee::select('id','name_dr','father_name_dr','current_position_dr','external_department_id')->paginate(10);
            return view('external-employee.external_employee_list',compact('external_employee'))->render();
        }
    }
    public function getExternalEmployeesSearch (Request $request) {
    
        if($request->ajax()){
            $external_employee=ExternalEmployee::select('id','name_dr','father_name_dr','current_position_dr','external_department_id')
            ->where('name_dr','LIKE','%'.$request->name_dr.'%')
            ->where('father_name_dr','LIKE','%'.$request->father_name_dr.'%')
            ->where('current_position_dr','LIKE','%'.$request->current_position_dr.'%')
            ->where('external_department_id','LIKE','%'.$request->external_department_id.'%')
            ->paginate(100);
            return view('external-employee.external_employee_list',compact('external_employee'))->render();
        }
    }
    public function getExternalEmployee ($id) {
        
        $external_employee=ExternalEmployee::find(decrypt($id));
        return $external_employee;
    }

}


