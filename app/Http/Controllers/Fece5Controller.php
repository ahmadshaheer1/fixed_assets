<?php

namespace App\Http\Controllers;

use App\Allotment;
use App\ContractItems;
use App\Fece5;
use Illuminate\Http\Request;
use App\Http\Requests\Fece5Request;
use App\Fece9;
use App\Employee;
use App\Exceptions\Fece5AlreadyExists;
use App\Exceptions\ItemDetailsSpecificationAlreadyAlloted;
use App\Fece5ItemDetails;
use App\ItemBalance;
use App\ItemDetailsSpecifications;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class Fece5Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fece5s=Fece5::all();

        return view('fece5s.index',compact('fece5s'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Fece5Request $request)
    {
        // return $request->input();
        DB::beginTransaction();
        

        try{
            // if same fece5 already registered dont proceed
            if(Fece5::where('number', $request->number)->where('date', $request->date)->exists()) {
                throw new Fece5AlreadyExists();
            }

            $fece5_item_details = json_decode($request->fece5_item_details, 1);
            // return $fece5_item_details;
            // create fece5
            $fece5 = Fece5::create($request->all());

            $fece9 = Fece9::find($request->fece9_id);


            
       

                foreach($fece5_item_details as $item_details_id => $item) {
    
                    // update fece9 requested items to distributed
                    $fece9->itemDetails()->updateExistingPivot($item_details_id, ['distributed' => 1]);
                    // return $item['receiver_employee_name'];
                    // yomian az zakhirawi
                    if($fece9->fece9_type ==0){
                        $receiver_employee_id = $item['receiver_employee_name']['id'];

                        $fece5_item_details = [
                            'fece5_id' => $fece5->id,
                            'fece9_id' => $fece9->id,
                            'receiver_employee_id' => $receiver_employee_id,
                            'item_details_id' => $item['id'],
                        ];

                        $allotment = [
                            'item_details_id' =>$item['id'],
                            'receiver_employee_id' => $receiver_employee_id,
                            'allotment_date' => $request->date,
                            'fece5_id' => $fece5->id,
                            'total_alloted' => (int)$item['pivot']['quantity'],
                            'condition' => '1',
                            'status' => '1',
                        ];

                        $item_balance = [
                            'item_details_id'   => (int)$item['id'],
                            'item_amount' => (int)$item['pivot']['quantity'],
                            'type'  => '0',
                            'receiver_employee_id' => $request->distributer_employee_id,
                        ];
    
                        $inserted_itme_balance = ItemBalance::create($item_balance);
                        Fece5ItemDetails::create($fece5_item_details);
                        
                        // update distributer amount
                        $allotment_item = Allotment::where('item_details_id',(int)$item['id'])->where('receiver_employee_id',$request->distributer_employee_id)->get()->first();
                        $new_total_alloted = $allotment_item->total_alloted - (int)$item['pivot']['quantity'];
                        $allotment_item->update(['total_alloted' => $new_total_alloted]);
                        //create new recard
                        Allotment::create($allotment);
                        

                    }else{
                    foreach($item as $item_details_specification) {
    
                        $fece5->itemDetails()->attach([Arr::only($item_details_specification, ['fece9_id', 'receiver_employee_id','item_details_id', 'item_details_specifications_id', 'condition', 'status'])]);
                        $allotment = [
                            'item_details_specifications_id' => $item_details_specification['id'],
                            'receiver_employee_id' => $item_details_specification['receiver_employee_id'],
                            'allotment_date' => $request->date,
                            'fece5_id' => $fece5->id,
                            'condition' => $item_details_specification['condition'],
                            'status' => '1',
                        ];
    
                        // if(Allotment::where('item_details_specifications_id', $item_details_specification['id'])->where('status', '1')->exists()) {
                        //     throw new ItemDetailsSpecificationAlreadyAlloted();
                        // }
    
                        //Akhaz azwaja 
                        if(Allotment::where('item_details_specifications_id', $item_details_specification['id'])->where('status', '1')->exists()) {
                            $akhaz_azwaj_motamed = Allotment::where('item_details_specifications_id', $item_details_specification['id'])->where('status', '1');
                            $akhaz_azwaj_motamed->update(['return_date' => $request->date,'status' => 0]);
                        }
                        //allot to new user
                        Allotment::create($allotment);
    
                        // change the alloted item status to 1 i.e. alloted
                        $item_details_specifications = ItemDetailsSpecifications::find($item_details_specification['id']);
                        $item_details_specifications->update(['status' => 1]);
                    }
                }
            }
                // allot items to user

            $distributed_items = DB::table('fece5_item_details')->where('fece9_id', $fece9->id)->count();

            $requested_items = array_sum($fece9->itemDetails->pluck('pivot.quantity')->toArray());

            if($requested_items == $distributed_items || $fece9->fece9_type ==0)
                $fece9->update(['status' => '2']);
            else
                $fece9->update(['status' => '3']);     
            DB::commit();


        }
        catch(\Exception $e) {
            DB::rollback();
            throw $e;
        }


        return redirect()->route('fece5s.show',$fece5->id);




    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Fece5  $fece5
     * @return \Illuminate\Http\Response
     */
    public function show(Fece5 $fece5)
    {
        $contract_item_details = '';
        if($fece5->fece9->fece9_type == 0){

           $item_details_ids = $fece5->itemDetails->pluck('id');
            $contract_item_details = ContractItems::whereIn('item_details_id',$item_details_ids)->get();
            
        }
        // foreach ($fece5->itemDetails as $index => $item) {
        //     // foreach ($item as $details) {
        //     //     echo $item;exit;

        //     // }
        // }
        // // exit;
        // return $fece5->itemDetails;

        return view('fece5s.show', compact('fece5','contract_item_details'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Fece5  $fece5
     * @return \Illuminate\Http\Response
     */
    public function edit(Fece5 $fece5)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Fece5  $fece5
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Fece5 $fece5)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Fece5  $fece5
     * @return \Illuminate\Http\Response
     */
    public function destroy(Fece5 $fece5)
    {
        //
    }
    public function fece5Conformation($id,$status){
      $fece5=Fece5::find($id);

      $fece5->update(['status' => $status]);

      return redirect()->route('fece5s.index');


    }
    public function rejectFece5(Request $request ,$id){

      $fece5=Fece5::find($id);
      $fece5->update(['reject_remark' => $request->reject_remark, 'status' => 2]);
      return redirect()->route('fece5s.index')->with('success', trans('global.create_successful'));

    }


    public function attachFileAndSend(Request $request) {
        $path = 'storage/uploads/fece5/';
        $fece5 = Fece5::find($request->id);
        $image_path = singleFileStore($request,$request->id,$path);
        $fece5->update(['processed_fece5_file' => $image_path]);
        return redirect()->back()->with('success', 'فایل پروسس شده موفقانه اضافه گردید.');
    }

    public function replaceAttachedFile(Request $request){

        $fece9 = Fece5::find($request->id);
        // unlink($fece9->processed_fece9_file);
        $path = 'storage/uploads/fece5/';
        singleFileStore($request,$request->id,$path); 
        return redirect()->back()->with('success', 'فایل پروسس شده موفقانه تصحیح گردید.');

    }

}
