<?php

namespace App\Http\Controllers;

use App\EndCategory;
use App\SubCategory;
use Illuminate\Http\Request;
use App\Http\Requests\EndCategoryRequest;
use App\MainCategory;
use Auth;
class EndCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = EndCategory::orderBy('id', 'desc')->get();

        return view('end_categories.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sub_categories=SubCategory::all();

        return view('end_categories.create',compact('sub_categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EndCategoryRequest $request)
    {

      $end_category=EndCategory::create($request->all());
      if($request->ajax()) {
          return response(['success' => trans('global.create_successful')], 200);

      }
      else {
          return redirect()->route('endCategories.create')->with('success', trans('global.create_successful'));
      }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EndCategory  $endCategory
     * @return \Illuminate\Http\Response
     */
    public function show(EndCategory $endCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EndCategory  $endCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(EndCategory $endCategory)
    {
      $sub_categories=SubCategory::all();
      $main_categories=MainCategory::all();

      return view('end_categories.edit',compact('sub_categories','endCategory','main_categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EndCategory  $endCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EndCategory $endCategory)
    {

      $request['updated_by'] = Auth::user()->id;
      $extra_fields = [
        'end_category_id'=>$request->end_category_id
        ];
      $endCategory->update($request->all()+ $extra_fields);
      return response(['success' => trans('global.update_successful')], 200);

      // return redirect()->route('end_categories.index')->with('success', trans('global.update_successful'));;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EndCategory  $endCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(EndCategory $endCategory)
    {

      $endCategory->delete();
      $endCategory['deleted_by']=Auth::user()->id;
      $endCategory->save();
      return response()->json([
          'status'  => 'success',
          'message' => trans('global.delete_successful')
      ]);
    }
}
