<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ContractorCompany;
use App\Http\Requests\ContractorCompanyRequest;


class ContractorCompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contractor_companies=ContractorCompany::all();
        return view('contractor_company.index',compact('contractor_companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contractor_company.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\ContractorCompanyRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContractorCompanyRequest $request)
    {

        $path = 'storage/uploads/company/';
         $file_name = singleFileStore($request,time(),$path);
        
        $request['company_file']=$file_name;
        ContractorCompany::create($request->input());

        return redirect()->route('contractor_company.create')->with('success', trans('global.create_successful'));  
      }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ContractorCompany $contractor_company)
    {
         return view('contractor_company.edit',compact('contractor_company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ContractorCompanyRequest $request,  ContractorCompany $contractor_company)
    {
        
        $contractor_company->update($request->input());

        return redirect()->route('contractor_company.index')->with('success', trans('global.update_successful'));;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContractorCompany $contractor_company)
    {
        $contractor_company->delete();
        // $contractor_company['deleted_by']=Auth::user()->id;
        $contractor_company->save();
        return response()->json([
            'status'  => 'success',
            'message' => trans('global.delete_successful')
        ]);
    }
}
