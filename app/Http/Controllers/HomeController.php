<?php

namespace App\Http\Controllers;

use App\Allotment;
use App\Fece5;
use App\Fece9;
use App\ItemDetailsSpecifications;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // return exec('getmac');
        $allotments = Allotment::distinct()->count('receiver_employee_id');
        $fece9 = Fece9::count();
        $fece5 = Fece5::count();
        $item_details_specifications = ItemDetailsSpecifications::where('status','1')->count();
        $item_details_specifications_count = ItemDetailsSpecifications::count();

        $users = User::whereNotIn('id',[1,5,6,7,8,9,12])->get();

        $all_item = array();
        $alloted_item = array();
        foreach ($users as $user){
            $created_by = ItemDetailsSpecifications::where('created_by',$user->id)->count();
            $alloted_by = ItemDetailsSpecifications::where('created_by',$user->id)->where('status','1')->count();
            array_push($all_item,$created_by);
            array_push($alloted_item,$alloted_by);
        }
         $all_item_created = json_encode($all_item);
         $alloted_item_created = json_encode($alloted_item);

        return view('home',compact('allotments','fece9','fece5','item_details_specifications','item_details_specifications_count','users','all_item_created','alloted_item_created'));
    }
}
