<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MotamedType;
use App\Http\Requests\StoreMotamedTypeRequest;
use App\Http\Requests\UpdateMotamedTypeRequest;
use Illuminate\Support\Facades\Auth;


class MotamedTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $motamed_type=MotamedType::paginate(10);
        return view('motamed_type.index',compact('motamed_type'));     
    }
    public function getMotamedTypeAjax(Request $request){
        if($request->ajax()){
            $motamed_type=MotamedType::select('id','name')->paginate(10);
            return view('motamed_type.motamed_type_list',compact('motamed_type'))->render();
        }
    }
    public function getMotamedTypeSearch(Request $request){
        $name = $request->name;
        if($request->ajax()){
            $motamed_type = MotamedType::search($name)->paginate(100);
            return view('motamed_type.motamed_type_list',compact('motamed_type'))->render();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('motamed_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMotamedTypeRequest $request)
    {
        $motamed_type = MotamedType::create($request->all());
        if($motamed_type){
            return redirect()->route('motamed_type.create')->with('success', trans('global.create_successful'));
        }else{
            return redirect()->route('motamed_type.create')->with('error', trans('global.error_store_message'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $motamed_type=MotamedType::find(decrypt($id));
        return view('motamed_type.edit', compact('motamed_type'));   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMotamedTypeRequest $request, $id)
    {
        $motamed_type=MotamedType::find(decrypt($id));
        $motamed_type->update($request->input());
        return redirect()->route('motamed_type.index')->with('success', trans('global.update_successful'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $motamed_type=MotamedType::find(decrypt($id));
        $motamed_type->delete();
        $motamed_type['deleted_by']=Auth::user()->id;
        $motamed_type->save();
        return response()->json([
            'status'  => 'success',
            'message' => trans('global.delete_successful')
        ]);
    }
}
