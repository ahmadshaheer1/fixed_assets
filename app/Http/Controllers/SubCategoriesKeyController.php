<?php

namespace App\Http\Controllers;

use App\ItemDetails;
use App\SubCategoriesKey;
use App\SubCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class SubCategoriesKeyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = SubCategoriesKey::orderBy('id', 'desc')->get();

        return view('sub_categories_keys.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id='')
    {
        // return Session::get('item_details_id');
        
        $sub_categories = SubCategory::all();
        return view('sub_categories_keys.create',compact('sub_categories', 'id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return Session::get('item_details_id');
        SubCategoriesKey::create($request->input());

        // based on wizard steps go to adding the actual items i.e. item details specifications
        if(Session::get('item_details_id')) {
            return redirect()->route('item_details.show', Session::get('item_details_id'));
        }

        return redirect()->route('sub_categories_keys.create')->with('success', trans('global.create_successful'));
    }

    /**
     * Display the specified resource.
     *
     * $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sub_categories_keys = SubCategoriesKey::find($id);
        $sub_categories = SubCategory::all();
        $edit_distribution_type = $sub_categories_keys->itemDetailsSpecifications->count();

        return view('sub_categories_keys.edit', compact('sub_categories_keys', 'sub_categories','edit_distribution_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sub_categories_keys = SubCategoriesKey::find($id);
        $sub_categories_keys->update($request->input());

        return redirect()->route('sub_categories_keys.index')->with('success', trans('global.update_successful'));;
    }

    /**
     * Remove the specified resource from storage.
     *
     * $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sub_categories_keys = SubCategoriesKey::find($id);
        $sub_categories_keys->delete();
        return response()->json([
            'status'  => 'success',
            'message' => trans('global.delete_successful')
        ]);
    }

    public function getSubCategoriesKeys($sub_category_id = NULL, Request $request) {
        // return $request->input();
        if($sub_category_id) {
            $sub_category = SubCategory::find($sub_category_id);
            $keys = $sub_category->subCategoryKeys;

            return $keys;

        }

    }

    public function getSubCategoriesSelectOptions($sub_category_id = NULL, Request $request) {
        if($sub_category_id  != 'none') {
            $sub_category = SubCategory::find($sub_category_id);
            if($sub_category->subCategoryKeys) {
                $keys = $sub_category->subCategoryKeys;
                $data = $sub_category->itemDetails()->whereNotNull('sub_categories_key_id')->get();
                return view('sub_categories_keys.get_sub_categories_select_options_response', compact('keys', 'data'));

            }

        }

    }

    public function getItemDetailsBySpecifications($sub_category_id = NULL, Request $request) {
        // return $request->sub_category_id;
        // return ItemDetails::where('sub_category_id', 1)->where('col1', 'like', $request->col1)->where('col2', 'like', $request->col2)->where('col3', 'like', $request->col3)->where('col4', 'like', $request->col4)->get();
        if($sub_category_id  != 'none') {
            $item_details = ItemDetails::where('sub_category_id', $sub_category_id)->col1($request->col1)->col2($request->col2)->col3($request->col3)->col4($request->col4)->get();
            return $item_details;

            // if($sub_category->subCategoryKeys) {
            //     $keys = $sub_category->subCategoryKeys;
            //     $data = $sub_category->itemDetails()->whereNotNull('sub_categories_key_id')->get();
            //     return view('sub_categories_keys.get_sub_categories_select_options_response', compact('keys', 'data'));

            // }

        }else{
            $item_details=SubCategory::all();
            return $item_details;
        }

    }
}
