<?php

namespace App\Http\Controllers;

use App\SubCategory;
use App\MainCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\SubCategoryRequest;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = SubCategory::orderBy('id', 'desc')->get();


        return view('sub_categories.index', compact('data'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

         $main_categories = MainCategory::all();
         return view('sub_categories.create',compact('main_categories'));


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubCategoryRequest $request)
    {

        // return $request->all();
        $extra_fields = [
            'main_category_id'=>$request->main_category_id
        ];


        SubCategory::create($request->input() + $extra_fields);
        if($request->ajax()) {
            return response(['success' => trans('global.create_successful')], 200);

        }
        else {
            return redirect()->route('sub_categories.create')->with('success', trans('global.create_successful'));
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function show(SubCategory $subCategory)
    {
        //


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sub_category = SubCategory::find($id);
        $main_categories = MainCategory::all();

        return view('sub_categories.edit', compact('sub_category','main_categories'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SubCategory $subCategory)
    {
        $extra_fields = [
            'main_category_id'=>$request->main_category_id
        ];

        $subCategory->update($request->input() + $extra_fields);
        return response(['success' => trans('global.update_successful')], 200);
        // return redirect()->route('sub_categories.index')->with('success', trans('global.update_successful'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubCategory $subCategory)
    {
        //
        $subCategory->delete();
        $subCategory['deleted_by']=Auth::user()->id;
        $subCategory->save();
        return response()->json([
            'status'  => 'success',
            'message' => trans('global.delete_successful')
        ]);
    }
}
