<?php

namespace App\Http\Controllers;

use App\Contract;
use App\ContractItemBalance;
use App\ContractorCompany;
use App\ContractType;
use App\Department;
use App\Http\Requests\ContractRequest;
use Illuminate\Http\Request;

class ContractController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $contracts = Contract::all();
         
        return view('contracts.index',compact('contracts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $departments = Department::whereType('1')->get();
        // $departments = Department::get();
        $contractor_companies = ContractorCompany::all();
        $contract_types = ContractType::all();
        return view('contracts.create',compact('departments','contractor_companies','contract_types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\ContractRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContractRequest $request)
    {
        $path = 'storage/uploads/contract/';
        $file_name = singleFileStore($request,time(),$path);
        $request['contract_file']=$file_name;

        $contract_info = Contract::create($request->all());
        // $contract_info = Contract::with('department')->with('contractorCompany')->OrderBy('id', 'desc')->first();

        return redirect()->route('contracts.show',$contract_info->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function show(Contract $contract)
    {
        //
        
        $contract_info = Contract::with('department')->with('contractType')->with('contractorCompany')->where('id', $contract->id)->first();
        // return $contract_info;
        $contract_info = json_decode($contract_info);
        
        return view('contracts.show',compact('contract_info'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function edit(Contract $contract)
    {
        //
        $departments = Department::whereType('1')->get();
        $contractor_companies = ContractorCompany::all();
        $contract_types = ContractType::all();
        return view('contracts.edit',compact('contract','departments','contractor_companies','contract_types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contract $contract)
    {
        //
        $contract->update($request->input());

        return redirect()->route('contracts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contract $contract)
    {
        //
        $contract->delete();
        $contract->save();
        return response()->json([
            'status'  => 'success',
            'message' => trans('global.delete_successful')
        ]);
    }

    public function contractsShowItem ($contract_id){
        // 
        
        $contract_info = Contract::with(['department','contractType','contractorCompany','contractItems.itemDetails.unit'])->where('id',$contract_id)->first();
        $contract_info = json_decode($contract_info);

        $contract_item_balance = ContractItemBalance::with(['itemDetails','fece9','meem7.receiverEmployee'])->where('contract_id',$contract_id)->get(); 




        return view('contracts.show_item',compact('contract_info','contract_item_balance'));
    }
}
