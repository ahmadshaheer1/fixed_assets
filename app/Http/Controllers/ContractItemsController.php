<?php

namespace App\Http\Controllers;

use App\ContractItems;
use App\ItemDetails;
use Illuminate\Http\Request;

class ContractItemsController extends Controller
{
    //
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     
        
        $data = isset($request->contract_items[0]) ? $request->contract_items : $request->input();
        
        foreach($data as $contract_item) {
           $contract_item['item_details_id'] = $contract_item['item_details']['id'];
           $contract_item['available_amount'] = $contract_item['item_amount'];
            ContractItems::create($contract_item);
        }
        return 'success';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $item_details = ContractItems::with(['itemDetails','itemDetails.unit'])->where('contract_id',$id)->get();
        return [
            'item_details' => $item_details, 
            'total_price_sum' =>$item_details->sum('total_price')
        ];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       
        $contract_item = ContractItems::with(['itemDetails','itemDetails.unit'])->find($id);
        return $contract_item;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request['item_details_id'] = $request['item_details']['id']; 
        $contract_item = ContractItems::find($id)->update($request->contract_items);
        return 'success';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
