<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EmployeeAttachment;
class EmployeeAttachmentController extends Controller
{
    public function attachFileAndSend (Request $request) {
        $path = 'storage/uploads/employee_attachments/';
         $item = EmployeeAttachment::where('employee_id',$request->id)->where('employee_type',$request->employee_type)->first();
         $image_path = singleFileStore($request, $request->id, $path);
         if(!$item){
            $new_employee_attachment = EmployeeAttachment::create([
                'employee_id' => $request->id,
                'attachment' => $image_path,
                'employee_type' => $request->employee_type
            ]);
            return redirect()->back()->with('success', 'فایل   موفقانه اضافه گردید.');
         }else{
            $item->update(['attachment' => $image_path,'employee_id',$request->id,'employee_type' => $request->employee_type]);
            return redirect()->back()->with('success', 'فایل   موفقانه تصحیح گردید.');
         }
        
       
    }
}
