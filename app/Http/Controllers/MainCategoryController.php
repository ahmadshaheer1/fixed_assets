<?php

namespace App\Http\Controllers;

use App\MainCategory;
use App\SubCategory;
use App\EndCategory;
use App\ItemDetails;
use Illuminate\Http\Request;
use App\Http\Requests\MainCategoryRequest;
use Illuminate\Support\Facades\Auth;

class  MainCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = MainCategory::orderBy('id', 'desc')->get();
        return view('main_categories.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('main_categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\MainCategoryRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MainCategoryRequest $request)
    {
        // if($request->ajax()) {
        //   MainCategory::create($request->input());
        //
        //
        // }
        // $request['nature'] = 0;
        MainCategory::create($request->input());
        if($request->ajax()) {
            return response(['success' => trans('global.create_successful')], 200);

        }else {
            return redirect()->route('main_categories.create')->with('success', trans('global.create_successful'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MainCategory  $main_category
     * @return \Illuminate\Http\Response
     */
    public function show(MainCategory $main_category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MainCategory  $main_category
     * @return \Illuminate\Http\Response
     */
    public function edit(MainCategory $main_category)
    {
        return view('main_categories.edit', compact('main_category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\MainCategoryRequest  $request
     * @param  \App\MainCategory  $main_category
     * @return \Illuminate\Http\Response
     */
    public function update(MainCategoryRequest $request, MainCategory $main_category)
    {
        $main_category->update($request->input());
        return response(['success' => trans('global.update_successful')], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MainCategory  $main_category
     * @return \Illuminate\Http\Response
     */
    public function destroy(MainCategory $main_category)
    {
        $main_category->delete();
        $main_category->save();
        return response()->json([
            'status'  => 'success',
            'message' => trans('global.delete_successful')
        ]);
    }
    public function itemsBasicInformations(){

      return view('items.index');

    }
    public function getSubCategories(Request $request){

      $categories=SubCategory::where('main_category_id',$request->id)->get();
      $id = 'sub_category_id';
      $option_name = 'sub_categories';
      return view('item_details.get_ajax_categories',compact('categories','id','option_name'));
    }
    public function getEndCategories(Request $request){

      $categories=EndCategory::where('sub_category_id',$request->id)->get();
      $id = 'end_category_id';
      $option_name = 'end_categories';
      return view('item_details.get_ajax_categories',compact('categories','id','option_name'));
    }
}
