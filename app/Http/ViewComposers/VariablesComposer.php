<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use Config;
class VariablesComposer
{

    public function compose(View $view)
    {
        $lang = Config::get('app.locale');

        $title = "title_".$lang;
        $name = "name_".$lang;
        $date = "date_".$lang;
        $description = "description_".$lang;



        $view->with([
            'lang'=>$lang,
            'title'=>$title,
            'name'=>$name,
            'date'=>$date,
            'description'=>$description,

        ]);
    }
}
