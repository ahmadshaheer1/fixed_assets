<?php

namespace App\Http\Requests;
use Config;
use Illuminate\Foundation\Http\FormRequest;

class ItemDetailsRequest extends FormRequest
{
  public function __construct()
  {
      $this->lang = Config::get('app.locale');
      $this->name = 'name_' . $this->lang;
  }
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
      return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
      return [
          $this->name => 'required',
          'main_category_id'=>'required | numeric',
          'sub_category_id'=>'required',
          'end_category_id'=>'required',
          'vendor_id'=>'required',
          'unit_id'=>'required',

      ];
  }
  public function messages()
  {

      return [
          $this->name . '.required' => 'نام جنس ضروری میباشد',
          // $this->name . '.unique' => 'نام کمپنی تکراری میباشد!',
          'main_category_id.required'=>'کتگوری اصلی ضروری می باشد.',
          'sub_category_id.required'=>'کتگوری فرعی ضروری میباشد',
          'end_category_id.required'=>'کتگوری نهایی ضروری میباشد',
          'vendor_id.required'=>'کمپنی ضروری میباشد',
          'unit_id.required'=>'واحد ضروری میباشد',

      ];
  }
}
