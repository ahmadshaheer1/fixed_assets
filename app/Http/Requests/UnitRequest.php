<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Config;

class UnitRequest extends FormRequest
{
    public function __construct()
    {
        $this->lang = Config::get('app.locale');
        $this->name = 'name_' . $this->lang;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            $this->name => 'required',


        ];
    }
    public function messages()
    {

        return [
            $this->name . '.required' => 'نام واحد ضروری میباشد!',
            // $this->name . '.unique' => 'نام واحد تکراری میباشد!',

        ];
    }
}
