<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Fece9Request extends FormRequest
{


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'number' => 'required',
            'date' => 'required',
            'department_id' => 'required',
            'list' => 'required',
            'description' => 'required',
            'text' => 'required',

        ];
    }
    public function messages()
    {

        return [
            'number.required' => 'نمبر ف س ۹ ضروری میباشد',
            'date.required' => 'تاریخ ف س ۹ ضروری میباشد',
            'department_id.required' => 'شعبه درخواست کننده ضروری میباشد',
            'list.required' => 'انتخاب اجناس ضروری میباشد',
            'description.required' => 'توضیحات ضروری میباشد',
            'text.required' => 'توضیحات ضروری میباشد',
            // $this->name . '.unique' => 'نام کمپنی تکراری میباشد!',

        ];
    }
}
