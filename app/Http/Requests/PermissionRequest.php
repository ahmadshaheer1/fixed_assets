<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Config;
class PermissionRequest extends FormRequest
{
    public function __construct()
    {
        $this->lang = Config::get('app.locale');
        $this->name = 'name_' . $this->lang;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'name_dr' => 'required',
        ];
    }
    public function messages()
    {

        return [
            'name.required' => 'نام انگلیسی ضروری میباشد',
            'name_dr.required' => 'نام دری ضروری میباشد',

        ];
    }
}
