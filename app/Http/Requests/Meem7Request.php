<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Meem7Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'number' => 'required',
            'date'=> 'required',
            'mahal_taslimi'=> 'required',
            // 'akhz_az_wajh_employee_id'=> 'required',
            // 'contract_id'=> 'required',
            'department_id'=> 'required',
            'maktoob_number'=> 'required',
            'maktoob_date'=> 'required',

        ];
    }
    public function messages()
    {

        return [
            'number.required'  => 'نمبر م ۷ ضروری میباشد.',
            'date.required' => 'تاریخ م ۷ ضروری میباشد.',
            'mahal_taslimi.required' => 'محل تسلیمی ضروری میباشد.',
            // 'akhz_az_wajh_employee_id.required' => 'اخذ از وجه کارمند ضروری میباشد.',
            // 'contract_id.required' => 'انتخاب قرارداد ضروری میباشد.',
            'department_id.required' => 'اداره درخواست کننده ضروری می باشد.',
            'maktoob_number.required' => 'نمبر مکتوب ضروری میباشد.',
            'maktoob_date.required' => 'تاریخ مکتوب ضروری میباشد.',
        ];
    }
}
