<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContractRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'contract_type_id' => 'required',
            'identification_number' => 'required|unique:contracts',
            'department_id' => 'required',
            'contractor_company_id' => 'required',
            'year' => 'required',
            'contract_amount' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
        ];
    }
    public function messages()
    {

        return [
          
            'contract_type_id.required' => 'نوعیت قرارداد ضروری میباشد!',
            'identification_number.required' =>'نمبر تشخصیه ضروری میباشد!',
            'identification_number.unique' =>'نمبر تشخصیه قبلا ثبت شده!',
            'department_id.required' =>'اداره ضروری میباشد!',
            'contractor_company_id.required' =>'شرکت های قراردادی ضروری میباشد!',
            'year.required' =>'سال ضروری میباشد!',
            'contract_amount.required' =>'مقدار پول ضروری میباشد!',
            'start_date.required' =>'تاریخ آغاز ضروری میباشد!',
            'end_date.required' =>'تاریخ ختم ضروری میباشد!',

        ];
    }
}
