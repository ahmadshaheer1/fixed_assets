<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContractorCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_name' => 'required',
            'license_number' => 'required| numeric',
            'tin' => 'required| numeric | unique:contractor_companies',
            'number' => 'required| numeric | unique:contractor_companies',
            // 'email' => 'required',
        ];
    }
    public function messages()
    {

        return [
            'company_name.required' => 'ضروری میباشد!',
            'license_number.required' => 'ضروری میباشد!',
            'tin.required' => 'ضروری میباشد!',
            'number.required' => 'ضروری میباشد!',
            // 'email.required' => 'ضروری میباشد!',
            // $this->name . '.unique' => 'نام واحد تکراری میباشد!',

        ];
    }
}
