<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Fece5Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'number' => 'required',
            'date' => 'required',
            'distributer_employee_id' => 'required',
            //'receiver_employee_id' => 'required',
            'description' => 'required',

        ];
    }

    public function messages()
    {

        return [
            'number.required' => 'نمبر ف س ۵ ضروری میباشد',
            'date.required' => 'تاریخ ف س ۵ ضروری میباشد',
            'distributer_employee_id.required' => 'ضروری میباشد',
            //'receiver_employee_id.required' => 'ضروری میباشد',
            'description.required' => 'توضیحات ضروری میباشد',
        ];
    }
}
