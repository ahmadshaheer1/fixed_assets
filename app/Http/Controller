<?php

namespace App\Http\Controllers;

use App\ItemDetails;
use App\MainCategory;
use App\SubCategory;
use App\EndCategory;
use App\Vendor;
use App\Unit;
use Illuminate\Http\Request;
use App\Http\Requests\ItemDetailsRequest;
use App\RequestedItem;
use App\SubCategoriesKey;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class ItemDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $item_details = ItemDetails::orderBy('id', 'desc')->paginate(10);
        return view('item_details.index', compact('item_details'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $main_categories = MainCategory::all();
        $sub_categories = SubCategory::all();
        $end_categories = EndCategory::all();
        $vendors = Vendor::all();
        $units = Unit::all();
        return view('item_details.create', compact('main_categories', 'sub_categories', 'end_categories', 'vendors', 'units'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ItemDetailsRequest $request)
    {

        $item_details = ItemDetails::create($request->all());

        //Check Request from requested item
        if ($request->requested_item) {
            RequestedItem::find($request->requested_item)->update(['status' => 1]);
        }
        if ($request->ajax()) {
            $response = ['success' => trans('global.create_successful')];

            // to move to next step i.e. add spec. type
            if (!SubCategoriesKey::where('sub_category_id', $request->sub_category_id)->exists()) {
                $response['sub_category_id'] = $request->sub_category_id;
            }
            $response['item_details_id'] = $item_details->id;
            Session::put('item_details_id', $item_details->id);

            return response($response, 200);
        }

        // if($request->request_id){
        //     RequestedItem::find($request->request_id)->update(array('status' =>1));
        //     return redirect()->route('requested_items.index')->with('success', trans('global.create_successful'));
        // }
        // else{
        //     return redirect()->route('item_details.create')->with('success', trans('global.create_successful'));
        // }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ItemDetails  $itemDetails
     * @return \Illuminate\Http\Response
     */
    // public function show(ItemDetails $item_details)
    public function show($id)
    {
        
        $item_details = ItemDetails::find($id);
        return view('item_details.show', compact('item_details'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ItemDetails  $itemDetails
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $item_details = ItemDetails::find($id);
        $main_categories = MainCategory::all();
        $sub_categories = SubCategory::all();
        $end_categories = EndCategory::all();
        $vendors = Vendor::all();
        $units = Unit::all();
        return view('item_details.edit', compact('item_details', 'main_categories', 'sub_categories', 'end_categories', 'vendors', 'units'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ItemDetails  $itemDetails
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $itemDetails = ItemDetails::find($id);
        if ($itemDetails->sub_categories_key_id != $request->sub_categories_key_id) {
            $request['col1'] = $request->col1;
            $request['col2'] = $request->col2;
            $request['col3'] = $request->col3;
            $request['col4'] = $request->col4;
        }
        $itemDetails->update($request->all());

        return redirect()->route('item_details.index')->with('success', trans('global.update_successful'));
    }


    public function indexRequestedItem()
    {

        return view('item_details.index_requested');
    }
    public function createRequestedItem()
    {
        $main_categories = MainCategory::all();
        $sub_categories = SubCategory::all();
        $end_categories = EndCategory::all();
        $vendors = Vendor::all();
        $units = Unit::all();
        return view('item_details.create_requested', compact('main_categories', 'sub_categories', 'end_categories', 'vendors', 'units'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ItemDetails  $itemDetails
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $itemDetails = ItemDetails::find($id);
        $itemDetails->delete();
        return response()->json([
            'status'  => 'success',
            'message' => trans('global.delete_successful')
        ]);
    }


    public function getSettingModalView(Request $request)
    {
        // return $request->input();
        return view($request->type . '.create', ['data' => $request->input()]);
    }

    public function getCategories()
    {
        $main_categories = MainCategory::all();
        $sub_categories = SubCategory::all();
        $end_categories = EndCategory::all();
        $vendors = Vendor::all();
        $units = Unit::all();
        $item_details = ItemDetails::all();

        return [
            'main_categories' => $main_categories,
            'sub_categories' => $sub_categories,
            'end_categories' => $end_categories,
            'vendors' => $vendors,
            'units' => $units,
            'item_details' => $item_details,
        ];
    }

    public function getRelatedCategories(Request $request)
    {
        $main_category = is_numeric($request->main_category_id) ? MainCategory::find($request->main_category_id) : null;
        $sub_category  = is_numeric($request->sub_category_id)  ? SubCategory::find($request->sub_category_id) : null;
        $end_category  = is_numeric($request->end_category_id)  ? EndCategory::find($request->end_category_id) : null;;
        $item_details  = is_numeric($request->item_details_id)  ? ItemDetails::find($request->item_details_id) : null;;

        $main_categories_result = collect();
        $sub_categories_result = collect();
        $end_categories_result = collect();
        $item_details_result = collect();

        // return ItemDetails::find($request->item_details_id)->endCategory->subCategory->mainCategory;

        if ($main_category) {
            $main_categories_result->push($main_category);
            $sub_categories_result = $main_category->subCategories;
        }
        if ($sub_category) {
            $sub_categories_result->push($sub_category);
            $end_categories_result = $sub_category->endCategories;
            $main_categories_result->push($sub_category->mainCategory);
        }
        if ($end_category) {
            $end_categories_result->push($end_category);
            $main_categories_result->push($end_category->subCategory->mainCategory);
            $sub_categories_result->push($end_category->subCategory);
            $item_details_result = $end_category->itemDetails;
        }
        if ($item_details) {
            $item_details_result->push($item_details);
            $end_categories_result->push($item_details->endCategory);
            $sub_categories_result->push($item_details->endCategory->subCategory);
            $main_categories_result->push($item_details->endCategory->subCategory->mainCategory);
        }


        return [
            'main_categories' => $main_categories_result,
            'sub_categories' => $sub_categories_result,
            'end_categories' => $end_categories_result,
            'item_details' => $item_details_result
        ];
    }

    public function fece9AllItemsAlloted($fece9_id, $item_details_id)
    {
        $total_requested = DB::table('fece5_item_details')->where('fece9_id', $fece9_id)->where('item_details_id', $item_details_id)->count();
        $total_alloted = DB::table('fece9_item_details')->where('fece9_id', $fece9_id)->where('item_details_id', $item_details_id)->first()->quantity;
        return response()->json($total_alloted == $total_requested);
    }

    public function getItemDetails()
    {
        $item_details = ItemDetails::all();
        return $item_details;
    }

    // Ajax Pagination
    public function getItemDetailsAjax()
    {
        $item_details = ItemDetails::orderBy('id', 'desc')->paginate(10);
        return view('item_details.item_details_list', compact('item_details'))->render();
    }

    public function itemDetailsSearch(Request $request)
    {
        $name = $request->name;
        $main_category = $request->main_category;
        $sub_category = $request->sub_category;
        $end_category = $request->end_category;
        // return $name;
        if ($request->ajax()) {
            $item_details = ItemDetails::where('name_dr', 'LIKE', '%' . $name . '%')
                ->whereHas('MainCategory', function ($q) use ($main_category) {
                    $q->where('name_dr', 'LIKE', '%' . $main_category . '%');
                })
                ->whereHas('subCategory', function ($q) use ($sub_category) {
                    $q->where('name_dr', 'LIKE', '%' . $sub_category . '%');
                })
                ->whereHas('endCategory', function ($q) use ($end_category) {
                    $q->where('name_dr', 'LIKE', '%' . $end_category . '%');
                })
                ->paginate(100);
            return view('item_details.item_details_list', compact('item_details'))->render();
        }
    }

    public function getItemDetailsConsumable()
    {

        $item_details = ItemDetails::with('unit')->whereHas('MainCategory', function ($q) {
            $q->where('nature', '1');
        })->get();
        return $item_details;
    }
}
