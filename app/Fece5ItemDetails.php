<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Traits\LogsActivity;

class Fece5ItemDetails extends Model
{
    protected $table = 'fece5_item_details';
    use LogsActivity;
    use SoftDeletes;

    
    protected $fillable = ['fece5_id','fece9_id','receiver_employee_id','item_details_id','item_details_specifications_id','condition','status','created_by','updated_by','deleted_by','deleted_at','created_at','updated_at'];
    protected static $logAttributes = ['fece5_id','fece9_id','receiver_employee_id','item_details_id','item_details_specifications_id','condition','status','created_by','updated_by','deleted_by','deleted_at','created_at','updated_at'];

    public static function boot()
    {
        parent::boot();

     
        // create a event to happen on updating
        static::updating(function ($table) {
            $table->updated_by = Auth::user()->id ;
        });

        // create a event to happen on deleting
        static::deleting(function ($table) {
            $table->deleted_by = Auth::user()->id ;
        });

        // create a event to happen on saving
        static::saving(function ($table) {
            $table->created_by = Auth::user()->id ;
        });
    }
    public function receiver() {
        return $this->belongsTo('App\Employee', 'receiver_employee_id', 'id');
    }




}
