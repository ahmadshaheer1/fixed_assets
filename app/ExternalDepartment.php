<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
class ExternalDepartment extends Model
{
    use LogsActivity;
    use SoftDeletes;
    protected $table = 'external_departments';
    protected $fillable = ['name_dr','name_en','name_pa','created_at','updated_at','created_by','updated_by','deleted_at','deleted_by'];
    protected static $logAttributes = ['name_dr','name_en','name_pa','created_at','updated_at','created_by','updated_by','deleted_at','deleted_by'];
    protected static function boot()
    {
        parent::boot();

        // create a event to happen on updating
        static::updating(function($table)  {
            $table->updated_by = Auth::user()->id ;
        });

        // create a event to happen on deleting
        static::deleting(function($table) {
            $table->deleted_by = Auth::user()->id ;
        });

        // create a event to happen on saving
        static::saving(function($table)  {
            $table->created_by = Auth::user()->id ;
        });
    }
}