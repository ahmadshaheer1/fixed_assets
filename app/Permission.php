<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Traits\LogsActivity;

class Permission extends Model
{
    //
    use LogsActivity;
    use SoftDeletes;
    protected $fillable = ['name','name_dr','guard_name','crated_at','updated_at'];

    protected static $logAttributes = ['name','name_dr','guard_name','crated_at','updated_at'];



}
