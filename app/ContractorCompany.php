<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;


class ContractorCompany extends Model
{
    use LogsActivity;
    use SoftDeletes;
    protected $fillable = ['id', 'company_name', 'license_number', 'tin', 'number', 'email','bank_account','company_file','created_by', 'updated_by', 'deleted_by', 'deleted_at', 'created_at', 'updated_at'];
    protected static $logAttributes = ['id', 'company_name', 'license_number', 'tin', 'number', 'email','bank_account','company_file', 'created_by', 'updated_by', 'deleted_by', 'deleted_at', 'created_at', 'updated_at'];

    //
    public static function boot() {
        parent::boot();

        // create a event to happen on updating
        static::updating(function($table)  {
            $table->updated_by = Auth::user()->id ;
        });

        // create a event to happen on deleting
        static::deleting(function($table)  {
            $table->deleted_by = Auth::user()->id ;
        });

        // create a event to happen on saving
        static::saving(function($table)  {
            $table->created_by = Auth::user()->id ;
        });
    }
}
