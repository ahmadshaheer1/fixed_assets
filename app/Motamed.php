<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Motamed extends Model
{
    use LogsActivity;
    use SoftDeletes;
    protected $table = 'motameds';
    protected $fillable = ['employee_id','department_id','motamed_type_id','created_at','updated_at','created_by','updated_by','deleted_at','deleted_by'];
    protected static $logAttributes = ['employee_id','department_id','motamed_type_id','created_at','updated_at','created_by','updated_by','deleted_at','deleted_by'];
    public function employee()
    {
        return $this->belongsTo('App\Employee');
    }
    public function department()
    {
        return $this->belongsTo('App\Department');
    }
    public function motamed_type()
    {
        return $this->belongsTo('App\MotamedType');
    }
    public function allotments() {
        return $this->hasMany('App\Allotment','receiver_employee_id','employee_id')->where('employee_type',2);
    }
    protected static function boot()
    {
        parent::boot();

        // create a event to happen on updating
        static::updating(function($table)  {
            $table->updated_by = Auth::user()->id ;
        });

        // create a event to happen on deleting
        static::deleting(function($table) {
            $table->deleted_by = Auth::user()->id ;
        });

        // create a event to happen on saving
        static::saving(function($table)  {
            $table->created_by = Auth::user()->id ;
        });
    }
    public function scopeSearch($query, $name, $father_name, $designation, $department)
    {
        return $query->select('id', 'name_dr', 'father_name_dr', 'current_position_dr', 'department_id', 'last_name')
            ->where('name_dr', 'LIKE', '%' . $name . '%')
            ->where('father_name_dr', 'LIKE', '%' . $father_name . '%')
            ->where('current_position_dr', 'LIKE', '%' . $designation . '%')
            ->whereHas('department', function ($q) use ($department) {
                $q->where('name_dr', 'LIKE', '%' . $department . '%');
            });
    }
}
