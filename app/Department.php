<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Department extends Model
{

    use LogsActivity;
    use SoftDeletes;
    protected $fillable = ['id', 'name_dr', 'name_pa', 'name_en', 'description', 'parent_id', 'type', 'created_by', 'updated_by', 'deleted_by', 'deleted_at', 'created_at', 'updated_at'];
    protected static $logAttributes = ['id', 'name_dr', 'name_pa', 'name_en', 'description', 'parent_id', 'type', 'created_by', 'updated_by', 'deleted_by', 'deleted_at', 'created_at', 'updated_at'];

    public function endItemDetails() {
        return $this->hasMany('App\ItemDetails');
    }

    public function meem7s() {
        return $this->hasMany('App\Meem7');
    }

    public function fece9s() {
        return $this->hasMany('App\Fece9');
    }
    public function fece2s() {
        return $this->hasMany('App\Fece2');
    }
   
    public function motamed()
    {
        return $this->hasOne('App\Motamed');
    }

    public function fece5s() {
        return $this->hasMany('App\Fece5');
    }
    
    public function scopeOfDepartment($query, $value) {
        if($value) {
          $query->where('id', $value);
        }
        return $query;
      }

    public static function boot() {
        parent::boot();

        // create a event to happen on updating
        static::updating(function($table)  {
            $table->updated_by = Auth::user()->id ;
        });

        // create a event to happen on deleting
        static::deleting(function($table)  {
            $table->deleted_by = Auth::user()->id ;
        });

        // create a event to happen on saving
        static::saving(function($table)  {
            $table->created_by = Auth::user()->id ;
        });
    }
}
