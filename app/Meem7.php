<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Meem7 extends Model
{
    protected $fillable = ['number', 'date', 'description', 'department_id','mahal_taslimi','akhz_az_wajh_employee_id','contract_id','maktoob_number','maktoob_date','meem3_file','fece9_id','meem7_type','status'];

    protected static $logAttributes = ['number', 'date', 'description', 'department_id','mahal_taslimi','akhz_az_wajh_employee_id','contract_id','maktoob_number','maktoob_date','meem3_file','fece9_id','meem7_type','status'];

    public function department() {
        return $this->belongsTo('App\Department');
    }
    public function getResposnableEmployee() {
        return $this->belongsTo('App\Employee', 'akhz_az_wajh_employee_id', 'id');
    }
    public function fece9() {
        return $this->belongsTo('App\Fece9', 'fece9_id', 'id');
    }
    public function itemDetailsSpecifications() {
        return $this->hasMany('App\ItemDetailsSpecifications', 'meem7_id', 'id');
    }

    public function contract(){
         return $this->belongsTo('App\Contract','contract_id','id');

    }
    // public function contractItems(){

    // }
    public function receiverEmployee(){
        return $this->belongsTo('App\Employee', 'mahal_taslimi', 'id');

    }
    public static function boot() {
        parent::boot();

        // create a event to happen on updating
        static::updating(function($table)  {
            $table->updated_by = Auth::user()->id ;
        });

        // create a event to happen on deleting
        static::deleting(function($table)  {
            $table->deleted_by = Auth::user()->id ;
        });

        // create a event to happen on saving
        static::saving(function($table)  {
            $table->created_by = Auth::user()->id ;
        });
    }
}
