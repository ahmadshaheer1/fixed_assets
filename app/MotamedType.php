<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;


class MotamedType extends Model
{
    use LogsActivity;
    use SoftDeletes;
    protected $table = 'motamed_types';
    protected $fillable = ['name','created_at','updated_at','created_by','updated_by','deleted_at','deleted_by'];
    protected static $logAttributes = ['name','created_at','updated_at','created_by','updated_by','deleted_at','deleted_by'];
    public function motamed()
    {
        return $this->hasOne('App\Motamed');
    }
    protected static function boot()
    {
        parent::boot();

        // create a event to happen on updating
        static::updating(function($table)  {
            $table->updated_by = Auth::user()->id ;
        });

        // create a event to happen on deleting
        static::deleting(function($table) {
            $table->deleted_by = Auth::user()->id ;
        });

        // create a event to happen on saving
        static::saving(function($table)  {
            $table->created_by = Auth::user()->id ;
        });
    }
    public function scopeSearch($query, $name)
    {
        return $query->select('id', 'name')
            ->where('name', 'LIKE', '%' . $name . '%');
    }
}
