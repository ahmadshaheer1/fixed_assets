<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Traits\LogsActivity;

class ItemDetailsSpecifications extends Model
{
    use LogsActivity;
    use SoftDeletes;

    protected $fillable = ['id', 'item_details_id','meem7_id', 'employee_type','price', 'currency', 'sub_categories_key_id','attachment' ,'status','item_process' ,'condition', 'col1', 'col2', 'col3', 'col4' ,'col5','col6','col7','col6','col8','col7','col9','col8','col10','col11','col12','details','item_amount','created_by', 'updated_by', 'deleted_by'];

    protected static $logAttributes = ['id', 'item_details_id',  'employee_type','meem7_id','price', 'currency', 'sub_categories_key_id', 'attachment','item_process','status', 'condition', 'col1', 'col2', 'col3', 'col4' ,'col5','col6','col7','col6','col8','col7','col9','col8','col10','col11','col12','details','item_amount','created_by', 'updated_by', 'deleted_by'];



    public function itemBalance(){
        return $this->hasMany('App\ItemBalance');
    }
    public function unit() {
        return $this->belongsTo('App\Unit');
    }

    public function department() {
        return $this->belongsTo('App\Department');
    }

    public function createdBy() {
        return $this->belongsTo('App\User','created_by','id');
    }

    public function employee() {
        return $this->belongsToMany('App\Employee', 'employee_end_item_details')->withPivot('allotment_date', 'return_date', 'status', 'total_alloted', 'fece5_id', 'condition')->withTimestamps();
    }

    public function motamed(){
        return $this->hasOne('App\Allotment', 'item_details_specifications_id', 'id');
    }
    

    public function fece9s() {
        return $this->belongsToMany('App\Fece9')->withPivot('name', 'quantity', 'created_by', 'updated_by', 'deleted_by')->withTimestamps();
    }

    public function fece5s() {
        return $this->belongsToMany('App\Fece5')->withPivot('name', 'quantity', 'created_by', 'updated_by', 'deleted_by')->withTimestamps();
    }

    public function subCategoryKeys() {
        return $this->hasOne('App\SubCategoriesKey', 'id', 'sub_categories_key_id');
    }

    public function itemDetails() {
        return $this->belongsTo('App\ItemDetails', 'item_details_id', 'id');
    }

    // public function allotments(){

    // }
    public function scopeCol1($query, $value) {
        if($value != null && $value != 'none') {
            $query->where('col1', 'LIKE', $value);
        }
    }

    public function scopeCol2($query, $value) {
        if($value != null && $value != 'none') {
            $query->where('col2', 'LIKE', $value);
        }
    }

    public function scopeCol3($query, $value) {
        if($value != null && $value != 'none') {
            $query->where('col3', 'LIKE', $value);
        }
    }

    public function scopeCol4($query, $value) {
        if($value != null && $value != 'none') {
            $query->where('col4', 'LIKE', $value);
        }
    }
    
    public function scopeOfCreated($query, $value) {
        if($value) {
          $query->where('created_by', $value);
        }
        return $query;
      }

    public static function boot() {
        parent::boot();

        // create a event to happen on updating
        static::updating(function($table)  {
            $table->updated_by = Auth::user()->id ;
        });

        // create a event to happen on deleting
        static::deleting(function($table)  {
            $table->deleted_by = Auth::user()->id ;
        });

        // create a event to happen on saving
        static::saving(function($table)  {
            $table->created_by = Auth::user()->id ;
        });
    }
}
